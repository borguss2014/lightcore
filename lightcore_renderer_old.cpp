// Already included in the same translation layer in win32_lightcore.cpp.
// To be enabled later when moving the renderer in its own abstraction layer.
//#include "win32_opengl_defines_generated.h"

internal void
RendererEndFrame()
{    
    SwapBuffers(wglGetCurrentDC());
}

// TODO(Cristian): This function collides with the one found in file.cpp!
char *
File_GetFileNameFromPath(const char *Path)
{
	char *Result = 0;
	if (Path)
	{
		mmsz Size = strnlen_s(Path, 4096);
        if(Size)
        {
            Result = (char *)malloc(Size * sizeof(char));
            if (Result)
            {
                u32 FilenameLength = 0;
                for (mmsz Index = Size - 1;
                     Index >= 0;
                     Index--)
                {
                    char TempChar = Path[Index];
                    if(TempChar == '/')
                    {
                        Result[FilenameLength] = '\0';
                        break;
                    }
                    
                    FilenameLength++;
                }
                
                memcpy(Result, Path + (Size - FilenameLength), FilenameLength);
            }
            else
            {
                // TODO(Cristian): Log failure here
            }
        }
        else
        {
            // TODO(Cristian): Log failure here
        }
	}
	else
	{
		// TODO(Cristian): Log failure here
	}
    
	return(Result);
}

GLint
Shader_CheckObjectCompileErrors(GLuint ObjectRef, char *ObjectType, open_gl *OpenGL)
{
    GLint Success = false;
    char InfoLog[512];
    
    if(StringsEqual(ObjectType, "PROGRAM"))
    {
        OpenGL->glGetProgramiv(ObjectRef, GL_LINK_STATUS, &Success);
        OpenGL->glGetProgramInfoLog(ObjectRef, 512, NULL, InfoLog);
    }
    else
    {
        OpenGL->glGetShaderiv(ObjectRef, GL_COMPILE_STATUS, &Success);
        OpenGL->glGetShaderInfoLog(ObjectRef, 512, NULL, InfoLog);
    }
    
    if (!Success)
    {
        // TODO(Cristian): Properly format the error messages.
        // Should also change the relative error line number to point to
        // the actual line in the bigger mixed shaders file
        
        fprintf(stderr, "ERROR::SHADER_ERROR of type: %s \n", ObjectType);
        fprintf(stderr, "   Infolog: %s\n", InfoLog);
    }
    
    return(Success);
}

b32
Shader_CompileShaderObject(GLuint ProgramID,
                           GLuint *ShaderObject,
                           char *Source,
                           GLint SourceSize,
                           GLenum ShaderType,
                           open_gl *OpenGL)
{
    b32 Result = false;
    
    if (Source)
    {
        *ShaderObject = OpenGL->glCreateShader(ShaderType);
        OpenGL->glShaderSource(*ShaderObject, 1, &Source, &SourceSize);
        OpenGL->glCompileShader(*ShaderObject);
        
        char *Type{};
        switch (ShaderType)
        {
            case GL_VERTEX_SHADER:
            {
                Type = "VERTEX";
            } break;
            case GL_FRAGMENT_SHADER:
            {
                Type = "FRAGMENT";
            } break;
            case GL_GEOMETRY_SHADER:
            {
                Type = "GEOMETRY";
            } break;
        }
        
        Result = Shader_CheckObjectCompileErrors(*ShaderObject, Type, OpenGL);
        
        if (Result)
        {
            OpenGL->glAttachShader(ProgramID, *ShaderObject);
            Result = true;
        }
    }
    else
    {
        fprintf(stderr, "SHADER::COMPILE_OBJECT: Shader source is NULL.\n");
    }
    
    return(Result);
}

b32
Shader_Compile(shader *Shader, open_gl *OpenGL)
{
    b32 ProgramCompileSuccess = false;
    
    string *VertexSourcePointer = &Shader->VertexSource;
    string *FragmentSourcePointer = &Shader->FragmentSource;
    string *GeometrySourcePointer = &Shader->GeometrySource;
    
    char *ShaderFileName = File_GetFileNameFromPath(Shader->FilePath);
    
    GLuint ProgramID = OpenGL->glCreateProgram();
    
    b32 ObjectCompilationErrors = false;
    
    GLuint VertexShaderObject = 0;
    if (!Shader_CompileShaderObject(ProgramID, &VertexShaderObject, VertexSourcePointer->Data,
                                    (GLint)VertexSourcePointer->Count, GL_VERTEX_SHADER, OpenGL))
    {
        ObjectCompilationErrors = true;
        fprintf(stderr, "ERROR::VERTEX: Shader object %s compilation unsuccessful in %s\n", "VERTEX", ShaderFileName);
    }
    
    GLuint FragmentShaderObject = 0;
    if (!Shader_CompileShaderObject(ProgramID, &FragmentShaderObject, FragmentSourcePointer->Data,
                                    (GLint)FragmentSourcePointer->Count, GL_FRAGMENT_SHADER, OpenGL))
    {
        ObjectCompilationErrors = true;
        fprintf(stderr, "ERROR::FRAGMENT: Shader object %s compilation unsuccessful in %s\n", "FRAGMENT", ShaderFileName);
    }
    
    GLuint GeometryShaderObject = 0;
    if (Shader->GeometrySource.Data != nullptr &&
        !Shader_CompileShaderObject(ProgramID, &GeometryShaderObject, GeometrySourcePointer->Data,
                                    (GLint)GeometrySourcePointer->Count, GL_GEOMETRY_SHADER, OpenGL))
    {
        ObjectCompilationErrors = true;
        fprintf(stderr, "ERROR::GEOMETRY: Shader object %s compilation unsuccessful in %s\n", "GEOMETRY", ShaderFileName);
    }
    
    OpenGL->glLinkProgram(ProgramID);
    ProgramCompileSuccess = Shader_CheckObjectCompileErrors(ProgramID, "PROGRAM", OpenGL);
    
    OpenGL->glDeleteShader(VertexShaderObject);
    OpenGL->glDeleteShader(FragmentShaderObject);
    OpenGL->glDeleteShader(GeometryShaderObject);
    
    if (!ProgramCompileSuccess)
    {
        fprintf(stderr, "ERROR::Shader program compilation was unsuccessful: %s\n\n\n", ShaderFileName);
    }
    else if (ObjectCompilationErrors && ProgramCompileSuccess)
    {
        fprintf(stderr, "WARNING: Shader program successful link *w/ object failure*: %s\n\n\n", ShaderFileName);
        Shader->ID = ProgramID;
    }
    else
    {
        fprintf(stdout, "SHADER::COMPILE: Successfully compiled shader program: %s\n", ShaderFileName);
        Shader->ID = ProgramID;
    }
    
    return(ProgramCompileSuccess);
}

//TODO(Cristian): Provide access to a memory arena
internal b32
Shader_ReadShaderFile(char *Path, shader *Shader)
{   
    b32 Result = true;
    
    char *VertexDefine = "@vertex_shader";
    char *FragmentDefine = "@fragment_shader";
    char *GeometryDefine = "@geometry_shader";
    
    string *PreviousSourcePointer = nullptr;
    string *CurrentSourcePointer = nullptr;
    
    // NOTE(Cristian): Had to define explicit file shared access.
    // CreateFile IS PLATFORM SPECIFIC!!!
    
    HANDLE File = CreateFile(Path, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);
    if (File)
    {
        LARGE_INTEGER LFileSize;
        GetFileSizeEx(File, &LFileSize);
        
        mmsz FileSize = LFileSize.QuadPart;
        char *Buffer = (char *)malloc(FileSize + 1);
        
        if (Buffer)
        {
            OVERLAPPED Overlapped = {};
            ReadFileEx(File, Buffer, (u32)FileSize, &Overlapped, 0);
            
            Buffer[FileSize] = 0;
            mmsz BOL = 0;
            mmsz BOChunk = 0;
            mmsz EOChunk = 0;
            for (mmsz At = 0;
                 At < FileSize;
                 )
            {
                char C0 = Buffer[At];
                // NOTE(Cristian): Buffer[FileSize] is specifically 0 so we don't go off range
                char C1 = Buffer[At + 1];
                mmsz EOL = At;
                b32 HitEOL = false;
                if ((C0 == '\r') && (C1 == '\n') ||
                    (C0 == '\n') && (C1 == '\r'))
                {
                    HitEOL = true;
                    At += 2;
                }
                else if (C0 == '\r')
                {
                    HitEOL = true;
                    At += 1;
                }
                else if (C0 == '\n')
                {
                    HitEOL = true;
                    At += 1;
                }
                else
                {
                    ++At;
                }
                
                if (HitEOL)
                {
                    // NOTE(Cristian): Get rid of empty lines
                    if (BOL < EOL)
                    {
                        string *Source = nullptr;
                        b32 FoundDefine = false;
                        if (strncmp(Buffer + BOL, VertexDefine, EOL - BOL) == 0)
                        {
                            FoundDefine = true;
                            CurrentSourcePointer = &Shader->VertexSource;
                        }
                        else if (strncmp(Buffer + BOL, FragmentDefine, EOL - BOL) == 0)
                        {
                            FoundDefine = true;
                            CurrentSourcePointer = &Shader->FragmentSource;
                        }
                        else if (strncmp(Buffer + BOL, GeometryDefine, EOL - BOL) == 0)
                        {
                            FoundDefine = true;
                            CurrentSourcePointer = &Shader->GeometrySource;
                        }
                        
                        if (FoundDefine)
                        {
                            EOChunk = BOL;
                            
                            // NOTE(Cristian): Data dump to source pointer
                            if (BOChunk != EOChunk)
                            {
                                if (PreviousSourcePointer->Data != nullptr)
                                {
                                    free(PreviousSourcePointer->Data);
                                }
                                
                                mmsz ChunkSize = EOChunk - BOChunk + 1;
                                PreviousSourcePointer->Data = (char *)malloc(ChunkSize);
                                if (PreviousSourcePointer->Data)
                                {
                                    strncpy_s(PreviousSourcePointer->Data, ChunkSize, &Buffer[BOChunk], _TRUNCATE);
                                    PreviousSourcePointer->Count = ChunkSize;
                                }
                                else
                                {
                                    fprintf(stderr, "ERROR: Couldn't allocate space for shader source pointer.\n");
                                    Result = false;
                                }
                                
                            }
                            PreviousSourcePointer = CurrentSourcePointer;
                            BOChunk = At;
                        }
                    }
                    BOL = At;
                }
            }
            
            // NOTE(Cristian): Final data dump to source pointer 
            // (there is no shader define to trigger the last one).
            mmsz ChunkSize = FileSize - BOChunk + 1;
            PreviousSourcePointer->Data = (char *)malloc(ChunkSize);
            if (PreviousSourcePointer->Data)
            {
                strncpy_s(PreviousSourcePointer->Data, ChunkSize, &Buffer[BOChunk], _TRUNCATE);
                PreviousSourcePointer->Count = ChunkSize;
            }
            else
            {
                fprintf(stderr, "ERROR: Couldn't allocate space for shader source pointer.\n");
                Result = false;
            }
        }
        else
        {
            fprintf(stderr, "ERROR: Couldn't allocate space for buffer.\n");
            Result = false;
        }
        free(Buffer);
        CloseHandle(File);
    }
    else
    {
        fprintf(stderr, "ERROR: Cannot open file %s.\n", Path);
        Result = false;
    }
    
    return(Result);
}

shader
Shader_Load(char *Path)
{
    shader Result = {};
    Result.FilePath = Path;
    
    Shader_ReadShaderFile(Path, &Result);
    
    /*int32_t CompileResult = Shader_Compile(&Result);
    if (CompileResult)
    {
        Shader_RefreshModifiedTime(&Result);
    }*/
    
    return(Result);
}

r64 Delta = 0;
r64 LastTime = 0;
r64 CurrentTime = 0;
camera SceneCamera;

b32 KeyboardKeys[512];

b32 FirstMouseMovement = true;
r64 LastMouseX = 0;
r64 LastMouseY = 0;

b32 ShadowMapDebugEnabled = false;
shadowmap_quality ShadowMapDebugLevel;
b32 ButtonPressed = false;

internal void
ProcessPendingMessages(HWND WindowHandle)
{
    MSG Message;
    while(PeekMessage(&Message, 0, 0, 0, PM_REMOVE))
    {
        u32 Key = (u32)Message.wParam;
        
        switch(Message.message)
        {
            case WM_QUIT:
            {
                ApplicationRunning = false;
            } break;
            
            case LC_PRESS:
            {
                if(Message.wParam == LC_ESCAPE)
                {
                    ApplicationRunning = false;
                }
                
                KeyboardKeys[Key] = true;
            } break;
            
            case LC_RELEASE:
            {
                KeyboardKeys[Key] = false;
            } break;
            
            case WM_INPUT:
            {
                ShowCursor(false);
                
                RECT CursorClipArea;           // new area for ClipCursor
                RECT OldCursorClipArea;        // previous area for ClipCursor
                GetClipCursor(&OldCursorClipArea);
                GetWindowRect(WindowHandle, &CursorClipArea);
                ClipCursor(&CursorClipArea);
                
                UINT dwSize = sizeof(RAWINPUT);
                BYTE lpb[sizeof(RAWINPUT)];
                
                GetRawInputData((HRAWINPUT)Message.lParam, RID_INPUT, lpb, &dwSize, sizeof(RAWINPUTHEADER));
                
                RAWINPUT *raw = (RAWINPUT*)lpb;
                
                if (raw->header.dwType == RIM_TYPEMOUSE) 
                {
                    s32 xPosRelative = raw->data.mouse.lLastX;
                    s32 yPosRelative = raw->data.mouse.lLastY;
                    
                    ProcessCameraDirection(&SceneCamera, xPosRelative, -yPosRelative);
                } 
                
            } break;
            
            default:
            {
                TranslateMessage(&Message);
                DispatchMessage(&Message);
            } break;
        }
    }
    
    // INPUT PROCESSING
    // NOTE(Cristian): Move this into its own function in the future
    
    if(KeyboardKeys[LC_SHIFT])
    {
        SceneCamera.Speed = 12.0f;
    }
    else
    {
        SceneCamera.Speed = 2.5f;
    }
    
    if(KeyboardKeys[LC_KEY_A])
    {
        ProcessCameraPosition(&SceneCamera, camera_direction::Left, Delta);
    }
    
    if(KeyboardKeys[LC_KEY_D])
    {
        ProcessCameraPosition(&SceneCamera, camera_direction::Right, Delta);
    }
    
    if(KeyboardKeys[LC_KEY_W])
    {
        ProcessCameraPosition(&SceneCamera, camera_direction::Forward, Delta);
    }
    
    if(KeyboardKeys[LC_KEY_S])
    {
        ProcessCameraPosition(&SceneCamera, camera_direction::Backward, Delta);
    }
    
    if(KeyboardKeys[LC_SPACE])
    {
        ProcessCameraPosition(&SceneCamera, camera_direction::Up, Delta);
    }
    
    if(KeyboardKeys[LC_CONTROL])
    {
        ProcessCameraPosition(&SceneCamera, camera_direction::Down, Delta);
    }
    
    
    if(KeyboardKeys[LC_F1] && !ButtonPressed)
    {
        ShadowMapDebugEnabled = !ShadowMapDebugEnabled;
        ButtonPressed = true;
    }
    else if(!KeyboardKeys[LC_F1])
    {
        ButtonPressed = false;
    }
    
    if(ShadowMapDebugEnabled)
    {
        if(KeyboardKeys[LC_NUMBER1])
        {
            ShadowMapDebugLevel = SHADOWMAP_QUALITY_ULTRA;
        }
        else if(KeyboardKeys[LC_NUMBER2])
        {
            ShadowMapDebugLevel = SHADOWMAP_QUALITY_HIGH;
        }
        else if(KeyboardKeys[LC_NUMBER3])
        {
            ShadowMapDebugLevel = SHADOWMAP_QUALITY_MEDIUM;
        }
        else if(KeyboardKeys[LC_NUMBER4])
        {
            ShadowMapDebugLevel = SHADOWMAP_QUALITY_LOW;
        }
    }
    
    
}

internal void
CenterCursorInWindow(HWND WindowHandle, u32 WindowWidth, u32 WindowHeight)
{
    POINT InitialCursorPos;
    InitialCursorPos.x = WindowWidth / 2;
    InitialCursorPos.y = WindowHeight / 2;
    
    ClientToScreen(WindowHandle, &InitialCursorPos);
    SetCursorPos(InitialCursorPos.x, InitialCursorPos.y);
}

RECT OldClipArea;
b32 ClipCursorArea = false;

struct surface_size
{
    u32 Width;
    u32 Height;
};

internal surface_size
GetWindowDimensions(HWND WindowHandle, b32 ExcludeBorders = true)
{
    u32 WindowWidth = 0;
    u32 WindowHeight = 0;
    RECT WindowSurfaceDimensions;
    
    if(ExcludeBorders)
    {
        if(GetClientRect(WindowHandle, &WindowSurfaceDimensions))
        {
            WindowWidth = WindowSurfaceDimensions.right - WindowSurfaceDimensions.left;
            WindowHeight = WindowSurfaceDimensions.bottom - WindowSurfaceDimensions.top;
        }
    }
    else
    {
        if(GetWindowRect(WindowHandle, &WindowSurfaceDimensions))
        {
            WindowWidth = WindowSurfaceDimensions.right - WindowSurfaceDimensions.left;
            WindowHeight = WindowSurfaceDimensions.bottom - WindowSurfaceDimensions.top;
        }
    }
    
    surface_size Dimensions = {};
    Dimensions.Width = WindowWidth;
    Dimensions.Height = WindowHeight;
    
    return(Dimensions);
}

internal void
ProcessCursorPosition(HWND WindowHandle, b32 LockCursor = false)
{
    POINT MouseCoord;
    if (GetCursorPos(&MouseCoord))
    {
        // Convert screen coordinates to window coordinates
        if (ScreenToClient(WindowHandle, &MouseCoord))
        {
            if(FirstMouseMovement)
            {
                LastMouseX = MouseCoord.x;
                LastMouseY = MouseCoord.y;
                FirstMouseMovement = false;
            }
            
            if(LockCursor)
            {
                ShowCursor(false);
                
                if(ClipCursorArea)
                {
                    RECT CursorClipArea;           // new area for ClipCursor
                    RECT OldCursorClipArea;        // previous area for ClipCursor
                    GetClipCursor(&OldCursorClipArea);
                    GetWindowRect(WindowHandle, &CursorClipArea);
                    ClipCursor(&CursorClipArea);
                }
                
                surface_size WindowSurface = GetWindowDimensions(WindowHandle);
                
                u32 WindowWidth = WindowSurface.Width;
                u32 WindowHeight = WindowSurface.Height;
                
                POINT CenteredCursorPos;
                CenteredCursorPos.x = WindowWidth / 2;
                CenteredCursorPos.y = WindowHeight / 2;
                
                r64 XOffset = MouseCoord.x - CenteredCursorPos.x;
                r64 YOffset = CenteredCursorPos.y - MouseCoord.y;
                
                ProcessCameraDirection(&SceneCamera, XOffset, YOffset);
                CenterCursorInWindow(WindowHandle, WindowWidth, WindowHeight);
            }
            else
            {
                ShowCursor(true);
                
                r64 XOffset = MouseCoord.x - LastMouseX;
                r64 YOffset = LastMouseY - MouseCoord.y; // Y coordinates go from bottom to top
                LastMouseX = MouseCoord.x;
                LastMouseY = MouseCoord.y;
                
                ProcessCameraDirection(&SceneCamera, XOffset, YOffset);
            }
        }
    }
}

internal void 
CenterWindowOnScreen(HWND WindowHandle, DWORD Style, DWORD ExStyle)
{
    u32 ScreenWidth = GetSystemMetrics(SM_CXSCREEN);
    u32 ScreenHeight = GetSystemMetrics(SM_CYSCREEN);    
    
    RECT ClientRect;
    GetClientRect(WindowHandle, &ClientRect);    
    AdjustWindowRectEx(&ClientRect, Style, FALSE, ExStyle);   
    
    u32 ClientWidth = ClientRect.right - ClientRect.left;
    u32 ClientHeight = ClientRect.bottom - ClientRect.top;    
    SetWindowPos(WindowHandle,
                 0,
                 ScreenWidth / 2 - ClientWidth / 2,
                 ScreenHeight / 2 - ClientHeight / 2,
                 ClientWidth, ClientHeight,
                 0);
}

internal r64
Win32GetCurrentTime()
{
    r64 Result = 0;
    LARGE_INTEGER PerformanceCount;
    if (!QueryPerformanceCounter(&PerformanceCount))
    {
        fprintf(stderr, "ERROR: Unable to retrieve performance counter\n");
    }
    else
    {
        Result = (r64)PerformanceCount.QuadPart;
    }
    return(Result);
}

internal r64
Win32GetPerformanceFrequency()
{
    r64 PerfCountFrequency = 0;
    LARGE_INTEGER PerformanceFrequency;
    if (!QueryPerformanceFrequency(&PerformanceFrequency))
    {
        fprintf(stderr, "ERROR: Unable to retrieve performance frequency\n");
    }
    else
    {
        PerfCountFrequency = (r64)PerformanceFrequency.QuadPart;
    }
    
    return(PerfCountFrequency);
}

internal void
SetGLState(open_gl *OpenGL, gl_state_cache *Cache, gl_state_type StateType, b32 Enabled)
{
    GLenum GLStateType = INVALID_STATE;
    
    b32 *StateRef = 0;
    switch(StateType)
    {
        case DEPTH_TEST:
        {
            StateRef = &Cache->DepthTest;
            GLStateType = GL_DEPTH_TEST;
        } break;
        
        case STENCIL_TEST:
        {
            StateRef = &Cache->StencilTest;
            GLStateType = GL_STENCIL_TEST;
        } break;
        
        case ALPHA_BLEND:
        {
            StateRef = &Cache->AlphaBlend;
            GLStateType = GL_BLEND;
        } break;
        
        case FACE_CULLING:
        {
            StateRef = &Cache->FaceCulling;
            GLStateType = GL_CULL_FACE;
        } break;
    }
    
    if(*StateRef != Enabled && GLStateType != INVALID_STATE)
    {
        *StateRef = Enabled;
        
        if(Enabled)
        {
            OpenGL->glEnable(GLStateType);
        }
        else
        {
            OpenGL->glDisable(GLStateType);
        }
    }
}

internal GLfloat *
GetCubeMesh(asset *Asset)
{
    global_variable GLfloat VertexData[] = {
        // back face
        -1.0f, -1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 0.0f, 0.0f, // bottom-left
        1.0f,  1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 1.0f, 1.0f, // top-right
        1.0f, -1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 1.0f, 0.0f, // bottom-right         
        1.0f,  1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 1.0f, 1.0f, // top-right
        -1.0f, -1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 0.0f, 0.0f, // bottom-left
        -1.0f,  1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 0.0f, 1.0f, // top-left
        // front face
        -1.0f, -1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f, 0.0f, // bottom-left
        1.0f, -1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 1.0f, 0.0f, // bottom-right
        1.0f,  1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 1.0f, 1.0f, // top-right
        1.0f,  1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 1.0f, 1.0f, // top-right
        -1.0f,  1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f, 1.0f, // top-left
        -1.0f, -1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f, 0.0f, // bottom-left
        // left face
        -1.0f,  1.0f,  1.0f, -1.0f,  0.0f,  0.0f, 1.0f, 1.0f, // top-right
        -1.0f,  1.0f, -1.0f, -1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // top-left
        -1.0f, -1.0f, -1.0f, -1.0f,  0.0f,  0.0f, 0.0f, 0.0f, // bottom-left
        -1.0f, -1.0f, -1.0f, -1.0f,  0.0f,  0.0f, 0.0f, 0.0f, // bottom-left
        -1.0f, -1.0f,  1.0f, -1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // bottom-right
        -1.0f,  1.0f,  1.0f, -1.0f,  0.0f,  0.0f, 1.0f, 1.0f, // top-right
        // right face
        1.0f,  1.0f,  1.0f,  1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // top-left
        1.0f, -1.0f, -1.0f,  1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // bottom-right
        1.0f,  1.0f, -1.0f,  1.0f,  0.0f,  0.0f, 1.0f, 1.0f, // top-right         
        1.0f, -1.0f, -1.0f,  1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // bottom-right
        1.0f,  1.0f,  1.0f,  1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // top-left
        1.0f, -1.0f,  1.0f,  1.0f,  0.0f,  0.0f, 0.0f, 0.0f, // bottom-left     
        // bottom face
        -1.0f, -1.0f, -1.0f,  0.0f, -1.0f,  0.0f, 1.0f, 1.0f, // top-right
        1.0f, -1.0f, -1.0f,  0.0f, -1.0f,  0.0f, 0.0f, 1.0f, // top-left
        1.0f, -1.0f,  1.0f,  0.0f, -1.0f,  0.0f, 0.0f, 0.0f, // bottom-left
        1.0f, -1.0f,  1.0f,  0.0f, -1.0f,  0.0f, 0.0f, 0.0f, // bottom-left
        -1.0f, -1.0f,  1.0f,  0.0f, -1.0f,  0.0f, 1.0f, 0.0f, // bottom-right
        -1.0f, -1.0f, -1.0f,  0.0f, -1.0f,  0.0f, 1.0f, 1.0f, // top-right
        // top face
        -1.0f,  1.0f, -1.0f,  0.0f,  1.0f,  0.0f, 0.0f, 1.0f, // top-left
        1.0f,  1.0f , 1.0f,  0.0f,  1.0f,  0.0f, 1.0f, 0.0f, // bottom-right
        1.0f,  1.0f, -1.0f,  0.0f,  1.0f,  0.0f, 1.0f, 1.0f, // top-right     
        1.0f,  1.0f,  1.0f,  0.0f,  1.0f,  0.0f, 1.0f, 0.0f, // bottom-right
        -1.0f,  1.0f, -1.0f,  0.0f,  1.0f,  0.0f, 0.0f, 1.0f, // top-left
        -1.0f,  1.0f,  1.0f,  0.0f,  1.0f,  0.0f, 0.0f, 0.0f  // bottom-left        
    };
    
    Asset->VertexComponents = 3;
    Asset->NormalComponents = 3;
    Asset->TextureComponents = 2;
    Asset->TotalVertices = ArrayCount(VertexData) / (Asset->VertexComponents + Asset->NormalComponents + Asset->TextureComponents);
    
    return(VertexData);
}

internal GLfloat *
GetSquare(asset *Asset)
{
    global_variable GLfloat VertexDataSquare[] = {
        // first triangle
        1.0f, -1.0f, 0.0f,   1.0f, 0.0f,  // bottom right
        1.0f,  1.0f, 0.0f,   1.0f, 1.0f,  // top right
        -1.0f,  1.0f, 0.0f,   0.0f, 1.0f,  // top left 
        
        // second triangle
        -1.0f,  1.0f, 0.0f,   0.0f, 1.0f,  // top left
        -1.0f, -1.0f, 0.0f,   0.0f, 0.0f, // bottom left
        1.0f, -1.0f, 0.0f,   1.0f, 0.0f  // bottom right
    };
    
    Asset->VertexComponents = 3;
    Asset->TextureComponents = 2;
    Asset->TotalVertices = ArrayCount(VertexDataSquare) / (Asset->VertexComponents + Asset->TextureComponents);
    
    return(VertexDataSquare);
}

internal f32
GetMaximum(f32 A, f32 B)
{
    f32 Result = A > B ? A : B;
    return(Result);
}

internal f32
GetMinimum(f32 A, f32 B)
{
    f32 Result = A < B ? A : B;
    return(Result);
}

internal extents_info
Find3DExtents(f32 *VertexData,
              u32 TotalVertices,
              u32 VertexStride)
{
    extents_info Result = {};
    
    if(TotalVertices)
    {
        glm::vec3 Vertex = glm::vec3(VertexData[0], VertexData[1], VertexData[2]);
        
        Result.Min = glm::vec3(Vertex.x, Vertex.y, Vertex.z);
        Result.Max = glm::vec3(Vertex.x, Vertex.y, Vertex.z);
        
        u32 TotalVertexData = TotalVertices * VertexStride;
        
        for(u32 Index = VertexStride;
            Index < TotalVertexData;
            Index = Index + VertexStride)
        {
            Vertex = glm::vec3(VertexData[Index], VertexData[Index + 1], VertexData[Index + 2]);
            
            Result.Min.x = GetMinimum(Result.Min.x, Vertex.x);
            Result.Max.x = GetMaximum(Result.Max.x, Vertex.x);
            
            Result.Min.y = GetMinimum(Result.Min.y, Vertex.y);
            Result.Max.y = GetMaximum(Result.Max.y, Vertex.y);
            
            Result.Min.z = GetMinimum(Result.Min.z, Vertex.z);
            Result.Max.z = GetMaximum(Result.Max.z, Vertex.z);
        }
    }
    
    return(Result);
}

internal void
GenerateDebugAABBBound(open_gl *OpenGL, asset *Asset)
{
    aabb_info *AABB = &Asset->AABBBound;
    extents_info *ExtentsInfo = &Asset->Extents;
    
    GLfloat AABBPoints[] = 
    {
        ExtentsInfo->Max.x, ExtentsInfo->Max.y, ExtentsInfo->Max.z,
        ExtentsInfo->Min.x, ExtentsInfo->Max.y, ExtentsInfo->Max.z,
        ExtentsInfo->Min.x, ExtentsInfo->Max.y, ExtentsInfo->Max.z,
        ExtentsInfo->Min.x, ExtentsInfo->Min.y, ExtentsInfo->Max.z,
        ExtentsInfo->Min.x, ExtentsInfo->Min.y, ExtentsInfo->Max.z,
        ExtentsInfo->Max.x, ExtentsInfo->Min.y, ExtentsInfo->Max.z,
        ExtentsInfo->Max.x, ExtentsInfo->Min.y, ExtentsInfo->Max.z,
        ExtentsInfo->Max.x, ExtentsInfo->Max.y, ExtentsInfo->Max.z,
        
        ExtentsInfo->Max.x, ExtentsInfo->Max.y, ExtentsInfo->Min.z,
        ExtentsInfo->Min.x, ExtentsInfo->Max.y, ExtentsInfo->Min.z,
        ExtentsInfo->Min.x, ExtentsInfo->Max.y, ExtentsInfo->Min.z,
        ExtentsInfo->Min.x, ExtentsInfo->Min.y, ExtentsInfo->Min.z,
        ExtentsInfo->Min.x, ExtentsInfo->Min.y, ExtentsInfo->Min.z,
        ExtentsInfo->Max.x, ExtentsInfo->Min.y, ExtentsInfo->Min.z,
        ExtentsInfo->Max.x, ExtentsInfo->Min.y, ExtentsInfo->Min.z,
        ExtentsInfo->Max.x, ExtentsInfo->Max.y, ExtentsInfo->Min.z,
        
        ExtentsInfo->Min.x, ExtentsInfo->Max.y, ExtentsInfo->Max.z,
        ExtentsInfo->Min.x, ExtentsInfo->Max.y, ExtentsInfo->Min.z,
        ExtentsInfo->Min.x, ExtentsInfo->Min.y, ExtentsInfo->Max.z,
        ExtentsInfo->Min.x, ExtentsInfo->Min.y, ExtentsInfo->Min.z,
        ExtentsInfo->Max.x, ExtentsInfo->Max.y, ExtentsInfo->Max.z,
        ExtentsInfo->Max.x, ExtentsInfo->Max.y, ExtentsInfo->Min.z,
        ExtentsInfo->Max.x, ExtentsInfo->Min.y, ExtentsInfo->Max.z,
        ExtentsInfo->Max.x, ExtentsInfo->Min.y, ExtentsInfo->Min.z
    };
    
    OpenGL->glCreateVertexArrays(1, &AABB->DebugDataVAO);
    OpenGL->glCreateBuffers(1, &AABB->DebugDataVBO);
    
    OpenGL->glBindVertexArray(AABB->DebugDataVAO);
    OpenGL->glBindBuffer(GL_ARRAY_BUFFER, AABB->DebugDataVBO);
    
    OpenGL->glEnableVertexAttribArray(0);
    OpenGL->glVertexAttribFormat(0, 3, GL_FLOAT, GL_FALSE, 0);
    
    OpenGL->glVertexAttribBinding(0, 0);
    OpenGL->glBindVertexBuffer(0, AABB->DebugDataVBO, 0, 3 * sizeof(GLfloat));
    
    OpenGL->glBindVertexArray(0);
    OpenGL->glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    OpenGL->glNamedBufferData(AABB->DebugDataVBO, sizeof(GLfloat) * ArrayCount(AABBPoints), AABBPoints, GL_STATIC_DRAW);
}

internal void
GenerateSphereBoundingBoxData(LC_Array(r32) VertexData, 
                              LC_Array(u32) IndicesData, 
                              r32 Radius,
                              u32 StackCount,
                              u32 SectorCount)
{
    r32 X, Y, Z, XY;                                             // vertex position
    r32 NormalX, NormalY, NormalZ, LengthInv = 1.0f / Radius;    // vertex normal
    r32 S, T;                                                    // vertex texCoord
    
    r32 SectorStep = (r32)(TWO_PI) / SectorCount;
    r32 StackStep = (r32)(PI) / StackCount;
    r32 SectorAngle, StackAngle;
    
    // generate CCW index list of sphere triangles
    // k1--k1+1
    // |  / |
    // | /  |
    // k2--k2+1
    u32 K1, K2;
    
    for(u32 StackIndex = 0;
        StackIndex <= StackCount;
        ++StackIndex)
    {
        StackAngle = (r32)HALF_PI - StackIndex * StackStep;        // starting from pi/2 to -pi/2
        XY = Radius * cosf(StackAngle);             // r * cos(u)
        Z = Radius * sinf(StackAngle);              // r * sin(u)
        
        K1 = StackIndex * (SectorCount + 1);     // beginning of current stack
        K2 = K1 + SectorCount + 1;              // beginning of next stack
        
        for(u32 SectorIndex = 0;
            SectorIndex <= SectorCount;
            ++SectorIndex,
            ++K1, 
            ++K2)
        {
            SectorAngle = SectorIndex * SectorStep;           // starting from 0 to 2pi
            
            // vertex position (x, y, z)
            X = XY * cosf(SectorAngle);             // r * cos(u) * cos(v)
            Y = XY * sinf(SectorAngle);             // r * cos(u) * sin(v)
            LC_ArrayAppend(VertexData, X);
            LC_ArrayAppend(VertexData, Y);
            LC_ArrayAppend(VertexData, Z);
            
            // normalized vertex normal (nx, ny, nz)
            NormalX = X * LengthInv;
            NormalY = Y * LengthInv;
            NormalZ = Z * LengthInv;
            LC_ArrayAppend(VertexData, NormalX);
            LC_ArrayAppend(VertexData, NormalY);
            LC_ArrayAppend(VertexData, NormalZ);
            
            // vertex tex coord (s, t) range between [0, 1]
            S = (r32)SectorIndex / SectorCount;
            T = (r32)StackIndex / StackCount;
            LC_ArrayAppend(VertexData, S);
            LC_ArrayAppend(VertexData, T);
            
            // 2 triangles per sector excluding first and last stacks
            // k1 => k2 => k1+1
            if(StackIndex != 0)
            {
                LC_ArrayAppend(IndicesData, K1);
                LC_ArrayAppend(IndicesData, K2);
                LC_ArrayAppend(IndicesData, K1 + 1);
            }
            
            // k1+1 => k2 => k2+1
            if(StackIndex != (StackCount - 1))
            {
                LC_ArrayAppend(IndicesData, K1 + 1);
                LC_ArrayAppend(IndicesData, K2);
                LC_ArrayAppend(IndicesData, K2 + 1);
            }
        }
    }
}

internal void
GenerateDebugSphereBound(open_gl *OpenGL,
                         memory_arena *Arena,
                         sphere_bound_info *SphereBound,
                         r32 Radius)
{
    SphereBound->Smoothness.StackCount = 10;
    SphereBound->Smoothness.SectorCount = 40;
    
    SphereBound->VertexComponents = 3;
    SphereBound->NormalComponents = 3;
    SphereBound->TextureComponents = 2;
    u32 VertexStride = SphereBound->VertexComponents + SphereBound->NormalComponents + SphereBound->TextureComponents;
    
    // NOTE(Cristian): Stack and sector count have one extra entry per the generation algorithm (<= StackCount/SectorCount)
    SphereBound->TotalVertices = (SphereBound->Smoothness.StackCount + 1) * (SphereBound->Smoothness.SectorCount + 1);
    
    temporary_memory TempMemory = BeginTemporaryMemory(Arena);
    
    LC_Array(r32) VertexData;
    LC_ArrayInit(VertexData, Arena, SphereBound->TotalVertices * VertexStride);
    LC_Array(u32) IndicesData;
    // NOTE(Cristian): First and last stack don't have 2 triangles each
    LC_ArrayInit(IndicesData, Arena, SphereBound->Smoothness.StackCount * (SphereBound->Smoothness.SectorCount + 1) * 6);
    
    GenerateSphereBoundingBoxData(VertexData, IndicesData, Radius, 
                                  SphereBound->Smoothness.StackCount, SphereBound->Smoothness.SectorCount);
    
    SphereBound->TotalIndices = (u32)LC_ArrayCount(IndicesData);
    
    OpenGL->glCreateVertexArrays(1, &SphereBound->DebugDataVAO);
    OpenGL->glCreateBuffers(1, &SphereBound->DebugDataVBO);
    OpenGL->glCreateBuffers(1, &SphereBound->DebugDataEBO);
    
    u32 PositionAttribIndex = 0;
    u32 NormalAttribIndex = 1;
    u32 TextureAttribIndex = 2;
    
    OpenGL->glBindVertexArray(SphereBound->DebugDataVAO);
    OpenGL->glBindBuffer(GL_ARRAY_BUFFER, SphereBound->DebugDataVBO);
    OpenGL->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, SphereBound->DebugDataEBO);
    
    OpenGL->glEnableVertexAttribArray(PositionAttribIndex);
    OpenGL->glEnableVertexAttribArray(NormalAttribIndex);
    OpenGL->glEnableVertexAttribArray(TextureAttribIndex);
    
    OpenGL->glVertexAttribFormat(PositionAttribIndex, 3, GL_FLOAT, GL_FALSE, 0);
    OpenGL->glVertexAttribFormat(NormalAttribIndex, 3, GL_FLOAT, GL_FALSE, (3 * sizeof(GLfloat)));
    OpenGL->glVertexAttribFormat(TextureAttribIndex, 2, GL_FLOAT, GL_FALSE, (6 * sizeof(GLfloat)));
    
    u32 BindingIndexStream = 0;
    OpenGL->glVertexAttribBinding(PositionAttribIndex, BindingIndexStream);
    OpenGL->glVertexAttribBinding(NormalAttribIndex, BindingIndexStream);
    OpenGL->glVertexAttribBinding(TextureAttribIndex, BindingIndexStream);
    
    OpenGL->glBindVertexBuffer(BindingIndexStream, SphereBound->DebugDataVBO, 0, 8 * sizeof(GLfloat));
    
    OpenGL->glBindVertexArray(0);
    OpenGL->glBindBuffer(GL_ARRAY_BUFFER, 0);
    OpenGL->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    
    OpenGL->glNamedBufferData(SphereBound->DebugDataVBO, sizeof(VertexData[0]) * LC_ArrayCount(VertexData), VertexData, GL_STATIC_DRAW);
    OpenGL->glNamedBufferData(SphereBound->DebugDataEBO, sizeof(IndicesData[0]) * SphereBound->TotalIndices, IndicesData, GL_STATIC_DRAW);
    
    EndTemporaryMemory(TempMemory);
}



internal asset
GenerateTerrainAsset(open_gl *OpenGL, memory_arena *Arena)
{
    asset Asset = {};
    
    GLfloat TerrainData[] = {
        -1.0f,  0.0f,  1.0f,  0.0f, 1.0f, 0.0f,  0.0f, 0.0f, // top-left
        1.0f,  0.0f,  1.0f,  0.0f, 1.0f, 0.0f,  1.0f, 0.0f, // bottom-right
        1.0f,  0.0f, -1.0f,  0.0f, 1.0f, 0.0f,  1.0f, 1.0f, // top-right
        
        1.0f,  0.0f, -1.0f,  0.0f, 1.0f, 0.0f,  1.0f, 1.0f, // bottom-right
        -1.0f,  0.0f, -1.0f,  0.0f, 1.0f, 0.0f,  0.0f, 1.0f, // top-left
        -1.0f,  0.0f,  1.0f,  0.0f, 1.0f, 0.0f,  0.0f, 0.0f  // bottom-left        
    };
    
    Asset.VertexComponents = 3;
    Asset.NormalComponents = 3;
    Asset.TextureComponents = 2;
    u32 VertexStride = Asset.VertexComponents + Asset.NormalComponents + Asset.TextureComponents;
    Asset.TotalVertices = ArrayCount(TerrainData) / VertexStride;
    
    Asset.Extents = Find3DExtents(TerrainData, Asset.TotalVertices, VertexStride);
    GenerateDebugAABBBound(OpenGL, &Asset);
    
    r32 Radius = glm::distance(Asset.Extents.Min, Asset.Extents.Max) / 2;
    GenerateDebugSphereBound(OpenGL, Arena, &Asset.SphereBound, Radius);
    
    // BUFFERS CREATION
    OpenGL->glCreateVertexArrays(1, &Asset.VAO);
    OpenGL->glCreateBuffers(1, &Asset.VBO);
    
    // DATA LAYOUT SETUP
    u32 PositionAttribIndex = 0;
    u32 NormalAttribIndex = 1;
    u32 TextureAttribIndex = 2;
    
    OpenGL->glBindVertexArray(Asset.VAO);
    OpenGL->glBindBuffer(GL_ARRAY_BUFFER, Asset.VBO);
    
    OpenGL->glEnableVertexAttribArray(PositionAttribIndex);
    OpenGL->glEnableVertexAttribArray(NormalAttribIndex);
    OpenGL->glEnableVertexAttribArray(TextureAttribIndex);
    
    OpenGL->glVertexAttribFormat(PositionAttribIndex, Asset.VertexComponents, GL_FLOAT, GL_FALSE, 0);
    OpenGL->glVertexAttribFormat(NormalAttribIndex, Asset.NormalComponents, GL_FLOAT, GL_FALSE, (Asset.VertexComponents * sizeof(TerrainData[0])));
    OpenGL->glVertexAttribFormat(TextureAttribIndex, Asset.TextureComponents, GL_FLOAT, GL_FALSE, ((Asset.VertexComponents + Asset.NormalComponents) * sizeof(TerrainData[0])));
    
    u32 BindingIndexStream = 0;
    OpenGL->glVertexAttribBinding(PositionAttribIndex, BindingIndexStream);
    OpenGL->glVertexAttribBinding(NormalAttribIndex, BindingIndexStream);
    OpenGL->glVertexAttribBinding(TextureAttribIndex, BindingIndexStream);
    
    OpenGL->glBindVertexBuffer(BindingIndexStream, Asset.VBO, 0, VertexStride * sizeof(TerrainData[0]));
    
    OpenGL->glBindVertexArray(0);
    OpenGL->glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    // BUFFER LOADING
    OpenGL->glNamedBufferData(Asset.VBO, Asset.TotalVertices * VertexStride * sizeof(TerrainData[0]), &TerrainData, GL_STATIC_DRAW);
    
    texture_file TerrainTexture = TextureLoader_ReadTextureFile("assets/Floor/floor_diffuse.jpg", true);
    Asset.Texture = RenderTexture_LoadTexture(OpenGL, &TerrainTexture, GL_TEXTURE_2D, true);
    Asset.Texture.Unit = 0;
    
    return(Asset);
}

internal asset
GenerateCubeAsset(open_gl *OpenGL, memory_arena *Arena)
{
    asset Asset = {};
    
    // ASSET DATA
    GLfloat *CubeData = GetCubeMesh(&Asset);
    
    u32 VertexStride = Asset.VertexComponents + Asset.NormalComponents + Asset.TextureComponents;
    Asset.Extents = Find3DExtents(CubeData, Asset.TotalVertices, VertexStride);
    GenerateDebugAABBBound(OpenGL, &Asset);
    
    r32 Radius = glm::distance(Asset.Extents.Min, Asset.Extents.Max) / 2;
    GenerateDebugSphereBound(OpenGL, Arena, &Asset.SphereBound, Radius);
    
    // BUFFERS CREATION
    OpenGL->glCreateVertexArrays(1, &Asset.VAO);
    OpenGL->glCreateBuffers(1, &Asset.VBO);
    
    // DATA LAYOUT SETUP
    u32 PositionAttribIndex = 0;
    u32 NormalAttribIndex = 1;
    u32 TextureAttribIndex = 2;
    
    OpenGL->glBindVertexArray(Asset.VAO);
    OpenGL->glBindBuffer(GL_ARRAY_BUFFER, Asset.VBO);
    
    OpenGL->glEnableVertexAttribArray(PositionAttribIndex);
    OpenGL->glEnableVertexAttribArray(NormalAttribIndex);
    OpenGL->glEnableVertexAttribArray(TextureAttribIndex);
    
    OpenGL->glVertexAttribFormat(PositionAttribIndex, Asset.VertexComponents, GL_FLOAT, GL_FALSE, 0);
    OpenGL->glVertexAttribFormat(NormalAttribIndex, Asset.NormalComponents, GL_FLOAT, GL_FALSE, (Asset.VertexComponents * sizeof(CubeData[0])));
    OpenGL->glVertexAttribFormat(TextureAttribIndex, Asset.TextureComponents, GL_FLOAT, GL_FALSE, ((Asset.VertexComponents + Asset.NormalComponents) * sizeof(CubeData[0])));
    
    u32 BindingIndexStream = 0;
    OpenGL->glVertexAttribBinding(PositionAttribIndex, BindingIndexStream);
    OpenGL->glVertexAttribBinding(NormalAttribIndex, BindingIndexStream);
    OpenGL->glVertexAttribBinding(TextureAttribIndex, BindingIndexStream);
    
    OpenGL->glBindVertexBuffer(BindingIndexStream, Asset.VBO, 0, VertexStride * sizeof(CubeData[0]));
    
    OpenGL->glBindVertexArray(0);
    
    OpenGL->glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    // BUFFER LOADING
    OpenGL->glNamedBufferData(Asset.VBO, Asset.TotalVertices * VertexStride * sizeof(CubeData[0]), CubeData, GL_STATIC_DRAW);
    
    texture_file ContainerTexture = TextureLoader_ReadTextureFile("assets/container.jpg", true);
    Asset.Texture = RenderTexture_LoadTexture(OpenGL, &ContainerTexture, GL_TEXTURE_2D, true);
    Asset.Texture.Unit = 0;
    
    return(Asset);
}

internal asset
Generate2DSurface(open_gl *OpenGL)
{
    asset Asset = {};
    
    // ASSET DATA
    GLfloat *SquareData = GetSquare(&Asset);
    
    u32 VertexStride = Asset.VertexComponents + Asset.TextureComponents;
    
    // BUFFERS CREATION
    OpenGL->glCreateVertexArrays(1, &Asset.VAO);
    OpenGL->glCreateBuffers(1, &Asset.VBO);
    
    // DATA LAYOUT SETUP
    u32 PositionAttribIndex = 0;
    u32 TextureAttribIndex = 1;
    
    OpenGL->glBindVertexArray(Asset.VAO);
    OpenGL->glBindBuffer(GL_ARRAY_BUFFER, Asset.VBO);
    
    OpenGL->glEnableVertexAttribArray(PositionAttribIndex);
    OpenGL->glEnableVertexAttribArray(TextureAttribIndex);
    
    OpenGL->glVertexAttribFormat(PositionAttribIndex, Asset.VertexComponents, GL_FLOAT, GL_FALSE, 0);
    OpenGL->glVertexAttribFormat(TextureAttribIndex, Asset.TextureComponents, GL_FLOAT, GL_FALSE, (Asset.VertexComponents * sizeof(SquareData[0])));
    
    u32 BindingIndexStream = 0;
    OpenGL->glVertexAttribBinding(PositionAttribIndex, BindingIndexStream);
    OpenGL->glVertexAttribBinding(TextureAttribIndex, BindingIndexStream);
    
    OpenGL->glBindVertexBuffer(BindingIndexStream, Asset.VBO, 0, VertexStride * sizeof(SquareData[0]));
    
    OpenGL->glBindVertexArray(0);
    
    OpenGL->glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    // BUFFER LOADING
    OpenGL->glNamedBufferData(Asset.VBO, Asset.TotalVertices * VertexStride * sizeof(SquareData[0]), SquareData, GL_STATIC_DRAW);
    
    Asset.Texture.Unit = 0;
    
    return(Asset);
}

internal asset
GenerateLightAsset(open_gl *OpenGL, memory_arena *Arena)
{
    // LIGHT SOURCE INIT
    
    asset Asset = {};
    
    // ASSET DATA
    GLfloat *LightSourceData = GetCubeMesh(&Asset);
    
    u32 VertexStride = Asset.VertexComponents + Asset.NormalComponents + Asset.TextureComponents;
    Asset.Extents = Find3DExtents(LightSourceData, Asset.TotalVertices, VertexStride);
    GenerateDebugAABBBound(OpenGL, &Asset);
    
    r32 Radius = glm::distance(Asset.Extents.Min, Asset.Extents.Max) / 2;
    GenerateDebugSphereBound(OpenGL, Arena, &Asset.SphereBound, Radius);
    
    // BUFFERS CREATION
    OpenGL->glCreateVertexArrays(1, &Asset.VAO);
    OpenGL->glCreateBuffers(1, &Asset.VBO);
    
    // DATA LAYOUT SETUP
    u32 PositionAttribIndex = 0;
    
    OpenGL->glBindVertexArray(Asset.VAO);
    OpenGL->glBindBuffer(GL_ARRAY_BUFFER, Asset.VBO);
    
    OpenGL->glEnableVertexAttribArray(PositionAttribIndex);
    
    OpenGL->glVertexAttribFormat(PositionAttribIndex, Asset.VertexComponents, GL_FLOAT, GL_FALSE, 0);
    
    u32 BindingIndexStream = 0;
    OpenGL->glVertexAttribBinding(PositionAttribIndex, BindingIndexStream);
    
    OpenGL->glBindVertexBuffer(BindingIndexStream, Asset.VBO, 0, VertexStride * sizeof(LightSourceData[0]));
    
    OpenGL->glBindVertexArray(0);
    
    // BUFFER LOADING
    OpenGL->glNamedBufferData(Asset.VBO, Asset.TotalVertices * VertexStride * sizeof(LightSourceData[0]), LightSourceData, GL_STATIC_DRAW);
    
    return(Asset);
}

struct transformation_matrices_storage
{
    GLuint UBO;
};

internal transformation_matrices_storage
GenerateTransformationMatricesStorage(open_gl *OpenGL, transformation_matrices *TransformMatrices)
{
    transformation_matrices_storage Storage = {};
    
    OpenGL->glGenBuffers(1, &Storage.UBO);
    
    OpenGL->glBindBuffer(GL_UNIFORM_BUFFER, Storage.UBO);
    OpenGL->glBufferData(GL_UNIFORM_BUFFER, sizeof(transformation_matrices), 0, GL_STATIC_DRAW);
    OpenGL->glBindBuffer(GL_UNIFORM_BUFFER, 0);
    
    u32 BindingPoint = 1;
    OpenGL->glBindBufferBase(GL_UNIFORM_BUFFER, BindingPoint, Storage.UBO);
    
    OpenGL->glNamedBufferData(Storage.UBO, sizeof(transformation_matrices), TransformMatrices, GL_STATIC_DRAW);
    
    return(Storage);
}

internal glm::vec2
GetAtlasXYOffset(u32 TileIndex, u32 TileSize, u32 AtlasSize)
{
    u32 NrRows = AtlasSize / TileSize;
    u32 Row = TileIndex / NrRows;
    u32 Column = TileIndex % NrRows;
    u32 XOffset = Column * TileSize;
    u32 YOffset = Row * TileSize;
    
    glm::vec2 Result;
    Result.x = (r32)XOffset;
    Result.y = (r32)YOffset;
    return(Result);
}

internal GLuint
CreateDepthMapTexture(open_gl *OpenGL, u32 Width, u32 Height)
{
    GLuint DepthMap;
    OpenGL->glGenTextures(1, &DepthMap);
    OpenGL->glBindTexture(GL_TEXTURE_2D, DepthMap);
    
    OpenGL->glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, Width, Height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);
    OpenGL->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    OpenGL->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    OpenGL->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    OpenGL->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
    
    f32 BorderColor[] = { 1.0f, 1.0f, 1.0f, 1.0f };
    OpenGL->glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, BorderColor);
    
    OpenGL->glBindTexture(GL_TEXTURE_2D, 0);
    
    return(DepthMap);
}

internal GLuint
CreateDepthFBO(open_gl *OpenGL, GLuint DepthMap)
{
    GLuint DepthFBO;
    OpenGL->glGenFramebuffers(1, &DepthFBO);
    
    OpenGL->glBindFramebuffer(GL_FRAMEBUFFER, DepthFBO);
    OpenGL->glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, DepthMap, 0);
    
    // Disable the color buffer so the FBO can be complete
    OpenGL->glDrawBuffer(GL_NONE);
    OpenGL->glReadBuffer(GL_NONE);
    
    GLenum Status = OpenGL->glCheckFramebufferStatus(GL_FRAMEBUFFER);
    if (Status != GL_FRAMEBUFFER_COMPLETE) 
    {
        DepthFBO = 0;
        
        // LOG ERROR HERE
    }
    
    OpenGL->glBindFramebuffer(GL_FRAMEBUFFER, 0);
    
    return(DepthFBO);
}

internal void
InitShadowmapAtlas(open_gl *OpenGL, memory_arena *Arena,
                   LC_Array(shadowmap_info) ShadowmapInfo,
                   shadowmap_quality Quality,
                   u32 AtlasSize,
                   u32 TileSize)
{
    ShadowmapInfo[Quality].Atlas.Size     = AtlasSize;
    ShadowmapInfo[Quality].Atlas.TileSize = TileSize;
    ShadowmapInfo[Quality].DepthMap       = CreateDepthMapTexture(OpenGL, AtlasSize, AtlasSize);
    ShadowmapInfo[Quality].FBO            = CreateDepthFBO(OpenGL, ShadowmapInfo[Quality].DepthMap);
    u32 TotalTiles                        = (AtlasSize / TileSize) * (AtlasSize / TileSize);
    LC_ArrayInit(ShadowmapInfo[Quality].Atlas.Freelist, Arena, TotalTiles);
}

internal void
CreateDirectionalLightViewProjectionMatrix(u32 ShadowMapWidth, u32 ShadowMapHeight, directional_light *Light)
{
    r32 AspectRatio = Cast(r32)ShadowMapWidth / Cast(r32)ShadowMapHeight;
    r32 NearPlane = -40.0f;
    r32 FarPlane = 40.0f;
    r32 FrustumLimit = 70.0f;
    glm::mat4 DirectionalLightProjection = glm::ortho(-FrustumLimit, FrustumLimit, -FrustumLimit, FrustumLimit, NearPlane, FarPlane);
    
    glm::vec3 Position = glm::vec3(-2.0f, 4.0f, -1.0f);
    glm::vec3 UpVector = glm::vec3(0.0f, 1.0f, 0.0f);
    Light->LightSpaceMatrix = DirectionalLightProjection * glm::lookAt(Position, glm::vec3(0.0f), UpVector);
}

internal void
InitDirectionalLightsShadowmap(LC_Array(directional_light) Lights, shadowmap_atlas *Atlas)
{
    for(u32 Index = 0;
        Index < LC_ArrayCount(Lights);
        ++Index)
    {
        directional_light *Light = &Lights[Index];
        
        u32 TotalTiles = (Atlas->Size / Atlas->TileSize) * (Atlas->Size / Atlas->TileSize);
        if(Atlas->NextFreeIndex < TotalTiles)
        {
            Light->ShadowmapTileIndex = Atlas->NextFreeIndex++;
            Light->ShadowmapQuality = SHADOWMAP_QUALITY_ULTRA;
            
            CreateDirectionalLightViewProjectionMatrix(Atlas->TileSize, Atlas->TileSize, Light);
        }
        else
        {
            // LOG ERROR HERE
        }
    }
}

internal u32
GetDistanceBasedTileSize(r32 Distance)
{
    u32 Result = 0;
    
    // NOTE(Cristian): Not final, needs more tweaking!
    r32 MinDistance = 15.0f;
    r32 MediumDistance1 = 30.0f;
    r32 MediumDistance2 = 45.0f;
    r32 MaxDistance = 60.0f;
    
    if(Distance >= 0 && Distance <= MinDistance)
    {
        Result = 1024;
    }
    else if(Distance > MinDistance && Distance <= MediumDistance1)
    {
        Result = 512;
    }
    else if(Distance > MediumDistance1 && Distance <= MediumDistance2)
    {
        Result = 256;
    }
    //else if(Distance > MediumDistance2 && Distance <= MaxDistance)
    else if(Distance > MediumDistance2)
    {
        Result = 128;
    }
    
    return(Result);
}

internal void
CreatePointLightViewProjectionMatrices(r32 AspectRatio, r32 NearPlane, r32 FarPlane, point_light *Light)
{
    glm::mat4 PointLightProjection = glm::perspective(glm::radians(90.0f), AspectRatio, NearPlane, FarPlane); 
    
    glm::vec3 LightPosition = glm::vec3(Light->Position);
    
    // Right
    Light->LightSpaceMatrix[SHADOWMAP_FACE_POSITIVE_X] = PointLightProjection * glm::lookAt(LightPosition, 
                                                                                            LightPosition + glm::vec3(1.0, 0.0, 0.0), 
                                                                                            glm::vec3(0.0, -1.0, 0.0));
    
    // Left
    Light->LightSpaceMatrix[SHADOWMAP_FACE_NEGATIVE_X] = PointLightProjection * glm::lookAt(LightPosition, 
                                                                                            LightPosition + glm::vec3(-1.0, 0.0, 0.0), 
                                                                                            glm::vec3(0.0, -1.0, 0.0));
    
    // Top
    Light->LightSpaceMatrix[SHADOWMAP_FACE_POSITIVE_Y] = PointLightProjection * glm::lookAt(LightPosition, 
                                                                                            LightPosition + glm::vec3(0.0, 1.0, 0.0), 
                                                                                            glm::vec3(0.0, 0.0, 1.0));
    
    // Bottom
    Light->LightSpaceMatrix[SHADOWMAP_FACE_NEGATIVE_Y] = PointLightProjection * glm::lookAt(LightPosition, 
                                                                                            LightPosition + glm::vec3(0.0, -1.0, 0.0), 
                                                                                            glm::vec3(0.0, 0.0, -1.0));
    
    // Near
    Light->LightSpaceMatrix[SHADOWMAP_FACE_POSITIVE_Z] = PointLightProjection * glm::lookAt(LightPosition, 
                                                                                            LightPosition + glm::vec3(0.0, 0.0, 1.0), 
                                                                                            glm::vec3(0.0, -1.0, 0.0));
    
    // Far
    Light->LightSpaceMatrix[SHADOWMAP_FACE_NEGATIVE_Z] = PointLightProjection * glm::lookAt(LightPosition, 
                                                                                            LightPosition + glm::vec3(0.0, 0.0, -1.0), 
                                                                                            glm::vec3(0.0, -1.0, 0.0));
}

internal shadowmap_quality
DetermineQualityLevel(u32 TileSize)
{
    shadowmap_quality Result = {};
    switch(TileSize)
    {
        case 1024:
        {
            Result = SHADOWMAP_QUALITY_ULTRA;
        } break;
        
        case 512:
        {
            Result = SHADOWMAP_QUALITY_HIGH;
        } break;
        
        case 256:
        {
            Result = SHADOWMAP_QUALITY_MEDIUM;
        } break;
        
        case 128:
        {
            Result = SHADOWMAP_QUALITY_LOW;
        } break;
    }
    
    return(Result);
}

internal void
DeletePointlightFromShadowmap(point_light *Light, shadowmap_atlas *Atlas)
{
    if(Light->ShadowmapQuality != SHADOWMAP_QUALITY_NONE)
    {
        for(u32 Index = 0;
            Index < 6;
            ++Index)
        {
            u32 TileIndex = Light->ShadowmapTileIndices[Index];
            if(TileIndex == (Atlas->NextFreeIndex - 1))
            {
                Atlas->NextFreeIndex--;
            }
            else
            {
                LC_ArrayAppend(Atlas->Freelist, TileIndex);
            }
        }
        Light->ShadowmapQuality = SHADOWMAP_QUALITY_NONE;
    }
}

internal void
UpdatePointlightsShadowmap(glm::vec3 CameraPosition, LC_Array(shadowmap_info) ShadowmapInfo, LC_Array(point_light) Lights)
{
    r32 NearPlane = 1.0f;
    r32 FarPlane = 25.0f;
    
    // TODO(Cristian): Check for light position updates
    r32 AspectRatio = 1.0f;
    
    for(u32 LightIndex = 0;
        LightIndex < LC_ArrayCount(Lights);
        ++LightIndex)
    {
        point_light *Light = &Lights[LightIndex];
        
        r32 Distance = glm::distance(CameraPosition, glm::vec3(Light->Position));
        u32 TileSize = GetDistanceBasedTileSize(Distance);
        shadowmap_quality UpdatedShadowmapQuality = DetermineQualityLevel(TileSize);
        
        CreatePointLightViewProjectionMatrices(AspectRatio, NearPlane, FarPlane, Light);
        
        if(Light->ShadowmapQuality != UpdatedShadowmapQuality)
        {
            DeletePointlightFromShadowmap(Light, &ShadowmapInfo[Light->ShadowmapQuality].Atlas);
            Light->ShadowmapQuality = UpdatedShadowmapQuality;
            
            shadowmap_atlas *Atlas = &ShadowmapInfo[UpdatedShadowmapQuality].Atlas;
            u32 TotalTiles = (Atlas->Size / Atlas->TileSize) * (Atlas->Size / Atlas->TileSize);
            
            for(u32 FaceIndex = SHADOWMAP_FACE_POSITIVE_X;
                FaceIndex < 6;
                ++FaceIndex)
            {
                if(LC_ArrayCount(Atlas->Freelist))
                {
                    Light->ShadowmapTileIndices[FaceIndex] = Atlas->Freelist[LC_ArrayCount(Atlas->Freelist) - 1];
                    LC_ArrayPop(Atlas->Freelist);
                }
                else
                {
                    if(Atlas->NextFreeIndex < TotalTiles)
                    {
                        Light->ShadowmapTileIndices[FaceIndex] = Atlas->NextFreeIndex++;
                    }
                }
            }
            
            Light->ShadowmapQuality = UpdatedShadowmapQuality;
        }
        Light->UpdateRequired = true;
    }
}

internal void
RenderScene(open_gl *OpenGL, shader *Shader, LC_Array(scene_node *) SceneNodes, LC_Array(asset) AssetStore)
{
    for(u32 NodesIndex = 0;
        NodesIndex < LC_ArrayCount(SceneNodes);
        ++NodesIndex)
    {
        scene_node *Node = SceneNodes[NodesIndex];
        if(Node->CastsShadows)
        {
            asset *Asset = &AssetStore[Node->AssetIndex];
            OpenGL->glUniformMatrix4fv(OpenGL->glGetUniformLocation(Shader->ID, "ModelMatrix"), 
                                       1, 
                                       GL_FALSE, 
                                       glm::value_ptr(Node->Transform.WorldModelMatrix));
            
            OpenGL->glBindVertexArray(Asset->VAO);
            OpenGL->glDrawArrays(GL_TRIANGLES, 0, Asset->TotalVertices);
        }
    }
    
    OpenGL->glBindVertexArray(0);
}

internal void
RenderScene(open_gl *OpenGL, shader *Shader, LC_Array(scene_node) SceneNodes, LC_Array(asset) AssetStore)
{
    for(u32 NodesIndex = 0;
        NodesIndex < LC_ArrayCount(SceneNodes);
        ++NodesIndex)
    {
        scene_node *Node = &SceneNodes[NodesIndex];
        if(Node->CastsShadows)
        {
            asset *Asset = &AssetStore[Node->AssetIndex];
            OpenGL->glUniformMatrix4fv(OpenGL->glGetUniformLocation(Shader->ID, "ModelMatrix"), 
                                       1, 
                                       GL_FALSE, 
                                       glm::value_ptr(Node->Transform.WorldModelMatrix));
            
            OpenGL->glBindVertexArray(Asset->VAO);
            OpenGL->glDrawArrays(GL_TRIANGLES, 0, Asset->TotalVertices);
        }
    }
    
    OpenGL->glBindVertexArray(0);
}

// NOTE(Cristian): Assumes the W vector component is 1
internal r32
PointToPlaneDistance(plane *Plane, glm::vec3 Vector)
{
    r32 Result = 
        Plane->A * Vector.x + 
        Plane->B * Vector.y + 
        Plane->C * Vector.z + 
        Plane->D;
    
    return(Result);
}

internal b32
SphereIntersectionTest(plane *FrustumPlanes, glm::vec3 PositionVector, r32 Radius)
{
    for(u32 Index = 0;
        Index < 6;
        ++Index)
    {
        if(PointToPlaneDistance(&FrustumPlanes[Index], PositionVector) <= -Radius)
        {
            return false;
        }
    }
    return true;
}

internal void
SphereFrustumCulling(plane *FrustumPlanes,
                     scene_node *Node,
                     LC_Array(asset) AssetStore,
                     LC_Array(scene_node *) VisibleNodes)
{
    if(!Node)
    {
        return;
    }
    
    asset *Asset = &AssetStore[Node->AssetIndex];
    
    r32 Radius = glm::distance(Asset->Extents.Min * Node->Transform.Scale, Asset->Extents.Max * Node->Transform.Scale) / 2.0f;
    if(SphereIntersectionTest(FrustumPlanes, Node->Transform.Position, Radius))
    {
        LC_ArrayAppend(VisibleNodes, Node);
    }
    
    
    if(Node->Children)
    {
        for(u32 Index = 0;
            Index < LC_ArrayCount(Node->Children);
            ++Index)
        {
            scene_node *ChildNode = &Node->Children[Index];
            SphereFrustumCulling(FrustumPlanes, ChildNode, AssetStore, VisibleNodes);
        }
    }
}

internal void
ShadowmapPass(open_gl * OpenGL, 
              gl_state_cache * Cache,
              scene_node * RootNode,
              LC_Array(asset) AssetStore,
              LC_Array(shadowmap_info) ShadowmapInfo,
              shader * ShadowmapShader,
              LC_Array(directional_light) DirectionalLights,
              LC_Array(point_light) PointLights,
              LC_Array(scene_node *) VisibleNodes, 
              LC_Array(scene_node *) LightPerspectiveVisibleNodes)
{
    OpenGL->glUseProgram(ShadowmapShader->ID);
    
    OpenGL->glBindFramebuffer(GL_FRAMEBUFFER, ShadowmapInfo[SHADOWMAP_QUALITY_LOW].FBO);
    OpenGL->glClear(GL_DEPTH_BUFFER_BIT);
    OpenGL->glBindFramebuffer(GL_FRAMEBUFFER, ShadowmapInfo[SHADOWMAP_QUALITY_MEDIUM].FBO);
    OpenGL->glClear(GL_DEPTH_BUFFER_BIT);
    OpenGL->glBindFramebuffer(GL_FRAMEBUFFER, ShadowmapInfo[SHADOWMAP_QUALITY_HIGH].FBO);
    OpenGL->glClear(GL_DEPTH_BUFFER_BIT);
    OpenGL->glBindFramebuffer(GL_FRAMEBUFFER, ShadowmapInfo[SHADOWMAP_QUALITY_ULTRA].FBO);
    OpenGL->glClear(GL_DEPTH_BUFFER_BIT);
    
    //OpenGL->glCullFace(GL_FRONT);
    
    for(u32 LightIndex = 0;
        LightIndex < LC_ArrayCount(DirectionalLights);
        ++LightIndex)
    {
        directional_light *Light = &DirectionalLights[LightIndex];
        
        shadowmap_quality Quality = Light->ShadowmapQuality;
        shadowmap_atlas *Atlas = &ShadowmapInfo[Quality].Atlas;
        
        glm::vec2 XYOffset = GetAtlasXYOffset(Light->ShadowmapTileIndex, Atlas->TileSize, Atlas->Size);
        OpenGL->glViewport(Cast(u32)XYOffset.x, Cast(u32)XYOffset.y, Atlas->TileSize, Atlas->TileSize);
        
        // TODO(Cristian): Sending the light space matrix as a uniform might not be required. The data is already stored
        OpenGL->glUniformMatrix4fv(OpenGL->glGetUniformLocation(ShadowmapShader->ID, "LightSpaceMatrix"),
                                   1,
                                   GL_FALSE,
                                   glm::value_ptr(Light->LightSpaceMatrix));
        
        OpenGL->glBindFramebuffer(GL_FRAMEBUFFER, ShadowmapInfo[Quality].FBO);
        RenderScene(OpenGL, ShadowmapShader, RootNode->Children, AssetStore);
    }
    
    for(u32 LightIndex = 0;
        LightIndex < LC_ArrayCount(PointLights);
        ++LightIndex)
    {
        point_light *Light = &PointLights[LightIndex];
        
        shadowmap_quality Quality = Light->ShadowmapQuality;
        shadowmap_atlas *Atlas = &ShadowmapInfo[Quality].Atlas;
        
        for(u32 LightFace = SHADOWMAP_FACE_POSITIVE_X;
            LightFace < 6;
            ++LightFace)
        {
            u32 TileIndex = Light->ShadowmapTileIndices[LightFace];
            
            glm::vec2 XYOffset = GetAtlasXYOffset(TileIndex, Atlas->TileSize, Atlas->Size);
            OpenGL->glViewport(Cast(u32)XYOffset.x, Cast(u32)XYOffset.y, Atlas->TileSize, Atlas->TileSize);
            
            // TODO(Cristian): Sending the light space matrix as a uniform might not be required. The data is already stored
            OpenGL->glUniformMatrix4fv(OpenGL->glGetUniformLocation(ShadowmapShader->ID, "LightSpaceMatrix"),
                                       1,
                                       GL_FALSE,
                                       glm::value_ptr(Light->LightSpaceMatrix[LightFace]));
            
            OpenGL->glBindFramebuffer(GL_FRAMEBUFFER, ShadowmapInfo[Quality].FBO);
            
            plane FrustumPlanes[6];
            FrustumPlanesExtraction(&Light->LightSpaceMatrix[LightFace], FrustumPlanes, true);
            SphereFrustumCulling(FrustumPlanes, RootNode, AssetStore, LightPerspectiveVisibleNodes);
            RenderScene(OpenGL, ShadowmapShader, LightPerspectiveVisibleNodes, AssetStore);
            LC_ArrayClear(LightPerspectiveVisibleNodes);
        }
    }
    
    //OpenGL->glCullFace(GL_BACK);
    
    OpenGL->glBindVertexArray(0);
    OpenGL->glBindFramebuffer(GL_FRAMEBUFFER, 0);
    OpenGL->glUseProgram(0);
}

internal point_light
CreateDefaultPointLight()
{
    point_light PointLight = {};
    PointLight.Ambient = glm::vec4(glm::vec3(1.0f), 0.05f);
    PointLight.Diffuse = glm::vec4(glm::vec3(1.0f), 0.4f);
    PointLight.Specular = glm::vec4(glm::vec3(1.0f), 0.5f);
    return(PointLight);
}

internal directional_light
CreateDefaultDirectionalLight()
{
    directional_light DirectionalLight = {};
    DirectionalLight.Ambient = glm::vec4(glm::vec3(1.0f), 0.05f);
    DirectionalLight.Diffuse = glm::vec4(glm::vec3(1.0f), 0.4f);
    DirectionalLight.Specular = glm::vec4(glm::vec3(1.0f), 0.5f);
    return(DirectionalLight);
}

internal glm::mat4
GetLocalWorldTransform(scene_node *Node)
{
    glm::mat4 ModelMatrix = glm::mat4(1.0f);
    ModelMatrix = glm::translate(ModelMatrix, Node->Transform.Position);
    if(Node->Transform.Rotation != glm::vec3(0.0f))
    {
        ModelMatrix = glm::rotate(ModelMatrix, glm::radians(Node->Transform.Rotation.x), glm::vec3(1.0f, 0.0f, 0.0f));
        ModelMatrix = glm::rotate(ModelMatrix, glm::radians(Node->Transform.Rotation.y), glm::vec3(0.0f, 1.0f, 0.0f));
        ModelMatrix = glm::rotate(ModelMatrix, glm::radians(Node->Transform.Rotation.z), glm::vec3(0.0f, 0.0f, 1.0f));
    }
    ModelMatrix = glm::scale(ModelMatrix, Node->Transform.Scale);
    
    return(ModelMatrix);
}

internal void
UpdateHierarchicalWorldTransform(scene_node *Node, scene_node *ParentNode = 0)
{
    if(!Node)
    {
        return;
    }
    
    if(Node->UpdateRequired)
    {
        if(ParentNode && !StringsEqual(ParentNode->Name, "Root"))
        {
            //Node->Transform.WorldModelMatrix = ParentNode->Transform.WorldModelMatrix * GetLocalWorldTransform(Node);
            glm::mat4 LocalNodeTransform = GetLocalWorldTransform(Node);
            SIMD256Matrix4x4Mul(&ParentNode->Transform.WorldModelMatrix, &LocalNodeTransform, &Node->Transform.WorldModelMatrix);
        }
        else
        {
            Node->Transform.WorldModelMatrix = GetLocalWorldTransform(Node);
        }
        
        Node->UpdateRequired = false;
        
        if(Node->Children)
        {
            for(u32 Index = 0;
                Index < LC_ArrayCount(Node->Children);
                ++Index)
            {
                scene_node *ChildNode = &Node->Children[Index];
                ChildNode->UpdateRequired = true;
                UpdateHierarchicalWorldTransform(ChildNode, Node);
            }
        }
    }
    else
    {
        if(Node->Children)
        {
            for(u32 Index = 0;
                Index < LC_ArrayCount(Node->Children);
                ++Index)
            {
                scene_node *ChildNode = &Node->Children[Index];
                UpdateHierarchicalWorldTransform(ChildNode, Node);
            }
        }
    }
}

internal b32
WithinBounds(r32 TestedPoint, r32 MinBound, r32 MaxBound)
{
    b32 Result = (MinBound <= TestedPoint && TestedPoint <= MaxBound) ? true : false;
    return(Result);
}

internal b32
TestAgainstPlane(r32 *Points)
{
    r32 One = 1.0f;
    r32 NegativeOne = -1.0f;
    
    __m256 XValues = _mm256_load_ps(&Points[0]);
    __m256 YValues = _mm256_load_ps(&Points[8]);
    __m256 ZValues = _mm256_load_ps(&Points[16]);
    __m256 WValues = _mm256_broadcast_ss(&One);
    
    __m256 NegativeOnes = _mm256_broadcast_ss(&NegativeOne);
    
    __m256 LeftPlaneTestMask = _mm256_cmp_ps(XValues, NegativeOnes, _CMP_LT_OQ);
    __m256 RightPlaneTestMask = _mm256_cmp_ps(XValues, WValues, _CMP_GT_OQ);
    __m256 TopPlaneTestMask = _mm256_cmp_ps(YValues, WValues, _CMP_GT_OQ);
    __m256 BottomPlaneTestMask = _mm256_cmp_ps(YValues, NegativeOnes, _CMP_LT_OQ);
    __m256 NearPlaneTestMask = _mm256_cmp_ps(ZValues, NegativeOnes, _CMP_LT_OQ);
    __m256 FarPlaneTestMask = _mm256_cmp_ps(ZValues, WValues, _CMP_GT_OQ);
    
    s32 LeftPlaneTestResult = _mm256_movemask_epi8(_mm256_castps_si256(LeftPlaneTestMask));
    s32 RightPlaneTestResult = _mm256_movemask_epi8(_mm256_castps_si256(RightPlaneTestMask));
    s32 TopPlaneTestResult = _mm256_movemask_epi8(_mm256_castps_si256(TopPlaneTestMask));
    s32 BottomPlaneTestResult = _mm256_movemask_epi8(_mm256_castps_si256(BottomPlaneTestMask));
    s32 NearPlaneTestResult = _mm256_movemask_epi8(_mm256_castps_si256(NearPlaneTestMask));
    s32 FarPlaneTestResult = _mm256_movemask_epi8(_mm256_castps_si256(FarPlaneTestMask));
    
    __m256i ResultData = _mm256_set_epi32(LeftPlaneTestResult,
                                          RightPlaneTestResult,
                                          TopPlaneTestResult,
                                          BottomPlaneTestResult,
                                          NearPlaneTestResult,
                                          FarPlaneTestResult,
                                          0, 
                                          0);
    b32 Result = (b32)_mm256_movemask_epi8(ResultData);
    return(Result);
}

internal b32
AABBFrustumIntersectionTest(glm::mat4 MVPMatrix, extents_info Extents)
{
    b32 IntersectionTest = true;
    
    glm::vec4 AABBPoints[8] = 
    {
        { Extents.Min.x, Extents.Min.y, Extents.Max.z, 1.0f },
        { Extents.Max.x, Extents.Min.y, Extents.Max.z, 1.0f },
        { Extents.Min.x, Extents.Max.y, Extents.Max.z, 1.0f },
        { Extents.Max.x, Extents.Max.y, Extents.Max.z, 1.0f },
        
        { Extents.Min.x, Extents.Min.y, Extents.Min.z, 1.0f },
        { Extents.Max.x, Extents.Min.y, Extents.Min.z, 1.0f },
        { Extents.Min.x, Extents.Max.y, Extents.Min.z, 1.0f },
        { Extents.Max.x, Extents.Max.y, Extents.Min.z, 1.0f }
    };
    
    /*r32 SIMD_AABBPoints[32] = 
    {
        Extents.Min.x, Extents.Max.x, Extents.Min.x, Extents.Max.x, Extents.Min.x, Extents.Max.x, Extents.Min.x, Extents.Max.x,
        Extents.Min.y, Extents.Min.y, Extents.Max.y, Extents.Max.y, Extents.Min.y, Extents.Min.y, Extents.Max.y, Extents.Max.y,
        Extents.Max.z, Extents.Max.z, Extents.Max.z, Extents.Max.z, Extents.Max.z, Extents.Max.z, Extents.Max.z, Extents.Max.z,
        1.0f,        1.0f,        1.0f,        1.0f,        1.0f,        1.0f,        1.0f,        1.0f
    };*/
    
    
    {
        TIMED_BLOCK();
        
        
        SIMD256Matrix4x4VectorMul(&MVPMatrix, &AABBPoints[0], &AABBPoints[0]);
        SIMD256Matrix4x4VectorMul(&MVPMatrix, &AABBPoints[1], &AABBPoints[1]);
        SIMD256Matrix4x4VectorMul(&MVPMatrix, &AABBPoints[2], &AABBPoints[2]);
        SIMD256Matrix4x4VectorMul(&MVPMatrix, &AABBPoints[3], &AABBPoints[3]);
        SIMD256Matrix4x4VectorMul(&MVPMatrix, &AABBPoints[4], &AABBPoints[4]);
        SIMD256Matrix4x4VectorMul(&MVPMatrix, &AABBPoints[5], &AABBPoints[5]);
        SIMD256Matrix4x4VectorMul(&MVPMatrix, &AABBPoints[6], &AABBPoints[6]);
        SIMD256Matrix4x4VectorMul(&MVPMatrix, &AABBPoints[7], &AABBPoints[7]);
    }
    
    r32 SIMD_AABBPoints[32] =
    {
        AABBPoints[0].x, AABBPoints[1].x, AABBPoints[2].x, AABBPoints[3].x, AABBPoints[4].x, AABBPoints[5].x, AABBPoints[6].x, AABBPoints[7].x, 
        AABBPoints[0].y, AABBPoints[1].y, AABBPoints[2].y, AABBPoints[3].y, AABBPoints[4].y, AABBPoints[5].y, AABBPoints[6].y, AABBPoints[7].y,
        AABBPoints[0].z, AABBPoints[1].z, AABBPoints[2].z, AABBPoints[3].z, AABBPoints[4].z, AABBPoints[5].z, AABBPoints[6].z, AABBPoints[7].z, 
        1.0f,        1.0f,        1.0f,        1.0f,        1.0f,        1.0f,        1.0f,        1.0f
    };
    IntersectionTest = TestAgainstPlane(SIMD_AABBPoints);
    
    /*b32 LeftPlaneResult = false;
    // TEST AGAINST LEFT PLANE
    LeftPlaneResult = (AABBPoints[0].x < -AABBPoints[0].w) &&
    (AABBPoints[1].x < -AABBPoints[1].w) &&
    (AABBPoints[2].x < -AABBPoints[2].w) &&
    (AABBPoints[3].x < -AABBPoints[3].w) &&
    (AABBPoints[4].x < -AABBPoints[4].w) &&
    (AABBPoints[5].x < -AABBPoints[5].w) &&
    (AABBPoints[6].x < -AABBPoints[6].w) &&
    (AABBPoints[7].x < -AABBPoints[7].w);
    
    b32 RightPlaneResult = false;
    // TEST AGAINST RIGHT PLANE
    RightPlaneResult = (AABBPoints[0].x > AABBPoints[0].w) &&
    (AABBPoints[1].x > AABBPoints[1].w) &&
    (AABBPoints[2].x > AABBPoints[2].w) &&
    (AABBPoints[3].x > AABBPoints[3].w) &&
    (AABBPoints[4].x > AABBPoints[4].w) &&
    (AABBPoints[5].x > AABBPoints[5].w) &&
    (AABBPoints[6].x > AABBPoints[6].w) &&
    (AABBPoints[7].x > AABBPoints[7].w);
    
    b32 TopPlaneResult = false;
    // TEST AGAINST TOP PLANE
    TopPlaneResult = (AABBPoints[0].y > AABBPoints[0].w) &&
    (AABBPoints[1].y > AABBPoints[1].w) &&
    (AABBPoints[2].y > AABBPoints[2].w) &&
    (AABBPoints[3].y > AABBPoints[3].w) &&
    (AABBPoints[4].y > AABBPoints[4].w) &&
    (AABBPoints[5].y > AABBPoints[5].w) &&
    (AABBPoints[6].y > AABBPoints[6].w) &&
    (AABBPoints[7].y > AABBPoints[7].w);
    
    b32 BottomPlaneResult = false;
    // TEST AGAINST BOTTOM PLANE
    BottomPlaneResult = (AABBPoints[0].y < -AABBPoints[0].w) &&
    (AABBPoints[1].y < -AABBPoints[1].w) &&
    (AABBPoints[2].y < -AABBPoints[2].w) &&
    (AABBPoints[3].y < -AABBPoints[3].w) &&
    (AABBPoints[4].y < -AABBPoints[4].w) &&
    (AABBPoints[5].y < -AABBPoints[5].w) &&
    (AABBPoints[6].y < -AABBPoints[6].w) &&
    (AABBPoints[7].y < -AABBPoints[7].w);
    
    b32 NearPlaneResult = false;
    // TEST AGAINST NEAR PLANE
    NearPlaneResult = (AABBPoints[0].z < -AABBPoints[0].w) &&
    (AABBPoints[1].z < -AABBPoints[1].w) &&
    (AABBPoints[2].z < -AABBPoints[2].w) &&
    (AABBPoints[3].z < -AABBPoints[3].w) &&
    (AABBPoints[4].z < -AABBPoints[4].w) &&
    (AABBPoints[5].z < -AABBPoints[5].w) &&
    (AABBPoints[6].z < -AABBPoints[6].w) &&
    (AABBPoints[7].z < -AABBPoints[7].w);
    
    b32 FarPlaneResult = false;
    // TEST AGAINST FAR PLANE
    FarPlaneResult = (AABBPoints[0].z > AABBPoints[0].w) &&
    (AABBPoints[1].z > AABBPoints[1].w) &&
    (AABBPoints[2].z > AABBPoints[2].w) &&
    (AABBPoints[3].z > AABBPoints[3].w) &&
    (AABBPoints[4].z > AABBPoints[4].w) &&
    (AABBPoints[5].z > AABBPoints[5].w) &&
    (AABBPoints[6].z > AABBPoints[6].w) &&
    (AABBPoints[7].z > AABBPoints[7].w);
    
    if(LeftPlaneResult || 
       RightPlaneResult || 
       TopPlaneResult || 
       BottomPlaneResult || 
       NearPlaneResult || 
       FarPlaneResult)
    {
        IntersectionTest = false;
    }*/
    
    return(IntersectionTest);
}

internal void
AABBFrustumCulling(LC_Array(scene_node *) SceneNodes,
                   glm::mat4 *ProjectionViewMatrix,
                   LC_Array(asset) AssetStore,
                   LC_Array(scene_node *) VisibleNodes)
{
    
    for(u32 Index = 0;
        Index < LC_ArrayCount(SceneNodes);
        ++Index)
    {
        scene_node *Node = SceneNodes[Index];
        
        asset *Asset = &AssetStore[Node->AssetIndex];
        
        glm::mat4 MVPMatrix = glm::mat4(1.0);
        SIMD256Matrix4x4Mul(ProjectionViewMatrix, &Node->Transform.WorldModelMatrix, &MVPMatrix);
        
        b32 IntersectionResult = AABBFrustumIntersectionTest(MVPMatrix, Asset->Extents);
        if(IntersectionResult)
        {
            LC_ArrayAppend(VisibleNodes, SceneNodes[Index]);
        }
        
    }
}

#define MAX_UNIFORM_IDENTIFIER_SIZE 128

internal void
DEBUGForwardPass(open_gl *OpenGL,
                 camera *Camera,
                 scene_node *Node,
                 LC_Array(asset) AssetStore,
                 LC_Array(point_light) PointLights_Information,
                 shader *SceneLightingShader,
                 shader *DebugLightShader,
                 shader *AABBDebugShader,
                 LC_Array(shadowmap_info) ShadowmapInfo)
{
    if(Node)
    {
        asset *Asset = &AssetStore[Node->AssetIndex];
        
        switch(Node->AssetType)
        {
            case ASSET_POINT_LIGHT:
            {
                OpenGL->glUseProgram(DebugLightShader->ID);
                
                OpenGL->glUniformMatrix4fv(OpenGL->glGetUniformLocation(DebugLightShader->ID, "ModelMatrix"), 1, GL_FALSE, glm::value_ptr(Node->Transform.WorldModelMatrix));
                
                glm::vec3 LightColor = glm::vec3(PointLights_Information[Node->LightInformationIndex].Diffuse);
                OpenGL->glUniform3f(OpenGL->glGetUniformLocation(DebugLightShader->ID, "LightColor"), LightColor.r, LightColor.g, LightColor.b);
                
                OpenGL->glBindVertexArray(Asset->VAO);
                OpenGL->glDrawArrays(GL_TRIANGLES, 0, Asset->TotalVertices);
                OpenGL->glBindVertexArray(0);
                
                /*OpenGL->glUseProgram(AABBDebugShader->ID);
                OpenGL->glUniformMatrix4fv(OpenGL->glGetUniformLocation(AABBDebugShader->ID, "ModelMatrix"), 1, GL_FALSE, glm::value_ptr(Node->Transform.WorldModelMatrix));*/
                
                // OpenGL->glBindVertexArray(Asset->AABBBound.DebugDataVAO);
                // OpenGL->glDrawArrays(GL_LINES, 0, 24);
                // OpenGL->glBindVertexArray(0);
                
                // OpenGL->glBindVertexArray(Asset->SphereBound.DebugDataVAO);
                // OpenGL->glDrawElements(GL_LINES, Asset->SphereBound.TotalIndices, GL_UNSIGNED_INT, 0);
                // OpenGL->glBindVertexArray(0);
            } break;
            
            case ASSET_GEOMETRY:
            {
                OpenGL->glUseProgram(SceneLightingShader->ID);
                
                OpenGL->glUniform3f(OpenGL->glGetUniformLocation(SceneLightingShader->ID, "CameraPos"), SceneCamera.Position.x, SceneCamera.Position.y, SceneCamera.Position.z);
                
                OpenGL->glUniformMatrix4fv(OpenGL->glGetUniformLocation(SceneLightingShader->ID, "ModelMatrix"), 1, GL_FALSE, glm::value_ptr(Node->Transform.WorldModelMatrix));
                
                // TODO(Cristian): Should this be renamed?
                glm::mat3 CorrectedNormalMatrix = glm::transpose(glm::inverse(Node->Transform.WorldModelMatrix));
                OpenGL->glUniformMatrix3fv(OpenGL->glGetUniformLocation(SceneLightingShader->ID, "CorrectedNormalMatrix"), 1, GL_FALSE, glm::value_ptr(CorrectedNormalMatrix));
                
                OpenGL->glUniform1i(OpenGL->glGetUniformLocation(SceneLightingShader->ID, "texture_diffuse"), Asset->Texture.Unit);
                
                {
                    mmsz Size = 128;
                    for(u32 Index = 1;
                        Index < SHADOWMAP_QUALITY_COUNT;
                        ++Index)
                    {
                        char FormattedIdentifier[MAX_UNIFORM_IDENTIFIER_SIZE];
                        snprintf(FormattedIdentifier, Size, "ShadowmapAtlas[%d]", Index);
                        
                        u32 TextureUnit = Index;
                        OpenGL->glUniform1i(OpenGL->glGetUniformLocation(SceneLightingShader->ID, FormattedIdentifier), TextureUnit);
                        
                        GLuint DepthMap = ShadowmapInfo[Index].DepthMap;
                        OpenGL->glActiveTexture(GL_TEXTURE0 + TextureUnit);
                        OpenGL->glBindTexture(GL_TEXTURE_2D, DepthMap);
                    }
                }
                
                RenderTexture_Bind(OpenGL, &Asset->Texture, true);
                OpenGL->glBindVertexArray(Asset->VAO);
                OpenGL->glDrawArrays(GL_TRIANGLES, 0, Asset->TotalVertices);
                OpenGL->glBindVertexArray(0);
                RenderTexture_Unbind(OpenGL, Asset->Texture.Target);
                
                /*OpenGL->glUseProgram(AABBDebugShader->ID);
                OpenGL->glUniformMatrix4fv(OpenGL->glGetUniformLocation(AABBDebugShader->ID, "ModelMatrix"), 1, GL_FALSE, glm::value_ptr(Node->Transform.WorldModelMatrix));*/
                
                // OpenGL->glBindVertexArray(Asset->AABBBound.DebugDataVAO);
                // OpenGL->glDrawArrays(GL_LINES, 0, 24);
                // OpenGL->glBindVertexArray(0);
                
                // OpenGL->glBindVertexArray(Asset->SphereBound.DebugDataVAO);
                // OpenGL->glDrawElements(GL_LINES, Asset->SphereBound.TotalIndices, GL_UNSIGNED_INT, 0);
                // OpenGL->glBindVertexArray(0);
            } break;
        }
        
    }
}


internal void
RenderLoop(HWND WindowHandle, u32 RenderTargetWidth, u32 RenderTargetHeight)
{
    SceneCamera = GetStandardPerspectiveCamera(RenderTargetWidth, RenderTargetHeight);
    
    r64 PerformanceFrequency = Win32GetPerformanceFrequency();
    
    CenterWindowOnScreen(WindowHandle, WS_OVERLAPPEDWINDOW, 0);
    
    u32 WindowWidth = 0;
    u32 WindowHeight = 0;
    RECT WindowSurfaceDimensions;
    if(GetClientRect(WindowHandle, &WindowSurfaceDimensions))
    {
        WindowWidth = WindowSurfaceDimensions.right - WindowSurfaceDimensions.left;
        WindowHeight = WindowSurfaceDimensions.bottom - WindowSurfaceDimensions.top;
    }
    
    CenterCursorInWindow(WindowHandle, WindowWidth, WindowHeight);
    
    memory_arena RendererArena = {};
    
    // PLATFORM SPECIFIC API INITIALIZATION
    open_gl *OpenGL = PushStruct(&RendererArena, open_gl);
    b32 OpenGLInitialized = OpenGLLoadFunctionPointers(OpenGL);
    Assert(OpenGLInitialized);
    
    //OpenGL->glViewport(0, 0, RenderTargetWidth, RenderTargetHeight);
    //OpenGL->glClearColor(0.0f, 0.0f, 0.05f, 1.0f);
    OpenGL->glClearColor(0.55f, 0.55f, 0.55f, 1.0f);
    
    gl_state_cache OpenGLCache = {};
    SetGLState(OpenGL, &OpenGLCache, DEPTH_TEST, true);
    SetGLState(OpenGL, &OpenGLCache, FACE_CULLING, true);
    OpenGL->glCullFace(GL_BACK);
    
    OpenGL->wglSwapIntervalEXT(0);
    
    LC_Array(asset) AssetStore;
    LC_ArrayInit(AssetStore, &RendererArena, 2048);
    
    asset CubeAsset = GenerateCubeAsset(OpenGL, &RendererArena);
    asset DebugLightAsset = GenerateLightAsset(OpenGL, &RendererArena);
    asset TerrainAsset = GenerateTerrainAsset(OpenGL, &RendererArena);
    
    shader SceneLightingShader = Shader_Load("../code/shaders/lighting.glsl");
    Shader_Compile(&SceneLightingShader, OpenGL);
    
    shader DebugLightShader = Shader_Load("../code/shaders/LightSource_Shader.glsl");
    Shader_Compile(&DebugLightShader, OpenGL);
    
    shader AABBDebugShader = Shader_Load("../code/shaders/aabb.glsl");
    Shader_Compile(&AABBDebugShader, OpenGL);
    
    shader ShadowmapShader = Shader_Load("../code/shaders/Shadowmap_Shader.glsl");
    Shader_Compile(&ShadowmapShader, OpenGL);
    
    shader SurfaceShader2D = Shader_Load("../code/shaders/2DSurface_Shader.glsl");
    Shader_Compile(&SurfaceShader2D, OpenGL);
    
    
    // LIGHTS UBO CREATION
    // GLuint PointLightsDataUBO;
    // OpenGL->glGenBuffers(1, &PointLightsDataUBO);
    
    // u32 MaxPointLights = 128;
    // u32 TotalUBOSize = sizeof(u32) * 4 + sizeof(point_light) * MaxPointLights;
    
    // OpenGL->glBindBuffer(GL_UNIFORM_BUFFER, PointLightsDataUBO);
    // OpenGL->glBufferData(GL_UNIFORM_BUFFER, TotalUBOSize, NULL, GL_DYNAMIC_DRAW);
    // OpenGL->glBindBuffer(GL_UNIFORM_BUFFER, 0);
    
    // OpenGL->glBindBufferBase(GL_UNIFORM_BUFFER, 0, PointLightsDataUBO);
    
    
    
    GLuint PointLightsDataSSBO;
    OpenGL->glGenBuffers(1, &PointLightsDataSSBO);
    
    u32 MaxPointLights = 128;
    u32 TotalSSBOSize = sizeof(u32) * 4 + sizeof(point_light) * MaxPointLights;
    
    OpenGL->glBindBuffer(GL_SHADER_STORAGE_BUFFER, PointLightsDataSSBO);
    OpenGL->glBufferData(GL_SHADER_STORAGE_BUFFER, TotalSSBOSize, 0, GL_DYNAMIC_DRAW);
    OpenGL->glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
    
    OpenGL->glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, PointLightsDataSSBO);
    
    
    
    GLuint DirectionalLightsDataUBO;
    OpenGL->glGenBuffers(1, &DirectionalLightsDataUBO);
    
    u32 MaxDirectionalLights = 4;
    u32 TotalUBOSize = sizeof(u32) * 4 + sizeof(directional_light) * MaxDirectionalLights;
    
    OpenGL->glBindBuffer(GL_UNIFORM_BUFFER, DirectionalLightsDataUBO);
    OpenGL->glBufferData(GL_UNIFORM_BUFFER, TotalUBOSize, NULL, GL_DYNAMIC_DRAW);
    OpenGL->glBindBuffer(GL_UNIFORM_BUFFER, 0);
    
    OpenGL->glBindBufferBase(GL_UNIFORM_BUFFER, 2, DirectionalLightsDataUBO);
    
    
    LC_Array(point_light) PointLights_Information;
    LC_ArrayInit(PointLights_Information, &RendererArena, MaxPointLights);
    
    LC_Array(directional_light) DirectionalLights_Information;
    LC_ArrayInit(DirectionalLights_Information, &RendererArena, MaxDirectionalLights);
    
    
    
    // DEVELOPMENT: Debug / development purposes only!
    // This should be normally built out of a cold storage file that describes the scene.
    scene_node SceneRootNode = {};
    {
        // SCENE INITIALIZATION
        SceneRootNode.Name = BundleZ("Root");
        LC_ArrayInit(SceneRootNode.Children, &RendererArena, 1024);
        
        
        scene_node PointLightNode1 = {};
        PointLightNode1.Name = BundleZ("Point Light 1");
        PointLightNode1.Transform.Position = glm::vec3(0.1f, 0.2f, 0.0f);
        PointLightNode1.Transform.Scale = glm::vec3(0.1f);
        PointLightNode1.AssetType = ASSET_POINT_LIGHT;
        PointLightNode1.AssetIndex = Cast(u32)LC_ArrayCount(AssetStore);
        PointLightNode1.LightInformationIndex = Cast(u32)LC_ArrayCount(PointLights_Information);
        PointLightNode1.UpdateRequired = true;
        PointLightNode1.CastsShadows = false;
        LC_ArrayAppend(SceneRootNode.Children, PointLightNode1);
        
        // @WARNING: Light data might not have default properties when read off disk!
        point_light PointLight = CreateDefaultPointLight();
        PointLight.Position = glm::vec4(PointLightNode1.Transform.Position, 1.0f);
        LC_ArrayAppend(PointLights_Information, PointLight);
        
        
        
        
        scene_node PointLightNode2 = {};
        PointLightNode2.Name = BundleZ("Point Light 2");
        PointLightNode2.Transform.Position = glm::vec3(-0.1f, 1.6f, 0.0f);
        PointLightNode2.Transform.Scale = glm::vec3(0.1f);
        PointLightNode2.AssetType = ASSET_POINT_LIGHT;
        PointLightNode2.AssetIndex = Cast(u32)LC_ArrayCount(AssetStore);
        PointLightNode2.LightInformationIndex = Cast(u32)LC_ArrayCount(PointLights_Information);
        PointLightNode2.UpdateRequired = true;
        LC_ArrayAppend(SceneRootNode.Children, PointLightNode2);
        
        glm::vec3 Color = glm::vec3(0.4f, 0.0f, 1.0f);
        point_light PointLight2 = CreateDefaultPointLight(); 
        PointLight2.Ambient = glm::vec4(Color, 0.1f);
        PointLight2.Diffuse = glm::vec4(Color, 0.5f);
        PointLight2.Specular = glm::vec4(Color, 0.5f);
        PointLight2.Position = glm::vec4(PointLightNode2.Transform.Position, 1.0f);
        LC_ArrayAppend(PointLights_Information, PointLight2);
        
        
        
        scene_node DirectionalLightNode = {};
        DirectionalLightNode.Name = BundleZ("Directional Light");
        DirectionalLightNode.AssetType = ASSET_DIRECTIONAL_LIGHT;
        DirectionalLightNode.LightInformationIndex = Cast(u32)LC_ArrayCount(DirectionalLights_Information);
        LC_ArrayAppend(SceneRootNode.Children, DirectionalLightNode);
        
        directional_light DirectionalLight = CreateDefaultDirectionalLight();
        DirectionalLight.Direction = glm::vec3(-0.2f, -1.0f, -0.3f);
        LC_ArrayAppend(DirectionalLights_Information, DirectionalLight);
        
        LC_ArrayAppend(AssetStore, DebugLightAsset);
        
        
        
        glm::vec2 XBounds = glm::vec2(-50.0f, 50.0f);
        glm::vec2 YBounds = glm::vec2(-50.0f, 50.0f);
        r32 Step = 5.0f;
        
        for(r32 X = XBounds.x;
            X <= XBounds.y;
            X += Step)
        {
            for(r32 Z = YBounds.x;
                Z <= YBounds.y;
                Z += Step)
            {
                scene_node CubeNode = {};
                CubeNode.Name = BundleZ("Cube");
                CubeNode.Transform.Position = glm::vec3(X, 0.0f, Z);
                CubeNode.Transform.Scale = glm::vec3(1.0f);
                CubeNode.AssetType = ASSET_GEOMETRY;
                CubeNode.AssetIndex = Cast(u32)LC_ArrayCount(AssetStore);
                CubeNode.UpdateRequired = true;
                CubeNode.CastsShadows = true;
                
                LC_ArrayAppend(SceneRootNode.Children, CubeNode);
            }
        }
        
        LC_ArrayAppend(AssetStore, CubeAsset);
        
        
        scene_node TerrainNode = {};
        TerrainNode.Name = BundleZ("Terrain");
        TerrainNode.Transform.Position = glm::vec3(0.0f, -1.01f, 0.0f);
        TerrainNode.Transform.Scale = glm::vec3(50.0f);
        TerrainNode.AssetType = ASSET_GEOMETRY;
        TerrainNode.AssetIndex = Cast(u32)LC_ArrayCount(AssetStore);
        TerrainNode.UpdateRequired = true;
        LC_ArrayAppend(SceneRootNode.Children, TerrainNode);
        
        LC_ArrayAppend(AssetStore, TerrainAsset);
    }
    
    // void *MappedBufferPointer = OpenGL->glMapNamedBuffer(PointLightsDataSSBO, GL_WRITE_ONLY);
    // memcpy(MappedBufferPointer, &Cast(u32)LC_ArrayCount(PointLights_Information), sizeof(u32));
    // memcpy((char *)MappedBufferPointer + sizeof(u32) * 4, PointLights_Information, sizeof(point_light) * LC_ArrayCount(PointLights_Information));
    // OpenGL->glUnmapNamedBuffer(PointLightsDataSSBO);
    
    SceneCamera.Position = glm::vec3(0.0f, 0.0f, 3.0f);
    
    LC_Array(shadowmap_info) ShadowmapInfo;
    LC_ArrayInit(ShadowmapInfo, &RendererArena, SHADOWMAP_QUALITY_COUNT);
    
    u32 AtlasSize = 8192;
    InitShadowmapAtlas(OpenGL, &RendererArena, ShadowmapInfo, SHADOWMAP_QUALITY_ULTRA, AtlasSize, 1024);
    InitShadowmapAtlas(OpenGL, &RendererArena, ShadowmapInfo, SHADOWMAP_QUALITY_HIGH, AtlasSize, 512);
    InitShadowmapAtlas(OpenGL, &RendererArena, ShadowmapInfo, SHADOWMAP_QUALITY_MEDIUM, AtlasSize, 256);
    InitShadowmapAtlas(OpenGL, &RendererArena, ShadowmapInfo, SHADOWMAP_QUALITY_LOW, AtlasSize, 128);
    
    InitDirectionalLightsShadowmap(DirectionalLights_Information, &ShadowmapInfo[SHADOWMAP_QUALITY_ULTRA].Atlas);
    UpdatePointlightsShadowmap(SceneCamera.Position, ShadowmapInfo, PointLights_Information);
    
    
    
    GLuint AtlasInfoUBO;
    OpenGL->glGenBuffers(1, &AtlasInfoUBO);
    
    u32 MaxAtlasInfo = 5;
    TotalUBOSize = MaxAtlasInfo * sizeof(ubo_atlas_info);
    
    OpenGL->glBindBuffer(GL_UNIFORM_BUFFER, AtlasInfoUBO);
    OpenGL->glBufferData(GL_UNIFORM_BUFFER, TotalUBOSize, NULL, GL_STATIC_READ);
    OpenGL->glBindBuffer(GL_UNIFORM_BUFFER, 0);
    
    OpenGL->glBindBufferBase(GL_UNIFORM_BUFFER, 3, AtlasInfoUBO);
    
    LC_Array(ubo_atlas_info) UBOAtlasInfo;
    LC_ArrayInit(UBOAtlasInfo, &RendererArena, MaxAtlasInfo);
    
    for(u32 Index = 0;
        Index < SHADOWMAP_QUALITY_COUNT;
        ++Index)
    {
        UBOAtlasInfo[Index].Size = ShadowmapInfo[Index].Atlas.Size;
        UBOAtlasInfo[Index].TileSize = ShadowmapInfo[Index].Atlas.TileSize;
    }
    
    OpenGL->glNamedBufferData(AtlasInfoUBO, TotalUBOSize, UBOAtlasInfo, GL_STATIC_READ);
    
    
    
    u32 ArrayCount = Cast(u32)LC_ArrayCount(PointLights_Information);
    OpenGL->glNamedBufferSubData(PointLightsDataSSBO, 0, sizeof(u32), &ArrayCount);
    OpenGL->glNamedBufferSubData(PointLightsDataSSBO, 16, sizeof(point_light) * ArrayCount, PointLights_Information);
    
    ArrayCount = Cast(u32)LC_ArrayCount(DirectionalLights_Information);
    OpenGL->glNamedBufferSubData(DirectionalLightsDataUBO, 0, sizeof(u32), &ArrayCount);
    OpenGL->glNamedBufferSubData(DirectionalLightsDataUBO, 16, sizeof(directional_light) * ArrayCount, DirectionalLights_Information);
    
    
    // TODO(Cristian): Matrices abstraction
    transformation_matrices TransformMatrices = {};
    TransformMatrices.PerspectiveMatrix = SceneCamera.PerspectiveMatrix;
    
    transformation_matrices_storage TransformsStorage = GenerateTransformationMatricesStorage(OpenGL, &TransformMatrices);
    
    LC_Array(scene_node *) VisibleSceneNodes;
    LC_ArrayInit(VisibleSceneNodes, &RendererArena, 512);
    
    LC_Array(scene_node *) LightPerspectiveVisibleSceneNodes;
    LC_ArrayInit(LightPerspectiveVisibleSceneNodes, &RendererArena, 512);
    
    LC_Array(scene_node *) AABBVisibleSceneNodes;
    LC_ArrayInit(AABBVisibleSceneNodes, &RendererArena, 512);
    
    glm::mat4 ViewMatrix;
    glm::mat4 PerspectiveViewMatrix;
    
    b32 GPUTimerQueried = false;
    b32 WaitingForResult = false;
    
    GLuint TimerQuery;
    OpenGL->glGenQueries(1, &TimerQuery);
    
    asset Surface2D = Generate2DSurface(OpenGL);
    
    // texture_file TestTexture = TextureLoader_ReadTextureFile("assets/Floor/floor_diffuse.jpg", true);
    // Surface2D.Texture = RenderTexture_LoadTexture(OpenGL, &TestTexture, GL_TEXTURE_2D, true);
    
    memory_size Size = 50 * sizeof(char);
    char *FrametimeOutput = (char *)malloc(Size);
    
    r64 TotalElapsedTime = 0;
    while(ApplicationRunning)
    {
        if(!GPUTimerQueried)
        {
            OpenGL->glBeginQuery(GL_TIME_ELAPSED, TimerQuery);
            GPUTimerQueried = true;
        }
        
        // UPDATE DELTA TIME (SIMULATION TIME ADVANCE)
        CurrentTime = Win32GetCurrentTime();
        Delta = (CurrentTime - LastTime) / PerformanceFrequency;
        LastTime = CurrentTime;
        
        r64 DeltaInMS = Delta * 1000.0f;
        TotalElapsedTime += DeltaInMS;
        if(TotalElapsedTime > 1000.0f)
        {
            u32 FPS = (u32)(1000.0 / DeltaInMS);
            
            snprintf(FrametimeOutput, Size, "Frame time: %0.02f ms | FPS: %d \n", DeltaInMS, FPS);
            OutputDebugStringA(FrametimeOutput);
            
            TotalElapsedTime = 0;
        }
        
        
        // PROCESS INPUT
        ProcessPendingMessages(WindowHandle);
        //ProcessCursorPosition(WindowHandle, true);
        
        // UPDATE STATE
        scene_node *Light1 = &SceneRootNode.Children[0];
        scene_node *Light2 = &SceneRootNode.Children[1];
        
        f32 Radius = 3.0f;
        f32 RotationSpeed = 50.0f;
        r64 Angle = glm::radians(CurrentTime / PerformanceFrequency) * RotationSpeed;
        
        Light1->Transform.Position = glm::vec3(glm::vec3(0.0f, 0.0f, 0.0f)) + glm::vec3(Radius * glm::cos(Angle), Light1->Transform.Position.y, Radius * glm::sin(Angle));
        PointLights_Information[Light1->LightInformationIndex].Position = glm::vec4(Light1->Transform.Position, 1.0f);
        Light1->UpdateRequired = true;
        
        Light2->Transform.Position = glm::vec3(glm::vec3(0.0f, 0.0f, 0.0f)) + glm::vec3(Radius * glm::sin(Angle), Light2->Transform.Position.y, Radius * glm::cos(Angle));
        PointLights_Information[Light2->LightInformationIndex].Position = glm::vec4(Light2->Transform.Position, 1.0f);
        Light2->UpdateRequired = true;
        
        
        
        
        // BEGIN FRAME
        OpenGL->glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        
        if(SceneCamera.UpdateRequired)
        {
            ViewMatrix = GetViewMatrix(&SceneCamera);
            OpenGL->glNamedBufferSubData(TransformsStorage.UBO, OFFSET_OF(transformation_matrices, ViewMatrix), sizeof(glm::mat4), &ViewMatrix);
            
            PerspectiveViewMatrix = SceneCamera.PerspectiveMatrix * SceneCamera.ViewMatrix;
            OpenGL->glNamedBufferSubData(TransformsStorage.UBO, OFFSET_OF(transformation_matrices, PerspectiveViewMatrix), sizeof(glm::mat4), &PerspectiveViewMatrix);
            
            FrustumPlanesExtraction(&PerspectiveViewMatrix, SceneCamera.FrustumPlanes, true);
            
            SceneCamera.UpdateRequired = false;
        }
        
        UpdateHierarchicalWorldTransform(&SceneRootNode);
        
        SphereFrustumCulling(SceneCamera.FrustumPlanes,
                             &SceneRootNode,
                             AssetStore,
                             VisibleSceneNodes);
        
        AABBFrustumCulling(VisibleSceneNodes,
                           &PerspectiveViewMatrix,
                           AssetStore,
                           AABBVisibleSceneNodes);
        
        UpdatePointlightsShadowmap(SceneCamera.Position, ShadowmapInfo, PointLights_Information);
        
        for(u32 LightIndex = 0;
            LightIndex < LC_ArrayCount(PointLights_Information);
            ++LightIndex)
        {
            point_light *Light = &PointLights_Information[LightIndex];
            if(Light->UpdateRequired)
            {
                memory_size LightOffset = LightIndex * sizeof(point_light) + 16;
                OpenGL->glNamedBufferSubData(PointLightsDataSSBO, LightOffset, sizeof(point_light), &PointLights_Information[LightIndex]);
            }
        }
        
        RECT ClientArea;
        GetClientRect(WindowHandle, &ClientArea);
        
        ShadowmapPass(OpenGL, 
                      &OpenGLCache,
                      &SceneRootNode,
                      AssetStore,
                      ShadowmapInfo,
                      &ShadowmapShader,
                      DirectionalLights_Information,
                      PointLights_Information,
                      AABBVisibleSceneNodes,
                      LightPerspectiveVisibleSceneNodes);
        
        OpenGL->glViewport(0, 0, ClientArea.right - ClientArea.left, ClientArea.bottom - ClientArea.top);
        
        for(u32 NodesIndex = 0;
            NodesIndex < LC_ArrayCount(AABBVisibleSceneNodes);
            ++NodesIndex)
        {
            DEBUGForwardPass(OpenGL,
                             &SceneCamera,
                             AABBVisibleSceneNodes[NodesIndex],
                             AssetStore,
                             PointLights_Information, 
                             &SceneLightingShader, 
                             &DebugLightShader,
                             &AABBDebugShader,
                             ShadowmapInfo);
        }
        
        //========================================================================================
        if(ShadowMapDebugEnabled)
        {
            OpenGL->glDisable(GL_DEPTH_TEST);
            OpenGL->glDisable(GL_CULL_FACE);
            
            OpenGL->glUseProgram(SurfaceShader2D.ID);
            
            glm::mat4 ModelMatrix = glm::mat4(1.0f);
            //ModelMatrix = glm::translate(ModelMatrix, glm::vec3(1.2f, -0.5f, 0.0f));
            ModelMatrix = glm::scale(ModelMatrix, glm::vec3(0.8f, 0.8f, 0.0f));
            OpenGL->glUniformMatrix4fv(OpenGL->glGetUniformLocation(SurfaceShader2D.ID, "ModelMatrix"), 1, GL_FALSE, glm::value_ptr(ModelMatrix));
            
            r32 AspectRatio = Cast(r32)RenderTargetWidth / Cast(r32)RenderTargetHeight;
            glm::mat4 Projection = glm::ortho(-AspectRatio, AspectRatio, -1.0f, 1.0f, -1.0f, 1.0f);
            OpenGL->glUniformMatrix4fv(OpenGL->glGetUniformLocation(SurfaceShader2D.ID, "ProjectionMatrix"), 1, GL_FALSE, glm::value_ptr(Projection));
            
            OpenGL->glUniform1i(OpenGL->glGetUniformLocation(SurfaceShader2D.ID, "DebugTexture"), 0);
            
            OpenGL->glActiveTexture(GL_TEXTURE0);
            OpenGL->glBindTexture(GL_TEXTURE_2D, ShadowmapInfo[ShadowMapDebugLevel].DepthMap);
            
            OpenGL->glBindVertexArray(Surface2D.VAO);
            OpenGL->glDrawArrays(GL_TRIANGLES, 0, 6);
            OpenGL->glBindVertexArray(0);
            
            OpenGL->glBindTexture(GL_TEXTURE_2D, 0);
            
            OpenGL->glEnable(GL_DEPTH_TEST);
            OpenGL->glEnable(GL_CULL_FACE);
        }
        //========================================================================================
        
        RendererEndFrame();
        
        LC_ArrayClear(VisibleSceneNodes);
        LC_ArrayClear(AABBVisibleSceneNodes);
        
        if(GPUTimerQueried && !WaitingForResult)
        {
            OpenGL->glEndQuery(GL_TIME_ELAPSED);
            WaitingForResult = true;
        }
        
        if(WaitingForResult)
        {
            GLint64 Result;
            OpenGL->glGetQueryObjecti64v(TimerQuery, GL_QUERY_RESULT_AVAILABLE, &Result);
            
            if(Result == GL_TRUE)
            {
                GLint64 ElapsedTime;
                OpenGL->glGetQueryObjecti64v(TimerQuery, GL_QUERY_RESULT, &ElapsedTime);
                WaitingForResult = false;
                GPUTimerQueried = false;
                
                r64 ElapsedTimeMS = (r64)ElapsedTime / 1000000;
                
                u32 FPS = (u32)(1000.0 / ElapsedTimeMS);
                
                snprintf(FrametimeOutput, Size, "GPU Frame time: %0.02f ms | FPS: %d\n", ElapsedTimeMS, FPS);
                //OutputDebugStringA(FrametimeOutput);
            }
        }
        
        for(u32 Index = 0;
            Index < 10;
            ++Index)
        {
            debug_record *Record = DebugRecordsArray + Index;
            u64 HitCount_CyclesCount = AtomicExchangeU64(&Record->HitCount_CyclesCount, 0);
            
            u32 HCount = GetAddressHigh(HitCount_CyclesCount);
            u32 CCount = GetAddressLow(HitCount_CyclesCount);
            
            // memory_size Size = 200 * sizeof(char);
            // char *Test = (char *)malloc(Size);
            
            // snprintf(Test, Size, "[%s] Hit count: %d | Cycles count: %d\n", Record->FunctionName, HCount, CCount);
            // OutputDebugStringA(Test);
            
            // delete Test;
        }
    }
}