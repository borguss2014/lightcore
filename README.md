## Preamble

`LightCore Engine` is the result of more than a year's worth of development and study. I've started this project out of a desire to learn more about the internal mechanics of a game engine (aka advanced software architecture and engine tools development). As such, I have focused more on the *engine* side at the expense of graphical capabilities/features, while experimenting with engine features which I believe should improve game development workflow. I intend to release a game with this engine and I'm still actively working on this project.

### Note

* The engine is written in a procedural style, having been inspired by Casey Muratori's *Handmade Hero* series.
* Usage of third-party libraries and C/C++ standard libraries is intentionally kept low. I intend to replace the ones currently in use with custom solutions.
* A minimal number of C++ features are used (operator/function overloading, constructors/destructors rarely etc.), and as such the project needs to be built with a C++ compiler.
* The *build system* consists entirely of a build.bat which compiles the project as a Unity build (only one translation unit).

* Scene used: https://www.intel.com/content/www/us/en/developer/topic-technology/graphics-research/samples.html

### Dependencies

* OpenGL 4.5+
* Assimp
* stb_image
* Dear ImGui
* OpenGL Mathematics - GLM (used only to test my own math library)
* Windows 10 minimum (I'm using a number of OS features which are only exposed by the Windows API from version 10 upwards)

## Engine-side features

* The engine is built with future multiplatform and multirenderer backends support in mind.
* Basic render *commands* and texture operations (uploads, state changes etc.) queues have been implemented.
* Custom allocators are used throughout the codebase (memory arenas, ring buffers, size-independent memory pools). They all take advantage of virtual memory capabilites exposed by the OS.
* A job queue system is currently used for texture streaming.

### Hot code reloading

As a substitute for scripting language systems (which are usually present in game engines), this engine relies on hot reloading the game layer. It is compiled separately and loaded as a DLL on engine startup. This allows the platform layer to call the game layer every frame, which updates the simulation state. The game layer in turn calls back into the platform layer (whenever it requires access to OS functionality) using a function table supplied by the platform layer. Note that this feature fails if memory layouts are changed at runtime.

![Hot_Code_Reloading](https://gitlab.com/borguss2014/lightcore/-/raw/master/resources/images/20230413_023532.gif "Hot Code Reloading")


### Texture streaming / reloading

Textures can be queued for loading and uploaded to GPU memory as soon as they're read from cold storage (i.e SSD, HDD etc). The engine keeps track of on-disk textures and re-uploads them whenever it detects changes. This allows users to modify textures in third-party applications (such as Photoshop) and instantly visualize the impact of their changes.

![Texture_Streaming](https://gitlab.com/borguss2014/lightcore/-/raw/master/resources/images/20230413_033422.gif "Texture Streaming")

![Texture_Reloading](https://gitlab.com/borguss2014/lightcore/-/raw/master/resources/images/20230427_123444.gif "Texture Reloading")


### Shaders reloading

Similarly to how textures are reloaded, shaders are also being tracked for modifications and reloaded.

![Shader_Reloading](https://gitlab.com/borguss2014/lightcore/-/raw/master/resources/images/20230413_042059.gif "Shader Reloading")


### Basic introspection

In order to parse a structure's members and display their name or value at runtime, I had to write a separate tool which scans the engine's codebase for introspection-tagged structs and generates the required data (before the engine's compilation).

![Introspection](https://gitlab.com/borguss2014/lightcore/-/raw/master/resources/images/20230413_044444.gif "Introspection")


### Profiling

Functions can be timed and the results displayed hierarchically. Timings are stored per frame, which allows per-frame investigations.

![Profiler](https://gitlab.com/borguss2014/lightcore/-/raw/master/resources/images/20230413_043409.gif "Profiler")


## Render-side features

* Forward rendering with Blinn-Phong shading
* Post processing
* Tangent space normal mapping
* Point light sources
* Gamma correction
* Render scaling

## Goals (in order of their priorities)

* Scene system
* Proper materials system
* Improvements to render commands and texture operations queues (sorting etc.)
* Directional light sources
* Point and directional light shadow mapping (a demonstration of omnidirectional shadowmapping with distance based resolution change implemented in a previous version of the engine but not currently integrated in this one, can be found [HERE](https://youtu.be/Z0Tf8vfWyz0))
----
* Scene editor
* On-disk scene/levels storage
----
* Parallax mapping
* Frustum culling
* AABB and bounding sphere generation
* Ambient occlusion
* Emissive mapping
* HDR & Tone mapping
* Bloom
* Alpha blending
* Implement Physically Based Rendering
* MSAA, FXAA, possibly TAA as well
----
* Object occlusion
* Camera rails system for in-engine presentations or cinematics
* Advanced camera features (depth of field, exposure control etc.)
* Particle system (a demonstration for a toy 2D particle system I've experimented with in the past can be found [HERE](https://youtu.be/M-l6K2-Ax8k))
----
* Text rendering
* Skybox
* GUI
* Billboards
* Direct3D backend
* Multithreaded rendering
* Replace assimp with a custom solution
* Replace stb_image with a custom solution
* Implement custom tests for the math library so I can completely remove GLM
