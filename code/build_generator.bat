@echo off

set CommonCompilerFlags=-diagnostics:column -FAsc -nologo -Gm- -GR- -EHa- -Od -Oi -WL -WX -W4 -wd4100 -wd4505 -wd4101 -wd4189 -wd4702 -wd4127 -FC -Z7

if not exist ..\..\build\ (mkdir ..\..\build\)
pushd ..\..\build

echo [ Compiling OpenGL defines generator ]

cl %CommonCompilerFlags% ..\Lightcore\code\win32_opengl_defines_generator.cpp

echo [ Running OpenGL defines generation pre-pass ]

start /b /WAIT "" win32_opengl_defines_generator.exe

move win32_opengl_defines_generated.h ..\Lightcore\code

popd