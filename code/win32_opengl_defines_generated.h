#include <gl/gl.h>
#include <GL/glext.h>
#include "GL/wglext.h"

typedef void (*DEBUGPROC)(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar *message, const void *userParam);

typedef void (WINAPI * type_glGetIntegerv)(GLenum pname, GLint * data);
typedef void (WINAPI * type_glGetProgramInterfaceiv)(GLuint program, GLenum programInterface, GLenum pname, GLint * params);
typedef void (WINAPI * type_glGetActiveUniform)(GLuint program, GLuint index, GLsizei bufSize, GLsizei *length, GLint *size, GLenum *type, GLchar *name);
typedef void (WINAPI * type_glDeleteRenderbuffers)(GLsizei n, GLuint *renderbuffers);
typedef void (WINAPI * type_glDeleteFramebuffers)(GLsizei n, GLuint *framebuffers);
typedef GLenum (WINAPI * type_glCheckNamedFramebufferStatus)(GLuint framebuffer, GLenum target);
typedef void (WINAPI * type_glNamedFramebufferRenderbuffer)(GLuint framebuffer, GLenum attachment, GLenum renderbuffertarget, GLuint renderbuffer);
typedef void (WINAPI * type_glNamedFramebufferTexture)(GLuint framebuffer, GLenum attachment, GLuint texture, GLint level);
typedef void (WINAPI * type_glCreateFramebuffers)(GLsizei n, GLuint *framebuffers);
typedef void (WINAPI * type_glNamedRenderbufferStorage)(GLuint renderbuffer, GLenum internalformat, GLsizei width, GLsizei height);
typedef void (WINAPI * type_glCreateRenderbuffers)(GLsizei n, GLuint *renderbuffers);
typedef void (WINAPI * type_glTextureParameterf)(GLuint texture, GLenum pname, GLfloat param);
typedef void (WINAPI * type_glMultiDrawElements)(GLenum mode, const GLsizei * count, GLenum type, const void * const * indices, GLsizei drawcount);
typedef void (WINAPI * type_glFrontFace)(GLenum mode);
typedef GLuint (WINAPI * type_glGetUniformBlockIndex)(GLuint program, const GLchar *uniformBlockName);
typedef void (WINAPI * type_glVertexArrayElementBuffer)(GLuint vaobj, GLuint buffer);
typedef void (WINAPI * type_glBindTextureUnit)(GLuint unit, GLuint texture);
typedef void (WINAPI * type_glGenerateTextureMipmap)(GLuint texture);
typedef void (WINAPI * type_glTextureParameteri)(GLuint texture, GLenum pname, GLint param);
typedef void (WINAPI * type_glTextureSubImage2D)(GLuint texture, GLint level, GLint xoffset, GLint yoffset, GLsizei width, GLsizei height, GLenum format, GLenum type, const void *pixels);
typedef void (WINAPI * type_glTextureStorage2D)(GLuint texture, GLsizei levels, GLenum internalformat, GLsizei width, GLsizei height);
typedef void (WINAPI * type_glCreateTextures)(GLenum target, GLsizei n, GLuint *textures);
typedef void (WINAPI * type_glVertexArrayVertexBuffer)(GLuint vaobj, GLuint bindingindex, GLuint buffer, GLintptr offset, GLsizei stride);
typedef void (WINAPI * type_glNamedBufferStorage)(GLuint buffer, GLsizeiptr size, const void *data, GLbitfield flags);
typedef GLint (WINAPI * type_glGetAttribLocation)(GLuint program, const GLchar *name);
typedef void (WINAPI * type_glEnableVertexArrayAttrib)(GLuint vaobj, GLuint index);
typedef void (WINAPI * type_glDisableVertexArrayAttrib)(GLuint vaobj, GLuint index);
typedef void (WINAPI * type_glVertexArrayAttribFormat)(GLuint vaobj, GLuint attribindex, GLint size, GLenum type, GLboolean normalized, GLuint relativeoffset);
typedef void (WINAPI * type_glVertexArrayAttribBinding)(GLuint vaobj, GLuint attribindex, GLuint bindingindex);
typedef void (WINAPI * type_glValidateProgram)(GLuint program);
typedef void (WINAPI * type_glDebugMessageCallback)(DEBUGPROC callback, const void * userParam);
typedef void (WINAPI * type_glDrawArrays)(GLenum mode, GLint first, GLsizei count);
typedef void (WINAPI * type_glDrawElements)(GLenum mode, GLsizei count, GLenum type, const void * indices);
typedef void (WINAPI * type_glEnable)(GLenum cap);
typedef void (WINAPI * type_glDisable)(GLenum cap);
typedef void (WINAPI * type_glGenQueries)(GLsizei n, GLuint * ids);
typedef void (WINAPI * type_glBeginQuery)(GLenum target, GLuint id);
typedef void (WINAPI * type_glEndQuery)(GLenum target);
typedef void (WINAPI * type_glGetQueryObjecti64v)(GLuint id, GLenum pname, GLint64 * params);
typedef void (WINAPI * type_glDepthMask)(GLboolean flag);
typedef void * (WINAPI * type_glMapBuffer)(GLenum target, GLenum access);
typedef void * (WINAPI * type_glMapNamedBuffer)(GLuint buffer, GLenum access);
typedef GLboolean (WINAPI * type_glUnmapBuffer)(GLenum target);
typedef GLboolean (WINAPI * type_glUnmapNamedBuffer)(GLuint buffer);
typedef void (WINAPI * type_glViewport)(GLint x, GLint y, GLsizei width, GLsizei height);
typedef void (WINAPI * type_glCullFace)(GLenum mode);
typedef void (WINAPI * type_glPolygonMode)(GLenum face, GLenum mode);
typedef void (WINAPI * type_glLineWidth)(GLfloat width);
typedef void (WINAPI * type_glDrawBuffer)(GLenum buf);
typedef void (WINAPI * type_glReadBuffer)(GLenum mode);
typedef void (WINAPI * type_glBindTexture)(GLenum target, GLuint texture);
typedef void (WINAPI * type_glCreateVertexArrays)(GLsizei n, GLuint *arrays);
typedef void (WINAPI * type_glGenVertexArrays)(GLsizei n, GLuint *arrays);
typedef void (WINAPI * type_glBindVertexArray)(GLuint array);
typedef void (WINAPI * type_glGenBuffers)(GLsizei n, GLuint * buffers);
typedef void (WINAPI * type_glDeleteBuffers)(GLsizei n, const GLuint * buffers);
typedef void (WINAPI * type_glDeleteVertexArrays)(GLsizei n, const GLuint *arrays);
typedef void (WINAPI * type_glEnableVertexAttribArray)(GLuint index);
typedef void (WINAPI * type_glVertexAttribFormat)(GLuint attribindex, GLint size, GLenum type, GLboolean normalized, GLuint relativeoffset);
typedef void (WINAPI * type_glVertexAttribBinding)(GLuint attribindex, GLuint bindingindex);
typedef void (WINAPI * type_glActiveTexture)(GLenum texture);
typedef void (WINAPI * type_glGenTextures)(GLsizei n, GLuint * textures);
typedef void (WINAPI * type_glDeleteTextures)(GLsizei n, const GLuint * textures);
typedef void (WINAPI * type_glGenerateMipmap)(GLenum target);
typedef void (WINAPI * type_glTexImage1D)(GLenum target, GLint level, GLint internalformat, GLsizei width, GLint border, GLenum format, GLenum type, const void * data);
typedef void (WINAPI * type_glTexImage2D)(GLenum target, GLint level, GLint internalformat, GLsizei width, GLsizei height, GLint border, GLenum format, GLenum type, const void * data);
typedef void (WINAPI * type_glTexImage3D)(GLenum target, GLint level, GLint internalformat, GLsizei width, GLsizei height, GLsizei depth, GLint border, GLenum format, GLenum type, const void *pixels);
typedef void (WINAPI * type_glTexParameteri)(GLenum target, GLenum pname, GLint param);
typedef void (WINAPI * type_glPixelStorei)(GLenum pname, GLint param);
typedef void (WINAPI * type_glTexParameterfv)(GLenum target, GLenum pname, const GLfloat * params);
typedef void (WINAPI * type_glGenRenderbuffers)(GLsizei n, GLuint *renderbuffers);
typedef void (WINAPI * type_glBindRenderbuffer)(GLenum target, GLuint renderbuffer);
typedef void (WINAPI * type_glRenderbufferStorage)(GLenum target, GLenum internalformat, GLsizei width, GLsizei height);
typedef void (WINAPI * type_glCreateBuffers)(GLsizei n, GLuint *buffers);
typedef void (WINAPI * type_glBindBuffer)(GLenum target, GLuint buffer);
typedef void (WINAPI * type_glBindBufferBase)(GLenum target, GLuint index, GLuint buffer);
typedef void (WINAPI * type_glBindBufferRange)(GLenum target, GLuint index, GLuint buffer, GLintptr offset, GLsizeiptr size);
typedef void (WINAPI * type_glBufferData)(GLenum target, GLsizeiptr size, const void * data, GLenum usage);
typedef void (WINAPI * type_glNamedBufferData)(GLuint buffer, GLsizeiptr size, const void *data, GLenum usage);
typedef void (WINAPI * type_glBufferSubData)(GLenum target, GLintptr offset, GLsizeiptr size, const void * data);
typedef void (WINAPI * type_glNamedBufferSubData)(GLuint buffer, GLintptr offset, GLsizeiptr size, const void *data);
typedef void (WINAPI * type_glBindVertexBuffer)(GLuint bindingindex, GLuint buffer, GLintptr offset, GLsizei stride);
typedef GLuint (WINAPI * type_glCreateProgram)(void);
typedef void (WINAPI * type_glDeleteProgram)(GLuint program);
typedef void (WINAPI * type_glLinkProgram)(GLuint program);
typedef void (WINAPI * type_glActiveShaderProgram)(GLuint pipeline, GLuint program);
typedef void (WINAPI * type_glUseProgram)(GLuint program);
typedef void (WINAPI * type_glGetProgramiv)(GLuint program, GLenum pname, GLint *params);
typedef void (WINAPI * type_glGetProgramInfoLog)(GLuint program, GLsizei maxLength, GLsizei *length, GLchar *infoLog);
typedef GLuint (WINAPI * type_glCreateShader)(GLenum shaderType);
typedef void (WINAPI * type_glDeleteShader)(GLuint shader);
typedef void (WINAPI * type_glAttachShader)(GLuint program, GLuint shader);
typedef void (WINAPI * type_glShaderSource)(GLuint shader, GLsizei count, const GLchar *const*string, const GLint *length);
typedef void (WINAPI * type_glCompileShader)(GLuint shader);
typedef void (WINAPI * type_glGetShaderiv)(GLuint shader, GLenum pname, GLint *params);
typedef void (WINAPI * type_glGetShaderInfoLog)(GLuint shader, GLsizei maxLength, GLsizei *length, GLchar *infoLog);
typedef void (WINAPI * type_glUniform1i)(GLint location, GLint v0);
typedef void (WINAPI * type_glUniform1f)(GLint location, GLfloat v0);
typedef void (WINAPI * type_glUniform3fv)(GLint location, GLsizei count, const GLfloat *value);
typedef void (WINAPI * type_glUniformMatrix3fv)(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);
typedef void (WINAPI * type_glUniformMatrix4fv)(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);
typedef void (WINAPI * type_glUniform3f)(GLint location, GLfloat v0, GLfloat v1, GLfloat v2);
typedef void (WINAPI * type_glUniform4f)(GLint location, GLfloat v0, GLfloat v1, GLfloat v2, GLfloat v3);
typedef GLint (WINAPI * type_glGetUniformLocation)(GLuint program, const GLchar *name);
typedef void (WINAPI * type_glGenFramebuffers)(GLsizei n, GLuint *ids);
typedef void (WINAPI * type_glBindFramebuffer)(GLenum target, GLuint framebuffer);
typedef void (WINAPI * type_glFramebufferTexture2D)(GLenum target, GLenum attachment, GLenum textarget, GLuint texture, GLint level);
typedef void (WINAPI * type_glFramebufferRenderbuffer)(GLenum target, GLenum attachment, GLenum renderbuffertarget, GLuint renderbuffer);
typedef GLenum (WINAPI * type_glCheckFramebufferStatus)(GLenum target);
typedef void (WINAPI * type_glClear)(GLbitfield mask);
typedef void (WINAPI * type_glClearColor)(GLclampf red, GLclampf green, GLclampf blue, GLclampf alpha);
typedef void (WINAPI * type_glClearTexImage)(GLuint texture, GLint level, GLenum format, GLenum type, const void * data);
typedef BOOL (WINAPI * type_wglSwapIntervalEXT)(int interval);

struct opengl_fn_table
{
    type_glGetIntegerv glGetIntegerv;
    type_glGetProgramInterfaceiv glGetProgramInterfaceiv;
    type_glGetActiveUniform glGetActiveUniform;
    type_glDeleteRenderbuffers glDeleteRenderbuffers;
    type_glDeleteFramebuffers glDeleteFramebuffers;
    type_glCheckNamedFramebufferStatus glCheckNamedFramebufferStatus;
    type_glNamedFramebufferRenderbuffer glNamedFramebufferRenderbuffer;
    type_glNamedFramebufferTexture glNamedFramebufferTexture;
    type_glCreateFramebuffers glCreateFramebuffers;
    type_glNamedRenderbufferStorage glNamedRenderbufferStorage;
    type_glCreateRenderbuffers glCreateRenderbuffers;
    type_glTextureParameterf glTextureParameterf;
    type_glMultiDrawElements glMultiDrawElements;
    type_glFrontFace glFrontFace;
    type_glGetUniformBlockIndex glGetUniformBlockIndex;
    type_glVertexArrayElementBuffer glVertexArrayElementBuffer;
    type_glBindTextureUnit glBindTextureUnit;
    type_glGenerateTextureMipmap glGenerateTextureMipmap;
    type_glTextureParameteri glTextureParameteri;
    type_glTextureSubImage2D glTextureSubImage2D;
    type_glTextureStorage2D glTextureStorage2D;
    type_glCreateTextures glCreateTextures;
    type_glVertexArrayVertexBuffer glVertexArrayVertexBuffer;
    type_glNamedBufferStorage glNamedBufferStorage;
    type_glGetAttribLocation glGetAttribLocation;
    type_glEnableVertexArrayAttrib glEnableVertexArrayAttrib;
    type_glDisableVertexArrayAttrib glDisableVertexArrayAttrib;
    type_glVertexArrayAttribFormat glVertexArrayAttribFormat;
    type_glVertexArrayAttribBinding glVertexArrayAttribBinding;
    type_glValidateProgram glValidateProgram;
    type_glDebugMessageCallback glDebugMessageCallback;
    type_glDrawArrays glDrawArrays;
    type_glDrawElements glDrawElements;
    type_glEnable glEnable;
    type_glDisable glDisable;
    type_glGenQueries glGenQueries;
    type_glBeginQuery glBeginQuery;
    type_glEndQuery glEndQuery;
    type_glGetQueryObjecti64v glGetQueryObjecti64v;
    type_glDepthMask glDepthMask;
    type_glMapBuffer glMapBuffer;
    type_glMapNamedBuffer glMapNamedBuffer;
    type_glUnmapBuffer glUnmapBuffer;
    type_glUnmapNamedBuffer glUnmapNamedBuffer;
    type_glViewport glViewport;
    type_glCullFace glCullFace;
    type_glPolygonMode glPolygonMode;
    type_glLineWidth glLineWidth;
    type_glDrawBuffer glDrawBuffer;
    type_glReadBuffer glReadBuffer;
    type_glBindTexture glBindTexture;
    type_glCreateVertexArrays glCreateVertexArrays;
    type_glGenVertexArrays glGenVertexArrays;
    type_glBindVertexArray glBindVertexArray;
    type_glGenBuffers glGenBuffers;
    type_glDeleteBuffers glDeleteBuffers;
    type_glDeleteVertexArrays glDeleteVertexArrays;
    type_glEnableVertexAttribArray glEnableVertexAttribArray;
    type_glVertexAttribFormat glVertexAttribFormat;
    type_glVertexAttribBinding glVertexAttribBinding;
    type_glActiveTexture glActiveTexture;
    type_glGenTextures glGenTextures;
    type_glDeleteTextures glDeleteTextures;
    type_glGenerateMipmap glGenerateMipmap;
    type_glTexImage1D glTexImage1D;
    type_glTexImage2D glTexImage2D;
    type_glTexImage3D glTexImage3D;
    type_glTexParameteri glTexParameteri;
    type_glPixelStorei glPixelStorei;
    type_glTexParameterfv glTexParameterfv;
    type_glGenRenderbuffers glGenRenderbuffers;
    type_glBindRenderbuffer glBindRenderbuffer;
    type_glRenderbufferStorage glRenderbufferStorage;
    type_glCreateBuffers glCreateBuffers;
    type_glBindBuffer glBindBuffer;
    type_glBindBufferBase glBindBufferBase;
    type_glBindBufferRange glBindBufferRange;
    type_glBufferData glBufferData;
    type_glNamedBufferData glNamedBufferData;
    type_glBufferSubData glBufferSubData;
    type_glNamedBufferSubData glNamedBufferSubData;
    type_glBindVertexBuffer glBindVertexBuffer;
    type_glCreateProgram glCreateProgram;
    type_glDeleteProgram glDeleteProgram;
    type_glLinkProgram glLinkProgram;
    type_glActiveShaderProgram glActiveShaderProgram;
    type_glUseProgram glUseProgram;
    type_glGetProgramiv glGetProgramiv;
    type_glGetProgramInfoLog glGetProgramInfoLog;
    type_glCreateShader glCreateShader;
    type_glDeleteShader glDeleteShader;
    type_glAttachShader glAttachShader;
    type_glShaderSource glShaderSource;
    type_glCompileShader glCompileShader;
    type_glGetShaderiv glGetShaderiv;
    type_glGetShaderInfoLog glGetShaderInfoLog;
    type_glUniform1i glUniform1i;
    type_glUniform1f glUniform1f;
    type_glUniform3fv glUniform3fv;
    type_glUniformMatrix3fv glUniformMatrix3fv;
    type_glUniformMatrix4fv glUniformMatrix4fv;
    type_glUniform3f glUniform3f;
    type_glUniform4f glUniform4f;
    type_glGetUniformLocation glGetUniformLocation;
    type_glGenFramebuffers glGenFramebuffers;
    type_glBindFramebuffer glBindFramebuffer;
    type_glFramebufferTexture2D glFramebufferTexture2D;
    type_glFramebufferRenderbuffer glFramebufferRenderbuffer;
    type_glCheckFramebufferStatus glCheckFramebufferStatus;
    type_glClear glClear;
    type_glClearColor glClearColor;
    type_glClearTexImage glClearTexImage;
    type_wglSwapIntervalEXT wglSwapIntervalEXT;
};

internal void *
LoadOpenGLFunctionPointer(char *FunctionName)
{
   // Try to load the function from the graphics driver
   void *OpenGLFunction = wglGetProcAddress(FunctionName);
   if((OpenGLFunction == 0) ||
   (OpenGLFunction == (void*)0x1) ||
   (OpenGLFunction == (void*)0x2) ||
   (OpenGLFunction == (void*)0x3) ||
   (OpenGLFunction == (void*)-1) )
   {
       // Try to load from the Win32 API
       HMODULE Module = LoadLibraryA("opengl32.dll");
       OpenGLFunction = GetProcAddress(Module, FunctionName);
   }
   return(OpenGLFunction);
}

internal b32
OpenGLLoadFunctionPointers(opengl_fn_table *OpenGL)
{
    b32 Result = true;

    if(OpenGL)
    {
		OpenGL->glGetIntegerv = (type_glGetIntegerv)LoadOpenGLFunctionPointer("glGetIntegerv");
		if(!OpenGL->glGetIntegerv) { Result = false; }

		OpenGL->glGetProgramInterfaceiv = (type_glGetProgramInterfaceiv)LoadOpenGLFunctionPointer("glGetProgramInterfaceiv");
		if(!OpenGL->glGetProgramInterfaceiv) { Result = false; }

		OpenGL->glGetActiveUniform = (type_glGetActiveUniform)LoadOpenGLFunctionPointer("glGetActiveUniform");
		if(!OpenGL->glGetActiveUniform) { Result = false; }

		OpenGL->glDeleteRenderbuffers = (type_glDeleteRenderbuffers)LoadOpenGLFunctionPointer("glDeleteRenderbuffers");
		if(!OpenGL->glDeleteRenderbuffers) { Result = false; }

		OpenGL->glDeleteFramebuffers = (type_glDeleteFramebuffers)LoadOpenGLFunctionPointer("glDeleteFramebuffers");
		if(!OpenGL->glDeleteFramebuffers) { Result = false; }

		OpenGL->glCheckNamedFramebufferStatus = (type_glCheckNamedFramebufferStatus)LoadOpenGLFunctionPointer("glCheckNamedFramebufferStatus");
		if(!OpenGL->glCheckNamedFramebufferStatus) { Result = false; }

		OpenGL->glNamedFramebufferRenderbuffer = (type_glNamedFramebufferRenderbuffer)LoadOpenGLFunctionPointer("glNamedFramebufferRenderbuffer");
		if(!OpenGL->glNamedFramebufferRenderbuffer) { Result = false; }

		OpenGL->glNamedFramebufferTexture = (type_glNamedFramebufferTexture)LoadOpenGLFunctionPointer("glNamedFramebufferTexture");
		if(!OpenGL->glNamedFramebufferTexture) { Result = false; }

		OpenGL->glCreateFramebuffers = (type_glCreateFramebuffers)LoadOpenGLFunctionPointer("glCreateFramebuffers");
		if(!OpenGL->glCreateFramebuffers) { Result = false; }

		OpenGL->glNamedRenderbufferStorage = (type_glNamedRenderbufferStorage)LoadOpenGLFunctionPointer("glNamedRenderbufferStorage");
		if(!OpenGL->glNamedRenderbufferStorage) { Result = false; }

		OpenGL->glCreateRenderbuffers = (type_glCreateRenderbuffers)LoadOpenGLFunctionPointer("glCreateRenderbuffers");
		if(!OpenGL->glCreateRenderbuffers) { Result = false; }

		OpenGL->glTextureParameterf = (type_glTextureParameterf)LoadOpenGLFunctionPointer("glTextureParameterf");
		if(!OpenGL->glTextureParameterf) { Result = false; }

		OpenGL->glMultiDrawElements = (type_glMultiDrawElements)LoadOpenGLFunctionPointer("glMultiDrawElements");
		if(!OpenGL->glMultiDrawElements) { Result = false; }

		OpenGL->glFrontFace = (type_glFrontFace)LoadOpenGLFunctionPointer("glFrontFace");
		if(!OpenGL->glFrontFace) { Result = false; }

		OpenGL->glGetUniformBlockIndex = (type_glGetUniformBlockIndex)LoadOpenGLFunctionPointer("glGetUniformBlockIndex");
		if(!OpenGL->glGetUniformBlockIndex) { Result = false; }

		OpenGL->glVertexArrayElementBuffer = (type_glVertexArrayElementBuffer)LoadOpenGLFunctionPointer("glVertexArrayElementBuffer");
		if(!OpenGL->glVertexArrayElementBuffer) { Result = false; }

		OpenGL->glBindTextureUnit = (type_glBindTextureUnit)LoadOpenGLFunctionPointer("glBindTextureUnit");
		if(!OpenGL->glBindTextureUnit) { Result = false; }

		OpenGL->glGenerateTextureMipmap = (type_glGenerateTextureMipmap)LoadOpenGLFunctionPointer("glGenerateTextureMipmap");
		if(!OpenGL->glGenerateTextureMipmap) { Result = false; }

		OpenGL->glTextureParameteri = (type_glTextureParameteri)LoadOpenGLFunctionPointer("glTextureParameteri");
		if(!OpenGL->glTextureParameteri) { Result = false; }

		OpenGL->glTextureSubImage2D = (type_glTextureSubImage2D)LoadOpenGLFunctionPointer("glTextureSubImage2D");
		if(!OpenGL->glTextureSubImage2D) { Result = false; }

		OpenGL->glTextureStorage2D = (type_glTextureStorage2D)LoadOpenGLFunctionPointer("glTextureStorage2D");
		if(!OpenGL->glTextureStorage2D) { Result = false; }

		OpenGL->glCreateTextures = (type_glCreateTextures)LoadOpenGLFunctionPointer("glCreateTextures");
		if(!OpenGL->glCreateTextures) { Result = false; }

		OpenGL->glVertexArrayVertexBuffer = (type_glVertexArrayVertexBuffer)LoadOpenGLFunctionPointer("glVertexArrayVertexBuffer");
		if(!OpenGL->glVertexArrayVertexBuffer) { Result = false; }

		OpenGL->glNamedBufferStorage = (type_glNamedBufferStorage)LoadOpenGLFunctionPointer("glNamedBufferStorage");
		if(!OpenGL->glNamedBufferStorage) { Result = false; }

		OpenGL->glGetAttribLocation = (type_glGetAttribLocation)LoadOpenGLFunctionPointer("glGetAttribLocation");
		if(!OpenGL->glGetAttribLocation) { Result = false; }

		OpenGL->glEnableVertexArrayAttrib = (type_glEnableVertexArrayAttrib)LoadOpenGLFunctionPointer("glEnableVertexArrayAttrib");
		if(!OpenGL->glEnableVertexArrayAttrib) { Result = false; }

		OpenGL->glDisableVertexArrayAttrib = (type_glDisableVertexArrayAttrib)LoadOpenGLFunctionPointer("glDisableVertexArrayAttrib");
		if(!OpenGL->glDisableVertexArrayAttrib) { Result = false; }

		OpenGL->glVertexArrayAttribFormat = (type_glVertexArrayAttribFormat)LoadOpenGLFunctionPointer("glVertexArrayAttribFormat");
		if(!OpenGL->glVertexArrayAttribFormat) { Result = false; }

		OpenGL->glVertexArrayAttribBinding = (type_glVertexArrayAttribBinding)LoadOpenGLFunctionPointer("glVertexArrayAttribBinding");
		if(!OpenGL->glVertexArrayAttribBinding) { Result = false; }

		OpenGL->glValidateProgram = (type_glValidateProgram)LoadOpenGLFunctionPointer("glValidateProgram");
		if(!OpenGL->glValidateProgram) { Result = false; }

		OpenGL->glDebugMessageCallback = (type_glDebugMessageCallback)LoadOpenGLFunctionPointer("glDebugMessageCallback");
		if(!OpenGL->glDebugMessageCallback) { Result = false; }

		OpenGL->glDrawArrays = (type_glDrawArrays)LoadOpenGLFunctionPointer("glDrawArrays");
		if(!OpenGL->glDrawArrays) { Result = false; }

		OpenGL->glDrawElements = (type_glDrawElements)LoadOpenGLFunctionPointer("glDrawElements");
		if(!OpenGL->glDrawElements) { Result = false; }

		OpenGL->glEnable = (type_glEnable)LoadOpenGLFunctionPointer("glEnable");
		if(!OpenGL->glEnable) { Result = false; }

		OpenGL->glDisable = (type_glDisable)LoadOpenGLFunctionPointer("glDisable");
		if(!OpenGL->glDisable) { Result = false; }

		OpenGL->glGenQueries = (type_glGenQueries)LoadOpenGLFunctionPointer("glGenQueries");
		if(!OpenGL->glGenQueries) { Result = false; }

		OpenGL->glBeginQuery = (type_glBeginQuery)LoadOpenGLFunctionPointer("glBeginQuery");
		if(!OpenGL->glBeginQuery) { Result = false; }

		OpenGL->glEndQuery = (type_glEndQuery)LoadOpenGLFunctionPointer("glEndQuery");
		if(!OpenGL->glEndQuery) { Result = false; }

		OpenGL->glGetQueryObjecti64v = (type_glGetQueryObjecti64v)LoadOpenGLFunctionPointer("glGetQueryObjecti64v");
		if(!OpenGL->glGetQueryObjecti64v) { Result = false; }

		OpenGL->glDepthMask = (type_glDepthMask)LoadOpenGLFunctionPointer("glDepthMask");
		if(!OpenGL->glDepthMask) { Result = false; }

		OpenGL->glMapBuffer = (type_glMapBuffer)LoadOpenGLFunctionPointer("glMapBuffer");
		if(!OpenGL->glMapBuffer) { Result = false; }

		OpenGL->glMapNamedBuffer = (type_glMapNamedBuffer)LoadOpenGLFunctionPointer("glMapNamedBuffer");
		if(!OpenGL->glMapNamedBuffer) { Result = false; }

		OpenGL->glUnmapBuffer = (type_glUnmapBuffer)LoadOpenGLFunctionPointer("glUnmapBuffer");
		if(!OpenGL->glUnmapBuffer) { Result = false; }

		OpenGL->glUnmapNamedBuffer = (type_glUnmapNamedBuffer)LoadOpenGLFunctionPointer("glUnmapNamedBuffer");
		if(!OpenGL->glUnmapNamedBuffer) { Result = false; }

		OpenGL->glViewport = (type_glViewport)LoadOpenGLFunctionPointer("glViewport");
		if(!OpenGL->glViewport) { Result = false; }

		OpenGL->glCullFace = (type_glCullFace)LoadOpenGLFunctionPointer("glCullFace");
		if(!OpenGL->glCullFace) { Result = false; }

		OpenGL->glPolygonMode = (type_glPolygonMode)LoadOpenGLFunctionPointer("glPolygonMode");
		if(!OpenGL->glPolygonMode) { Result = false; }

		OpenGL->glLineWidth = (type_glLineWidth)LoadOpenGLFunctionPointer("glLineWidth");
		if(!OpenGL->glLineWidth) { Result = false; }

		OpenGL->glDrawBuffer = (type_glDrawBuffer)LoadOpenGLFunctionPointer("glDrawBuffer");
		if(!OpenGL->glDrawBuffer) { Result = false; }

		OpenGL->glReadBuffer = (type_glReadBuffer)LoadOpenGLFunctionPointer("glReadBuffer");
		if(!OpenGL->glReadBuffer) { Result = false; }

		OpenGL->glBindTexture = (type_glBindTexture)LoadOpenGLFunctionPointer("glBindTexture");
		if(!OpenGL->glBindTexture) { Result = false; }

		OpenGL->glCreateVertexArrays = (type_glCreateVertexArrays)LoadOpenGLFunctionPointer("glCreateVertexArrays");
		if(!OpenGL->glCreateVertexArrays) { Result = false; }

		OpenGL->glGenVertexArrays = (type_glGenVertexArrays)LoadOpenGLFunctionPointer("glGenVertexArrays");
		if(!OpenGL->glGenVertexArrays) { Result = false; }

		OpenGL->glBindVertexArray = (type_glBindVertexArray)LoadOpenGLFunctionPointer("glBindVertexArray");
		if(!OpenGL->glBindVertexArray) { Result = false; }

		OpenGL->glGenBuffers = (type_glGenBuffers)LoadOpenGLFunctionPointer("glGenBuffers");
		if(!OpenGL->glGenBuffers) { Result = false; }

		OpenGL->glDeleteBuffers = (type_glDeleteBuffers)LoadOpenGLFunctionPointer("glDeleteBuffers");
		if(!OpenGL->glDeleteBuffers) { Result = false; }

		OpenGL->glDeleteVertexArrays = (type_glDeleteVertexArrays)LoadOpenGLFunctionPointer("glDeleteVertexArrays");
		if(!OpenGL->glDeleteVertexArrays) { Result = false; }

		OpenGL->glEnableVertexAttribArray = (type_glEnableVertexAttribArray)LoadOpenGLFunctionPointer("glEnableVertexAttribArray");
		if(!OpenGL->glEnableVertexAttribArray) { Result = false; }

		OpenGL->glVertexAttribFormat = (type_glVertexAttribFormat)LoadOpenGLFunctionPointer("glVertexAttribFormat");
		if(!OpenGL->glVertexAttribFormat) { Result = false; }

		OpenGL->glVertexAttribBinding = (type_glVertexAttribBinding)LoadOpenGLFunctionPointer("glVertexAttribBinding");
		if(!OpenGL->glVertexAttribBinding) { Result = false; }

		OpenGL->glActiveTexture = (type_glActiveTexture)LoadOpenGLFunctionPointer("glActiveTexture");
		if(!OpenGL->glActiveTexture) { Result = false; }

		OpenGL->glGenTextures = (type_glGenTextures)LoadOpenGLFunctionPointer("glGenTextures");
		if(!OpenGL->glGenTextures) { Result = false; }

		OpenGL->glDeleteTextures = (type_glDeleteTextures)LoadOpenGLFunctionPointer("glDeleteTextures");
		if(!OpenGL->glDeleteTextures) { Result = false; }

		OpenGL->glGenerateMipmap = (type_glGenerateMipmap)LoadOpenGLFunctionPointer("glGenerateMipmap");
		if(!OpenGL->glGenerateMipmap) { Result = false; }

		OpenGL->glTexImage1D = (type_glTexImage1D)LoadOpenGLFunctionPointer("glTexImage1D");
		if(!OpenGL->glTexImage1D) { Result = false; }

		OpenGL->glTexImage2D = (type_glTexImage2D)LoadOpenGLFunctionPointer("glTexImage2D");
		if(!OpenGL->glTexImage2D) { Result = false; }

		OpenGL->glTexImage3D = (type_glTexImage3D)LoadOpenGLFunctionPointer("glTexImage3D");
		if(!OpenGL->glTexImage3D) { Result = false; }

		OpenGL->glTexParameteri = (type_glTexParameteri)LoadOpenGLFunctionPointer("glTexParameteri");
		if(!OpenGL->glTexParameteri) { Result = false; }

		OpenGL->glPixelStorei = (type_glPixelStorei)LoadOpenGLFunctionPointer("glPixelStorei");
		if(!OpenGL->glPixelStorei) { Result = false; }

		OpenGL->glTexParameterfv = (type_glTexParameterfv)LoadOpenGLFunctionPointer("glTexParameterfv");
		if(!OpenGL->glTexParameterfv) { Result = false; }

		OpenGL->glGenRenderbuffers = (type_glGenRenderbuffers)LoadOpenGLFunctionPointer("glGenRenderbuffers");
		if(!OpenGL->glGenRenderbuffers) { Result = false; }

		OpenGL->glBindRenderbuffer = (type_glBindRenderbuffer)LoadOpenGLFunctionPointer("glBindRenderbuffer");
		if(!OpenGL->glBindRenderbuffer) { Result = false; }

		OpenGL->glRenderbufferStorage = (type_glRenderbufferStorage)LoadOpenGLFunctionPointer("glRenderbufferStorage");
		if(!OpenGL->glRenderbufferStorage) { Result = false; }

		OpenGL->glCreateBuffers = (type_glCreateBuffers)LoadOpenGLFunctionPointer("glCreateBuffers");
		if(!OpenGL->glCreateBuffers) { Result = false; }

		OpenGL->glBindBuffer = (type_glBindBuffer)LoadOpenGLFunctionPointer("glBindBuffer");
		if(!OpenGL->glBindBuffer) { Result = false; }

		OpenGL->glBindBufferBase = (type_glBindBufferBase)LoadOpenGLFunctionPointer("glBindBufferBase");
		if(!OpenGL->glBindBufferBase) { Result = false; }

		OpenGL->glBindBufferRange = (type_glBindBufferRange)LoadOpenGLFunctionPointer("glBindBufferRange");
		if(!OpenGL->glBindBufferRange) { Result = false; }

		OpenGL->glBufferData = (type_glBufferData)LoadOpenGLFunctionPointer("glBufferData");
		if(!OpenGL->glBufferData) { Result = false; }

		OpenGL->glNamedBufferData = (type_glNamedBufferData)LoadOpenGLFunctionPointer("glNamedBufferData");
		if(!OpenGL->glNamedBufferData) { Result = false; }

		OpenGL->glBufferSubData = (type_glBufferSubData)LoadOpenGLFunctionPointer("glBufferSubData");
		if(!OpenGL->glBufferSubData) { Result = false; }

		OpenGL->glNamedBufferSubData = (type_glNamedBufferSubData)LoadOpenGLFunctionPointer("glNamedBufferSubData");
		if(!OpenGL->glNamedBufferSubData) { Result = false; }

		OpenGL->glBindVertexBuffer = (type_glBindVertexBuffer)LoadOpenGLFunctionPointer("glBindVertexBuffer");
		if(!OpenGL->glBindVertexBuffer) { Result = false; }

		OpenGL->glCreateProgram = (type_glCreateProgram)LoadOpenGLFunctionPointer("glCreateProgram");
		if(!OpenGL->glCreateProgram) { Result = false; }

		OpenGL->glDeleteProgram = (type_glDeleteProgram)LoadOpenGLFunctionPointer("glDeleteProgram");
		if(!OpenGL->glDeleteProgram) { Result = false; }

		OpenGL->glLinkProgram = (type_glLinkProgram)LoadOpenGLFunctionPointer("glLinkProgram");
		if(!OpenGL->glLinkProgram) { Result = false; }

		OpenGL->glActiveShaderProgram = (type_glActiveShaderProgram)LoadOpenGLFunctionPointer("glActiveShaderProgram");
		if(!OpenGL->glActiveShaderProgram) { Result = false; }

		OpenGL->glUseProgram = (type_glUseProgram)LoadOpenGLFunctionPointer("glUseProgram");
		if(!OpenGL->glUseProgram) { Result = false; }

		OpenGL->glGetProgramiv = (type_glGetProgramiv)LoadOpenGLFunctionPointer("glGetProgramiv");
		if(!OpenGL->glGetProgramiv) { Result = false; }

		OpenGL->glGetProgramInfoLog = (type_glGetProgramInfoLog)LoadOpenGLFunctionPointer("glGetProgramInfoLog");
		if(!OpenGL->glGetProgramInfoLog) { Result = false; }

		OpenGL->glCreateShader = (type_glCreateShader)LoadOpenGLFunctionPointer("glCreateShader");
		if(!OpenGL->glCreateShader) { Result = false; }

		OpenGL->glDeleteShader = (type_glDeleteShader)LoadOpenGLFunctionPointer("glDeleteShader");
		if(!OpenGL->glDeleteShader) { Result = false; }

		OpenGL->glAttachShader = (type_glAttachShader)LoadOpenGLFunctionPointer("glAttachShader");
		if(!OpenGL->glAttachShader) { Result = false; }

		OpenGL->glShaderSource = (type_glShaderSource)LoadOpenGLFunctionPointer("glShaderSource");
		if(!OpenGL->glShaderSource) { Result = false; }

		OpenGL->glCompileShader = (type_glCompileShader)LoadOpenGLFunctionPointer("glCompileShader");
		if(!OpenGL->glCompileShader) { Result = false; }

		OpenGL->glGetShaderiv = (type_glGetShaderiv)LoadOpenGLFunctionPointer("glGetShaderiv");
		if(!OpenGL->glGetShaderiv) { Result = false; }

		OpenGL->glGetShaderInfoLog = (type_glGetShaderInfoLog)LoadOpenGLFunctionPointer("glGetShaderInfoLog");
		if(!OpenGL->glGetShaderInfoLog) { Result = false; }

		OpenGL->glUniform1i = (type_glUniform1i)LoadOpenGLFunctionPointer("glUniform1i");
		if(!OpenGL->glUniform1i) { Result = false; }

		OpenGL->glUniform1f = (type_glUniform1f)LoadOpenGLFunctionPointer("glUniform1f");
		if(!OpenGL->glUniform1f) { Result = false; }

		OpenGL->glUniform3fv = (type_glUniform3fv)LoadOpenGLFunctionPointer("glUniform3fv");
		if(!OpenGL->glUniform3fv) { Result = false; }

		OpenGL->glUniformMatrix3fv = (type_glUniformMatrix3fv)LoadOpenGLFunctionPointer("glUniformMatrix3fv");
		if(!OpenGL->glUniformMatrix3fv) { Result = false; }

		OpenGL->glUniformMatrix4fv = (type_glUniformMatrix4fv)LoadOpenGLFunctionPointer("glUniformMatrix4fv");
		if(!OpenGL->glUniformMatrix4fv) { Result = false; }

		OpenGL->glUniform3f = (type_glUniform3f)LoadOpenGLFunctionPointer("glUniform3f");
		if(!OpenGL->glUniform3f) { Result = false; }

		OpenGL->glUniform4f = (type_glUniform4f)LoadOpenGLFunctionPointer("glUniform4f");
		if(!OpenGL->glUniform4f) { Result = false; }

		OpenGL->glGetUniformLocation = (type_glGetUniformLocation)LoadOpenGLFunctionPointer("glGetUniformLocation");
		if(!OpenGL->glGetUniformLocation) { Result = false; }

		OpenGL->glGenFramebuffers = (type_glGenFramebuffers)LoadOpenGLFunctionPointer("glGenFramebuffers");
		if(!OpenGL->glGenFramebuffers) { Result = false; }

		OpenGL->glBindFramebuffer = (type_glBindFramebuffer)LoadOpenGLFunctionPointer("glBindFramebuffer");
		if(!OpenGL->glBindFramebuffer) { Result = false; }

		OpenGL->glFramebufferTexture2D = (type_glFramebufferTexture2D)LoadOpenGLFunctionPointer("glFramebufferTexture2D");
		if(!OpenGL->glFramebufferTexture2D) { Result = false; }

		OpenGL->glFramebufferRenderbuffer = (type_glFramebufferRenderbuffer)LoadOpenGLFunctionPointer("glFramebufferRenderbuffer");
		if(!OpenGL->glFramebufferRenderbuffer) { Result = false; }

		OpenGL->glCheckFramebufferStatus = (type_glCheckFramebufferStatus)LoadOpenGLFunctionPointer("glCheckFramebufferStatus");
		if(!OpenGL->glCheckFramebufferStatus) { Result = false; }

		OpenGL->glClear = (type_glClear)LoadOpenGLFunctionPointer("glClear");
		if(!OpenGL->glClear) { Result = false; }

		OpenGL->glClearColor = (type_glClearColor)LoadOpenGLFunctionPointer("glClearColor");
		if(!OpenGL->glClearColor) { Result = false; }

		OpenGL->glClearTexImage = (type_glClearTexImage)LoadOpenGLFunctionPointer("glClearTexImage");
		if(!OpenGL->glClearTexImage) { Result = false; }

		OpenGL->wglSwapIntervalEXT = (type_wglSwapIntervalEXT)LoadOpenGLFunctionPointer("wglSwapIntervalEXT");
		if(!OpenGL->wglSwapIntervalEXT) { Result = false; }

    }
else
{
    Result = false;
}

    return(Result);
}
