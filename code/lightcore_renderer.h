#define STB_IMAGE_IMPLEMENTATION
#include <stb/stb_image.h>

enum texture_wrapping
{
    TextureWrap_Unavailable = 0,
    
    TextureWrap_CLAMP_TO_EDGE,
    TextureWrap_CLAMP_TO_BORDER, 
    TextureWrap_MIRRORED_REPEAT, 
    TextureWrap_REPEAT,
    TextureWrap_MIRROR_CLAMP_TO_EDGE,
};

enum texture_filter
{
    TextureFilter_Unavailable = 0,
    
    TextureFilter_NEAREST,
    TextureFilter_LINEAR,
    TextureFilter_NEAREST_MIPMAP_NEAREST,
    TextureFilter_LINEAR_MIPMAP_NEAREST,
    TextureFilter_NEAREST_MIPMAP_LINEAR,
    TextureFilter_LINEAR_MIPMAP_LINEAR,
};

enum render_status
{
    RenderStatus_Unavailable = 0,
    
    RenderStatus_LoadToStorage,
    RenderStatus_InTransit,
    RenderStatus_Available,
    RenderStatus_FailedToLoad
};

enum render_texture_type
{
    RenderTextureType_Unavailable = 0,
    
    RenderTextureType_DIFFUSE,
    RenderTextureType_SPECULAR,
    RenderTextureType_NORMAL,
    
    RenderTextureType_DEFAULT_DIFFUSE,
    RenderTextureType_DEFAULT_NORMAL,
    RenderTextureType_DEFAULT_SPECULAR,
};

enum texture_op_status
{
    TextureOpStatus_Uninitialized = 0,
    
    TextureOpStatus_Ignored,
    TextureOpStatus_PendingUpload,
    TextureOpStatus_UploadAvailable,
    TextureOpStatus_UploadFailed,
    TextureOpStatus_GenerateMipmaps,
    TextureOpStatus_SetTextureParameters,
};

enum render_pass_type
{
    RenderPassType_Unspecified = 0,
    
    RenderPassType_ForwardPass,
    RenderPassType_Shadowmapping,
    RenderPassType_Postprocess,
};

enum render_pass_flags
{
    RenderPass_ClearColor = 0x1,
    RenderPass_ClearDepth = 0x2,
    
    RenderPass_DefaultFramebuffer = 0x4,
    
    RenderPass_Default = (RenderPass_ClearColor | RenderPass_ClearDepth | RenderPass_DefaultFramebuffer),
    RenderPass_OffscreenRender = (RenderPass_ClearColor | RenderPass_ClearDepth)
};

#define RenderEntry(type) RenderEntryType_##type

enum render_entry_type
{
    RenderEntryType_render_entry_clear_color,
    RenderEntryType_render_entry_clear_depth,
    
    RenderEntryType_render_entry_forwardpass_begin,
    RenderEntryType_render_entry_forwardpass_end,
    
    RenderEntryType_render_entry_postprocesspass_begin,
    RenderEntryType_render_entry_postprocesspass_end,
    
    RenderEntryType_render_entry_upload_vertex_buffer_data,
    RenderEntryType_render_entry_upload_buffer_data,
    
    RenderEntryType_render_entry_setup_asset,
    RenderEntryType_render_entry_mesh_draw,
    RenderEntryType_render_entry_asset_draw_indexed,
    
    RenderEntryType_render_entry_camera_update,
};

struct platform_renderer
{
    memory_arena *RendererArena;
    
    u32 RenderOutputWidth;
    u32 RenderOutputHeight;
    u32 RenderOutputScale;
    
    u32 WindowDrawRegionWidth;
    u32 WindowDrawRegionHeight;
};

struct texture_parameters
{
    texture_wrapping WrapS;
    texture_wrapping WrapT;
    texture_wrapping WrapR;
    
    texture_filter MinFilter;
    texture_filter MagFilter;
    
    f32 AnisotropicLevel;
};

struct render_texture
{
    u32 ID;
    
    u32 Width;
    u32 Height;
    u32 ColorChannels;
    
    char Path[512];
    
    render_status Status;
    render_texture_type Type;
    
    buffer RawData;
    platform_file_info FileInfo;
};

struct texture_op
{
    texture_op *Next;
    texture_op_status Status;
    
    render_texture *Texture;
    
    b32 GenerateMipMaps;
    b32 GenerateID;
    
    u32 Width;
    u32 Height;
    u32 ColorChannels;
    
    u32 MipmapLevels;
    
    texture_parameters TextureParameters;
    
    buffer TextureData;
};

struct texture_op_queue
{
    u32 NextFreeOpSlot;
    texture_op *Head;
    texture_op *Tail;
    texture_op Operations[256];
};

struct camera_matrices
{
    mat4x4 CameraViewMatrix;
    mat4x4 CameraPerspectiveMatrix;
    mat4x4 CameraViewPerspectiveMatrix;
};

struct render_setup
{
    b32 EnableVSync;
    b32 EnableGammaCorrection;
};

struct window_settings
{
    b32 Fullscreen;
};

struct game_render_commands
{
    mmsz PushBufferSize;
    u8 *PushBufferBase;
    u8 *PushBufferAt;
    
    u32 BufferObjectsCount;
    u32 NextBufferID;
    
    render_setup CurrentSettings;
    window_settings CurrentWindowSettings;
    
    u32 RenderOutputWidth;
    u32 RenderOutputHeight;
    u32 RenderOutputScale;
    
    texture_op_queue TextureOpQueue;
};

struct textured_vertex
{
    vec3 Position;
    vec3 Normal;
    vec2 UVCoords;
    vec3 Color;
    vec3 Tangent;
    vec3 Bitangent;
};

struct transform
{
    vec3 Position;
    vec3 Rotation;
    vec3 Scale;
    
    b32 UpdateTransform;
};

struct relational_node
{
    char Name[128];
    
    struct mesh *MeshHeadRef;
    
    relational_node *Parent;
    relational_node *LevelNextNode;
    relational_node *SublevelNextNode;
    
    mat4x4 LocalMatrix;
    mat4x4 WorldMatrix;
    
    b32 Used;
};

struct mesh
{
    char Name[128];
    
    u32 MaterialID;
    
    u32 VerticesOffset;
    u32 VerticesCount;
    
    mmsz IndicesOffset;
    u32 IndicesCount;
    
    mat4x4 WorldMatrix;
    
    mesh *Next;
    mesh *NextRef;
    
    b32 InUse;
};

struct mesh_list
{
    mesh *Head;
    mesh *Tail;
    
    u32 TotalMeshes;
};

struct asset
{
    char Path[1024];
    
    u32 BufferID;
    render_status Status;
    
    u32 VerticesCount;
    u32 IndicesCount;
    
    mesh_list Meshes;
    transform Transform;
    relational_node *RootNode;
};

union render_textures
{
    struct
    {
        render_texture *Diffuse;
        render_texture *Specular;
        render_texture *Normal;
    };
    render_texture *Data[3];
};

introspect(category:test) struct material
{
    char Name[128];
    
    render_textures Textures;
    
    vec4 AmbientColor;
    vec4 DiffuseColor;
    vec4 SpecularColor;
    vec4 TransparentColor;
    f32 Shininess;
    f32 ShininessStrength;
    f32 Opacity;
    f32 TransparencyFactor;
    
    b32 TwoSided;
};

struct mesh_storage
{
    mesh *Meshes;
    
    u32 Used;
    u32 Capacity;
};

struct texture_load_entry_data
{
    char *Path;
    texture_op *TextureOp;
    buffer TextureData;
};

struct scene_storage;
struct asset_load_entry_data
{
    asset *Asset;
    scene_storage *Storage;
    game_render_commands *RenderCommands;
};

struct material_storage
{
    material *Materials;
    
    u32 NextMaterialSlot;
    u32 Capacity;
};

struct asset_storage
{
    asset *Assets;
    u32 Used;
    u32 Capacity;
    
    ring_buffer AssetTransientStorage;
    
    asset_load_entry_data LoadEntries[256];
    u32 NextLoadEntry;
};

struct nodes_storage
{
    relational_node *Nodes;
    
    u32 Used;
    u32 Capacity;
};

struct textures_storage
{
    render_texture *Textures;
    u32 Used;
    u32 Capacity;
    
    pool_allocator MemoryPool;
    
    texture_load_entry_data LoadEntries[256];
    u32 NextLoadEntry;
    
    r64 ElapsedTime;
};

struct alignas(16) point_light
{
    vec4 Position;
    
    vec4 Ambient;
    vec4 Diffuse;
    vec4 Specular;
    
    b32 UpdateRequired;
};

struct pointlight_storage
{
    u32 BufferID;
    u32 BufferLocation;
    LC_Array(point_light) PointLights;
};

struct scene_storage
{
    memory_arena SceneArena;
    
    asset_storage AssetStorage;
    mesh_storage MeshStorage;
    material_storage MaterialStorage;
    textures_storage RenderTexturesStorage;
    nodes_storage NodesStorage;
    pointlight_storage PointLightsStorage;
};

struct render_pass
{
    u32 Flags;
    render_pass_type Type;
    
    game_render_commands *Commands;
};

struct render_entry_header
{
    render_entry_type Type;
};

struct render_entry_forwardpass_begin
{
    b32 OffscreenRender;
};

struct render_entry_postprocesspass_begin
{
    u32 _Placeholder;
};

struct render_entry_clear_color
{
    v4 Color;
    b32 FullClear;
};

struct render_entry_upload_vertex_buffer_data
{
    u32 BufferID;
    
    void *Data;
    
    mmsz VerticesSize;
    mmsz IndicesSize;
    
    u32 *Flags;
};

struct render_entry_upload_buffer_data
{
    b32 LargeBuffer;
    u32 BufferID;
    u32 BufferLocation;
    smp Offset;
    void *Data;
    mmsz Size;
};

struct render_entry_setup_asset
{
    u32 BufferID;
    b32 IndexedBuffer;
};

struct render_entry_mesh_draw
{
    u32 VerticesOffset;
    u32 VerticesCount;
    
    mmsz IndicesOffset;
    u32 IndicesCount;
    
    material *Material;
    mat4x4 ModelMatrix;
};

struct render_entry_asset_draw_indexed
{
    u32 BufferID;
    
    u32 Counts[1024];
    mmsz Offsets[1024];
    
    u32 Size;
    
    u32 DiffuseTextureID;
    u32 SpecularTextureID;
    u32 NormalTextureID;
    
    render_pass_type RenderPassType;
};

struct render_entry_camera_update
{
    camera_matrices CameraTransforms;
    vec3 CameraPosition;
};

// TODO(Cristian): This doesn't feel like it belongs in the renderer...? Should maybe go into utils and be generalized?
struct hash_slot
{
    render_texture *CachedTexture;
    hash_slot *NextHashSlot;
};

struct hash_table
{
    hash_slot HashSlots[1024];
};

internal b32
AreEqual(render_setup *PreviousSetup, render_setup *CurrentSetup)
{
    b32 Result = MemoryIsEqual(sizeof(render_setup), PreviousSetup, CurrentSetup);
    return(Result);
}

internal b32
AreEqual(window_settings *PreviousSettings, window_settings *CurrentSettings)
{
    b32 Result = MemoryIsEqual(sizeof(window_settings), PreviousSettings, CurrentSettings);
    return(Result);
}

#define RENDERER_BEGIN_FRAME(name) game_render_commands * name(platform_renderer *Renderer)
typedef RENDERER_BEGIN_FRAME(renderer_begin_frame);

#define RENDERER_END_FRAME(name) void name(platform_renderer *Renderer, game_render_commands *Frame)
typedef RENDERER_END_FRAME(renderer_end_frame);