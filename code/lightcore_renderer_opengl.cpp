internal b32
IsValidAttribute(GLint Attribute)
{
    b32 Result = (Attribute != -1);
    return(Result);
}

internal void
OpenGLCreateProgram(open_gl *OpenGL, opengl_program *Program, shader_code Code)
{
    GLuint VertexShaderID = OPENGL_API.glCreateShader(GL_VERTEX_SHADER);
    OPENGL_API.glShaderSource(VertexShaderID, 1, &Code.VertexCode.Data, &(GLint)Code.VertexCode.Count);
    OPENGL_API.glCompileShader(VertexShaderID);
    
    GLuint FragmentShaderID = OPENGL_API.glCreateShader(GL_FRAGMENT_SHADER);
    OPENGL_API.glShaderSource(FragmentShaderID, 1, &Code.FragmentCode.Data, &(GLint)Code.FragmentCode.Count);
    OPENGL_API.glCompileShader(FragmentShaderID);
    
    GLuint ProgramID = OPENGL_API.glCreateProgram();
    OPENGL_API.glAttachShader(ProgramID, VertexShaderID);
    OPENGL_API.glAttachShader(ProgramID, FragmentShaderID);
    OPENGL_API.glLinkProgram(ProgramID);
    
    OPENGL_API.glValidateProgram(ProgramID);
    
    GLint Linked = false;
    OPENGL_API.glGetProgramiv(ProgramID, GL_LINK_STATUS, &Linked);
    if(!Linked)
    {
        GLsizei Ignored;
        char VertexErrors[4096];
        char FragmentErrors[4096];
        char ProgramErrors[4096];
        OPENGL_API.glGetShaderInfoLog(VertexShaderID, sizeof(VertexErrors), &Ignored, VertexErrors);
        OPENGL_API.glGetShaderInfoLog(FragmentShaderID, sizeof(FragmentErrors), &Ignored, FragmentErrors);
        OPENGL_API.glGetProgramInfoLog(ProgramID, sizeof(ProgramErrors), &Ignored, ProgramErrors);
        
        Assert(!"Shader linkage failed");
    }
    
    OPENGL_API.glDeleteShader(VertexShaderID);
    OPENGL_API.glDeleteShader(FragmentShaderID);
    
    Program->Handle = ProgramID;
    Program->PositionAttribID  = OPENGL_API.glGetAttribLocation(ProgramID, "PositionAttrib");
    Program->NormalAttribID    = OPENGL_API.glGetAttribLocation(ProgramID, "NormalAttrib");
    Program->UVAttribID        = OPENGL_API.glGetAttribLocation(ProgramID, "UVAttrib");
    Program->ColorAttribID     = OPENGL_API.glGetAttribLocation(ProgramID, "ColorAttrib");
    Program->TangentAttribID   = OPENGL_API.glGetAttribLocation(ProgramID, "TangentAttrib");
    Program->BitangentAttribID = OPENGL_API.glGetAttribLocation(ProgramID, "BitangentAttrib");
    OPENGL_API.glCreateVertexArrays(1, &Program->VertexArrayID);
    
    u32 VertPComp  = MemberNumberOfComponents(textured_vertex, Position,  f32);
    u32 VertNComp  = MemberNumberOfComponents(textured_vertex, Normal,    f32);
    u32 VertUVComp = MemberNumberOfComponents(textured_vertex, UVCoords,  f32);
    u32 VertCComp  = MemberNumberOfComponents(textured_vertex, Color,     f32);
    u32 VertTComp  = MemberNumberOfComponents(textured_vertex, Tangent,   f32);
    u32 VertBTComp = MemberNumberOfComponents(textured_vertex, Bitangent, f32);
    
    GLuint VertexArrayObject = Program->VertexArrayID;
    if(VertexArrayObject)
    {
        u32 BindingIndex = 0;
        
        if(IsValidAttribute(Program->PositionAttribID))
        {
            OPENGL_API.glEnableVertexArrayAttrib(VertexArrayObject, Program->PositionAttribID);
            OPENGL_API.glVertexArrayAttribFormat(VertexArrayObject, Program->PositionAttribID, VertPComp, GL_FLOAT, GL_FALSE, offsetof(textured_vertex, Position));
            OPENGL_API.glVertexArrayAttribBinding(VertexArrayObject, Program->PositionAttribID, BindingIndex);
        }
        
        if(IsValidAttribute(Program->NormalAttribID))
        {
            OPENGL_API.glEnableVertexArrayAttrib(VertexArrayObject, Program->NormalAttribID);
            OPENGL_API.glVertexArrayAttribFormat(VertexArrayObject, Program->NormalAttribID, VertNComp, GL_FLOAT, GL_FALSE, offsetof(textured_vertex, Normal));
            OPENGL_API.glVertexArrayAttribBinding(VertexArrayObject, Program->NormalAttribID, BindingIndex);
        }
        
        if(IsValidAttribute(Program->UVAttribID))
        {
            OPENGL_API.glEnableVertexArrayAttrib(VertexArrayObject, Program->UVAttribID);
            OPENGL_API.glVertexArrayAttribFormat(VertexArrayObject, Program->UVAttribID, VertUVComp, GL_FLOAT, GL_FALSE, offsetof(textured_vertex, UVCoords));
            OPENGL_API.glVertexArrayAttribBinding(VertexArrayObject, Program->UVAttribID, BindingIndex);
        }
        
        if(IsValidAttribute(Program->ColorAttribID))
        {
            OPENGL_API.glEnableVertexArrayAttrib(VertexArrayObject, Program->ColorAttribID);
            OPENGL_API.glVertexArrayAttribFormat(VertexArrayObject, Program->ColorAttribID, VertCComp, GL_FLOAT, GL_FALSE, offsetof(textured_vertex, Color));
            OPENGL_API.glVertexArrayAttribBinding(VertexArrayObject, Program->ColorAttribID, BindingIndex);
        }
        
        if(IsValidAttribute(Program->TangentAttribID))
        {
            OPENGL_API.glEnableVertexArrayAttrib(VertexArrayObject, Program->TangentAttribID);
            OPENGL_API.glVertexArrayAttribFormat(VertexArrayObject, Program->TangentAttribID, VertTComp, GL_FLOAT, GL_FALSE, offsetof(textured_vertex, Tangent));
            OPENGL_API.glVertexArrayAttribBinding(VertexArrayObject, Program->TangentAttribID, BindingIndex);
        }
        
        if(IsValidAttribute(Program->BitangentAttribID))
        {
            OPENGL_API.glEnableVertexArrayAttrib(VertexArrayObject, Program->BitangentAttribID);
            OPENGL_API.glVertexArrayAttribFormat(VertexArrayObject, Program->BitangentAttribID, VertBTComp, GL_FLOAT, GL_FALSE, offsetof(textured_vertex, Bitangent));
            OPENGL_API.glVertexArrayAttribBinding(VertexArrayObject, Program->BitangentAttribID, BindingIndex);
        }
    }
}

internal void
UseProgramBegin(open_gl *OpenGL, opengl_program *Program)
{
    Assert(Program);
    
    OPENGL_API.glUseProgram(Program->Handle);
}

internal void
UseProgramEnd(open_gl *OpenGL)
{
    OPENGL_API.glUseProgram(0);
}

// NOTE(Cristian): Function assumes VAO and IBO located in the same buffer
internal void
ApplyObjectAttributes(open_gl *OpenGL, opengl_program *Program, GLuint BufferObject, b32 IndexedBuffer = false)
{
    Assert(Program);
    
    u32 BindingIndex = 0;
    OPENGL_API.glVertexArrayVertexBuffer(Program->VertexArrayID, BindingIndex, BufferObject, 0, sizeof(textured_vertex));
    
    if(IndexedBuffer)
    {
        OPENGL_API.glVertexArrayElementBuffer(Program->VertexArrayID, BufferObject);
    }
}

internal u32
PushTextureID(open_gl *OpenGL)
{
    u32 Result = 0;
    if(OpenGL->NextTextureID < ArrayCount(OpenGL->TextureHandles))
    {
        Result = OpenGL->NextTextureID++;
    }
    return(Result);
}

internal GLenum
OpenGLGetWrapMethod(texture_wrapping Wrapping)
{
    GLenum Result = 0;
    switch(Wrapping)
    {
        case TextureWrap_CLAMP_TO_EDGE:
        {
            Result = GL_CLAMP_TO_EDGE;
        } break;
        
        case TextureWrap_CLAMP_TO_BORDER:
        {
            Result = GL_CLAMP_TO_BORDER;
        } break;
        
        case TextureWrap_MIRRORED_REPEAT:
        {
            Result = GL_MIRRORED_REPEAT;
        } break;
        
        case TextureWrap_REPEAT:
        {
            Result = GL_REPEAT;
        } break;
        
        case TextureWrap_MIRROR_CLAMP_TO_EDGE:
        {
            Result = GL_MIRROR_CLAMP_TO_EDGE;
        } break;
    }
    
    return(Result);
}

internal GLenum
OpenGLGetFilterMethod(texture_filter Filter)
{
    GLenum Result = 0;
    
    switch(Filter)
    {
        case TextureFilter_NEAREST:
        {
            Result = GL_NEAREST;
        } break;
        
        case TextureFilter_LINEAR:
        {
            Result = GL_LINEAR;
        } break;
        
        case TextureFilter_NEAREST_MIPMAP_NEAREST:
        {
            Result = GL_NEAREST_MIPMAP_NEAREST;
        } break;
        
        case TextureFilter_LINEAR_MIPMAP_NEAREST:
        {
            Result = GL_LINEAR_MIPMAP_NEAREST;
        } break;
        
        case TextureFilter_NEAREST_MIPMAP_LINEAR:
        {
            Result = GL_NEAREST_MIPMAP_LINEAR;
        } break;
        
        case TextureFilter_LINEAR_MIPMAP_LINEAR:
        {
            Result = GL_LINEAR_MIPMAP_LINEAR;
        } break;
    }
    
    return(Result);
}

internal void
OpenGLSetTextureParameters(open_gl *OpenGL, texture_op *TextureOperation, u32 TextureHandle)
{
    if(TextureOperation->TextureParameters.AnisotropicLevel)
    {
        OPENGL_API.glTextureParameterf(TextureHandle, GL_TEXTURE_MAX_ANISOTROPY, TextureOperation->TextureParameters.AnisotropicLevel);
    }
    
    if(TextureOperation->TextureParameters.WrapS)
    {
        OPENGL_API.glTextureParameteri(TextureHandle, GL_TEXTURE_WRAP_S, OpenGLGetWrapMethod(TextureOperation->TextureParameters.WrapS));
    }
    
    if(TextureOperation->TextureParameters.WrapT)
    {
        OPENGL_API.glTextureParameteri(TextureHandle, GL_TEXTURE_WRAP_T, OpenGLGetWrapMethod(TextureOperation->TextureParameters.WrapT));
    }
    
    if(TextureOperation->TextureParameters.WrapR)
    {
        OPENGL_API.glTextureParameteri(TextureHandle, GL_TEXTURE_WRAP_R, OpenGLGetWrapMethod(TextureOperation->TextureParameters.WrapR));
    }
    
    if(TextureOperation->TextureParameters.MinFilter)
    {
        OPENGL_API.glTextureParameteri(TextureHandle, GL_TEXTURE_MIN_FILTER, OpenGLGetFilterMethod(TextureOperation->TextureParameters.MinFilter));
    }
    
    if(TextureOperation->TextureParameters.MagFilter)
    {
        OPENGL_API.glTextureParameteri(TextureHandle, GL_TEXTURE_MAG_FILTER, OpenGLGetFilterMethod(TextureOperation->TextureParameters.MagFilter));
    }
}

internal void
OpenGLManageTextureOperations(open_gl *OpenGL, texture_op_queue *Queue)
{
    u32 TextureLoads = 0;
    
    texture_op *PrevNode = 0;
    texture_op *CurrentNode = Queue->Head;
    while(CurrentNode)
    {
        b32 RemoveNode = false;
        
        texture_op *TextureOperation = CurrentNode;
        texture_op_status OpStatus = CurrentNode->Status;
        
        render_texture *Texture = TextureOperation->Texture;
        
        if(OpStatus == TextureOpStatus_PendingUpload)
        {
            
        }
        else if(OpStatus == TextureOpStatus_Ignored)
        {
            RemoveNode = true;
        }
        else if(OpStatus == TextureOpStatus_UploadFailed)
        {
            Texture->Status = RenderStatus_FailedToLoad;
            
            //SetRingBufferBlockFlags(TextureOperation->Data, BufferBlock_Delete);
            
            RemoveNode = true;
        }
        else if(OpStatus == TextureOpStatus_UploadAvailable)
        {
            if(Texture->Type != RenderTextureType_DEFAULT_DIFFUSE &&
               Texture->Type != RenderTextureType_DEFAULT_NORMAL &&
               Texture->Type != RenderTextureType_DEFAULT_SPECULAR)
            {
                Texture->ID = PushTextureID(OpenGL);
            }
            
            Texture->Width = TextureOperation->Width;
            Texture->Height = TextureOperation->Height;
            Texture->ColorChannels = TextureOperation->ColorChannels;
            
            GLenum InternalStorageFormat = 0;
            GLenum PixelDataFormat = 0;
            GLenum PixelDataType = 0;
            
            b32 GammaCorrection = false;
            if(OpenGL->Settings.EnableGammaCorrection && Texture->Type == RenderTextureType_DIFFUSE)
            {
                GammaCorrection = true;
            }
            
            u32 ColorChannels = TextureOperation->ColorChannels;
            if(ColorChannels == 1)
            {
                InternalStorageFormat = GL_R8;
                PixelDataFormat = GL_RED;
                PixelDataType = GL_UNSIGNED_BYTE;
            }
            else if(ColorChannels == 3)
            {
                InternalStorageFormat = GammaCorrection ? GL_SRGB8 : GL_RGB8;
                PixelDataFormat = GL_RGB;
                PixelDataType = GL_UNSIGNED_BYTE;
            }
            else if(ColorChannels == 4)
            {
                InternalStorageFormat = GammaCorrection ? GL_SRGB8_ALPHA8 : GL_RGBA8;
                PixelDataFormat = GL_RGBA;
                PixelDataType = GL_UNSIGNED_BYTE;
            }
            
            u32 TextureHandle = OpenGL->TextureHandles[Texture->ID];
            if(!TextureHandle) 
            {
                //OPENGL_API.glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
                
                OPENGL_API.glCreateTextures(GL_TEXTURE_2D, 1, &TextureHandle);
                
                OPENGL_API.glTextureStorage2D(TextureHandle, TextureOperation->MipmapLevels, InternalStorageFormat, TextureOperation->Width, TextureOperation->Height);
                
                OpenGL->TextureHandles[TextureOperation->Texture->ID] = TextureHandle;
            }
            
            OPENGL_API.glTextureSubImage2D(TextureHandle,
                                           0,
                                           0,
                                           0,
                                           TextureOperation->Width,
                                           TextureOperation->Height,
                                           PixelDataFormat,
                                           PixelDataType,
                                           TextureOperation->TextureData.Data);
            
            TextureLoads++;
            
            texture_parameters Zero = {};
            if(!MemoryIsEqual(sizeof(TextureOperation->TextureParameters), &TextureOperation->TextureParameters, &Zero))
            {
                OpenGLSetTextureParameters(OpenGL, TextureOperation, TextureHandle);
            }
            
            if(TextureOperation->MipmapLevels)
            {
                OPENGL_API.glGenerateTextureMipmap(TextureHandle);
            }
            
            Texture->Status = RenderStatus_Available;
            
            //SetRingBufferBlockFlags(TextureOperation->Data, BufferBlock_Delete);
            
            char Msg[512];
            sprintf_s(Msg, "TEXTURE_OP Uploaded data for texture: %s \n", Texture->Path);
            OutputDebugStringA(Msg);
            
            RemoveNode = true;
        }
        else if(OpStatus == TextureOpStatus_GenerateMipmaps)
        {
            if(Texture->ID)
            {
                u32 TextureHandle = OpenGL->TextureHandles[Texture->ID];
                OPENGL_API.glGenerateTextureMipmap(TextureHandle);
            }
            
            RemoveNode = true;
        }
        else if(OpStatus == TextureOpStatus_SetTextureParameters)
        {
            if(Texture->ID)
            {
                u32 TextureHandle = OpenGL->TextureHandles[Texture->ID];
                OpenGLSetTextureParameters(OpenGL, TextureOperation, TextureHandle);
            }
            
            RemoveNode = true;
        }
        
        if(RemoveNode)
        {
            if(CurrentNode == Queue->Tail)
            {
                Queue->Tail = PrevNode;
            }
            
            if(PrevNode)
            {
                PrevNode->Next = CurrentNode->Next;
                CurrentNode = CurrentNode->Next;
            }
            else
            {
                CurrentNode = Queue->Head = CurrentNode->Next;
            }
            
            *TextureOperation = {};
        }
        else
        {
            PrevNode = CurrentNode;
            CurrentNode = CurrentNode->Next;
        }
    }
    
    if(TextureLoads)
    {
        char Buffer[256];
        sprintf_s(Buffer, "===== Loaded %u textures this frame! ====== \n", TextureLoads);
        OutputDebugStringA(Buffer);
    }
}

internal void
OpenGLChangeSettings(open_gl *OpenGL, render_setup *CurrentSettings)
{
    if(OpenGL->Settings.EnableVSync != CurrentSettings->EnableVSync)
    {
        PlatformSetVSync(CurrentSettings->EnableVSync);
    }
    
    OpenGL->Settings = *CurrentSettings;
}

internal GLuint
OpenGLCreateFBOColorAttachment(open_gl *OpenGL, u32 Width, u32 Height)
{
    GLuint ColorAttachmentHandle;
    OPENGL_API.glCreateTextures(GL_TEXTURE_2D, 1, &ColorAttachmentHandle);
    
    u32 MipmapLevels = 1;
    OPENGL_API.glTextureStorage2D(ColorAttachmentHandle, 1, GL_RGB8, Width, Height);
    
    OPENGL_API.glTextureParameteri(ColorAttachmentHandle, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    OPENGL_API.glTextureParameteri(ColorAttachmentHandle, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    
    OPENGL_API.glTextureParameterf(ColorAttachmentHandle, GL_TEXTURE_MAX_ANISOTROPY, 16.0f);
    
    return(ColorAttachmentHandle);
}

internal GLuint
OpenGLCreateRenderbuffer(open_gl *OpenGL, GLenum InternalFormat, u32 Width, u32 Height)
{
    GLuint RBOHandle;
    OPENGL_API.glCreateRenderbuffers(1, &RBOHandle);
    OPENGL_API.glNamedRenderbufferStorage(RBOHandle, InternalFormat, Width, Height);
    return(RBOHandle);
}

internal opengl_rendertarget_info
OpenGLCreateRenderTarget_ForwardPass(open_gl *OpenGL, u32 RenderWidth, u32 RenderHeight)
{
    opengl_rendertarget_info Result = {};
    
    GLuint FBOHandle;
    OPENGL_API.glCreateFramebuffers(1, &FBOHandle);
    
    // Create color attachment texture
    GLuint ColorAttachmentHandle = OpenGLCreateFBOColorAttachment(OpenGL, RenderWidth, RenderHeight);
    OPENGL_API.glNamedFramebufferTexture(FBOHandle, GL_COLOR_ATTACHMENT0, ColorAttachmentHandle, 0);
    
    // Create depth + stencil attachment for renderbuffer object
    GLuint RBOHandle = OpenGLCreateRenderbuffer(OpenGL, GL_DEPTH24_STENCIL8, RenderWidth, RenderHeight);
    OPENGL_API.glNamedFramebufferRenderbuffer(FBOHandle, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, RBOHandle);
    
    if(OPENGL_API.glCheckNamedFramebufferStatus(FBOHandle, GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE)
    {
        Result.RenderTargetHandle = FBOHandle;
        Result.DepthBufferHandle = RBOHandle;
        Result.OutputTextureHandle = ColorAttachmentHandle;
    }
    else
    {
        Assert("FBO completeness check failed");
    }
    return(Result);
}

internal game_render_commands *
OpenGLBeginFrame(open_gl *OpenGL)
{
    game_render_commands *Frame = &OpenGL->RenderCommands;
    Frame->PushBufferSize = sizeof(OpenGL->PushBufferMemory);
    Frame->PushBufferBase = OpenGL->PushBufferMemory;
    Frame->PushBufferAt = OpenGL->PushBufferMemory;
    
    Frame->BufferObjectsCount = ArrayCount(OpenGL->BufferObjects);
    Frame->NextBufferID = OpenGL->NextBufferID;
    
    Frame->RenderOutputWidth = OpenGL->RenderOutputWidth;
    Frame->RenderOutputHeight = OpenGL->RenderOutputHeight;
    Frame->RenderOutputScale = OpenGL->RenderOutputScale;
    
    return(Frame);
}

internal void
OpenGLEndFrame(open_gl *OpenGL, game_render_commands *Frame)
{
    OpenGL->NextBufferID = Frame->NextBufferID;
    
    OpenGLManageTextureOperations(OpenGL, &Frame->TextureOpQueue);
    
    if(!AreEqual(&OpenGL->Settings, &Frame->CurrentSettings))
    {
        OpenGLChangeSettings(OpenGL, &Frame->CurrentSettings);
    }
    
    if(OpenGL->WindowDrawRegionWidth != OpenGL->Header.WindowDrawRegionWidth || 
       OpenGL->WindowDrawRegionHeight != OpenGL->Header.WindowDrawRegionHeight)
    {
        OPENGL_API.glViewport(0, 0, OpenGL->Header.WindowDrawRegionWidth, OpenGL->Header.WindowDrawRegionHeight);
        
        OpenGL->WindowDrawRegionWidth = OpenGL->Header.WindowDrawRegionWidth;
        OpenGL->WindowDrawRegionHeight = OpenGL->Header.WindowDrawRegionHeight;
    }
    
    if(OpenGL->RenderOutputWidth != Frame->RenderOutputWidth ||
       OpenGL->RenderOutputHeight != Frame->RenderOutputHeight ||
       OpenGL->RenderOutputScale != Frame->RenderOutputScale)
    {
        OPENGL_API.glDeleteFramebuffers(1, &OpenGL->ForwardPassRenderTarget.RenderTargetHandle);
        OPENGL_API.glDeleteTextures(1, &OpenGL->ForwardPassRenderTarget.OutputTextureHandle);
        OPENGL_API.glDeleteRenderbuffers(1, &OpenGL->ForwardPassRenderTarget.DepthBufferHandle);
        
        OpenGL->ForwardPassRenderTarget.RenderTargetHandle = 0;
        
        OpenGL->RenderOutputWidth = Frame->RenderOutputWidth;
        OpenGL->RenderOutputHeight = Frame->RenderOutputHeight;
        OpenGL->RenderOutputScale = Frame->RenderOutputScale;
    }
    
    //OPENGL_API.glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    
    u8 *HeaderAt = Frame->PushBufferBase;
    while(HeaderAt < Frame->PushBufferAt)
    {
        render_entry_header *Header = (render_entry_header *)HeaderAt;
        HeaderAt += sizeof(render_entry_header);
        void *CommandPayload = (u8 *)Header + sizeof(render_entry_header);
        switch(Header->Type)
        {
            case RenderEntry(render_entry_clear_color):
            {
                HeaderAt += sizeof(render_entry_clear_color);
                
                render_entry_clear_color *ClearEntry = (render_entry_clear_color *)CommandPayload;
                
                GLbitfield Mask = GL_COLOR_BUFFER_BIT;
                
                v4 ClearColor = ClearEntry->Color;
                if(!IsVectorEqual(OpenGL->BufferClearColor, ClearColor))
                {
                    OPENGL_API.glClearColor(ClearColor.r, ClearColor.g, ClearColor.b, ClearColor.a);
                    OpenGL->BufferClearColor = ClearColor;
                }
                
                if(ClearEntry->FullClear)
                {
                    Mask += GL_DEPTH_BUFFER_BIT;
                }
                
                OPENGL_API.glClear(Mask);
            } break;
            
            case RenderEntry(render_entry_camera_update):
            {
                HeaderAt += sizeof(render_entry_camera_update);
                
                render_entry_camera_update *Entry = (render_entry_camera_update *)CommandPayload;
                
                OPENGL_API.glNamedBufferSubData(OpenGL->CameraTransformsHandle, 0, sizeof(mat4x4) * 3, &Entry->CameraTransforms);
                
                if(OpenGL->CurrentProgram)
                {
                    OPENGL_API.glUniform3fv(OPENGL_API.glGetUniformLocation(OpenGL->CurrentProgram->Handle, "CameraPos"), 1, Entry->CameraPosition.Data);
                }
            } break;
            
            case RenderEntryType_render_entry_clear_depth:
            {
                OPENGL_API.glClear(GL_DEPTH_BUFFER_BIT);
            } break;
            
            case RenderEntry(render_entry_forwardpass_begin):
            {
                HeaderAt += sizeof(render_entry_forwardpass_begin);
                
                render_entry_forwardpass_begin *Entry = (render_entry_forwardpass_begin *)CommandPayload;
                if(Entry->OffscreenRender)
                {
                    u32 RenderResWidth = (OpenGL->RenderOutputWidth * OpenGL->RenderOutputScale) / 100;
                    u32 RenderResHeight = (OpenGL->RenderOutputHeight * OpenGL->RenderOutputScale) / 100;
                    
                    if(!OpenGL->ForwardPassRenderTarget.RenderTargetHandle)
                    {
                        OpenGL->ForwardPassRenderTarget = OpenGLCreateRenderTarget_ForwardPass(OpenGL, RenderResWidth, RenderResHeight);
                    }
                    
                    if(OpenGL->ForwardPassRenderTarget.RenderTargetHandle)
                    {
                        OPENGL_API.glBindFramebuffer(GL_FRAMEBUFFER, OpenGL->ForwardPassRenderTarget.RenderTargetHandle);
                        OPENGL_API.glViewport(0, 0, RenderResWidth, RenderResHeight);
                        OPENGL_API.glEnable(GL_DEPTH_TEST);
                        
                        OpenGL->RenderPassInterAttachment = OpenGL->ForwardPassRenderTarget.OutputTextureHandle;
                    }
                }
                
                OpenGL->CurrentProgram = OpenGL->ForwardPassLightingProgram;
                
                UseProgramBegin(OpenGL, OpenGL->CurrentProgram);
                
                OPENGL_API.glUniform1i(OPENGL_API.glGetUniformLocation(OpenGL->CurrentProgram->Handle, "GammaCorrected"), OpenGL->Settings.EnableGammaCorrection);
                
                OPENGL_API.glBindVertexArray(OpenGL->CurrentProgram->VertexArrayID);
            } break;
            
            case RenderEntryType_render_entry_forwardpass_end:
            {
                UseProgramEnd(OpenGL);
                
                OPENGL_API.glBindVertexArray(0);
                OPENGL_API.glBindFramebuffer(GL_FRAMEBUFFER, 0);
                
                OpenGL->CurrentProgram = 0;
            } break;
            
            case RenderEntry(render_entry_postprocesspass_begin):
            {
                HeaderAt += sizeof(render_entry_postprocesspass_begin);
                
                render_entry_postprocesspass_begin *Entry = (render_entry_postprocesspass_begin *)CommandPayload;
                
                OPENGL_API.glViewport(0, 0, OpenGL->WindowDrawRegionWidth, OpenGL->WindowDrawRegionHeight);
                OPENGL_API.glDisable(GL_DEPTH_TEST);
                
                OpenGL->CurrentProgram = OpenGL->PostprocessPassProgram;
            } break;
            
            case RenderEntryType_render_entry_postprocesspass_end:
            {
                UseProgramBegin(OpenGL, OpenGL->CurrentProgram);
                
                if(OpenGL->RenderPassInterAttachment)
                {
                    OPENGL_API.glBindTextureUnit(0, OpenGL->RenderPassInterAttachment);
                }
                
                ApplyObjectAttributes(OpenGL, OpenGL->CurrentProgram, OpenGL->PostprocessHandle);
                
                OPENGL_API.glBindVertexArray(OpenGL->CurrentProgram->VertexArrayID);
                OPENGL_API.glDrawArrays(GL_TRIANGLES, 0, 6);
                OPENGL_API.glBindVertexArray(0);
                
                UseProgramEnd(OpenGL);
                
                OpenGL->CurrentProgram = 0;
            } break;
            
            case RenderEntry(render_entry_setup_asset):
            {
                HeaderAt += sizeof(render_entry_setup_asset);
                
                render_entry_setup_asset *Entry = (render_entry_setup_asset *)CommandPayload;
                
                GLuint BufferHandle = OpenGL->BufferObjects[Entry->BufferID];
                if(BufferHandle)
                {
                    ApplyObjectAttributes(OpenGL, OpenGL->CurrentProgram, BufferHandle, Entry->IndexedBuffer);
                }
            } break;
            
            case RenderEntry(render_entry_mesh_draw):
            {
                HeaderAt += sizeof(render_entry_mesh_draw);
                
                render_entry_mesh_draw *Entry = (render_entry_mesh_draw *)CommandPayload;
                
                material *Material = Entry->Material;
                
                GLuint DefaultDiffuseTextureHandle = OpenGL->TextureHandles[0];
                
                for(u32 Index = 0;
                    Index < ArrayCount(Material->Textures.Data);
                    ++Index)
                {
                    render_texture *Texture = Material->Textures.Data[Index];
                    if(Texture)
                    {
                        GLuint TextureHandle = OpenGL->TextureHandles[Texture->ID];
                        
                        switch(Texture->Type)
                        {
                            case RenderTextureType_DIFFUSE:
                            {
                                if(Texture->Status == RenderStatus_FailedToLoad)
                                {
                                    TextureHandle = DefaultDiffuseTextureHandle;
                                }
                                
                                OPENGL_API.glBindTextureUnit(0, TextureHandle);
                            } break;
                            
                            case RenderTextureType_SPECULAR:
                            {
                                OPENGL_API.glBindTextureUnit(1, TextureHandle);
                            } break;
                            
                            case RenderTextureType_NORMAL:
                            {
                                OPENGL_API.glBindTextureUnit(2, TextureHandle);
                            } break;
                        }
                    }
                }
                
                b32 SampleDiffuseTexture = Material->Textures.Diffuse ? true : false;
                OPENGL_API.glUniform1i(OPENGL_API.glGetUniformLocation(OpenGL->CurrentProgram->Handle, "SampleDiffuseTexture"), SampleDiffuseTexture);
                
                vec4 DiffuseColor = Material->DiffuseColor;
                if(Material->Textures.Diffuse && Material->Textures.Diffuse->Status == RenderStatus_FailedToLoad)
                {
                    DiffuseColor = V4(1.0f);
                    
                    OPENGL_API.glUniform1i(OPENGL_API.glGetUniformLocation(OpenGL->CurrentProgram->Handle, "IsDefaultDiffuseTexture"), true);
                }
                else
                {
                    OPENGL_API.glUniform1i(OPENGL_API.glGetUniformLocation(OpenGL->CurrentProgram->Handle, "IsDefaultDiffuseTexture"), false);
                }
                
                OPENGL_API.glUniform4f(OPENGL_API.glGetUniformLocation(OpenGL->CurrentProgram->Handle, "DiffuseColorContribution"), DiffuseColor.r, DiffuseColor.g, DiffuseColor.b, DiffuseColor.a);
                
                OPENGL_API.glUniformMatrix4fv(OPENGL_API.glGetUniformLocation(OpenGL->CurrentProgram->Handle, "ModelMatrix"), 1, GL_FALSE, Entry->ModelMatrix.DataL);
                
                mat3x3 CorrectedNormalMatrix = M3x3(Transpose(Inverse(Entry->ModelMatrix)));
                OPENGL_API.glUniformMatrix3fv(OPENGL_API.glGetUniformLocation(OpenGL->CurrentProgram->Handle, "CorrectedNormalMatrix"), 1, GL_FALSE, CorrectedNormalMatrix.DataL);
                
                if(Entry->IndicesCount)
                {
                    OPENGL_API.glDrawElements(GL_TRIANGLES, Entry->IndicesCount, GL_UNSIGNED_INT, (void *)Entry->IndicesOffset);
                } 
                else if(Entry->VerticesCount)
                {
                    OPENGL_API.glDrawArrays(GL_TRIANGLES, Entry->VerticesOffset, Entry->VerticesCount);
                }
                
                for(u32 Index = 0;
                    Index < ArrayCount(Material->Textures.Data);
                    ++Index)
                {
                    OPENGL_API.glBindTextureUnit(Index, 0);
                }
            } break;
            
            case RenderEntry(render_entry_upload_vertex_buffer_data):
            {
                HeaderAt += sizeof(render_entry_upload_vertex_buffer_data);
                
                render_entry_upload_vertex_buffer_data *Entry = (render_entry_upload_vertex_buffer_data *)CommandPayload;
                
                AssertMsg(Entry->BufferID < ArrayCount(OpenGL->BufferObjects), "Invalid asset buffer ID!");
                
                mmsz TotalAllocationSize = Entry->VerticesSize + Entry->IndicesSize;
                
                GLuint BufferHandle = OpenGL->BufferObjects[Entry->BufferID];
                if(!BufferHandle)
                {
                    OPENGL_API.glCreateBuffers(1, &BufferHandle);
                    OPENGL_API.glNamedBufferStorage(BufferHandle, TotalAllocationSize, 0, GL_DYNAMIC_STORAGE_BIT);
                    
                    OpenGL->BufferObjects[Entry->BufferID] = BufferHandle;
                }
                
                OPENGL_API.glNamedBufferSubData(BufferHandle, 0, TotalAllocationSize, Entry->Data);
                
                if(Entry->Flags)
                {
                    *Entry->Flags = BufferBlock_Delete;
                }
            } break;
            
            case RenderEntry(render_entry_upload_buffer_data):
            {
                HeaderAt += sizeof(render_entry_upload_buffer_data);
                
                render_entry_upload_buffer_data *Entry = (render_entry_upload_buffer_data *)CommandPayload;
                
                if(Entry->BufferID)
                {
                    GLuint BufferHandle = OpenGL->BufferObjects[Entry->BufferID];
                    if(!BufferHandle)
                    {
                        OPENGL_API.glCreateBuffers(1, &BufferHandle);
                        OPENGL_API.glNamedBufferStorage(BufferHandle, Entry->Size, 0, GL_DYNAMIC_STORAGE_BIT);
                        
                        if(Entry->LargeBuffer)
                        {
                            OPENGL_API.glBindBufferBase(GL_SHADER_STORAGE_BUFFER, Entry->BufferLocation, BufferHandle);
                        }
                        else
                        {
                            OPENGL_API.glBindBufferBase(GL_UNIFORM_BUFFER, Entry->BufferLocation, BufferHandle);
                        }
                        
                        OpenGL->BufferObjects[Entry->BufferID] = BufferHandle;
                    }
                    
                    if(Entry->Data)
                    {
                        OPENGL_API.glNamedBufferSubData(BufferHandle, Entry->Offset, Entry->Size, Entry->Data);
                    }
                }
            } break;
        }
    }
}