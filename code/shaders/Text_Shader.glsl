@vertex_shader
#version 450 core

layout (location = 0) in vec2 PositionIn;
layout (location = 1) in vec2 TexCoordsIn;

out vec2 TexCoords;

uniform mat4 Projection;

void main()
{
	TexCoords = TexCoordsIn;
    gl_Position = Projection * vec4(PositionIn, 0.0, 1.0);
}


@fragment_shader
#version 450 core
	
in vec2 TexCoords;
out vec4 OutColor;

uniform sampler2D Text;
uniform vec3 TextColor;

void main()
{
	vec4 SampledColor = vec4(1.0, 1.0, 1.0, texture(Text, TexCoords).r);
	OutColor = SampledColor * vec4(TextColor, 1.0);
}