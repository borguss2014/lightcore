@vertex_shader
#version 450 core
	
layout (location = 0) in vec3 PositionIn;
layout (location = 1) in vec3 NormalIn;
layout (location = 2) in vec2 TextureCoordIn;

out vec3 Normal;
out vec2 TextureCoord;

uniform mat4 Model;
uniform mat4 View;
uniform mat4 Projection;

void main()
{
	mat3 NormalMatrix = mat3(transpose(inverse(Model)));
    Normal = NormalMatrix * NormalIn;
    TextureCoord = TextureCoordIn;
	
	gl_Position = Projection * View * Model * vec4(PositionIn, 1.0);
}


@fragment_shader
#version 450 core
	
out vec4 OutColor;

void main()
{
	OutColor = vec4(0.04, 0.28, 0.26, 1.0);
	
	// Gamma encoding
	OutColor.rgb = pow(OutColor.rgb, vec3(1.0/2.2));
}