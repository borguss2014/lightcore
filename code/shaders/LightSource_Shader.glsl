@vertex_shader
#version 450 core

layout (location = 0) in vec3 PositionIn;

layout (std140, binding = 1) uniform Transformation_Matrices
{
	mat4 ViewMatrix;
	mat4 ProjectionMatrix;
};
	
uniform mat4 ModelMatrix;

void main()
{
    gl_Position = ProjectionMatrix * ViewMatrix * ModelMatrix * vec4(PositionIn, 1.0);
}

@fragment_shader
#version 450 core

out vec4 OutColor;

uniform vec3 LightColor;

void main()
{
    OutColor = vec4(LightColor, 1.0);
}
