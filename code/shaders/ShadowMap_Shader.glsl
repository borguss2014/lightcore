@vertex_shader
#version 450 core

layout (location = 0) in vec3 PositionIn;

uniform mat4 LightSpaceMatrix;
uniform mat4 ModelMatrix;

void main()
{
    gl_Position = LightSpaceMatrix * ModelMatrix * vec4(PositionIn, 1.0);
}

@fragment_shader
#version 450 core

void main()
{             
    // gl_FragDepth = gl_FragCoord.z;
}