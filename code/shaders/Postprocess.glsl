@VERTEX_SHADER
#version 450 core

layout (location = 0) in vec3 PositionAttrib;
layout (location = 1) in vec2 UVAttrib;

out vec2 UVAttribFrag;

void main()
{
	UVAttribFrag = UVAttrib;
	gl_Position = vec4(PositionAttrib.x, PositionAttrib.y, 0.0, 1.0);
}

@FRAGMENT_SHADER
#version 450 core

out vec4 OutputColor;

in vec2 UVAttribFrag;

layout(binding = 0) uniform sampler2D TextureSampler_Diffuse;

//uniform int GammaCorrected;

void main()
{
	OutputColor = texture(TextureSampler_Diffuse, UVAttribFrag);

	// Color inversion
	// OutputColor = vec4(vec3(1.0 - texture(TextureSampler_Diffuse, UVAttribFrag)), 1.0);

	//Grayscale
	//float Average = (OutputColor.r + OutputColor.g + OutputColor.b) / 3.0;
	// float Average = 0.2126 * OutputColor.r + 0.7152 * OutputColor.g + 0.0722 * OutputColor.b;
	// OutputColor = vec4(vec3(Average), 1.0);
}
