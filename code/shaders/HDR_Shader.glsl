@vertex_shader
#version 450 core

layout (location = 0) in vec3 PositionIn;
layout (location = 1) in vec2 TextureIn;

out vec2 TextureCoords;
	
void main()
{
	TextureCoords = TextureIn;
    gl_Position = vec4(PositionIn, 1.0);
}



@fragment_shader
#version 450 core

in vec2 TextureCoords;
out vec4 OutColor;

uniform sampler2D HDRBuffer;

void main()
{
	vec3 HDRData = texture(HDRBuffer, TextureCoords).rgb;
	
	// Exposure level
	
	// LOW EXPOSURE (better detail in bright areas)
	float ExposureFactor = 0.1;
	
	//float ExposureFactor = 1.0; // STANDARD EXPOSURE
	
	// HIGH EXPOSURE (better detail in shadowed areas)
	//float ExposureFactor = 5.0;
	
    vec3 ExposureHDR = vec3(1.0) - exp(-HDRData * ExposureFactor);
	
	// Gamma encoding
	float GammaFactor = 2.2;
	
	vec3 GammaEncodedData;
	
	bool HDREnabled = true;
	if(HDREnabled)
	{
		GammaEncodedData = pow(ExposureHDR, vec3(1.0/GammaFactor));
	}
	else
	{
		GammaEncodedData = pow(HDRData, vec3(1.0/GammaFactor));
	}
	
    OutColor = vec4(GammaEncodedData, 1.0);
}