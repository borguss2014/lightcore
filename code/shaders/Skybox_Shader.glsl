@vertex_shader
#version 450 core

layout (location = 0) in vec3 PositionIn;

out vec3 TextureCoords;

uniform mat4 View;
uniform mat4 Projection;

void main()
{
	TextureCoords = PositionIn;
	vec4 Pos = Projection * View * vec4(PositionIn, 1.0);
	gl_Position = Pos.xyww;
}


@fragment_shader
#version 450 core

out vec4 OutColor;

in vec3 TextureCoords;
uniform samplerCube SkyboxTexture;
	
void main()
{
	OutColor = texture(SkyboxTexture, TextureCoords);
	
	// Gamma encoding
	OutColor.rgb = pow(OutColor.rgb, vec3(1.0/2.2));
}