@vertex_shader
#version 450 core

layout (location = 0) in vec3 PositionIn;
layout (location = 1) in vec3 NormalIn;
layout (location = 2) in vec2 TextureCoordIn;
layout (location = 3) in vec3 TangentIn;

uniform mat4 Model;
uniform mat4 View;
uniform mat4 Projection;

uniform vec3 CameraPos;

out vec3 Normal;
out vec3 FragmentPos;
out vec2 TextureCoord;

out mat3 TBNMatrix;
out mat3 InverseTBN;
out vec3 TangentFragmentPos;
out vec3 TangentCameraPos;
out vec3 TangentNormal;

out vec3 Test;

void main()
{
	mat3 NormalMatrix = mat3(transpose(inverse(Model)));
	Normal = NormalMatrix * NormalIn;

	vec3 T = normalize(NormalMatrix * TangentIn);
	vec3 N = normalize(NormalMatrix * NormalIn);
	T = normalize(T - dot(T, N) * N);
	vec3 B = cross(N, T);

	TBNMatrix = mat3(T, B, N);
	InverseTBN = transpose(TBNMatrix);

	FragmentPos = vec3(Model * vec4(PositionIn, 1.0));

	TangentFragmentPos = InverseTBN * FragmentPos;
	TangentCameraPos = InverseTBN * CameraPos;
	TangentNormal = InverseTBN * NormalMatrix * NormalIn;

    TextureCoord = TextureCoordIn;

	Test = vec3(TextureCoord, 0.0);

    gl_Position = Projection * View * vec4(FragmentPos, 1.0);
}



@fragment_shader
#version 450 core

#define NR_POINTLIGHTS 1

struct material
{
  sampler2D Diffuse;
  sampler2D Specular;
  float Shininess;
};

struct spotlight
{
    vec3 Position;

    vec3 Ambient;
    vec3 Diffuse;
    vec3 Specular;

    float Constant;
    float Linear;
    float Quadratic;
};

struct directional_light
{
    vec3 Direction;

    vec3 Ambient;
    vec3 Diffuse;
    vec3 Specular;
};

struct point_light
{
    vec3 Position;

    vec3 Ambient;
    vec3 Diffuse;
    vec3 Specular;

    float Constant;
    float Linear;
    float Quadratic;
};

in vec3 Normal;
in vec3 FragmentPos;
in vec2 TextureCoord;

in mat3 TBNMatrix;
in mat3 InverseTBN;
in vec3 TangentCameraPos;
in vec3 TangentFragmentPos;
in vec3 TangentNormal;

in vec3 Test;

out vec4 OutColor;

uniform vec3 CameraPos;
uniform vec3 ObjectColor;

uniform material Material;
uniform spotlight SpotLight;
uniform directional_light DirectionalLight;
uniform point_light PointLight[NR_POINTLIGHTS];

vec3 ProcessSpotlight(spotlight SpotLight);
vec3 ProcessDirectionalLight(directional_light DirectionalLight);
vec3 ProcessPointLight(point_light PointLight);
float CalculateShadow(vec3 LightPos);

uniform sampler2D texture_diffuse1;
uniform sampler2D texture_specular1;
uniform sampler2D texture_emission1;
uniform sampler2D texture_normal1;
uniform sampler2D texture_height1;
uniform samplerCube ShadowMap;

uniform bool UseNormalMap;
uniform bool UseDisplacementMap;

vec3 SampleOffsetDirections[20] = vec3[]
(
   vec3( 1,  1,  1), vec3( 1, -1,  1), vec3(-1, -1,  1), vec3(-1,  1,  1),
   vec3( 1,  1, -1), vec3( 1, -1, -1), vec3(-1, -1, -1), vec3(-1,  1, -1),
   vec3( 1,  1,  0), vec3( 1, -1,  0), vec3(-1, -1,  0), vec3(-1,  1,  0),
   vec3( 1,  0,  1), vec3(-1,  0,  1), vec3( 1,  0, -1), vec3(-1,  0, -1),
   vec3( 0,  1,  1), vec3( 0, -1,  1), vec3( 0, -1, -1), vec3( 0,  1, -1)
);

void main()
{
	vec3 result;
    //vec3 result = ProcessSpotlight(SpotLight);
    //result += ProcessDirectionalLight(DirectionalLight);

    for(uint i=0; i<NR_POINTLIGHTS; i++)
    {
        result += ProcessPointLight(PointLight[i]);
    }

	//vec3 Emission = texture(texture_emission1, TextureCoord).rgb;
	//result += Emission;

    OutColor = vec4(result, 1.0);


//	float LineWidth = 2;
//	float LeftBound = 1360 - LineWidth / 2;
//	float RightBound = 1360 + LineWidth / 2;
//	if(gl_FragCoord.x >= LeftBound && gl_FragCoord.x <= RightBound)
//	{
//		OutColor = vec4(1.0, 0.0, 0.0, 1.0);
//	}

	// Z BUFFER VISUALIZATION
	/* float near = 0.1;
	float far = 100.0;

	float depth = gl_FragCoord.z;
	float z = depth * 2.0 - 1.0;
	OutColor = vec4(vec3( ( (2.0 * near * far) / (far + near - z * (far - near) ) ) / far ), 1.0); */

	// SHADOWMAP VISUALIZATION
//	vec3 LightDir = FragmentPos - PointLight[0].Position;
//	float ClosestDepth = texture(ShadowMap, LightDir).r;
//	OutColor = vec4(vec3(ClosestDepth), 1.0);

	// NORMAL MAP VISUALIZATION
//	vec3 NormalizedNormal = texture(texture_normal1, TextureCoord).rgb;
//	OutColor.rgb = NormalizedNormal;

	// DISPLACEMENT MAP VISUALIZATION
//	vec3 Displacement = texture(texture_height1, TextureCoord).rgb;
//	OutColor.rgb = Displacement;

//	vec3 Texture = texture(texture_diffuse1, TextureCoord).rgb;
//	OutColor.rgb = Texture;
}

float CalculateShadow(vec3 LightPos)
{
	float FarPlane = 50.0;

	vec3 LightDir = FragmentPos - LightPos;
	float CurrentDepth = length(LightDir);

	float ShadowAverage = 0.0;
	float Bias    = 0.2;
	int Samples = 20;

	float ViewDistance = length(CameraPos - FragmentPos);
	float DiskRadius = (1.0 + (ViewDistance / FarPlane)) / FarPlane;

	for(int i = 0; i < Samples; i++)
	{
		float ClosestDepth = texture(ShadowMap, LightDir + SampleOffsetDirections[i] * DiskRadius).r;
		ClosestDepth *= FarPlane;

		if(CurrentDepth - Bias > ClosestDepth)
		{
			ShadowAverage += 1.0;
		}
	}

	ShadowAverage /= float(Samples);

	return(ShadowAverage);
}

// STANDARD PARALLAX
//vec2 ParallaxMapping(vec2 TexCoords, vec3 ViewDir)
//{
//	float Height = texture(texture_height1, TexCoords).r;
//	vec2 DisplacedPVector = ViewDir.xy * (Height * 0.1);
//
//	return(TexCoords - DisplacedPVector);
//}

// STEEP PARALLAX
vec2 ParallaxMapping(vec2 TexCoords, vec3 ViewDir, vec3 SurfaceNormal)
{
	 // number of depth layers
    //const float NumLayers = 50;
	const float MinLayers = 8.0;
	const float MaxLayers = 200.0;
	float NumLayers = mix(MaxLayers, MinLayers, max(dot(SurfaceNormal, ViewDir), 0.0));
	NumLayers = 50.0;
    // calculate the size of each layer
    float LayerDepth = 1.0 / NumLayers;
    // depth of current layer
    float CurrentLayerDepth = 0.0;
    // the amount to shift the texture coordinates per layer (from vector P)
    vec2 P = ViewDir.xy * 0.1;
    vec2 DeltaTexCoords = P / NumLayers;

	// get initial values
	vec2  CurrentTexCoords     = TexCoords;
	float CurrentDepthMapValue = texture(texture_height1, CurrentTexCoords).r;

	while(CurrentLayerDepth < CurrentDepthMapValue)
	{
		// shift texture coordinates along direction of P
		CurrentTexCoords -= DeltaTexCoords;
		// get depthmap value at current texture coordinates
		CurrentDepthMapValue = texture(texture_height1, CurrentTexCoords).r;
		// get depth of next layer
		CurrentLayerDepth += LayerDepth;
	}

	// get texture coordinates before collision (reverse operations)
    vec2 PrevTexCoords = CurrentTexCoords + DeltaTexCoords;

    // get depth after and before collision for linear interpolation
    float AfterDepth  = CurrentDepthMapValue - CurrentLayerDepth;
    float BeforeDepth = texture(texture_height1, PrevTexCoords).r - CurrentLayerDepth + LayerDepth;

    // interpolation of texture coordinates
    float Weight = AfterDepth / (AfterDepth - BeforeDepth);
    vec2 FinalTexCoords = PrevTexCoords * Weight + CurrentTexCoords * (1.0 - Weight);

	return FinalTexCoords;
	//return CurrentTexCoords;
}

vec3 ProcessPointLight(point_light PointLight)
{
	vec3 TangentLightPos = InverseTBN * PointLight.Position;
	vec3 SurfaceNormal = texture(texture_normal1, TextureCoord).rgb;
	SurfaceNormal = normalize(SurfaceNormal * 2.0 - 1.0);

	vec3 ViewDir = normalize(TangentCameraPos - TangentFragmentPos);
	vec2 ParallaxTexCoords = ParallaxMapping(TextureCoord, ViewDir, SurfaceNormal);
	//ParallaxTexCoords = TextureCoord;

	vec2 TexCoords = UseDisplacementMap ? ParallaxTexCoords : TextureCoord;

	vec3 Color = texture(texture_diffuse1, TexCoords).rgb;

    // Ambient light
    vec3 AmbientLight = Color * PointLight.Ambient;

    // Diffuse light
	vec3 NormalizedNormal;
	if(UseNormalMap)
	{
		NormalizedNormal = texture(texture_normal1, TexCoords).rgb;
		NormalizedNormal = normalize(NormalizedNormal * 2.0 - 1.0);
	}
	else
	{
		NormalizedNormal = normalize(TangentNormal);
	}

    vec3 LightDir = normalize(TangentLightPos - TangentFragmentPos);
    float DiffuseFactor = max(dot(LightDir, NormalizedNormal), 0.0);
    //vec3 DiffuseLight = Color * DiffuseFactor * PointLight.Diffuse;
	vec3 DiffuseLight = Color * DiffuseFactor * vec3(200.0);

    // Specular light
	// 		Phong model
    //vec3 ReflectVector = reflect(-LightDir, NormalizedNormal);
    //float SpecularFactor = pow(max(dot(ViewDir, ReflectVector), 0.0), Material.Shininess);

	// 		Blinn-Phong model
	vec3 HalfwayVector = normalize(ViewDir + LightDir);
	float SpecularFactor = pow(max(dot(HalfwayVector, NormalizedNormal), 0.0), Material.Shininess);

    vec3 SpecularLight;
	SpecularLight = texture(texture_specular1, TexCoords).rgb * SpecularFactor * PointLight.Specular;

	// LIGHT ATTENUATION
    float DistanceToLight = length(TangentLightPos - TangentFragmentPos);
	float LightAttenuation = 1.0 / (DistanceToLight * DistanceToLight);

    AmbientLight *= LightAttenuation;
    DiffuseLight *= LightAttenuation;
    SpecularLight *= LightAttenuation;

	float ShadowFactor = CalculateShadow(PointLight.Position);
	vec3 Result = AmbientLight + (1.0 - ShadowFactor) * (DiffuseLight + SpecularLight);
	//vec3 Result = AmbientLight + (DiffuseLight + SpecularLight);
    return(Result);
}

//vec3 ProcessDirectionalLight(directional_light DirectionalLight)
//{
//    // Ambient light
//    vec3 AmbientLight = texture(texture_diffuse1, TextureCoord).rgb * DirectionalLight.Ambient;
//
//    // Diffuse light
//    vec3 NormalizedNormal = normalize(Normal);
//    vec3 LightDir = normalize(-DirectionalLight.Direction);
//    float DiffuseFactor = max(dot(NormalizedNormal, LightDir), 0.0);
//    vec3 DiffuseLight = texture(texture_diffuse1, TextureCoord).rgb * DiffuseFactor * DirectionalLight.Diffuse;
//
//    // Specular light
//    vec3 ViewDir = normalize(CameraPos - FragmentPos);
//
//	// Phong model
//   // vec3 ReflectVector = reflect(-LightDir, NormalizedNormal);
//   // float SpecularFactor = pow(max(dot(ViewDir, ReflectVector), 0.0), Material.Shininess);
//
//	// Blinn-Phong model
//	vec3 HalfwayVector = normalize(ViewDir + LightDir);
//	float SpecularFactor = pow(max(dot(HalfwayVector, NormalizedNormal), 0.0), Material.Shininess);
//
//    vec3 SpecularLight = texture(texture_specular1, TextureCoord).rgb * SpecularFactor * DirectionalLight.Specular;
//
//    float ShadowFactor = CalculateShadow(DirectionalLight.Direction);
//    vec3 Result = AmbientLight + (1.0 - ShadowFactor) * (DiffuseLight + SpecularLight);
//    return(Result);
//}
//
//vec3 ProcessSpotlight(spotlight SpotLight)
//{
//    // Ambient light
//    vec3 AmbientLight = texture(texture_diffuse1, TextureCoord).rgb * SpotLight.Ambient;
//
//    // Diffuse light
//    vec3 NormalizedNormal = normalize(Normal);
//    vec3 LightDir = normalize(SpotLight.Position - FragmentPos);
//    float DiffuseFactor = max(dot(NormalizedNormal, LightDir), 0.0);
//    vec3 DiffuseLight = texture(texture_diffuse1, TextureCoord).rgb * DiffuseFactor * SpotLight.Diffuse;
//
//    // Specular light
//    vec3 ViewDir = normalize(CameraPos - FragmentPos);
//
//	// Phong model
////    vec3 ReflectVector = reflect(-LightDir, NormalizedNormal);
////    float SpecularFactor = pow(max(dot(ViewDir, ReflectVector), 0.0), Material.Shininess);
//
//	// Blinn-Phong model
//	vec3 HalfwayVector = normalize(ViewDir + LightDir);
//	float SpecularFactor = pow(max(dot(HalfwayVector, NormalizedNormal), 0.0), Material.Shininess);
//
//    vec3 SpecularLight = texture(texture_specular1, TextureCoord).rgb * SpecularFactor * SpotLight.Specular;
//
//    float DistanceToLight = length(SpotLight.Position - FragmentPos);
//    float LightAttenuation = 1.0 / (SpotLight.Constant +
//                                    SpotLight.Linear * DistanceToLight +
//                                    SpotLight.Quadratic * DistanceToLight * DistanceToLight);
//
//	LightAttenuation = 1.0 / (DistanceToLight * DistanceToLight);
//
//    AmbientLight *= LightAttenuation;
//    DiffuseLight *= LightAttenuation;
//    SpecularLight *= LightAttenuation;
//
//    float SpotlightInnerCutoff = cos(radians(15));
//    float SpotlightOuterCutoff = cos(radians(17.5));
//    vec3 SpotlightDir = vec3(0.0, -1.0, 0.0);
//
//    float SpotlightFactor = dot(LightDir, normalize(-SpotlightDir));
//    float SmoothSpotlight = (SpotlightFactor - SpotlightOuterCutoff) / (SpotlightInnerCutoff - SpotlightOuterCutoff);
//    SmoothSpotlight = clamp(SmoothSpotlight, 0.0, 1.0);
//
//    DiffuseLight *= SmoothSpotlight;
//    SpecularLight *= SmoothSpotlight;
//
//    vec3 Result = AmbientLight + DiffuseLight + SpecularLight;
//    return(Result);
//}
