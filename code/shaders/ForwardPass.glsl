@VERTEX_SHADER
#version 450 core

layout (location = 0) in vec3 PositionAttrib;
layout (location = 1) in vec3 NormalAttrib;
layout (location = 2) in vec2 UVAttrib;
layout (location = 3) in vec3 ColorAttrib;
layout (location = 4) in vec3 TangentAttrib;
layout (location = 5) in vec3 BitangentAttrib;

out vec3 NormalAttribFrag;
out vec2 UVAttribFrag;
out vec3 ColorAttribFrag;

out vec3 NormalVector;
out vec3 FragmentPosition;

out mat3 TBNMatrix;

layout (std140, binding = 0) uniform CameraTransforms
{
	mat4 ViewMatrix;
	mat4 PerspectiveMatrix;
	mat4 ViewPerspectiveMatrix;
};

uniform mat4 ModelMatrix;
uniform mat3 CorrectedNormalMatrix;

void main()
{
	NormalAttribFrag = NormalAttrib;
	ColorAttribFrag = ColorAttrib;
	UVAttribFrag = UVAttrib;

	FragmentPosition = vec3(vec4(PositionAttrib, 1.0) * ModelMatrix);
	NormalVector = CorrectedNormalMatrix * NormalAttrib;

	vec3 Tangent = normalize(vec3(vec4(TangentAttrib, 0.0) * ModelMatrix));
	//vec3 Bitangent = normalize(vec3(vec4(BitangentAttrib, 0.0) * ModelMatrix));
	vec3 Normal = normalize(vec3(vec4(NormalAttrib, 0.0) * ModelMatrix));

	Tangent = normalize(Tangent - dot(Tangent, Normal) * Normal);
	vec3 Bitangent = normalize(cross(Normal, Tangent));

	TBNMatrix = transpose(mat3(Tangent, Bitangent, Normal));

	gl_Position = vec4(PositionAttrib, 1.0) * ModelMatrix * ViewPerspectiveMatrix;
}


@FRAGMENT_SHADER
#version 450 core

in vec3 NormalAttribFrag;
in vec2 UVAttribFrag;
in vec3 ColorAttribFrag;

in vec3 NormalVector;
in vec3 FragmentPosition;
in mat3 TBNMatrix;

out vec4 OutputColor;

layout(binding = 0) uniform sampler2D TextureSampler_Diffuse;
layout(binding = 1) uniform sampler2D TextureSampler_Specular;
layout(binding = 2) uniform sampler2D TextureSampler_Normal;

uniform vec4 DiffuseColorContribution;
uniform int SampleDiffuseTexture;
uniform int IsDefaultDiffuseTexture;
uniform int GammaCorrected;

uniform vec3 CameraPos;

vec3 NormalizedNormalVector;

struct point_light
{
	vec4 Position;
    
    vec4 Ambient;
    vec4 Diffuse;
    vec4 Specular;

    bool UpdateRequired;
};

#define MAX_LIGHTS_STORAGE 64

layout (std430, binding = 2) buffer Point_Lights
{
	uint PointLightsCount;
	point_light PointLights[MAX_LIGHTS_STORAGE];
};

float 
Squared(float x)
{
	return x * x;
}

float
AttenuateNoCusp(float Distance, float Radius, float MaxIntensity, float FallOff)
{
	float Result = 0.0;
	float NormalizedDistance = Distance / Radius;
	if(NormalizedDistance < 1.0)
	{
		float SquaredNormalizedDistance = Squared(NormalizedDistance);
		Result = MaxIntensity * Squared(1 - SquaredNormalizedDistance) / (1 + FallOff * SquaredNormalizedDistance);
	}

	return(Result);
}

float
AttenuateWithCusp(float Distance, float Radius, float MaxIntensity, float FallOff)
{
	float Result = 0.0;
	float NormalizedDistance = Distance / Radius;
	if(NormalizedDistance < 1.0)
	{
		float SquaredNormalizedDistance = Squared(NormalizedDistance);
		Result = MaxIntensity * Squared(1 - SquaredNormalizedDistance) / (1 + FallOff * NormalizedDistance);
	}

	return(Result);
}

vec3
ProcessPointLight(uint LightIndex, vec3 SampledDiffuse, vec3 SampledSpecular)
{
    point_light PointLight = PointLights[LightIndex];
	float AmbientIntensity = PointLight.Ambient.a;
	float DiffuseIntensity = PointLight.Diffuse.a;
	float SpecularIntensity = PointLight.Specular.a;
	
	// Ambient lighting
	vec3 AmbientLight = vec3(PointLight.Ambient) * AmbientIntensity;
    
	// Diffuse lighting
	vec3 LightSourceDir = normalize(vec3(PointLight.Position) - FragmentPosition);
    
    //NormalizedNormalVector = normalize(NormalVector);
	float DiffuseFactor = max(0.0, dot(LightSourceDir, NormalizedNormalVector));
	vec3 DiffuseLight = DiffuseFactor * vec3(PointLight.Diffuse) * DiffuseIntensity;
    
	//Specular lighting
	vec3 CameraViewDir = normalize(CameraPos - FragmentPosition);
	vec3 HalfwayVector = normalize(LightSourceDir + CameraViewDir);
    
	float ShineFactor = 64;
	float SpecularFactor = pow(max(dot(NormalizedNormalVector, HalfwayVector), 0.0), ShineFactor);
	vec3 SpecularLight = SpecularFactor * vec3(PointLight.Diffuse) * SpecularIntensity;
	SpecularLight = SpecularLight * SampledSpecular;
    
	// Attenuation
	float DistanceToLight = length(vec3(PointLight.Position) - FragmentPosition);
	//float LightAttenuation = 1.0 / (1 + DistanceToLight * DistanceToLight);
	//float LightAttenuation = 1.0 / (1.0 + 0.14 * DistanceToLight + 0.07 * (DistanceToLight * DistanceToLight));

	float Radius = 8;
	float FallOff = 5;
	float LightAttenuation = AttenuateNoCusp(DistanceToLight, Radius, DiffuseIntensity, FallOff);
    
	vec3 PointLightContribution = ((AmbientLight + DiffuseLight) * SampledDiffuse + SpecularLight) * LightAttenuation;
	return(PointLightContribution);
}

void main()
{
	vec3 SampledDiffuseColor = vec3(DiffuseColorContribution);
	vec3 SampledNormal = texture(TextureSampler_Normal, UVAttribFrag).rgb;
	vec3 SampledSpecular = texture(TextureSampler_Specular, UVAttribFrag).rrr;

	NormalizedNormalVector = SampledNormal;
	NormalizedNormalVector = NormalizedNormalVector * 2.0 - 1.0;
	NormalizedNormalVector = normalize(NormalizedNormalVector * TBNMatrix);
	
	if(SampleDiffuseTexture == 1)
	{
		vec3 SampledDiffuse = texture(TextureSampler_Diffuse, UVAttribFrag).rgb;
		SampledDiffuseColor = SampledDiffuse;
	}

	vec3 FinalLightOutput = vec3(0.0);
	for(int LightsIndex = 0;
	    LightsIndex < PointLightsCount;
	    ++LightsIndex)
    {
    	FinalLightOutput += ProcessPointLight(LightsIndex, SampledDiffuseColor, SampledSpecular);
    }

    if(IsDefaultDiffuseTexture == 1)
	{
		OutputColor = vec4(SampledDiffuseColor, 1.0);
	}
	else
	{
		FinalLightOutput = FinalLightOutput;
		OutputColor = vec4(FinalLightOutput, 1.0);
	}

    // if(PointLightsCount == 0)
	// {
	// 	OutputColor = vec4(1.0, 0.0, 0.0, 1.0);
	// }
	// else if(PointLightsCount == 1)
	// {
	// 	OutputColor = vec4(0.0, 1.0, 0.0, 1.0);	
	// }
	// else if(PointLightsCount == 2)
	// {
	// 	OutputColor = vec4(0.0, 0.0, 1.0, 1.0);	
	// }

    //OutputColor = vec4(FinalLightOutput, 1.0);
    //OutputColor = vec4(SampledDiffuseColor, 1.0);
	
	//OutputColor = DiffuseColorContribution;
	//OutputColor = vec4(ColorAttribFrag, 1.0);
	//OutputColor = vec4(NormalAttribFrag, 1.0);
	//OutputColor = SampledNormalMap;
	//OutputColor = vec4(1.0, 0.0, 0.0, 1.0);
	//OutputColor = vec4(ViewMatrix[0][0], ViewMatrix[0][1], ViewMatrix[0][2], ViewMatrix[0][3]);

	//OutputColor = vec4(NormalizedNormalVector, 1.0);
	//OutputColor = vec4(normalize(NormalVector), 1.0);

	// Gamma encoding
    // (to get cancelled by the monitor automatically)
    if(GammaCorrected == 1)
	{
		float Gamma = 2.2;
    	OutputColor = vec4(pow(vec3(OutputColor), vec3(1.0 / Gamma)), OutputColor.a);
	}
}