@vertex_shader
#version 450 core

layout (location = 0) in vec3 PositionIn;
layout (location = 1) in vec2 TextureCoordIn;

out vec2 TextureCoord;

uniform mat4 Model;
uniform mat4 View;
uniform mat4 Projection;

void main()
{
	TextureCoord = TextureCoordIn;
	gl_Position = Projection * View * Model * vec4(PositionIn, 1.0);
}


@fragment_shader
#version 450 core

in vec2 TextureCoord;
out vec4 OutColor;

uniform sampler2D GrassTexture;
	
void main()
{
	vec4 FragmentColor = texture(GrassTexture, TextureCoord);
	
	if(FragmentColor.a < 0.1)
	{
		discard;
	}
	
	OutColor = FragmentColor;
	
	// Gamma encoding
	OutColor.rgb = pow(OutColor.rgb, vec3(1.0/2.2));
}