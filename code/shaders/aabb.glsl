@vertex_shader
#version 450 core

layout (location = 0) in vec3 PositionIn;

layout (std140, binding = 1) uniform Transformation_Matrices
{
	mat4 ViewMatrix;
	mat4 ProjectionMatrix;
	mat4 ProjectionViewMatrix;
};

uniform mat4 ModelMatrix;

void main()
{
	gl_Position = ProjectionViewMatrix * vec4(vec3(ModelMatrix * vec4(PositionIn, 1.0)), 1.0);
	//gl_Position = vec4(PositionIn, 1.0);
}

@fragment_shader
#version 450 core

out vec4 OutputColor;

void main()
{
	OutputColor = vec4(1.0, 1.0, 1.0, 1.0);
}