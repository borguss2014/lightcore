@VERTEX_SHADER
#version 450 core

layout (location = 0) in vec3 PositionIn;
layout (location = 1) in vec3 NormalIn;
layout (location = 2) in vec2 TextureCoordIn;

layout (std140, binding = 1) uniform Transformation_Matrices
{
	mat4 ViewMatrix;
	mat4 ProjectionMatrix;
	mat4 ProjectionViewMatrix;
};

uniform mat4 ModelMatrix;
uniform mat3 CorrectedNormalMatrix;

out vec3 NormalVector;
out vec3 FragmentPosition;
out vec2 TextureCoord;

void main()
{
    NormalVector = CorrectedNormalMatrix * NormalIn;
    
    FragmentPosition = vec3(ModelMatrix * vec4(PositionIn, 1.0));
    TextureCoord = TextureCoordIn;
    
    gl_Position = ProjectionViewMatrix * vec4(FragmentPosition, 1.0);
}

@FRAGMENT_SHADER
#version 450 core

#define MAX_TOTAL_POINT_LIGHTS 1
#define MAX_TOTAL_DIRECTIONAL_LIGHTS 4

#define SHADOWMAP_FACE_POSITIVE_X 0
#define SHADOWMAP_FACE_NEGATIVE_X 1
#define SHADOWMAP_FACE_POSITIVE_Y 2
#define SHADOWMAP_FACE_NEGATIVE_Y 3
#define SHADOWMAP_FACE_POSITIVE_Z 4
#define SHADOWMAP_FACE_NEGATIVE_Z 5

struct pointlight_fragposlightspace
{
    vec4 FragPosLightSpace[6];
};

struct point_light
{
	vec4 Position;
    
    vec4 Ambient;
    vec4 Diffuse;
    vec4 Specular;
	
	mat4 LightSpaceMatrix[6];
	uint ShadowmapTileIndices[6];
	
	int ShadowmapQuality;
	bool EnableShadows;
	
    bool UpdateRequired;
};

struct directional_light
{
	vec4 Ambient;
    vec4 Diffuse;
    vec4 Specular;
	
	mat4 LightSpaceMatrix;
	
	vec3 Direction;
	
	int ShadowmapQuality;
	bool EnableShadows;
	
	uint ShadowmapTileIndex;
    bool UpdateRequired;
};

layout (std430, binding = 0) buffer Point_Lights
{
	uint PointLightsNr;
	point_light PointLights[MAX_TOTAL_POINT_LIGHTS];
};

layout (std140, binding = 2) uniform Directional_Lights
{
	uint DirectionalLightsNr;
	directional_light DirectionalLights[MAX_TOTAL_DIRECTIONAL_LIGHTS];
};

// struct material_properties
// {
//   sampler2D DiffuseTexture;
//   sampler2D SpecularTexture;
//   float ShineFactor;
// };

in vec3 NormalVector;
in vec3 FragmentPosition;
in vec2 TextureCoord;

in pointlight_fragposlightspace PointFragPosLightSpace[MAX_TOTAL_POINT_LIGHTS];
in vec4 DirectionalFragPosLightSpace[MAX_TOTAL_DIRECTIONAL_LIGHTS];

out vec4 OutputColor;

uniform vec3 CameraPos;
uniform sampler2D texture_diffuse;

#define SHADOWMAP_QUALITY_NONE 0
#define SHADOWMAP_QUALITY_LOW 1
#define SHADOWMAP_QUALITY_MEDIUM 2
#define SHADOWMAP_QUALITY_HIGH 3
#define SHADOWMAP_QUALITY_ULTRA 4

uniform sampler2D ShadowmapAtlas[5];

struct atlas_info
{
    uint Size;
    uint TileSize;
};

#define MAX_ATLAS_INFO 5
layout (std140, binding = 3) uniform ShadowmapAtlas_Info
{
    atlas_info ShadowmapAtlasInfo[MAX_ATLAS_INFO];
};

uint
GetCubemapFace(vec3 Direction)
{
    uint FaceIndex;
    
    vec3 DirAbs = abs(Direction);
    if(DirAbs.z >= DirAbs.x && DirAbs.z >= DirAbs.y)
    {
        FaceIndex = Direction.z < 0 ? SHADOWMAP_FACE_NEGATIVE_Z : SHADOWMAP_FACE_POSITIVE_Z;
    }
    else if(DirAbs.y >= DirAbs.x)
    {
        FaceIndex = Direction.y < 0 ? SHADOWMAP_FACE_NEGATIVE_Y : SHADOWMAP_FACE_POSITIVE_Y;
    }
    else
    {
        FaceIndex = Direction.x < 0 ? SHADOWMAP_FACE_NEGATIVE_X : SHADOWMAP_FACE_POSITIVE_X;
    }
    
    return(FaceIndex);
}

//uint 
//GetCubemapFace(vec3 Direction)
//{
//float MaxComponent = max(max(abs(Direction.x), abs(Direction.y)), abs(Direction.z));
//uint FaceIdx = 0;
//
//if(Direction.x == MaxComponent)
//{
//FaceIdx = 0;
//}
//else if(-Direction.x == MaxComponent)
//{
//FaceIdx = 1;
//}
//else if(Direction.y == MaxComponent)
//{
//FaceIdx = 2;
//}
//else if(-Direction.y == MaxComponent)
//{
//FaceIdx = 3;
//}
//else if(Direction.z == MaxComponent)
//{
//FaceIdx = 4;
//}
//else if(-Direction.z == MaxComponent)
//{
//FaceIdx = 5;
//}
//
//return(FaceIdx);
//}

float
LinearMap(float A1, float A2, 
          float B1, float B2, 
          float ARangeInputValue)
{
    float BRangeOutputValue = B1 + (((ARangeInputValue - A1) * (B2 - B1)) / (A2 - A1));
    return(BRangeOutputValue);
}

vec2
MapCoordinatesToAtlasTile(uint TileIndex, uint TileSize, uint AtlasMinSize, uint AtlasMaxSize, vec2 Coordinates)
{
    uint TileSelectionMinX = (TileIndex * TileSize) % AtlasMaxSize;
    uint TileSelectionMaxX = TileSelectionMinX + TileSize;
    
    uint NrColumns = AtlasMaxSize / TileSize;
    uint YScaler = TileIndex / NrColumns;
    uint TileSelectionMinY = YScaler * TileSize;
    uint TileSelectionMaxY = TileSelectionMinY + TileSize;
    
    float TileLowerBoundUVX = LinearMap(AtlasMinSize, AtlasMaxSize, 0, 1, TileSelectionMinX);
    float TileLowerBoundUVY = LinearMap(AtlasMinSize, AtlasMaxSize, 0, 1, TileSelectionMinY);
    float TileUpperBoundUVX = LinearMap(AtlasMinSize, AtlasMaxSize, 0, 1, TileSelectionMaxX);
    float TileUpperBoundUVY = LinearMap(AtlasMinSize, AtlasMaxSize, 0, 1, TileSelectionMaxY);
    
    float MappedX = LinearMap(0, 1, TileLowerBoundUVX, TileUpperBoundUVX, Coordinates.x);
    float MappedY = LinearMap(0, 1, TileLowerBoundUVY, TileUpperBoundUVY, Coordinates.y);
    return(vec2(MappedX, MappedY));
}

float
CalculateShadowContribution(vec4 FragPosLightSpace, uint TileIndex, int ShadowmapQuality, vec3 Normal, vec3 LightDirection)
{
    atlas_info AtlasInfo = ShadowmapAtlasInfo[ShadowmapQuality];
    
    vec3 NDCCoords = FragPosLightSpace.xyz / FragPosLightSpace.w;
    NDCCoords = NDCCoords * 0.5 + 0.5;
    
    NDCCoords.xy = MapCoordinatesToAtlasTile(TileIndex, AtlasInfo.TileSize, 0, AtlasInfo.Size, vec2(NDCCoords.x, NDCCoords.y));
    
    // float ClosestDepth = texture(ShadowmapAtlas[ShadowmapQuality], NDCCoords.xy).r;
    // float CurrentDepth = NDCCoords.z;
    
    // float Bias = max(0.05 * (1.0 - dot(Normal, LightDirection)), 0.005);
    // CurrentDepth -= Bias;
    
    // float ShadowContribution = (CurrentDepth > ClosestDepth) ? 1.0 : 0.0;
	
	
	float Bias = max(0.05 * (1.0 - dot(Normal, LightDirection)), 0.005);
	float CurrentDepth = NDCCoords.z;
	float ShadowContribution = 0.0;
	vec2 TexelSize = 1.0 / textureSize(ShadowmapAtlas[ShadowmapQuality], 0);
	for(int x = -1; x <= 1; ++x)
	{
		for(int y = -1; y <= 1; ++y)
		{
			float PcfDepth = texture(ShadowmapAtlas[ShadowmapQuality], NDCCoords.xy + vec2(x, y) * TexelSize).r; 
			ShadowContribution += CurrentDepth - Bias > PcfDepth ? 1.0 : 0.0;
		}    
	}
    
	ShadowContribution /= 9.0;
    
    if(NDCCoords.z > 1.0)
    {
        ShadowContribution = 0.0;
    }
	
    return(ShadowContribution);
}

vec3
ProcessPointLight(uint LightIndex)
{
    point_light PointLight = PointLights[LightIndex];
	float AmbientIntensity = PointLight.Ambient.a;
	float DiffuseIntensity = PointLight.Diffuse.a;
	float SpecularIntensity = PointLight.Specular.a;
	
	vec3 NormalizedNormalVector = normalize(NormalVector);
	
	// Ambient lighting
	vec3 AmbientLight = vec3(PointLight.Ambient) * AmbientIntensity;
    
	// Diffuse lighting
	vec3 LightSourceDir = normalize(vec3(PointLight.Position) - FragmentPosition);
    
	float DiffuseFactor = max(dot(LightSourceDir, NormalizedNormalVector), 0.0);
	vec3 DiffuseLight = DiffuseFactor * vec3(PointLight.Diffuse) * DiffuseIntensity;
    
	//Specular lighting
	//vec3 LightSourceReflectDir = reflect(-LightSourceDir, NormalVector);
	vec3 CameraViewDir = normalize(CameraPos - FragmentPosition);
	vec3 HalfwayVector = normalize(LightSourceDir + CameraViewDir);
    
	float ShineFactor = 64;
	float SpecularFactor = pow(max(dot(NormalizedNormalVector, HalfwayVector), 0.0), ShineFactor);
	vec3 SpecularLight = SpecularFactor * vec3(PointLight.Specular) * SpecularIntensity;
    
	// Attenuation
	float DistanceToLight = length(vec3(PointLight.Position) - FragmentPosition);
	float LightAttenuation = 1.0 / (DistanceToLight * DistanceToLight);
    
    vec3 LightToFragDir = FragmentPosition - vec3(PointLight.Position);
    uint FaceIndex = GetCubemapFace(LightToFragDir);
    
    /*vec3 DebugColor = vec3(1.0);
    switch(FaceIndex)
    {
        case SHADOWMAP_FACE_POSITIVE_X:
        {
            DebugColor = vec3(1.0, 0.0, 0.0);
        } break;
        
        case SHADOWMAP_FACE_NEGATIVE_X:
        {
            DebugColor = vec3(0.0, 1.0, 0.0);
        } break;
        
        case SHADOWMAP_FACE_POSITIVE_Y:
        {
            DebugColor = vec3(1.0, 1.0, 0.0);
        } break;
        
        case SHADOWMAP_FACE_NEGATIVE_Y:
        {
            DebugColor = vec3(0.93, 0.5, 0.93);
        } break;
        
        case SHADOWMAP_FACE_POSITIVE_Z:
        {
            DebugColor = vec3(1.0, 0.64, 0.0);
        } break;
        
        case SHADOWMAP_FACE_NEGATIVE_Z:
        {
            DebugColor = vec3(0.0, 0.0, 1.0);
        } break;
    }*/
    
    /*vec3 DebugColor = vec3(1.0);
    switch(PointLight.ShadowmapQuality)
    {
        case SHADOWMAP_QUALITY_ULTRA:
        {
            DebugColor = vec3(1.0, 0.0, 0.0);
        } break;
        
        case SHADOWMAP_QUALITY_HIGH:
        {
            DebugColor = vec3(0.0, 1.0, 0.0);
        } break;
        
        case SHADOWMAP_QUALITY_MEDIUM:
        {
            DebugColor = vec3(1.0, 1.0, 0.0);
        } break;
        
        case SHADOWMAP_QUALITY_LOW:
        {
            DebugColor = vec3(0.0, 0.0, 1.0);
        } break;
    }*/
    
    vec4 FragPosLightSpace = PointLight.LightSpaceMatrix[FaceIndex] * vec4(FragmentPosition, 1.0);
    
    float ShadowContribution = CalculateShadowContribution(FragPosLightSpace, PointLight.ShadowmapTileIndices[FaceIndex], PointLight.ShadowmapQuality, NormalizedNormalVector, LightSourceDir);
    
	vec3 PointLightContribution = (AmbientLight + (1.0 - ShadowContribution) * (DiffuseLight + SpecularLight)) * LightAttenuation;
    //PointLightContribution += DebugColor * ShadowContribution;
	return(PointLightContribution);
}

vec3
ProcessDirectionalLight(uint LightIndex)
{
    directional_light DirectionalLight = DirectionalLights[LightIndex];
	float AmbientIntensity = DirectionalLight.Ambient.a;
	float DiffuseIntensity = DirectionalLight.Diffuse.a;
	float SpecularIntensity = DirectionalLight.Specular.a;
	
	vec3 NormalizedNormalVector = normalize(NormalVector);
	
	// Ambient lighting
	vec3 AmbientLight = normalize(vec3(DirectionalLight.Ambient));
    
	// Diffuse lighting
	vec3 LightSourceDir = normalize(-DirectionalLight.Direction);
    
	float DiffuseFactor = max(dot(LightSourceDir, NormalizedNormalVector), 0.0);
	vec3 DiffuseLight = DiffuseFactor * vec3(DirectionalLight.Diffuse);
    
	//Specular lighting
	//vec3 LightSourceReflectDir = reflect(-LightSourceDir, NormalVector);
	vec3 CameraViewDir = normalize(CameraPos - FragmentPosition);
	vec3 HalfwayVector = normalize(LightSourceDir + CameraViewDir);
    
	float ShineFactor = 64;
	float SpecularFactor = pow(max(dot(NormalizedNormalVector, HalfwayVector), 0.0), ShineFactor);
	vec3 SpecularLight = SpecularFactor * vec3(DirectionalLight.Specular);
    
    vec4 FragPosLightSpace = DirectionalLight.LightSpaceMatrix * vec4(FragmentPosition, 1.0);
    
    float ShadowContribution = CalculateShadowContribution(FragPosLightSpace, DirectionalLight.ShadowmapTileIndex, DirectionalLight.ShadowmapQuality, NormalizedNormalVector, LightSourceDir);
    
	vec3 DirectionalLightContribution = AmbientLight * AmbientIntensity + (1.0 - ShadowContribution) * (DiffuseLight * DiffuseIntensity + SpecularLight * SpecularIntensity);
	return(DirectionalLightContribution);
}

void main()
{
	vec3 SampledDiffuseTexture = texture(texture_diffuse, TextureCoord).rgb;
    
    vec3 FinalLightOutput = vec3(0.0);
	
	for(int LightsIndex = 0;
        LightsIndex < DirectionalLightsNr;
        ++LightsIndex)
    {
    	//FinalLightOutput += ProcessDirectionalLight(LightsIndex);
    }
	
    for(int LightsIndex = 0;
        LightsIndex < PointLightsNr;
        ++LightsIndex)
    {
    	FinalLightOutput += ProcessPointLight(LightsIndex);
    }
    
    FinalLightOutput = FinalLightOutput * SampledDiffuseTexture;
    
    // Gamma encoding
    // (to get cancelled by the monitor automatically)
    float Gamma = 2.2;
    FinalLightOutput = pow(FinalLightOutput, vec3(1.0 / Gamma));
    
	OutputColor = vec4(FinalLightOutput, 1.0);
	
	//OutputColor = vec4(1.0, 1.0, 1.0, 1.0);
	//OutputColor = vec4(vec3(DirectionalLights[9].Diffuse), 1.0f) * DirectionalLights[9].Diffuse.a;
	//OutputColor = vec4(DirectionalLights[9].Diffuse);
    
}
