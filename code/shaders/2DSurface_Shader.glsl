@vertex_shader
#version 450 core

layout (location = 0) in vec3 aPos;
layout (location = 1) in vec2 aTexCoord;

out vec2 TexCoord;

uniform mat4 ProjectionMatrix;
uniform mat4 ModelMatrix;

void main()
{
	gl_Position = ProjectionMatrix * ModelMatrix * vec4(aPos, 1.0);
	TexCoord = aTexCoord;
}

@fragment_shader
#version 450 core

out vec4 FragColor;

in vec2 TexCoord;

// texture sampler
uniform sampler2D DebugTexture;

float
LinearMap(float A1, float A2, 
          float B1, float B2, 
          float ARangeInputValue)
{
    float BRangeOutputValue = B1 + ((ARangeInputValue - A1) * (B2 - B1)) / (A2 - A1);
    return(BRangeOutputValue);
}

vec2
SampleAtlasTile(uint TileIndex, uint TileSize, uint AtlasMinSize, uint AtlasMaxSize)
{
    uint TileSelectionMinX = TileIndex * TileSize;
    uint TileSelectionMaxX = TileSelectionMinX + TileSize;
    
    uint YScaler = (TileIndex * TileSize) / AtlasMaxSize;
    uint TileSelectionMinY = YScaler * TileSize;
    uint TileSelectionMaxY = (YScaler + 1) * TileSize;
    
    float TileLowerBoundUVX = LinearMap(AtlasMinSize, AtlasMaxSize, 0, 1, TileSelectionMinX);
    float TileLowerBoundUVY = LinearMap(AtlasMinSize, AtlasMaxSize, 0, 1, TileSelectionMinY);
    float TileUpperBoundUVX = LinearMap(AtlasMinSize, AtlasMaxSize, 0, 1, TileSelectionMaxX);
    float TileUpperBoundUVY = LinearMap(AtlasMinSize, AtlasMaxSize, 0, 1, TileSelectionMaxY);
    
    float MappedX = LinearMap(0, 1, TileLowerBoundUVX, TileUpperBoundUVX, TexCoord.x);
    float MappedY = LinearMap(0, 1, TileLowerBoundUVY, TileUpperBoundUVY, TexCoord.y);
    return(vec2(MappedX, MappedY));
}

void main()
{
    //vec2 SampledTile = SampleAtlasTile(0, 1024, 0, 8192);
    //float DepthData = texture(DebugTexture, SampledTile).r;
    
	float DepthData = texture(DebugTexture, TexCoord).r;
	FragColor = vec4(vec3(DepthData), 1.0);
	//FragColor = vec4(1.0, 0.0, 0.0, 1.0);
}