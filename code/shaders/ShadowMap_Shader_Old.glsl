@vertex_shader
#version 450 core

layout (location = 0) in vec3 PositionIn;

uniform mat4 Model;

void main()
{
    gl_Position = Model * vec4(PositionIn, 1.0);
}


@geometry_shader
#version 450 core

layout (triangles) in;
layout (triangle_strip, max_vertices = 18) out;

uniform mat4 LightTransforms[6];

out vec4 FragPos;

void main()
{
	for(int face = 0; face < 6; face++)
	{
		gl_Layer = face;
		
		for(int i = 0; i < 3; i++)
		{
			FragPos = gl_in[i].gl_Position;
			gl_Position = LightTransforms[face] * FragPos;
			EmitVertex();
		}
		EndPrimitive();
	}
}


@fragment_shader
#version 450 core
	
in vec4 FragPos;

uniform vec3 LightPos;
uniform float FarPlane;

void main()
{
	// Distance between fragment and light position,
	// to be used as depth value
    float LightDistance = length(FragPos.xyz - LightPos);
	
	// Map to [0,1] range by dividing with the far plane
	// (works because the bigget value is far plane, and
	// far plane / far plane = 1)
	LightDistance /= FarPlane;
	
	gl_FragDepth = LightDistance;
}