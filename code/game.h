#include "lightcore_platform.h"
#include "lightcore_math.h"
#include "lightcore_shared_utils.h"
#include "lightcore_memory.h"

#include "lightcore_debug.h"

#include "lightcore_renderer.h"

#include "lightcore_camera.cpp"

struct game_state
{
    memory_arena Arena;
    scene_storage SceneStorage;
    camera SceneCamera;
};