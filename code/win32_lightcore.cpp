#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#include <Windows.h>

#include "lightcore_platform.h"
#include "lightcore_shared_utils.h"
#include "meta.h"
#include "lightcore_memory.h"

#include "lightcore_debug.h"

#if LIGHTCORE_DEBUG
#include "lightcore_debug.cpp"
#include "lightcore_debug_ui.cpp"

debug_table *GlobalDebugTable;

f32 TimingSamples[MAX_DEBUG_FRAMES];

extern IMGUI_IMPL_API LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
#endif

u32 FramesInASecond = 0;
f32 DeltaTimeMS = 0;

u32 FramesAccumulator = 0;
r64 TimingAccumulator = 0;


b32 ApplicationRunning = true;

#include "lightcore_math.h"

#include "win32_lightcore.h"


// NOTE(Cristian): We prefer discrete GPUs (in laptops) where available
extern "C"
{
    __declspec(dllexport) DWORD NvOptimusEnablement = 0x01;
    __declspec(dllexport) DWORD AmdPowerXpressRequestHighPerformance = 0x01;
}

platform_api Platform;

global_variable win32_state GlobalWin32State;

#include "lightcore_renderer.h"
#include "lightcore_renderer.cpp"
#include "win32_renderer_table.h"

#include "meta.cpp"

platform_renderer *Renderer;


//#include <imgui/includes/imgui_demo.cpp>

PLATFORM_FILE_ERROR(Win32FileError)
{
    OutputDebugString("FILE ERROR: ");
    OutputDebugString(Message);
    OutputDebugString("\n");
    
    Handle->NoErrors = false;
}

PLATFORM_GET_FILE_SIZE(Win32GetFileSize)
{
    LARGE_INTEGER LFileSize;
    GetFileSizeEx(*(HANDLE *)&Handle->PlatformHandle, &LFileSize);
    mmsz FileSize = LFileSize.QuadPart;
    return(FileSize);
}

PLATFORM_READ_FILE(Win32ReadFile)
{
    if(Handle->NoErrors)
    {
        OVERLAPPED Overlapped = {};
        Overlapped.Offset = (DWORD)(Offset & 0xFFFFFFFF);
        Overlapped.OffsetHigh = (DWORD)((Offset >> 32) & 0xFFFFFFFF);
        
        DWORD BytesRead;
        
        if(!ReadFile(*(HANDLE *)&Handle->PlatformHandle, Dest, (u32)Size, &BytesRead, &Overlapped) ||
           (BytesRead < (u32)Size))
        {
            Win32FileError(Handle, "Cannot read file");
        }
    }
}

//PLATFORM_GET_FILE_INFO(Win32GetFileInfo)
//{
//platform_file_info Result = {};
//
//if(Handle->NoErrors)
//{
//BY_HANDLE_FILE_INFORMATION FileInfo = {};
//if(GetFileInformationByHandle(*(HANDLE *)&Handle->PlatformHandle, &FileInfo))
//{
// TODO(Cristian): Handle may need to be re-opened in order for the OS to report the new size correctly
//Result.Size = ((u64)FileInfo.nFileSizeHigh << (u64)32) | (u64)FileInfo.nFileSizeLow;
//Result.CreationTime = ((u64)FileInfo.ftCreationTime.dwHighDateTime << (u64)32) | (u64)FileInfo.ftCreationTime.dwLowDateTime;
//Result.LastAccessTime = ((u64)FileInfo.ftLastAccessTime.dwHighDateTime << (u64)32) | (u64)FileInfo.ftLastAccessTime.dwLowDateTime;
//Result.LastWriteTime = ((u64)FileInfo.ftLastWriteTime.dwHighDateTime << (u64)32) | (u64)FileInfo.ftLastWriteTime.dwLowDateTime;
//}
//else
//{
//Win32FileError(Handle, "Couldn't retrieve file information");
//}
//}
//
//return(Result);
//}

PLATFORM_GET_FILE_INFO(Win32GetFileInfo)
{
    platform_file_info Result = {};
    
    WIN32_FILE_ATTRIBUTE_DATA FileInfo = {};
    GetFileAttributesExA(Path, GetFileExInfoStandard, &FileInfo);
    
    Result.Size = ((u64)FileInfo.nFileSizeHigh << (u64)32) | (u64)FileInfo.nFileSizeLow;
    Result.CreationTime = ((u64)FileInfo.ftCreationTime.dwHighDateTime << (u64)32) | (u64)FileInfo.ftCreationTime.dwLowDateTime;
    Result.LastAccessTime = ((u64)FileInfo.ftLastAccessTime.dwHighDateTime << (u64)32) | (u64)FileInfo.ftLastAccessTime.dwLowDateTime;
    Result.LastWriteTime = ((u64)FileInfo.ftLastWriteTime.dwHighDateTime << (u64)32) | (u64)FileInfo.ftLastWriteTime.dwLowDateTime;
    
    return(Result);
}

PLATFORM_CHECK_FILE_EXISTS(Win32CheckIfFileExists)
{
    b32 Result = false;
    
    DWORD Attributes = GetFileAttributes(Path);
    if(Attributes != INVALID_FILE_ATTRIBUTES && !(Attributes & FILE_ATTRIBUTE_DIRECTORY))
    {
        Result = true;
    }
    return(Result);
}

PLATFORM_OPEN_FILE(Win32OpenFile)
{
    platform_file_handle Result = {};
    
    DWORD RequestedAccess = 0;
    DWORD ShareMode = 0;
    DWORD CreationMode = 0;
    
    if(Flags & OpenFile_Read)
    {
        RequestedAccess |= GENERIC_READ;
        ShareMode |= FILE_SHARE_READ | FILE_SHARE_WRITE;
        CreationMode = OPEN_EXISTING;
    }
    
    if(Flags & OpenFile_Write)
    {
        RequestedAccess |= GENERIC_WRITE;
        ShareMode |= (FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE);
        CreationMode = OPEN_ALWAYS;
    }
    
    HANDLE FileHandle = CreateFile(Path, RequestedAccess, ShareMode, 0, CreationMode, FILE_ATTRIBUTE_NORMAL, 0);
    if(FileHandle != INVALID_HANDLE_VALUE)
    {
        Result.NoErrors = true;
        Result.PlatformHandle = FileHandle;
    }
    else
    {
        Win32FileError(&Result, "Failed to create file handle");
    }
    
    return(Result);
}

PLATFORM_CLOSE_FILE(Win32CloseFile)
{
    HANDLE FileHandle = *(HANDLE *)&Handle->PlatformHandle;
    if(FileHandle != INVALID_HANDLE_VALUE)
    {
        CloseHandle(FileHandle);
    }
    else
    {
        Win32FileError(Handle, "Failed to close the file");
    }
}

PLATFORM_CHECK_FILE_IN_USE(Win32CheckFileInUse)
{
    b32 Result = false;
    
    HANDLE FileHandle = CreateFile(Path, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);
    if(FileHandle == INVALID_HANDLE_VALUE) 
    {
        DWORD Errcode = GetLastError();
        if (Errcode == ERROR_SHARING_VIOLATION) 
        {
            Result = true;
        } 
    }
    else
    {
        CloseHandle(FileHandle);
    }
    
    return(Result);
}

PLATFORM_GET_FILE_PATH(Win32GetFilePath)
{
    GetFinalPathNameByHandleA(*(HANDLE *)&Handle->PlatformHandle, Dst, (u32)DstSz, FILE_NAME_NORMALIZED);
}

struct surface_size
{
    u32 Width;
    u32 Height;
};

internal surface_size
Win32GetWindowDimensions(HWND WindowHandle, b32 ExcludeBorders = true)
{
    surface_size Dimensions = {};
    
    RECT WindowSurfaceDimensions;
    
    b32 Result = 0;
    if(ExcludeBorders)
    {
        Result = GetClientRect(WindowHandle, &WindowSurfaceDimensions);
    }
    else
    {
        Result = GetWindowRect(WindowHandle, &WindowSurfaceDimensions);
    }
    
    if(Result)
    {
        Dimensions.Width = WindowSurfaceDimensions.right - WindowSurfaceDimensions.left;
        Dimensions.Height = WindowSurfaceDimensions.bottom - WindowSurfaceDimensions.top;
    }
    
    return(Dimensions);
}

internal void
Win32CenterCursorInWindow(HWND WindowHandle, u32 WindowWidth, u32 WindowHeight)
{
    POINT InitialCursorPos;
    InitialCursorPos.x = WindowWidth / 2;
    InitialCursorPos.y = WindowHeight / 2;
    
    ClientToScreen(WindowHandle, &InitialCursorPos);
    SetCursorPos(InitialCursorPos.x, InitialCursorPos.y);
}

b32 ClipCursorArea = false;

b32 FirstMouseMovement = true;
r64 LastMouseX = 0;
r64 LastMouseY = 0;

internal void
Win32ProcessCursorPosition(HWND WindowHandle, b32 LockCursor = false)
{
    POINT MouseCoord;
    if (GetCursorPos(&MouseCoord))
    {
        // Convert screen coordinates to window coordinates
        if (ScreenToClient(WindowHandle, &MouseCoord))
        {
            if(FirstMouseMovement)
            {
                LastMouseX = MouseCoord.x;
                LastMouseY = MouseCoord.y;
                FirstMouseMovement = false;
            }
            
            if(LockCursor)
            {
                ShowCursor(false);
                
                if(ClipCursorArea)
                {
                    RECT CursorClipArea;           // new area for ClipCursor
                    RECT OldCursorClipArea;        // previous area for ClipCursor
                    GetClipCursor(&OldCursorClipArea);
                    GetWindowRect(WindowHandle, &CursorClipArea);
                    ClipCursor(&CursorClipArea);
                }
                
                surface_size WindowSurface = Win32GetWindowDimensions(WindowHandle);
                
                u32 WindowWidth = WindowSurface.Width;
                u32 WindowHeight = WindowSurface.Height;
                
                POINT CenteredCursorPos;
                CenteredCursorPos.x = WindowWidth / 2;
                CenteredCursorPos.y = WindowHeight / 2;
                
                r64 XOffset = MouseCoord.x - CenteredCursorPos.x;
                r64 YOffset = CenteredCursorPos.y - MouseCoord.y;
                
                Win32CenterCursorInWindow(WindowHandle, WindowWidth, WindowHeight);
            }
            else
            {
                ShowCursor(true);
                
                r64 XOffset = MouseCoord.x - LastMouseX;
                r64 YOffset = LastMouseY - MouseCoord.y; // Y coordinates go from bottom to top
                LastMouseX = MouseCoord.x;
                LastMouseY = MouseCoord.y;
            }
        }
    }
}

internal void 
Win32CenterWindowOnScreen(HWND WindowHandle, DWORD Style, DWORD ExStyle)
{
    u32 ScreenWidth = GetSystemMetrics(SM_CXSCREEN);
    u32 ScreenHeight = GetSystemMetrics(SM_CYSCREEN);    
    
    RECT ClientRect;
    GetClientRect(WindowHandle, &ClientRect);    
    AdjustWindowRectEx(&ClientRect, Style, FALSE, ExStyle);   
    
    u32 ClientWidth = ClientRect.right - ClientRect.left;
    u32 ClientHeight = ClientRect.bottom - ClientRect.top;    
    SetWindowPos(WindowHandle,
                 0,
                 ScreenWidth / 2 - ClientWidth / 2,
                 ScreenHeight / 2 - ClientHeight / 2,
                 ClientWidth, ClientHeight,
                 0);
}

internal void
Win32SetFullscreenMode(win32_window_info *WindowInfo, b32 EnterFullscreen = false)
{
    HWND WindowHandle = WindowInfo->WindowHandle;
    WINDOWPLACEMENT *PreviousPlacement = &WindowInfo->WindowPreviousPlacement;
    
    DWORD WindowStyle = GetWindowLong(WindowHandle, GWL_STYLE);
    
    if(EnterFullscreen)
    {
        if (WindowStyle & WS_OVERLAPPEDWINDOW)
        {
            MONITORINFOEX MonitorInfo = {};
            MonitorInfo.cbSize = sizeof(MonitorInfo);
            
            if (GetWindowPlacement(WindowHandle, PreviousPlacement) && 
                GetMonitorInfo(MonitorFromWindow(WindowHandle, MONITOR_DEFAULTTOPRIMARY), &MonitorInfo)) 
            {
                WindowStyle = WindowStyle & ~WS_OVERLAPPEDWINDOW;
                
                SetWindowLong(WindowHandle, GWL_STYLE, WindowStyle);
                
                u32 Width = MonitorInfo.rcMonitor.right - MonitorInfo.rcMonitor.left;
                u32 Height = MonitorInfo.rcMonitor.bottom - MonitorInfo.rcMonitor.top;
                WindowInfo->Width = Width;
                WindowInfo->Height = Height;
                SetWindowPos(WindowHandle, HWND_TOP,
                             MonitorInfo.rcMonitor.left, MonitorInfo.rcMonitor.top,
                             Width, Height,
                             SWP_NOOWNERZORDER | SWP_FRAMECHANGED);
                
                /*SetWindowPos(WindowHandle, HWND_TOP,
                             MonitorInfo.rcMonitor.left, MonitorInfo.rcMonitor.top,
                             2580, 1460,
                             SWP_NOOWNERZORDER | SWP_FRAMECHANGED);*/
                
                /*MONITORINFOEX info = {};
                info.cbSize = sizeof(MONITORINFOEX);
                
                GetMonitorInfo(MonitorFromWindow(WindowHandle, MONITOR_DEFAULTTOPRIMARY),&info);
                
                DEVMODE devmode = {};
                devmode.dmSize = sizeof(DEVMODE);
                EnumDisplaySettings(info.szDevice, ENUM_CURRENT_SETTINGS, &devmode);
                
                u32 Width = devmode.dmPelsWidth;
                u32 Height = devmode.dmPelsHeight;
                
                u32 test2 = 0;*/
            }
        }
    }
    else
    {
        if(!(WindowStyle & WS_OVERLAPPEDWINDOW))
        {
            WindowStyle = WindowStyle | WS_OVERLAPPEDWINDOW;
            SetWindowLong(WindowHandle, GWL_STYLE, WindowStyle);
            SetWindowPlacement(WindowHandle, PreviousPlacement);
            SetWindowPos(WindowHandle, NULL, 0, 0, 0, 0,
                         SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER |
                         SWP_NOOWNERZORDER | SWP_FRAMECHANGED);
            
            RECT test = {};
            GetClientRect(WindowHandle, &test);
            
            RECT window = {};
            GetWindowRect(WindowHandle, &window);
            
            u32 t = 0;
        }
    }
}

inline FILETIME
Win32GetFileLastWriteTime(char *Filename)
{
    FILETIME LastWriteTime = {};
    
    WIN32_FILE_ATTRIBUTE_DATA Data;
    if(GetFileAttributesExA(Filename, GetFileExInfoStandard, &Data))
    {
        LastWriteTime = Data.ftLastWriteTime;
    }
    
    return(LastWriteTime);
}

inline b32
Win32FileTimeIsValid(FILETIME Time)
{
    b32 IsValid = (Time.dwLowDateTime != 0) || (Time.dwHighDateTime != 0);
    return(IsValid);
}

internal char *
Win32GetFileNameFromPath(char *Path)
{
	char *Result = 0;
	if (Path)
	{
		mmsz Size = strnlen_s(Path, 4096);
        if(Size)
        {
            Result = (char *)malloc(Size * sizeof(char));
            if (Result)
            {
                u32 FilenameLength = 0;
                for (mmsz Index = Size - 1;
                     Index >= 0;
                     Index--)
                {
                    char TempChar = Path[Index];
                    if(TempChar == '/')
                    {
                        Result[FilenameLength] = '\0';
                        break;
                    }
                    
                    FilenameLength++;
                }
                
                memcpy(Result, Path + (Size - FilenameLength), FilenameLength);
            }
            else
            {
                // TODO(Cristian): Log failure here
            }
        }
        else
        {
            // TODO(Cristian): Log failure here
        }
	}
	else
	{
		// TODO(Cristian): Log failure here
	}
    
	return(Result);
}

internal void
Win32UnloadCode(win32_dynamic_code *DynamicCode)
{
    if(DynamicCode->DLL)
    {
        // TODO(Cristian): This might cause problems in the future with pointers to strings inside the DLL.
        //FreeLibrary(DynamicCode->DLL);
    }
    
    DynamicCode->DLL = 0;
    DynamicCode->IsValid = false;
    
    for(u32 FunctionIndex = 0;
        FunctionIndex < DynamicCode->FunctionsCount;
        ++FunctionIndex)
    {
        DynamicCode->Functions[FunctionIndex] = 0;
    }
}

internal void
Win32ConcatEXEPath(win32_state *State, char *DLLName, char *Dest, memory_size DestSize)
{
    char PathToEXE[MAX_PATH];
    memcpy_s(PathToEXE, sizeof(PathToEXE), State->EXEPath, State->OnePastLastEXEPathSlash - State->EXEPath);
    *(PathToEXE + (State->OnePastLastEXEPathSlash - State->EXEPath)) = 0;
    
    snprintf(Dest, DestSize, "%s%s", PathToEXE, DLLName);
}

internal void
Win32ConcatEXEPath(win32_state *State, char *DLLName, u32 UniqueTag, char *Dest, memory_size DestSize)
{
    char PathToEXE[MAX_PATH];
    memcpy_s(PathToEXE, sizeof(PathToEXE), State->EXEPath, State->OnePastLastEXEPathSlash - State->EXEPath);
    *(PathToEXE + (State->OnePastLastEXEPathSlash - State->EXEPath)) = 0;
    
    snprintf(Dest, DestSize, "%s%d_%s", PathToEXE, UniqueTag, DLLName);
}

internal void
Win32LoadCode(win32_state *State, win32_dynamic_code *DynamicCode)
{
    char *SourceDLLPath = DynamicCode->SourceDLLPath;
    char *LockFilePath = DynamicCode->LockFilePath;
    
    char TempDLLPath[MAX_PATH];
    
    WIN32_FILE_ATTRIBUTE_DATA Ignored;
    if(!GetFileAttributesExA(LockFilePath, GetFileExInfoStandard, &Ignored))
    {
        DynamicCode->LastDLLWriteTime = Win32GetFileLastWriteTime(SourceDLLPath);
        
        b32 AttemptsFailed = true;
        for(u32 AttemptIndex = 0;
            AttemptIndex < 128;
            ++AttemptIndex)
        {
            Win32ConcatEXEPath(State, DynamicCode->TransientDLLName, DynamicCode->TempDLLNumber, TempDLLPath, sizeof(TempDLLPath));
            
            if(++DynamicCode->TempDLLNumber >= 1024)
            {
                DynamicCode->TempDLLNumber = 0;
            }
            
            if(CopyFile(SourceDLLPath, TempDLLPath, false))
            {
                AttemptsFailed = false;
                break;
            }
        }
        
        if (!AttemptsFailed)
        {
            DynamicCode->DLL = LoadLibrary(TempDLLPath);
            if (DynamicCode->DLL)
            {
                DynamicCode->IsValid = true;
                
                for(u32 FunctionIndex = 0;
                    FunctionIndex < DynamicCode->FunctionsCount;
                    ++FunctionIndex)
                {
                    void *Function = GetProcAddress(DynamicCode->DLL, DynamicCode->FunctionNames[FunctionIndex]);
                    if(Function)
                    {
                        DynamicCode->Functions[FunctionIndex] = Function;
                    }
                    else
                    {
                        DynamicCode->IsValid = false;
                    }
                }
            }
        }
        else
        {
            OutputDebugStringA("Game DLL reload attemp failed!\n");
            Assert(!AttemptsFailed);
        }
    }
    
    if(!DynamicCode->IsValid)
    {
        Win32UnloadCode(DynamicCode);
    }
}

internal void
Win32ReloadCode(win32_state *State, win32_dynamic_code *DynamicCode)
{
    FILETIME CurrentDLLWriteTime = Win32GetFileLastWriteTime(DynamicCode->SourceDLLPath);
    
    if(Win32FileTimeIsValid(DynamicCode->LastDLLWriteTime) && 
       Win32FileTimeIsValid(CurrentDLLWriteTime))
    {
        b32 DLLNeedsToReload = (CompareFileTime(&DynamicCode->LastDLLWriteTime, &CurrentDLLWriteTime) != 0);
        if(DLLNeedsToReload)
        {
            Win32UnloadCode(DynamicCode);
            Win32LoadCode(State, DynamicCode);
        }
    }
}

internal void
Win32GetEXEPath(win32_state *State)
{
    DWORD MainEXEFullPathSize = GetModuleFileNameA(0, State->EXEPath, sizeof(State->EXEPath));
    State->OnePastLastEXEPathSlash = State->EXEPath;
    for(char *At = State->EXEPath;
        *At;
        ++At)
    {
        if(*At == '\\')
        {
            State->OnePastLastEXEPathSlash = At + 1;
        }
    }
}

// TODO(Cristian): Simplify and further abstract the job queue system

internal b32
Win32DoNextAvailableJobEntry(platform_job_queue *Queue, DWORD LogicalThreadIndex, memory_arena *ThreadLocalStorage)
{
    b32 JobAvailable = false;
    
    u32 OriginalNextEntryToRead = Queue->NextJobEntryToRead;
    if (OriginalNextEntryToRead != Queue->NextJobEntryToWrite)
    {
        u32 EntryIndex = AtomicCompareExchangeU32(&Queue->NextJobEntryToRead, 
                                                  (OriginalNextEntryToRead + 1) % ArrayCount(Queue->JobEntries), 
                                                  OriginalNextEntryToRead);
        if (EntryIndex == OriginalNextEntryToRead)
        {
            AtomicDecrement(&Queue->AvailableEntriesCount);
            
            platform_job_queue_entry JobEntry = Queue->JobEntries[EntryIndex];
            JobEntry.Callback(JobEntry.Data, LogicalThreadIndex, ThreadLocalStorage);
            
            JobAvailable = true;
        }
    }
    
    return(JobAvailable);
}

internal DWORD
ThreadProc(void *lpParameter)
{
    win32_thread_info *ThreadInfo = (win32_thread_info *)lpParameter;
    platform_job_queue *Queue = ThreadInfo->Queue;
    
    memory_arena ThreadLocalStorage = {};
    
    for (;;)
    {
        if (!Win32DoNextAvailableJobEntry(Queue, ThreadInfo->LogicalThreadIndex, &ThreadLocalStorage))
        {
            WaitForSingleObjectEx(Queue->SemaphoreHandle, INFINITE, FALSE);
        }
    }
    
    return(0);
}

internal void
Win32MakeJobQueue(platform_job_queue *Queue, win32_thread_info *ThreadInfos,  u32 ThreadCount)
{
    u32 InitialCount = 0;
    HANDLE SemaphoreHandle = CreateSemaphoreEx(0,
                                               InitialCount,
                                               ThreadCount,
                                               0, 0, SEMAPHORE_ALL_ACCESS);
    Queue->SemaphoreHandle = SemaphoreHandle;
    
    for (u32 ThreadIndex = 0;
         ThreadIndex < ThreadCount;
         ++ThreadIndex)
    {
        win32_thread_info *ThreadInfo = ThreadInfos + ThreadIndex;
        ThreadInfo->LogicalThreadIndex = ThreadIndex + 1;
        ThreadInfo->Queue = Queue;
        
        DWORD ThreadID;
        HANDLE ThreadHandle = CreateThread(0, 0, ThreadProc, ThreadInfo, 0, &ThreadID);
        if (ThreadHandle)
        {
            CloseHandle(ThreadHandle);
        }
    }
}

// TODO(Cristian): Add an InterlockedCompareExchange on TotalAvailableJobs
// for the multiple producers - multiple consumers scenario.
internal void
Win32AddJobEntry(platform_job_queue *Queue, platform_job_queue_callback *Callback, void *Data, u32 ThreadIndex)
{
    u32 NewNextJobEntryToWrite = (Queue->NextJobEntryToWrite + 1) % ArrayCount(Queue->JobEntries);
    if(NewNextJobEntryToWrite != Queue->NextJobEntryToRead)
    {
        platform_job_queue_entry *JobEntry = Queue->JobEntries + Queue->NextJobEntryToWrite;
        JobEntry->Callback = Callback;
        JobEntry->Data = Data;
        
        AtomicIncrement(&Queue->AvailableEntriesCount);
        
        WRITE_BARRIER;
        
        Queue->NextJobEntryToWrite = NewNextJobEntryToWrite;
        
        ReleaseSemaphore(Queue->SemaphoreHandle, 1, 0);
    }
}

internal void
Win32CompleteAllWork(platform_job_queue *Queue, u32 ThreadIndex, memory_arena *ThreadLocalStorage)
{
    while(Queue->AvailableEntriesCount)
    {
        Win32DoNextAvailableJobEntry(Queue, ThreadIndex, ThreadLocalStorage);
    }
}

internal void
Win32FreeMemoryBlock(win32_memory_block *Block)
{
    TicketMutexLock(&GlobalWin32State.MemoryMutex);
    Block->Prev->Next = Block->Next;
    Block->Next->Prev = Block->Prev;
    TicketMutexUnlock(&GlobalWin32State.MemoryMutex);
    
    b32 Result = VirtualFree(Block, 0, MEM_RELEASE);
    Assert(Result);
}

PLATFORM_ALLOCATE_MEMORY(Win32AllocateMemory)
{
    memory_size TotalSize = Size + sizeof(win32_memory_block);
    memory_size BlockOffset = sizeof(win32_memory_block);
    memory_size ProtectOffset = 0;
    
    memory_size PageSize = 4096;
    if(AllocationFlags & PlatformFlags_UnderflowCheck)
    {
        TotalSize = 2 * PageSize + Size;
        BlockOffset = 2 * PageSize;
        ProtectOffset = PageSize;
    }
    else if(AllocationFlags & PlatformFlags_OverflowCheck)
    {
        memory_size PageAlignedSize = AlignToPow2(Size, PageSize);
        TotalSize = PageSize + PageAlignedSize + PageSize;
        BlockOffset = PageSize + PageAlignedSize - Size;
        ProtectOffset = PageSize + PageAlignedSize;
    }
    
    win32_memory_block *NewBlock = (win32_memory_block *)VirtualAlloc(0, TotalSize, MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
    Assert(NewBlock);
    
    NewBlock->Block.Base = (ump)NewBlock + BlockOffset;
    
    Assert(NewBlock->Block.Used == 0);
    Assert(NewBlock->Block.PrevBlock == 0);
    
    if(AllocationFlags & (PlatformFlags_UnderflowCheck | PlatformFlags_OverflowCheck))
    {
        DWORD OldProtect = 0;
        BOOL ProtectSuccess = VirtualProtect((void *)((ump)NewBlock + ProtectOffset), PageSize, PAGE_NOACCESS, &OldProtect);
        Assert(ProtectSuccess);
    }
    
    win32_memory_block *Sentinel = &GlobalWin32State.MemorySentinel;
    NewBlock->Next = Sentinel;
    NewBlock->Block.Size = Size;
    NewBlock->Block.Flags = AllocationFlags;
    
    
    TicketMutexLock(&GlobalWin32State.MemoryMutex);
    NewBlock->Prev = Sentinel->Prev;
    NewBlock->Prev->Next = NewBlock;
    NewBlock->Next->Prev = NewBlock;
    TicketMutexUnlock(&GlobalWin32State.MemoryMutex);
    
    platform_memory_block *PlatformBlock = &NewBlock->Block;
    return(PlatformBlock);
}

PLATFORM_DEALLOCATE_MEMORY(Win32DeallocateMemory)
{
    win32_memory_block *Block = (win32_memory_block *)PlatformBlock;
    
    if(Block)
    {
        Win32FreeMemoryBlock(Block);
    }
}

PLATFORM_ALLOCATE_RINGBUFFER_MEMORY(Win32AllocateRingBufferMemory)
{
    ring_buffer Result = {};
    
    memory_size PageSize = 4096;
    RequestedSize = AlignToPow2(RequestedSize, PageSize);
    Result.Size = RequestedSize;
    
    //
    // Reserve a placeholder region where the buffer will be mapped.
    //
    void *PlaceholderRegion1 = VirtualAlloc2(0,
                                             0,
                                             2 * RequestedSize,
                                             MEM_RESERVE | MEM_RESERVE_PLACEHOLDER,
                                             PAGE_NOACCESS,
                                             0, 0);
    
    if (!PlaceholderRegion1) 
    {
        OutputDebugStringA("Failed to reserve placeholder region, error %#x");
        //OutputDebugStringA(GetLastError());
        OutputDebugStringA("\n");
    }
    else
    {
        //
        // Split the placeholder region into two regions of equal size.
        //
        if (!VirtualFree(PlaceholderRegion1,
                         RequestedSize,
                         MEM_RELEASE | MEM_PRESERVE_PLACEHOLDER))
        {
            OutputDebugStringA("Failed to split placeholder region, error %#x");
            //OutputDebugStringA(GetLastError());
            OutputDebugStringA("\n");
            
            VirtualFree(PlaceholderRegion1, 0, MEM_RELEASE);
        }
        else
        {
            void *PlaceholderRegion2 = (void *) ((ump) PlaceholderRegion1 + RequestedSize);
            
            //
            // Create a pagefile-backed section for the buffer.
            //
            HANDLE FileMapping = CreateFileMapping(INVALID_HANDLE_VALUE,
                                                   0,
                                                   PAGE_READWRITE,
                                                   GetAddressHigh(RequestedSize), GetAddressLow(RequestedSize), 
                                                   0);
            
            if (!FileMapping) 
            {
                OutputDebugStringA("Couldn't create file mapping, error %#x");
                //OutputDebugStringA(GetLastError());
                OutputDebugStringA("\n");
                
                VirtualFree(PlaceholderRegion1, 0, MEM_RELEASE);
                VirtualFree(PlaceholderRegion2, 0, MEM_RELEASE);
            }
            else
            {
                //
                // Map the FileMapping into the first placeholder region.
                //
                void *MappedRegion1 = MapViewOfFile3(FileMapping,
                                                     0,
                                                     PlaceholderRegion1,
                                                     0,
                                                     RequestedSize,
                                                     MEM_REPLACE_PLACEHOLDER,
                                                     PAGE_READWRITE,
                                                     0, 0);
                
                if (!MappedRegion1) 
                {
                    OutputDebugStringA("Failed to map the first placeholder region, error %#x");
                    //OutputDebugStringA(GetLastError());
                    OutputDebugStringA("\n");
                    
                    VirtualFree(PlaceholderRegion1, 0, MEM_RELEASE);
                    VirtualFree(PlaceholderRegion2, 0, MEM_RELEASE);
                    CloseHandle(FileMapping);
                }
                else
                {
                    //
                    // Map the FileMapping into the second placeholder region.
                    //
                    void *MappedRegion2 = MapViewOfFile3(FileMapping,
                                                         0,
                                                         PlaceholderRegion2,
                                                         0,
                                                         RequestedSize,
                                                         MEM_REPLACE_PLACEHOLDER,
                                                         PAGE_READWRITE,
                                                         0, 0);
                    
                    if(!MappedRegion2) 
                    {
                        OutputDebugStringA("Failed to map the second placeholder region, error %#x");
                        //OutputDebugStringA(GetLastError());
                        OutputDebugStringA("\n");
                        
                        VirtualFree(PlaceholderRegion1, 0, MEM_RELEASE);
                        VirtualFree(PlaceholderRegion2, 0, MEM_RELEASE);
                        UnmapViewOfFileEx(MappedRegion1, 0);
                    }
                    else
                    {
                        Result.Buffer = MappedRegion1;
                    }
                    
                    CloseHandle(FileMapping);
                }
            }
        }
    }
    
    return(Result);
}

PLATFORM_DEALLOCATE_RINGBUFFER_MEMORY(Win32DeallocateRingBufferMemory)
{
    void *MirroredRegion = (void *)((ump)RingBuffer->Buffer + RingBuffer->Size);
    UnmapViewOfFile(RingBuffer->Buffer);
    UnmapViewOfFile(MirroredRegion);
}

internal u32
GetSystemPageSize()
{
    SYSTEM_INFO Info = {};
    GetSystemInfo(&Info);
    return(Info.dwPageSize);
}

internal
PLATFORM_CREATE_POOL_ALLOCATOR(Win32CreatePoolAllocator)
{
    pool_allocator Allocator = {};
    
    u32 SystemPageSize = GetSystemPageSize();
    
    memory_size ChunkSize = AlignToPow2(Params.ChunkSize, SystemPageSize);
    RequestedSize = AlignToPow2(RequestedSize, ChunkSize);
    
    u32 ChunksTotal = (u32)(RequestedSize / ChunkSize);
    memory_size ChunkHeaderSize = AlignToPow2(sizeof(chunk_header), SystemPageSize);
    
    memory_size ActualSize = ChunkHeaderSize * ChunksTotal + RequestedSize;
    
    void *PlaceholderRegion = VirtualAlloc2(0,
                                            0,
                                            ActualSize,
                                            MEM_RESERVE | MEM_RESERVE_PLACEHOLDER,
                                            PAGE_NOACCESS,
                                            0, 0);
    if(!PlaceholderRegion)
    {
        goto Exit;
    }
    
    HANDLE FileMapping = CreateFileMapping(INVALID_HANDLE_VALUE,
                                           0,
                                           PAGE_READWRITE,
                                           GetAddressHigh(ActualSize), GetAddressLow(ActualSize), 
                                           0);
    if(!FileMapping)
    {
        goto Exit;
    }
    
    void *MappedRegion = MapViewOfFile3(FileMapping,
                                        0,
                                        PlaceholderRegion,
                                        0,
                                        ActualSize,
                                        MEM_REPLACE_PLACEHOLDER,
                                        PAGE_READWRITE,
                                        0, 0);
    if(!MappedRegion)
    {
        goto Exit;
    }
    
    Allocator.Base = MappedRegion;
    Allocator.BaseAllocationSize = ActualSize;
    Allocator.ChunkSize = ChunkSize;
    Allocator.ChunksTotal = ChunksTotal;
    Allocator.ChunkHeaderSize = ChunkHeaderSize;
    Allocator.PageFileBackedFileMappingHANDLE = FileMapping;
    Allocator.MagicNumber = Params.MagicNumber;
    
    PlaceholderRegion = 0;
    FileMapping = 0;
    MappedRegion = 0;
    
    Exit:
    {
        if(PlaceholderRegion) 
        {
            VirtualFree(PlaceholderRegion, 0, MEM_RELEASE);
        }
        
        if(FileMapping) 
        {
            CloseHandle(FileMapping);
        }
        
        if(MappedRegion) 
        {
            UnmapViewOfFileEx(MappedRegion, 0);
        }
    }
    
    return(Allocator);
}

PLATFORM_FREE_POOL_ALLOCATOR(Win32FreePoolAllocator)
{
    b32 Result = true;
    
    Assert(Allocator->Base);
    
    b32 ClearResult = ClearPoolAllocator(Allocator);
    b32 UnmapResult = UnmapViewOfFile(Allocator->Base);
    if(!UnmapResult || !ClearResult)
    {
        Result = false;
    }
    
    CloseHandle(Allocator->PageFileBackedFileMappingHANDLE);
    
    return(Result);
}

PLATFORM_CONSTRUCT_CONTIGUOUS_VIEW(Win32ConstructContiguousView)
{
    void *Result = 0;
    
    u32 MappedChunks = 0;
    void *MapViewHeader = 0;
    void *PlaceholderRegion = 0;
    
    PlaceholderRegion = VirtualAlloc2(0,
                                      0,
                                      Size,
                                      MEM_RESERVE | MEM_RESERVE_PLACEHOLDER,
                                      PAGE_NOACCESS,
                                      0, 0);
    if(!PlaceholderRegion)
    {
        goto Exit;
    }
    
    if(Size > (Allocator->ChunkHeaderSize + Allocator->ChunkSize))
    {
        b32 FreeResult = VirtualFree(PlaceholderRegion,
                                     Allocator->ChunkHeaderSize + Allocator->ChunkSize,
                                     MEM_RELEASE | MEM_PRESERVE_PLACEHOLDER);
        if(!FreeResult)
        {
            goto Exit;
        }
    }
    
    chunk_header *CurrentChunkHeader = Header;
    
    HANDLE PageFileBackedFileMapping = (HANDLE)Allocator->PageFileBackedFileMappingHANDLE;
    
    MapViewHeader = MapViewOfFile3(PageFileBackedFileMapping,
                                   0,
                                   PlaceholderRegion,
                                   CurrentChunkHeader->ChunkOffset - Allocator->ChunkHeaderSize,
                                   Allocator->ChunkHeaderSize + Allocator->ChunkSize,
                                   MEM_REPLACE_PLACEHOLDER,
                                   PAGE_READWRITE,
                                   0, 0);
    if(!MapViewHeader)
    {
        goto Exit;
    }
    
    CurrentChunkHeader = CurrentChunkHeader->Next;
    
    mmsz RegionOffset = Allocator->ChunkHeaderSize + Allocator->ChunkSize;
    
    while(CurrentChunkHeader)
    {
        PlaceholderRegion = (void *)((ump)PlaceholderRegion + RegionOffset);
        
        if(CurrentChunkHeader->Next)
        {
            b32 FreeResult = VirtualFree(PlaceholderRegion,
                                         Allocator->ChunkSize,
                                         MEM_RELEASE | MEM_PRESERVE_PLACEHOLDER);
            if(!FreeResult)
            {
                goto Exit;
            }
        }
        
        void *MapViewChunk = MapViewOfFile3(PageFileBackedFileMapping,
                                            0,
                                            PlaceholderRegion,
                                            CurrentChunkHeader->ChunkOffset,
                                            Allocator->ChunkSize,
                                            MEM_REPLACE_PLACEHOLDER,
                                            PAGE_READWRITE,
                                            0, 0);
        if(!MapViewChunk)
        {
            goto Exit;
        }
        
        MappedChunks++;
        
        CurrentChunkHeader = CurrentChunkHeader->Next;
        RegionOffset = Allocator->ChunkSize;
    }
    
    Result = MapViewHeader;
    
    PlaceholderRegion = 0;
    MapViewHeader = 0;
    MappedChunks = 0;
    
    Exit:
    {
        if(MappedChunks)
        {
            void *CleanupChunksPtr = (void *)((ump)MapViewHeader + Allocator->ChunkHeaderSize);
            while(MappedChunks)
            {
                UnmapViewOfFileEx(CleanupChunksPtr, 0);       
                CleanupChunksPtr = (void *)((ump)MapViewHeader + Allocator->ChunkSize);
                MappedChunks--;
            }
        }
        
        if(PlaceholderRegion)
        {
            VirtualFree(PlaceholderRegion, 0, MEM_RELEASE);
        }
        
        if(MapViewHeader)
        {
            UnmapViewOfFileEx(MapViewHeader, 0);
        }
    }
    
    return(Result);
}

PLATFORM_FREE_MAPPED_REGION(Win32FreeMappedRegion)
{
    Assert(MappedRegion);
    
    b32 Result = UnmapViewOfFileEx(MappedRegion, MEM_UNMAP_WITH_TRANSIENT_BOOST);
    return(Result);
}

internal void
Win32ProcessPendingMessages(HWND WindowHandle, game_input *Input)
{
    Input->MouseDeltaX = 0;
    Input->MouseDeltaY = 0;
    
#if LIGHTCORE_DEBUG
    b32 DebugSystemFocused = ImGui::IsWindowFocused(ImGuiFocusedFlags_AnyWindow);
    if(!DebugSystemFocused)
    {
#endif
        u32 MsgBufferSize;
        if(GetRawInputBuffer(0, &MsgBufferSize, sizeof(RAWINPUTHEADER)) == 0)
        {
            if(MsgBufferSize)
            {
                MsgBufferSize *= 16; // up to 16 messages
                
                // TODO(Cristian): Get rid of malloc and pass it an arena
                RAWINPUT *RawInputBuffer  = (RAWINPUT *)malloc(MsgBufferSize);
                if (RawInputBuffer)
                {
                    u32 MouseXAccumulator = 0;
                    u32 MouseYAccumulator = 0;
                    
                    for (;;)
                    {
                        u32 TotalInputMessages = GetRawInputBuffer(RawInputBuffer, &MsgBufferSize, sizeof(RAWINPUTHEADER));
                        if (TotalInputMessages == 0)
                        {
                            break;
                        }
                        
                        Assert(TotalInputMessages > 0);
                        
                        for(u32 Index = 0;
                            Index < TotalInputMessages;
                            ++Index)
                        {
                            RAWINPUT *RawInput = &RawInputBuffer[Index];
                            if (RawInput->header.dwType == RIM_TYPEMOUSE) 
                            {
                                MouseXAccumulator += (u32)RawInput->data.mouse.lLastX;
                                MouseYAccumulator += (u32)RawInput->data.mouse.lLastY;
                            }
                        }
                    }
                    
                    Input->MouseDeltaX = -(s32)MouseXAccumulator;
                    Input->MouseDeltaY = -(s32)MouseYAccumulator;
                }
                
                free(RawInputBuffer);
            }
        }
#if LIGHTCORE_DEBUG
    }
#endif
    
    MSG Message;
    while(PeekMessage(&Message, 0, 0, 0, PM_REMOVE))
    {
        b32 MessageNotIdentified = true;
        
        u32 Key = (u32)Message.wParam;
        
#if LIGHTCORE_DEBUG
        b32 ShouldntProcess = (b32)ImGui_ImplWin32_WndProcHandler(WindowHandle, Message.message, Message.wParam, Message.lParam);
        if(!ShouldntProcess && !DebugSystemFocused)
        {
#endif
            switch(Message.message)
            {
                case LC_PRESS:
                {
                    if(Message.wParam == LC_ESCAPE)
                    {
                        ApplicationRunning = false;
                    }
                    
                    Input->KeyboardKeys[Key] = true;
                    
                    MessageNotIdentified = false;
                } break;
                
                case LC_RELEASE:
                {
                    Input->KeyboardKeys[Key] = false;
                    
                    MessageNotIdentified = false;
                } break;
            }
#if LIGHTCORE_DEBUG
        }
#endif
        
        if(MessageNotIdentified)
        {
            TranslateMessage(&Message);
            DispatchMessage(&Message);
        }
    }
}

internal r64
Win32GetHighResolutionTimestamp()
{
    LARGE_INTEGER PerformanceCount;
    b32 ReturnResult = QueryPerformanceCounter(&PerformanceCount);
    
    Assert(ReturnResult);
    
    r64 Result = (r64)PerformanceCount.QuadPart;
    return(Result);
}

/*
Retrieves the frequency of the performance counter. The frequency of the performance counter is fixed 
at system boot and is consistent across all processors. Therefore, the frequency need only be queried 
upon application initialization, and the result can be cached.
*/
internal r64
Win32GetPerformanceCounterFrequency()
{
    LARGE_INTEGER PerformanceFrequency;
    Assert(QueryPerformanceFrequency(&PerformanceFrequency));
    
    r64 PerfCountFrequency = (r64)PerformanceFrequency.QuadPart;
    return(PerfCountFrequency);
}

//uint64_t
//PrintCycles(uint64_t PreviousCycles = 0)
//{
//uint64_t Cycles = __rdtsc();
//if (Cycles > PreviousCycles)
//{
//uint64_t ElapsedMegaCycles = Cycles - PreviousCycles;
//fprintf(stdout, "Cycles: %0.02f mc\n", (float)ElapsedMegaCycles / (1000 * 1000));
//}
//else
//{
//fprintf(stderr, "ERROR: Counter overflow\n");
//}
//
//return(Cycles);
//}

LRESULT CALLBACK
Win32WindowMessagesCallback(HWND WindowHandle, UINT OSMessage, WPARAM WParam, LPARAM LParam)
{
    LRESULT Result = 0;
    
    switch(OSMessage)
    {
        case WM_SIZE:
        {
            if(Renderer)
            {
                surface_size WindowDrawRegion = Win32GetWindowDimensions(WindowHandle);
                Renderer->WindowDrawRegionWidth = WindowDrawRegion.Width;
                Renderer->WindowDrawRegionHeight = WindowDrawRegion.Height;
            }
        } break;
        
        case WM_DESTROY:
        {
            ApplicationRunning = false;
        } break;
        
        case WM_CLOSE:
        {
            ApplicationRunning = false;
            PostQuitMessage(0);
        } break;
        
        case WM_ACTIVATE:
        {
            if(LOWORD(WParam) == WA_INACTIVE)
            {
                OutputDebugStringA("WINDOW INACTIVE\n");
            }
            else // WA_ACTIVE or WA_CLICKACTIVE
            {
                OutputDebugStringA("WINDOW ACTIVE\n");
            }
        } break;
        
        default:
        {
            Result = DefWindowProc(WindowHandle, OSMessage, WParam, LParam);
        } break;
    }
    
    return(Result);
}

#if LIGHTCORE_DEBUG

debug_state GlobalDebugState = {};

internal void 
Win32HandleDebugFrame(debug_memory *DebugMemory, f32 DeltaTime, u32 FPS)
{
    GlobalDebugTable->EventArrayIndex = !GlobalDebugTable->EventArrayIndex;
    u64 EventArrayIndex_EventIndex = AtomicExchangeU64(&GlobalDebugTable->EventArrayIndex_EventIndex, 
                                                       (u64)GlobalDebugTable->EventArrayIndex << 32);
    
    u32 EventCount = EventArrayIndex_EventIndex & 0xFFFFFFFF;
    u32 EventArrayIndex = EventArrayIndex_EventIndex >> 32;
    
    u32 LastWrittenFrameIndex = (DebugMemory->NextFrame ? DebugMemory->NextFrame : MAX_DEBUG_FRAMES) - 1;
    
    if(GlobalDebugState.DebugRecordingEnabled)
    {
        ProcessDebugEvents(DebugMemory, &GlobalDebugTable->Events[EventArrayIndex * DEBUG_EVENTS_MAX_SIZE], EventCount);
        
        TimingSamples[LastWrittenFrameIndex] = DeltaTime;
    }
    else
    {
        debug_thread *CurrentThread = DebugMemory->FirstThreadNode;
        while(CurrentThread)
        {
            debug_open_block *OpenBlock = CurrentThread->LastOpenBlock;
            if(OpenBlock)
            {
                FREELIST_DEALLOCATE(OpenBlock, DebugMemory->FirstFreeOpenBlock);
                OpenBlock = OpenBlock->Next;
            }
            
            CurrentThread->LastOpenBlock = 0;
            CurrentThread = CurrentThread->Next;
        }
    }
    
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplWin32_NewFrame();
    ImGui::NewFrame();
    
    //ImGui::ShowDemoWindow();
    
    CreateMainMenuBar(&GlobalDebugState);
    
    bool OverlayOpen = true;
    CreateSimpleOverlay(&OverlayOpen, "FPS: %d \nFrametime: %.2f", FPS, DeltaTime, OverlayPosition_BottomRight);
    
    if(GlobalDebugState.ProfilingWindowActive)
    {
        bool *WindowActive = &GlobalDebugState.ProfilingWindowActive;
        ImGui::Begin("Profiling tool", WindowActive, ImGuiWindowFlags_MenuBar);
        {
            if(ImGui::Button("Pause")) { GlobalDebugState.DebugRecordingEnabled = false; }
            ImGui::SameLine();
            if(ImGui::Button("Play")) { GlobalDebugState.DebugRecordingEnabled = true; }
            ImGui::SameLine();
            
            local_persist bool IsDisplayLastFrame = false;
            ImGui::Checkbox("Current Frame", &IsDisplayLastFrame);
            
            
            ImGui::SetNextItemWidth(-FLT_MIN);
            
            local_persist s32 SelectedFrameIndex = 0;
            
            if(IsDisplayLastFrame)
            {
                SelectedFrameIndex = LastWrittenFrameIndex;
                
                PlotHistogram(SelectedFrameIndex, LastWrittenFrameIndex, "##values", TimingSamples, ArrayCount(TimingSamples), 0, 0, 0.0f, 50.0f, ImVec2(0, 100.0f));
            }
            else
            {
                s32 HoveredID = PlotHistogram(SelectedFrameIndex, LastWrittenFrameIndex, "##values", TimingSamples, ArrayCount(TimingSamples), 0, 0, 0.0f, 50.0f, ImVec2(0, 100.0f));
                
                if(HoveredID >= 0)
                {
                    if(ImGui::IsMouseClicked(ImGuiMouseButton_Left) || ImGui::IsMouseDown(ImGuiMouseButton_Left))
                    {
                        SelectedFrameIndex = (u32)HoveredID;
                    }
                }
            }
            
            ImGui::TextColored(ImVec4(1,1,0,1), "Selected Frame: %d", SelectedFrameIndex);
            
            ImGui::BeginChild("Scrolling");
            debug_frame *SelectedFrame = &DebugMemory->Frames[SelectedFrameIndex];
            DrawDebugFrameHierarchical(SelectedFrame, DebugMemory);
            //DrawDebugFrameFlattened(SelectedFrame, DebugMemory);
            ImGui::EndChild();
            
            
            /*ImGui::BeginChild("Test");
            ImDrawList* draw_list = ImGui::GetWindowDrawList();
            // Get the current ImGui cursor position
            ImVec2 p = ImGui::GetCursorScreenPos();
            // Draw a red circle
            draw_list->AddCircleFilled(ImVec2(p.x + 50, p.y + 50), 30.0f, IM_COL32(255, 0, 0, 255), 16);
            // Draw a 3 pixel thick yellow line
            draw_list->AddLine(ImVec2(p.x, p.y), ImVec2(p.x + 100.0f, p.y + 100.0f), IM_COL32(255, 255, 0, 255), 3.0f);
            // Advance the ImGui cursor to claim space in the window (otherwise the window will appear small and needs to be resized)
            ImGui::Dummy(ImVec2(100, 100));
            ImGui::EndChild();*/
        }
        ImGui::End();
    }
    else if(GlobalDebugState.DebugRecordingEnabled)
    {
        GlobalDebugState.DebugRecordingEnabled = false;
    }
    
    ImGui::Render();
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
}

#endif


s32 CALLBACK
WinMain(HINSTANCE InstanceHandle,
        HINSTANCE PrevInstanceHandle,
        LPSTR     CommandLine,
        s32       ShowCode)
{
    //u32 ID = GetCurrentProcessorNumber();
    
    r64 PerformanceCounterFrequency = Win32GetPerformanceCounterFrequency();
    
    win32_state *State = &GlobalWin32State;
    State->MemorySentinel.Prev = &State->MemorySentinel;
    State->MemorySentinel.Next = &State->MemorySentinel;
    Win32GetEXEPath(State);
    
    char GameDLLFullPath[MAX_PATH];
    Win32ConcatEXEPath(State, "game.dll", GameDLLFullPath, sizeof(GameDLLFullPath));
    
    char RendererDLLFullPath[MAX_PATH];
    Win32ConcatEXEPath(State, "win32_renderer_opengl.dll", RendererDLLFullPath, sizeof(RendererDLLFullPath));
    
    char LockFileFullPath[MAX_PATH];
    Win32ConcatEXEPath(State, "lock.tmp", LockFileFullPath, sizeof(LockFileFullPath));
    
    win32_game_function_table GameFunctions = {};
    win32_dynamic_code GameCode = {};
    GameCode.SourceDLLPath = GameDLLFullPath;
    GameCode.TransientDLLName = "game_temp.dll";
    GameCode.LockFilePath = LockFileFullPath;
    GameCode.FunctionsCount = ArrayCount(Win32GameFunctionTableNames);
    GameCode.FunctionNames = Win32GameFunctionTableNames;
    GameCode.Functions = (void **)&GameFunctions;
    Win32LoadCode(State, &GameCode);
    
    win32_renderer_function_table RendererFunctions = {};
    win32_dynamic_code RendererCode = {};
    RendererCode.SourceDLLPath = RendererDLLFullPath;
    RendererCode.TransientDLLName = "renderer_opengl_temp.dll";
    RendererCode.LockFilePath = LockFileFullPath;
    RendererCode.FunctionsCount = ArrayCount(Win32RendererFunctionTableNames);
    RendererCode.FunctionNames = Win32RendererFunctionTableNames;
    RendererCode.Functions = (void **)&RendererFunctions;
    Win32LoadCode(State, &RendererCode);
    
    memory_arena PermAllocArena = {};
    //PermAllocArena.AllocationFlags |= PlatformFlags_OverflowCheck;
    //PermAllocArena.AllocationFlags |= PlatformFlags_UnderflowCheck;
    
    //memory_arena TransientArena = {};
    //TransientArena.AllocationFlags |= ArenaFlags_OverflowCheck;
    //TransientArena.AllocationFlags |= ArenaFlags_UnderflowCheck;
    
    game_memory GameMemory = {};
    GameMemory.PlatformAPI.JobQueueAddEntry = Win32AddJobEntry;
    GameMemory.PlatformAPI.JobQueueCompleteAllWork = Win32CompleteAllWork;
    
    GameMemory.PlatformAPI.AllocateMemory = Win32AllocateMemory;
    GameMemory.PlatformAPI.DeallocateMemory = Win32DeallocateMemory;
    
    GameMemory.PlatformAPI.AllocateRingBufferMemory = Win32AllocateRingBufferMemory;
    GameMemory.PlatformAPI.DeallocateRingBufferMemory = Win32DeallocateRingBufferMemory;
    
    GameMemory.PlatformAPI.CreatePoolAllocator = Win32CreatePoolAllocator;
    GameMemory.PlatformAPI.FreePoolAllocator = Win32FreePoolAllocator;
    GameMemory.PlatformAPI.ConstructContiguousView = Win32ConstructContiguousView;
    GameMemory.PlatformAPI.FreeMappedRegion = Win32FreeMappedRegion;
    
    GameMemory.PlatformAPI.OpenFile = Win32OpenFile;
    GameMemory.PlatformAPI.GetFileInfo = Win32GetFileInfo;
    GameMemory.PlatformAPI.GetFileSize = Win32GetFileSize;
    GameMemory.PlatformAPI.ReadFile = Win32ReadFile;
    GameMemory.PlatformAPI.FileError = Win32FileError;
    GameMemory.PlatformAPI.CloseFile = Win32CloseFile;
    GameMemory.PlatformAPI.GetFilePath = Win32GetFilePath;
    GameMemory.PlatformAPI.CheckFileInUse = Win32CheckFileInUse;
    GameMemory.PlatformAPI.CheckIfFileExists = Win32CheckIfFileExists;
    
    Platform = GameMemory.PlatformAPI;
    
    
#if LIGHTCORE_DEBUG
    memory_arena DebugArena = {};
    GlobalDebugTable = PushStruct(&DebugArena, debug_table);
    GlobalDebugTable->Events = (debug_event *)PushArray(&DebugArena, 2 * DEBUG_EVENTS_MAX_SIZE, debug_event);
    
    debug_memory *DebugMemory = PushStruct(&DebugArena, debug_memory);
    DebugMemory->DebugArena = DebugArena;
    
    GlobalDebugTable->EventIncrement = 1;
    
    GameMemory.DebugTable = GlobalDebugTable;
    GameMemory.DebugMemory = DebugMemory;
    
    GlobalDebugState.DebugRecordingEnabled = false;
    GlobalDebugState.ProfilingWindowActive = false;
#endif
    
    
    SYSTEM_INFO SystemInfo;
    GetSystemInfo(&SystemInfo);
    
    //const u32 ThreadCount = SystemInfo.dwNumberOfProcessors - 1;
    const u32 ThreadCountHighPriority = 6;
    const u32 ThreadCountLowPriority = 4;
    
    platform_job_queue HighPriorityQueue = {};
    platform_job_queue LowPriorityQueue = {};
    
    win32_thread_info *HighPriorityThreadInfos = PushArray(&PermAllocArena, ThreadCountHighPriority, win32_thread_info);
    Win32MakeJobQueue(&HighPriorityQueue, HighPriorityThreadInfos, ThreadCountHighPriority);
    
    win32_thread_info *LowPriorityThreadInfos = PushArray(&PermAllocArena, ThreadCountLowPriority, win32_thread_info);
    Win32MakeJobQueue(&LowPriorityQueue, LowPriorityThreadInfos, ThreadCountLowPriority);
    
    GameMemory.HighPriorityQueue = &HighPriorityQueue;
    GameMemory.LowPriorityQueue = &LowPriorityQueue;
    
    win32_window_info WindowInfo = {};
    WindowInfo.WindowTitle = "Lightcore Engine";
    WindowInfo.WindowClassName = "LightcoreWindowClass";
    WindowInfo.WindowPreviousPlacement = {sizeof(WINDOWPLACEMENT)};
    
    WNDCLASS WindowClass = {};
    WindowClass.hInstance = InstanceHandle;
    WindowClass.lpfnWndProc = Win32WindowMessagesCallback;
    WindowClass.lpszClassName = WindowInfo.WindowClassName;
    WindowClass.hCursor = LoadCursor(0, IDC_ARROW);
    
    window_settings WindowSettings = {};
    
    if(RegisterClassA(&WindowClass))
    {
        DWORD Exstyle = 0;
        DWORD Style = WS_OVERLAPPEDWINDOW;
        
        //NOTE(Cristian): Need to request an improper window size, because I cannot get DPI scale value without a window
        LONG RequestedDrawRegionWidth = 1920;
        LONG RequestedDrawRegionHeight = 1080;
        RECT DPIUnawareWindowRegion = { 0, 0, RequestedDrawRegionWidth, RequestedDrawRegionHeight };
        AdjustWindowRect(&DPIUnawareWindowRegion, Style, FALSE);
        
        WindowInfo.Width = DPIUnawareWindowRegion.right - DPIUnawareWindowRegion.left;
        WindowInfo.Height = DPIUnawareWindowRegion.bottom - DPIUnawareWindowRegion.top;
        
        if(!SetProcessDpiAwarenessContext(DPI_AWARENESS_CONTEXT_PER_MONITOR_AWARE_V2))
        {
            OutputDebugStringA("WARNING: Couldn't enable DPI awareness \n");
        }
        
        WindowInfo.WindowHandle = CreateWindowExA(Exstyle,
                                                  WindowInfo.WindowClassName,
                                                  WindowInfo.WindowTitle,
                                                  Style,
                                                  CW_USEDEFAULT, CW_USEDEFAULT,
                                                  WindowInfo.Width,
                                                  WindowInfo.Height,
                                                  0,
                                                  0,
                                                  InstanceHandle,
                                                  0);
        
        
        u32 DPIValue = GetDpiForWindow(WindowInfo.WindowHandle);
        RECT DPIAwareWindowRegion = { 0, 0, RequestedDrawRegionWidth, RequestedDrawRegionHeight };
        AdjustWindowRectExForDpi(&DPIAwareWindowRegion, Style, FALSE, Exstyle, DPIValue);
        
        SetWindowPos(WindowInfo.WindowHandle, NULL, 
                     0, 0, 
                     DPIAwareWindowRegion.right - DPIAwareWindowRegion.left, DPIAwareWindowRegion.bottom - DPIAwareWindowRegion.top,
                     SWP_NOMOVE | SWP_NOZORDER |
                     SWP_NOOWNERZORDER | SWP_FRAMECHANGED);
        
        
        RAWINPUTDEVICE Rid[1];
        Rid[0].usUsagePage = HID_USAGE_PAGE_GENERIC; 
        Rid[0].usUsage = HID_USAGE_GENERIC_MOUSE; 
        Rid[0].dwFlags = 0;   
        Rid[0].hwndTarget = WindowInfo.WindowHandle;
        RegisterRawInputDevices(Rid, 1, sizeof(Rid[0]));
        
        if(WindowInfo.WindowHandle)
        {
            ShowWindow(WindowInfo.WindowHandle, ShowCode);
            
            Win32CenterWindowOnScreen(WindowInfo.WindowHandle, WS_OVERLAPPEDWINDOW, 0);
            
            memory_arena RendererArena = {};
            
            HDC DeviceContextHandle = GetDC(WindowInfo.WindowHandle);
            
            surface_size WindowDrawRegionSize = Win32GetWindowDimensions(WindowInfo.WindowHandle);
            
            Renderer = RendererFunctions.LoadRenderer(DeviceContextHandle, Platform, &RendererArena);
            Renderer->WindowDrawRegionWidth = WindowDrawRegionSize.Width;
            Renderer->WindowDrawRegionHeight = WindowDrawRegionSize.Height;
            
            WindowInfo.DeviceContext = DeviceContextHandle;
            //WindowInfo.RenderContext = OpenglRenderContext;
            
            r64 CurrentTimestamp = 0;
            r64 PreviousTimestamp = 0;
            r64 Delta = 0;
            
            game_input Input = {};
            
#if LIGHTCORE_DEBUG
            IMGUI_CHECKVERSION();
            ImGui::CreateContext();
            ImGuiIO &io = ImGui::GetIO(); (void)io;
            io.Fonts->AddFontFromFileTTF("L://Lightcore//resources//assets//Roboto-Regular.ttf", 0.15f * DPIValue);
            
            ImGui::StyleColorsDark();
            
            ImGui_ImplWin32_Init(WindowInfo.WindowHandle);
            ImGui_ImplOpenGL3_Init("#version 450");
            
            //ImGui_ImplWin32_EnableDpiAwareness();
#endif
            
            while(ApplicationRunning)
            {
                CurrentTimestamp = Win32GetHighResolutionTimestamp();
                Delta = (CurrentTimestamp - PreviousTimestamp) / PerformanceCounterFrequency;
                PreviousTimestamp = CurrentTimestamp;
                
#if LIGHTCORE_DEBUG
                TimingAccumulator += Delta;
#endif
                BEGIN_TIMED_FRAME();
                
                
                BEGIN_TIMED_BLOCK("Code Reload");
                Win32ReloadCode(State, &GameCode);
                END_TIMED_BLOCK();
                
                BEGIN_TIMED_BLOCK("Input Processing");
                Win32ProcessPendingMessages(WindowInfo.WindowHandle, &Input);
                GameMemory.Input = Input;
                END_TIMED_BLOCK();
                
                BEGIN_TIMED_BLOCK("Frame Begin");
                game_render_commands *Frame = RendererFunctions.BeginFrame(Renderer);
                END_TIMED_BLOCK();
                
                if(GameFunctions.GameUpdateAndRender)
                {
                    BEGIN_TIMED_BLOCK("Upper update and render");
                    GameFunctions.GameUpdateAndRender(&GameMemory, Frame, Delta);
                    END_TIMED_BLOCK();
                }
                
                BEGIN_TIMED_BLOCK("Frame End");
                RendererFunctions.EndFrame(Renderer, Frame);
                END_TIMED_BLOCK();
                
                
                END_TIMED_FRAME();
                
                if(!AreEqual(&WindowSettings, &Frame->CurrentWindowSettings))
                {
                    Win32SetFullscreenMode(&WindowInfo, Frame->CurrentWindowSettings.Fullscreen);
                    
                    WindowSettings = Frame->CurrentWindowSettings;
                }
                
                FramesAccumulator++;
                
#if LIGHTCORE_DEBUG
                Win32HandleDebugFrame(GameMemory.DebugMemory, (f32)Delta * 1000, FramesInASecond);
#endif
                
                f32 ElapsedTimeMS = ((f32)TimingAccumulator * 1000);
                if(ElapsedTimeMS >= 1000)
                {
                    FramesInASecond = FramesAccumulator;
                    DeltaTimeMS = (f32)Delta * 1000;
                    
                    FramesAccumulator = 0;
                    TimingAccumulator = 0;
                }
                
                SwapBuffers(DeviceContextHandle);
            }
            
            UnregisterClassA(WindowInfo.WindowClassName, InstanceHandle);
        }
        else
        {
            // LOGGING HERE
        }
    }
    else
    {
        // LOGGING HERE
    }
    
    ExitProcess(0);
}