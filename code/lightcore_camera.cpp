enum camera_direction
{
	CameraDirection_Forward,
	CameraDirection_Backward,
	CameraDirection_Left,
	CameraDirection_Right,
    CameraDirection_Up,
    CameraDirection_Down
};

enum frustum_plane
{
    FrustumPlane_Right,
    FrustumPlane_Left,
    FrustumPlane_Bottom,
    FrustumPlane_Top,
    FrustumPlane_Far,
    FrustumPlane_Near
};

struct camera_transforms
{
    mat4x4 ViewMatrix;
    mat4x4 PerspectiveMatrix;
    mat4x4 ViewPerspectiveMatrix;
};

struct camera
{
    u32 BufferID;
    
    camera_transforms Transforms;
    
    plane FrustumPlanes[6];
    
    f32 AspectRatio;
    
	vec3 Up;
	vec3 Front;
	vec3 Right;
	vec3 Position;
	vec3 WorldUp;
    
	f32 LookSensitivity;
	f32 Yaw;
	f32 Pitch;
	f32 Speed;
    
    f32 FieldOfView;
    f32 NearPlaneDistance;
    f32 FarPlaneDistance;
    
    b32 UpdateRequired;
};

internal void
FrustumPlanesExtraction(mat4x4 *Matrix, plane *Planes, b32 Normalize)
{
    f32 *MatrixData = Matrix->DataL;
    
    // Right clipping plane
    Planes[FrustumPlane_Right].A = MatrixData[3] - MatrixData[0];
    Planes[FrustumPlane_Right].B = MatrixData[7] - MatrixData[4];
    Planes[FrustumPlane_Right].C = MatrixData[11] - MatrixData[8];
    Planes[FrustumPlane_Right].D = MatrixData[15] - MatrixData[12];
    
    // Left clipping plane
    Planes[FrustumPlane_Left].A = MatrixData[3] + MatrixData[0];
    Planes[FrustumPlane_Left].B = MatrixData[7] + MatrixData[4];
    Planes[FrustumPlane_Left].C = MatrixData[11] + MatrixData[8];
    Planes[FrustumPlane_Left].D = MatrixData[15] + MatrixData[12];
    
    // Bottom clipping plane
    Planes[FrustumPlane_Bottom].A = MatrixData[3] + MatrixData[1];
    Planes[FrustumPlane_Bottom].B = MatrixData[7] + MatrixData[5];
    Planes[FrustumPlane_Bottom].C = MatrixData[11] + MatrixData[9];
    Planes[FrustumPlane_Bottom].D = MatrixData[15] + MatrixData[13];
    
    // Top clipping plane
    Planes[FrustumPlane_Top].A = MatrixData[3] - MatrixData[1];
    Planes[FrustumPlane_Top].B = MatrixData[7] - MatrixData[5];
    Planes[FrustumPlane_Top].C = MatrixData[11] - MatrixData[9];
    Planes[FrustumPlane_Top].D = MatrixData[15] - MatrixData[13];
    
    // Far clipping plane
    Planes[FrustumPlane_Far].A = MatrixData[3] - MatrixData[2];
    Planes[FrustumPlane_Far].B = MatrixData[7] - MatrixData[6];
    Planes[FrustumPlane_Far].C = MatrixData[11] - MatrixData[10];
    Planes[FrustumPlane_Far].D = MatrixData[15] - MatrixData[14];
    
    // Near clipping plane
    Planes[FrustumPlane_Near].A = MatrixData[3] + MatrixData[2];
    Planes[FrustumPlane_Near].B = MatrixData[7] + MatrixData[6];
    Planes[FrustumPlane_Near].C = MatrixData[11] + MatrixData[10];
    Planes[FrustumPlane_Near].D = MatrixData[15] + MatrixData[14];
    
    if(Normalize)
    {
        NormalizePlane(&Planes[FrustumPlane_Right]);
        NormalizePlane(&Planes[FrustumPlane_Left]);
        NormalizePlane(&Planes[FrustumPlane_Bottom]);
        NormalizePlane(&Planes[FrustumPlane_Top]);
        NormalizePlane(&Planes[FrustumPlane_Far]);
        NormalizePlane(&Planes[FrustumPlane_Near]);
    }
}

internal mat4x4
GetPerspectiveProjectionMatrix(camera *Camera)
{
    mat4x4 Result = PerspectiveProjection(Camera->FieldOfView, Camera->AspectRatio, Camera->NearPlaneDistance, Camera->FarPlaneDistance);
    return(Result);
}

internal mat4x4
InitViewMatrix(vec3 CameraPos, vec3 CameraDirection, vec3 CameraUp, vec3 CameraRight)
{
    // NOTE(Cristian): Pre-multiplied camera transposed rotation matrix + negated translation matrix
    mat4x4 LookAt = {
        CameraRight.x,     CameraRight.y,     CameraRight.z,       -Dot(CameraRight, CameraPos),
        CameraUp.x,        CameraUp.y,        CameraUp.z,          -Dot(CameraUp, CameraPos),
        -CameraDirection.x, -CameraDirection.y, -CameraDirection.z, Dot(CameraDirection, CameraPos),
        0, 0, 0, 1
    };
    
    return(LookAt);
}

internal mat4x4
GetViewMatrix(camera *Camera)
{
    mat4x4 LookAt = InitViewMatrix(Camera->Position, Camera->Front, Camera->Up, Camera->Right);
    return(LookAt);
}

internal mat4x4
GetViewMatrix(vec3 CameraPos, vec3 CameraTarget, vec3 UpWorldVector)
{
    vec3 CameraDirection = Normalize(CameraTarget - CameraPos);
    vec3 CameraRight = Normalize(Cross(CameraDirection, UpWorldVector));
    vec3 CameraUp = Cross(CameraRight, CameraDirection);
    
    mat4x4 LookAt = InitViewMatrix(CameraPos, CameraDirection, CameraUp, CameraRight);
    return(LookAt);
}

internal void 
UpdateCameraVectors(camera *Camera)
{
    // TODO(Cristian): Switch from euler angles (which apparently aren't true euler angles) to quaternions
    /*vec3 FrontVector;
    FrontVector.x = cos(DegreesToRadians(Camera->Yaw)) * cos(DegreesToRadians(Camera->Pitch));
    FrontVector.y = sin(DegreesToRadians(Camera->Pitch));
    FrontVector.z = sin(DegreesToRadians(Camera->Yaw)) * cos(DegreesToRadians(Camera->Pitch));
    Camera->Front = Normalize(FrontVector);*/
    
    //vec3 View = V3(1.0f, 0.0f, 0.0f);
    Camera->Front = Normalize(Rotate(Camera->Front, DegreesToRadians(Camera->Yaw), Camera->WorldUp));
    
    vec3 RightVector;
    RightVector = Normalize(Cross(Camera->Front, Camera->WorldUp));
    Camera->Right = RightVector;
    
    Camera->Front = Normalize(Rotate(Camera->Front, DegreesToRadians(Camera->Pitch), Camera->Right));
    
    vec3 UpVector;
    UpVector = Normalize(Cross(Camera->Right, Camera->Front));
    Camera->Up = UpVector;
}

internal camera
GetStandardPerspectiveCamera(f32 AspectRatio = 1.0f)
{
    camera Result = {};
    Result.Yaw = 0.0f;
    Result.Pitch = 0.0f;
    Result.Speed = 2.0f;
    Result.LookSensitivity = 0.1f;
    
    Result.WorldUp = V3(0.0f, 1.0f, 0.0f);
    Result.Front = V3(0.0f, 0.0f, -1.0f);
    Result.Position = V3(0.0f, 0.2f, 1.5f);
    
    Result.FieldOfView = 45.0f;
    Result.NearPlaneDistance = 0.1f;
    Result.FarPlaneDistance = 100.0f;
    
    Result.AspectRatio = AspectRatio;
    
    Result.UpdateRequired = true;
    
    UpdateCameraVectors(&Result);
    
    return(Result);
}

internal camera
BuildCustomPerspectiveCamera(vec3 Position, 
                             vec3 Front,
                             f32 Yaw, 
                             f32 Pitch, 
                             f32 Speed, 
                             f32 LookSensitivity, 
                             f32 FieldOfView,
                             f32 AspectRatio)
{
    camera Result;
    Result.Yaw = Yaw;
    Result.Pitch = Pitch;
    Result.Speed = Speed;
    Result.LookSensitivity = LookSensitivity;
    
    Result.WorldUp = V3(0.0f, 1.0f, 0.0f);
    Result.Front = Front;
    Result.Position = Position;
    
    Result.FieldOfView = FieldOfView;
    
    UpdateCameraVectors(&Result);
    
    return(Result);
}

internal void
ProcessCameraPosition(camera *Camera, camera_direction Direction, r64 Delta)
{
    f32 CameraSpeed = Camera->Speed * (f32) Delta;
    
    switch(Direction)
    {
        case CameraDirection_Forward:
        {
            Camera->Position += Camera->Front * CameraSpeed;
        } break;
        case CameraDirection_Backward:
        {
            Camera->Position -= Camera->Front * CameraSpeed;
        } break;
        case CameraDirection_Left:
        {
            Camera->Position -= Camera->Right * CameraSpeed;
        } break;
        case CameraDirection_Right:
        {
            Camera->Position += Camera->Right * CameraSpeed;
        } break;
        case CameraDirection_Up:
        {
            Camera->Position += Camera->Up * CameraSpeed;
        } break;
        case CameraDirection_Down:
        {
            Camera->Position -= Camera->Up * CameraSpeed;
        } break;
    }
    
    Camera->UpdateRequired = true;
}

internal void
ProcessCameraDirection(camera *Camera, s32 XOffset, s32 YOffset, b32 ConstrainPitch = true)
{
    f32 PitchUpperBound = 89.0f;
    f32 PitchLowerBound = -89.0f;
    
    // NOTE(Cristian): Delta value is not required since mouse screen coordinates act as a delta themselves
    f32 SensAdjustedX = (f32)XOffset * Camera->LookSensitivity;
    f32 SensAdjustedY = (f32)YOffset * Camera->LookSensitivity;
    
    Camera->Yaw = SensAdjustedX;
    Camera->Pitch = SensAdjustedY;
    
    /*if (ConstrainPitch)
    {
        if (Camera->Pitch > PitchUpperBound)
        {
            Camera->Pitch = PitchUpperBound;
        }
        
        if (Camera->Pitch < PitchLowerBound)
        {
            Camera->Pitch = PitchLowerBound;
        }
    }*/
    
    UpdateCameraVectors(Camera);
    
    Camera->UpdateRequired = true;
}