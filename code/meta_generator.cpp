#include "Windows.h"
#include "stdio.h"

#include "lightcore_platform.h"
#include "lightcore_shared_utils.h"
#include "lightcore_tokenizer.h"
#include "lightcore_tokenizer.cpp"

// TODO(Cristian): Parse static arrays


// NOTE(Cristian): Function for parsing multilevel pointers
// static void
// DumpPtr(void *Pointed, int Depth)
// {
//     void *ptr = (int *)&Pointed;
//     for(int i = 0;
//         i < Depth;
//         ++i)
//     {
//         ptr = *(void **)ptr;
//     }

//     int XVal = *(int *)ptr;

//     printf("%d\n", XVal);
// }

void
WriteNewLine(u32 IndentationLevel, HANDLE File, char *FormattedInput, ...)
{
    va_list ArgsList;
	va_start(ArgsList, FormattedInput);
    
    u32 MessageSize = vsnprintf(0, 0, FormattedInput, ArgsList);
    MessageSize += IndentationLevel + 2;
    
    char *FormattedString = (char *)malloc(sizeof(char) * MessageSize);
    if(FormattedString)
    {
        for(u32 Level = 0;
            Level < IndentationLevel;
            ++Level)
        {
            FormattedString[Level] = '\t';
        }
        
        // WARNING(Cristian): This function seems to null terminate the output stream
        vsprintf_s(FormattedString + IndentationLevel, MessageSize - IndentationLevel, FormattedInput, ArgsList);
        FormattedString[MessageSize - 2] = '\n';
        FormattedString[MessageSize - 1] = 0;
        
        // NOTE(Cristian): 0xFFFFFFFF in Offset and OffsetHigh tells WriteFile to write to the end of the file
        OVERLAPPED Overlapped = {};
        Overlapped.Offset = 0xFFFFFFFF;
        Overlapped.OffsetHigh = 0xFFFFFFFF;
        
        DWORD BytesToWrite = MessageSize - 1;
        DWORD BytesWritten;
        WriteFile(File, 
                  FormattedString,
                  BytesToWrite, 
                  &BytesWritten,
                  &Overlapped);
        
        if(BytesToWrite != BytesWritten)
        {
            fprintf(stderr, "ERROR: Failed to write to the file!\n");
        }
    }
    
    va_end(ArgsList);
    free(FormattedString);
}

internal size_t
GetFileSize(HANDLE FileHandle)
{
    LARGE_INTEGER LFileSize;
    GetFileSizeEx(FileHandle, &LFileSize);
    size_t FileSize = LFileSize.QuadPart;
    return(FileSize);
}

internal string
ReadFileIntoMemory(char *FileName)
{
    HANDLE FileHandle = CreateFile(FileName, GENERIC_READ, 
                                   FILE_SHARE_READ | FILE_SHARE_WRITE, 
                                   0, 
                                   OPEN_EXISTING, 
                                   FILE_ATTRIBUTE_NORMAL, 0);
    
    string Result = {};
    
    if(FileHandle != INVALID_HANDLE_VALUE)
    {
        size_t FileSize = GetFileSize(FileHandle);
        
        Result.Count = FileSize + 1;
        Result.Data = (char *)malloc(FileSize + 1);
        
        if(Result.Data)
        {
            DWORD BytesRead;
            OVERLAPPED Overlapped = {};
            
            if(ReadFile(FileHandle, Result.Data, (u32)FileSize, &BytesRead, &Overlapped) &&
               (BytesRead < (u32)FileSize))
            {
                fprintf(stderr, "ERROR: Failed to read the entire FileHandle %s\n", FileName);
            }
                        
            Result.Data[FileSize] = 0;
        }
        else
        {
            fprintf(stderr, "ERROR: Failed to allocate memory in order to read file %s\n", FileName);
        }
        
        CloseHandle(FileHandle);
    }
    else
    {
        fprintf(stderr, "ERROR: Failed to open file %s\n", FileName);
    }
    
    return(Result);
}

struct meta_struct_member
{
    string Type;
    string Identifier;
    b32 IsPointer;
    string Counter;
    u32 PointerDepthCounter;
    string ArrayCounter;
    b32 IsLCArray;
    b32 IsArray;
};

#define MEMBERS_MAX_COUNT 256
struct meta_struct
{
    string Name;
    meta_struct *Next;
    b32 IsIntrospectable;
    
    u32 MembersCount;
    meta_struct_member Members[MEMBERS_MAX_COUNT];
};

struct meta_enum_type
{
    string TypeName;
};

#define TYPES_MAX_COUNT 256
struct meta_enum
{
    string Name;
    meta_enum *Next;
    b32 IsIntrospectable;
    
    u32 TypesCount;
    meta_enum_type Types[TYPES_MAX_COUNT];
};

meta_struct *FirstStructNode;
meta_enum *FirstEnumNode;

internal b32
ParseIntrospectionParams(tokenizer *Tokenizer)
{
    b32 Valid = true;
    
    for(;;)
    {
        token Token = GetToken(Tokenizer);
        
        if(Token.Type == Token_CloseParen)
        {
            break;
        }
        else if(Token.Type == Token_EndOfStream)
        {
            Valid = false;
            break;
        }
    }
    
    return(Valid);
}

internal b32
Parsing(tokenizer *Tokenizer)
{
    b32 Result = !Tokenizer->Error;
    return(Result);
}

internal b32
IsTokenWithParameters(tokenizer *Tokenizer)
{
    b32 Result = false;
    token FutureToken = PeekToken(Tokenizer);
    if(FutureToken.Type == Token_OpenParen)
    {
        Result = true;
    }
    return(Result);
}

internal void
SkipTokenWithParameters(tokenizer *Tokenizer)
{
    b32 IsParsing = true;
    while(IsParsing)
    {
        token Token = GetToken(Tokenizer);
        switch(Token.Type)
        {
            case Token_EndOfStream:
            case Token_CloseParen:
            {
                IsParsing = false;
            } break;
        }
    }
}

internal b32
ParseTokenWithParameters(tokenizer *Tokenizer, token ParametersIdentifierToken, token *ParametersOutput, u32 MaxParameters)
{
    b32 Result = true;
    b32 ExpectNextParameter = true;
    
    RequireToken(Tokenizer, Token_OpenParen);
    if(Parsing(Tokenizer))
    {
        u32 ParametersIndex = 0;
        
        b32 Parsing = true;
        while(Parsing)
        {
            token Token = GetToken(Tokenizer);
            switch(Token.Type)
            {
                case Token_CloseParen:
                {
                    if(ExpectNextParameter && ParametersIndex > 0)
                    {
                        Error(Tokenizer, Token, "Expected another parameter after comma in parameters list '%.*s'", 
                              ParametersIdentifierToken.Text.Count, ParametersIdentifierToken.Text.Data);
                    }
                    else if(ExpectNextParameter && (ParametersIndex == 0))
                    {
                        Error(Tokenizer, Token, "The parameters list '%.*s' is empty", 
                              ParametersIdentifierToken.Text.Count, ParametersIdentifierToken.Text.Data);
                    }
                    
                    Parsing = false;
                } break;
                
                case Token_EndOfStream:
                {
                    Error(Tokenizer, Token, "Reached end of stream for '%.*s'",
                          ParametersIdentifierToken.Text.Count, ParametersIdentifierToken.Text.Data);
                    Parsing = false;
                } break;
                
                case Token_Identifier:
                {
                    if(ExpectNextParameter)
                    {
                        if(ParametersIndex < MaxParameters)
                        {
                            ParametersOutput[ParametersIndex++] = Token;
                            ExpectNextParameter = false;
                        }
                        else
                        {
                            // Reached the maximum allowed parameters list
                            Warning(Tokenizer, Token, "Parameters list '%.*s' exceeds the maximum allowed items of %d",
                                    ParametersIdentifierToken.Text.Count, ParametersIdentifierToken.Text.Data, 
                                    MaxParameters);
                            SkipTokenWithParameters(Tokenizer);
                            Parsing = false;
                            continue;
                        }
                    }
                    
                    token FutureToken = PeekToken(Tokenizer);
                    if(FutureToken.Type == Token_Comma)
                    {
                        ExpectNextParameter = true;
                    }
                    else if(FutureToken.Type != Token_CloseParen)
                    {
                        Error(Tokenizer, Token, "The token '%.*s' in the parameters list '%.*s' is invalid. Expected a comma before another parameter or a closed parenthesis.", 
                              FutureToken.Text.Count, FutureToken.Text.Data, 
                              ParametersIdentifierToken.Text.Count, ParametersIdentifierToken.Text.Data);
                        Parsing = false;
                    }
                } break;
            }
        }
    }
    return(Result);
}


internal b32
ParseCountedArrayParams(tokenizer *Tokenizer, string *MemberArrayCounter)
{
    b32 Valid = false;
    RequireToken(Tokenizer, Token_OpenParen);
    if(Parsing(Tokenizer))
    {
        for(;;)
        {
            token Token = GetToken(Tokenizer);
            if(Token.Type == Token_EndOfStream)
            {
                break;
            }
            else if(Token.Type == Token_CloseParen)
            {
                Valid = true;
                break;
            }
            else if(Token.Type == Token_Identifier)
            {
                CopyString(MemberArrayCounter, &Token.Text);
            }
        }
    }
    return(Valid);
}

internal void
SkipMacro(tokenizer *Tokenizer)
{
    b32 MacroAppendChar = false;
    b32 IsParsing = true;
    
    while(IsParsing)
    {
        token Token = GetTokenRaw(Tokenizer);
        switch(Token.Type)
        {
            case Token_EndOfStream:
            {
                IsParsing = false;
            } break;
            
            case Token_EndOfLine:
            {
                if(!MacroAppendChar)
                {
                    IsParsing = false;
                }
            } break;
            
            case Token_BackwardSlash:
            {
                MacroAppendChar = true;
            } break;
            
            default:
            {
                MacroAppendChar = false;
            }
        }
    }
}

internal meta_struct_member
ParseMember(tokenizer *Tokenizer, token MemberToken, token StructNameToken)
{
    meta_struct_member MemberInfo = {};
    token IdentifierToken = {};
    
    b32 IsPointer = false;
    b32 IsLCArray = false;
    b32 IsArray = false;
    b32 ParsingTokens = true;
    
    u32 PointerDepthCounter = 0;
    string Counter = {};
    
    token MemberTypeToken = {};
    token MemberIdentifierToken = {};
    
    b32 TypeFound = false;
    
    string MemberArrayCounter;
    
    while(ParsingTokens)
    {
        switch(MemberToken.Type)
        {
            case Token_Semicolon:
            case Token_EndOfStream:
            {
                ParsingTokens = false;
            } break;
            
            case Token_Identifier:
            {
                if(IsTokenWithParameters(Tokenizer))
                {
                    if(TokenEquals(MemberToken, "alignas"))
                    {
                        SkipTokenWithParameters(Tokenizer);
                    }
                    else if(TokenEquals(MemberToken, "LC_Array"))
                    {
                        token ArrayTypeToken = {};
                        if(ParseTokenWithParameters(Tokenizer, MemberToken, &ArrayTypeToken, 1))
                        {
                            MemberTypeToken = ArrayTypeToken;
                            TypeFound = true;
                        }
                        
                        IsLCArray = true;
                    }
                    else if(TokenEquals(MemberToken, "counted_array"))
                    {
                        if(ParseCountedArrayParams(Tokenizer, &MemberArrayCounter))
                        {
                            MemberInfo.ArrayCounter = MemberArrayCounter;
                        }
                    }
                    else if(TokenEquals(MemberToken, "volatile"))
                    {
                        // Skip volatile
                    }
                }
                else if(TokenEquals(MemberToken, "struct"))
                {
                    // Skip forward declaration
                }
                else
                {
                    if(!TypeFound)
                    {
                        MemberTypeToken = MemberToken;
                        TypeFound = true;
                    }
                    else
                    {
                        MemberIdentifierToken = MemberToken;
                    }
                }
            } break;
            
            case Token_OpenBracket:
            {
                IsArray = true;
                
                MemberToken = GetToken(Tokenizer);
                if(MemberToken.Type == Token_Identifier || 
                   MemberToken.Type == Token_Number)
                {
                    CopyString(&Counter, &MemberToken.Text);
                }
                else
                {
                    Error(Tokenizer, MemberToken, "Array parameter unknown. Only numbers allowed.");
                }
                RequireToken(Tokenizer, Token_CloseBracket);
            } break;
            
            case Token_Asterisk:
            {
                IsPointer = true;
                PointerDepthCounter = 1;
                
                for(;;)
                {
                    token NextToken = PeekToken(Tokenizer);
                    if(NextToken.Type == Token_EndOfStream ||
                       NextToken.Type != Token_Asterisk)
                    {
                        break;
                    }
                    
                    MemberToken = GetToken(Tokenizer);
                    ++PointerDepthCounter;
                }
                
            } break;
            
            case Token_Colon:
            {
                RequireToken(Tokenizer, Token_Colon);
                if(Parsing(Tokenizer))
                {
                    MemberTypeToken = GetToken(Tokenizer);
                }
                else
                {
                    ParsingTokens = false;
                }
            } break;
        }
        
        if(ParsingTokens)
        {
            MemberToken = GetToken(Tokenizer);
        }
    }
    
    CopyString(&MemberInfo.Type, &MemberTypeToken.Text);
    CopyString(&MemberInfo.Identifier, &MemberIdentifierToken.Text);
    
    MemberInfo.IsPointer = IsPointer;
    CopyString(&MemberInfo.Counter, &Counter);
    MemberInfo.PointerDepthCounter = PointerDepthCounter;
    MemberInfo.IsLCArray = IsLCArray;
    MemberInfo.IsArray = IsArray;
    
    return(MemberInfo);
}

internal b32
CheckIfMemberExists(meta_struct *MetaStructInfo, string Member)
{
    b32 Result = false;
    for(u32 MembersIndex = 0;
        MembersIndex < MetaStructInfo->MembersCount;
        ++MembersIndex)
    {
        if(StringsEqual(MetaStructInfo->Members[MembersIndex].Identifier, Member))
        {
            Result = true;
            break;
        }
    }
    return(Result);
}

u32 MetaTypesCount = 0;
#define META_TYPES_MAX_COUNT 100
string MetaTypes[META_TYPES_MAX_COUNT];

internal void
RetrieveAdditionalMetaTypes(string Type)
{
    b32 MetaTypeAlreadyStored = false;
    
    for(u32 MetaTypesIndex = 0;
        MetaTypesIndex < MetaTypesCount;
        ++MetaTypesIndex)
    {
        if(StringsEqual(MetaTypes[MetaTypesIndex], Type))
        {
            MetaTypeAlreadyStored = true;
            break;
        }
    }
    
    if(!MetaTypeAlreadyStored)
    {
        Assert(MetaTypesCount < META_TYPES_MAX_COUNT);
        CopyString(&MetaTypes[MetaTypesCount++], &Type);
        
        b32 FoundInStructs = false;
        for(meta_struct *Meta = FirstStructNode;
            Meta;
            Meta = Meta->Next)
        {
            if(StringsEqual(Meta->Name, Type))
            {
                FoundInStructs = true;
                Meta->IsIntrospectable = true;
                
                for(u32 MembersIndex = 0;
                    MembersIndex < Meta->MembersCount;
                    ++MembersIndex)
                {
                    RetrieveAdditionalMetaTypes(Meta->Members[MembersIndex].Type);
                }
                break;
            }
        }
        
        if(!FoundInStructs)
        {
            for(meta_enum *Meta = FirstEnumNode;
                Meta;
                Meta = Meta->Next)
            {
                if(StringsEqual(Meta->Name, Type))
                {
                    Meta->IsIntrospectable = true;
                    break;
                }
            }
        }
    }
}

internal void
ParseStruct(tokenizer *Tokenizer, b32 IsIntrospectable = false)
{
    token StructNameToken = GetToken(Tokenizer);
    if(TokenEquals(StructNameToken, "alignas"))
    {
        SkipTokenWithParameters(Tokenizer);
        StructNameToken = GetToken(Tokenizer);
    }
    
    if(OptionalToken(Tokenizer, Token_OpenBrace))
    {
        meta_struct *Node = (meta_struct *)malloc(sizeof(meta_struct));
        *Node = {};
        
        CopyString(&Node->Name, &StructNameToken.Text);
        Node->IsIntrospectable = IsIntrospectable;
        Node->MembersCount = 0;
        
        Node->Next = FirstStructNode;
        FirstStructNode = Node;
        
        for(;;)
        {
            token MemberToken = GetToken(Tokenizer);
            if(MemberToken.Type == Token_CloseBrace)
            {
                break;
            }
            else if(MemberToken.Type == Token_Hash)
            {
                token MacroIdentifierToken = GetToken(Tokenizer);
                if(TokenEquals(MacroIdentifierToken, "include") ||
                   TokenEquals(MacroIdentifierToken, "define") ||
                   TokenEquals(MacroIdentifierToken, "ifdef") ||
                   TokenEquals(MacroIdentifierToken, "ifndef") ||
                   TokenEquals(MacroIdentifierToken, "if") ||
                   TokenEquals(MacroIdentifierToken, "else") ||
                   TokenEquals(MacroIdentifierToken, "endif"))
                {
                    SkipMacro(Tokenizer);
                }
            }
            else
            {
                Assert(Node->MembersCount < MEMBERS_MAX_COUNT);
                
                meta_struct_member MemberInfo = ParseMember(Tokenizer, MemberToken, StructNameToken);
                Node->Members[Node->MembersCount++] = MemberInfo;
            }
        }
        
        if(IsIntrospectable)
        {
            CopyString(&MetaTypes[MetaTypesCount++], &StructNameToken.Text);
            
            for(u32 MembersIndex = 0;
                MembersIndex < Node->MembersCount;
                ++MembersIndex)
            {
                RetrieveAdditionalMetaTypes(Node->Members[MembersIndex].Type);
            }
        }
    }
}

internal void
ParseEnum(tokenizer *Tokenizer, b32 IsIntrospectable = false)
{
    token EnumNameToken = GetToken(Tokenizer);
    if(OptionalToken(Tokenizer, Token_OpenBrace))
    {
        meta_enum *Enum = (meta_enum *)malloc(sizeof(meta_enum));
        *Enum = {};
        
        CopyString(&Enum->Name, &EnumNameToken.Text);
        Enum->TypesCount = 0;
        Enum->IsIntrospectable = IsIntrospectable;
        
        Enum->Next = FirstEnumNode;
        FirstEnumNode = Enum;
        
        for(;;)
        {
            b32 EndOfEnum = false;
            
            token EnumTypeNameToken = GetToken(Tokenizer);
            if(EnumTypeNameToken.Type == Token_CloseBrace)
            {
                break;
            }
            else if(EnumTypeNameToken.Type == Token_Identifier)
            {
                Assert(Enum->TypesCount <= TYPES_MAX_COUNT);
                
                meta_enum_type EnumType = {};
                CopyString(&EnumType.TypeName, &EnumTypeNameToken.Text);
                Enum->Types[Enum->TypesCount++] = EnumType;
                
                b32 Parsing = true;
                while(Parsing)
                {
                    token Token = GetToken(Tokenizer);
                    switch(Token.Type)
                    {
                        case Token_Comma:
                        case Token_EndOfStream:
                        {
                            Parsing = false;
                        } break;
                        
                        case Token_CloseBrace:
                        {
                            EndOfEnum = true;
                            Parsing = false;
                        } break;
                    }
                }
            }
            
            if(EndOfEnum)
            {
                break;
            }
        }
    }
}

internal void
ParseIntrospectable(tokenizer *Tokenizer)
{
    RequireToken(Tokenizer, Token_OpenParen);
    if(Parsing(Tokenizer))
    {
        if(ParseIntrospectionParams(Tokenizer))
        {
            token IdentifierToken = GetToken(Tokenizer);
            if(TokenEquals(IdentifierToken, "struct"))
            {
                ParseStruct(Tokenizer, true);
            }
            else if(TokenEquals(IdentifierToken, "enum"))
            {
                ParseEnum(Tokenizer, true);
            }
            else
            {
                fprintf(stderr, "ERROR: Currently only structs & enums support introspection! \n");
            }
        }
    }
}

internal void
InsertMetaTypesEnum(HANDLE HeaderFile)
{
    if(MetaTypesCount)
    {
        WriteNewLine(0, HeaderFile, "enum meta_type");
        WriteNewLine(0, HeaderFile, "{");
        for(u32 Index = 0;
            Index < MetaTypesCount;
            ++Index)
        {
            WriteNewLine(1, HeaderFile, "MetaType_%.*s%s", 
                         (u32)MetaTypes[Index].Count, MetaTypes[Index].Data, 
                         ((Index + 1) < MetaTypesCount) ? "," : "");
        }
        WriteNewLine(0, HeaderFile, "};\n");
    }
}

global_variable u32 TotalLinesCount = 0;

internal void
TokenizeAndParse(string FileContents, string FileName)
{
    tokenizer Tokenizer = Tokenize(FileContents, FileName);
    
    b32 Parsing = true;
    while(Parsing)
    {
        token Token = GetToken(&Tokenizer);
        switch(Token.Type)
        {
            case Token_EndOfStream:
            {
                Parsing = false;
            } break;
            
            case Token_Unknown:
            {
                
            } break;
            
            case Token_Identifier:
            {
                if(TokenEquals(Token, "introspect"))
                {
                    ParseIntrospectable(&Tokenizer);
                }
                else if(TokenEquals(Token, "struct"))
                {
                    ParseStruct(&Tokenizer);
                }
                else if(TokenEquals(Token, "enum"))
                {
                    ParseEnum(&Tokenizer);
                }
            } break;
            
            case Token_Hash:
            {
                token IdentifierToken = GetToken(&Tokenizer);
                if(TokenEquals(IdentifierToken, "define") ||
                   TokenEquals(IdentifierToken, "ifndef") ||
                   TokenEquals(IdentifierToken, "if") ||
                   TokenEquals(IdentifierToken, "else") ||
                   TokenEquals(IdentifierToken, "endif") ||
                   TokenEquals(IdentifierToken, "include") ||
                   TokenEquals(IdentifierToken, "ifdef"))
                {
                    SkipMacro(&Tokenizer);
                }
            } break;
            
            default:
            {
                //printf("Type %d: %.*s\n", Token.Type, (u32)Token.TextLength, Token.Text);
            } break;
        }
    }
    
    TotalLinesCount += Tokenizer.LineNumber;
}

#define MAX_PATH_LENGTH 1024

internal void
ScanProjectForMetaInformation(char *Path)
{
    Assert(Path != 0);
    
    if(StringLength(Path) > MAX_PATH_LENGTH)
    {
        fprintf(stderr, "ERROR: Path %s size bigger than MAX_PATH_LENGTH\n", Path);
        return;
    }
    
    WIN32_FIND_DATA Data = {};
    HANDLE FileSearchHandle = FindFirstFile(Path, &Data);
    if(FileSearchHandle != INVALID_HANDLE_VALUE)
    {
        while(FindNextFile(FileSearchHandle, &Data))
        {
            // NOTE(Cristian): Might want to disable this in the future,
            // in order to allow the meta generator to process its
            // own files and generate data for itself.
            if(StringsEqual(Data.cFileName, "..") || 
               // NOTE(Cristian): Skip previous directory ("..") in order to avoid looping
               StringsEqual(Data.cFileName, ".") ||
               StringsEqual(Data.cFileName, "meta.h") || 
               StringsEqual(Data.cFileName, "meta.cpp") ||
               StringsEqual(Data.cFileName, "meta_generator.cpp"))
            {
                continue;
            }
            
            char CurrentDirectory[MAX_PATH_LENGTH];
            GetCurrentDirectory(MAX_PATH_LENGTH, CurrentDirectory);
            
            char FullPathTempBuffer[MAX_PATH_LENGTH];
            sprintf_s(FullPathTempBuffer, "%s\\%s", CurrentDirectory, Data.cFileName);
            
            if(GetFileAttributes(FullPathTempBuffer) == FILE_ATTRIBUTE_DIRECTORY)
            {
                SetCurrentDirectory(FullPathTempBuffer);
                                
                char PathAndFilter[MAX_PATH_LENGTH];
                sprintf_s(PathAndFilter, "%s\\*", FullPathTempBuffer);
                
                ScanProjectForMetaInformation(PathAndFilter);
                
                SetCurrentDirectory(CurrentDirectory);
            }
            else
            {
                string ErrorLocation = BundleZ("File name identifier");
                
                string FileName = {};
                CopyString(&FileName, FullPathTempBuffer);
                
                tokenizer FileNameTokenizer = Tokenize(FileName, ErrorLocation);
                
                b32 Searching = true;
                while(Searching)
                {
                    token Token = GetToken(&FileNameTokenizer);
                    switch(Token.Type)
                    {
                        case Token_EndOfStream:
                        {
                            Searching = false;
                        } break;
                        
                        case Token_Dot:
                        {
                            token IdentifierToken = PeekToken(&FileNameTokenizer);
                            if(TokenEquals(IdentifierToken, "cpp") ||
                               TokenEquals(IdentifierToken, "h") ||
                               TokenEquals(IdentifierToken, "c"))
                            {
                                string FileContents = ReadFileIntoMemory(FullPathTempBuffer);
                                TokenizeAndParse(FileContents, FileName);
                                free(FileContents.Data);
                            }
                        } break;
                    }
                }
                
                free(FileName.Data);
            }
        }
        
        FindClose(FileSearchHandle);
    }
}

internal void
GenerateStructIntrospectionData(HANDLE HeaderFile, meta_struct *MetaStructInfo)
{
    if(MetaStructInfo->MembersCount > 0)
    {
        WriteNewLine(0, HeaderFile, "static struct_member_info MembersOf_%.*s[] = ", (u32)MetaStructInfo->Name.Count, MetaStructInfo->Name.Data);
        WriteNewLine(0, HeaderFile, "{");
        
        for(u32 MembersIndex = 0;
            MembersIndex < MetaStructInfo->MembersCount;
            ++MembersIndex)
        {
            meta_struct_member *StructMember = &MetaStructInfo->Members[MembersIndex];
            
            if(StructMember->ArrayCounter.Count)
            {
                if(!CheckIfMemberExists(MetaStructInfo, StructMember->ArrayCounter))
                {
                    fprintf(stderr, "ERROR: Array counter '%.*s' for member %.*s in struct %.*s doesn't exist!\n\n",
                            (u32)StructMember->ArrayCounter.Count, StructMember->ArrayCounter.Data,
                            (u32)StructMember->Identifier.Count, StructMember->Identifier.Data,
                            (u32)MetaStructInfo->Name.Count, MetaStructInfo->Name.Data);
                    StructMember->ArrayCounter = {};
                }
            }
            
            string Counter = BundleZ("0");
            
            string Flags = {};
            if(StructMember->IsPointer && !StructMember->ArrayCounter.Count)
            {
                Flags = BundleZ("MetaFlag_IsPointer");
                
                Counter.Count = snprintf(0, 0, "%d", StructMember->PointerDepthCounter);
                Counter.Data = (char *)malloc(Counter.Count + 1);
                snprintf(Counter.Data, Counter.Count + 1, "%d", StructMember->PointerDepthCounter);
            }
            else if(StructMember->IsLCArray)
            {
                Flags = BundleZ("MetaFlag_IsLCArray");
            }
            else if(StructMember->IsArray)
            {
                Flags = BundleZ("MetaFlag_IsArray");
                
                Counter = {};
                CopyString(&Counter, &StructMember->Counter);
            }
            else
            {
                Flags = BundleZ("0");
            }
            
            WriteNewLine(1, HeaderFile, "{ %.*s, %.*s, MetaType_%.*s, \"%.*s\", \"%.*s\", OFFSET_OF(%.*s, %.*s) }%s", 
                         Flags.Count, Flags.Data,
                         Counter.Count, Counter.Data,
                         (u32)StructMember->Type.Count, StructMember->Type.Data,
                         (u32)StructMember->Identifier.Count, StructMember->Identifier.Data,
                         (u32)StructMember->ArrayCounter.Count, StructMember->ArrayCounter.Data,
                         (u32)MetaStructInfo->Name.Count, MetaStructInfo->Name.Data,
                         (u32)StructMember->Identifier.Count, StructMember->Identifier.Data,
                         ((MembersIndex + 1) < MetaStructInfo->MembersCount) ? "," : "");
        }
        WriteNewLine(0, HeaderFile, "};\n");
    }
    else
    {
        fprintf(stderr, "ERROR: Struct %.*s has no members!\n\n", 
                (u32)MetaStructInfo->Name.Count, MetaStructInfo->Name.Data);
    }
}

internal void
GenerateEnumIntrospectionData(HANDLE HeaderFile, meta_enum *MetaEnumInfo)
{
    if(MetaEnumInfo->TypesCount > 0)
    {
        WriteNewLine(0, HeaderFile, "static enum_member_info MembersOf_%.*s[] = ", (u32)MetaEnumInfo->Name.Count, MetaEnumInfo->Name.Data);
        WriteNewLine(0, HeaderFile, "{");
        
        for(u32 TypesIndex = 0;
            TypesIndex < MetaEnumInfo->TypesCount;
            ++TypesIndex)
        {
            meta_enum_type *EnumType = &MetaEnumInfo->Types[TypesIndex];
            
            WriteNewLine(1, HeaderFile, "{ %.*s, \"%.*s\" }%s", 
                         (u32)EnumType->TypeName.Count, EnumType->TypeName.Data,
                         (u32)EnumType->TypeName.Count, EnumType->TypeName.Data,
                         ((TypesIndex + 1) < MetaEnumInfo->TypesCount) ? ",": "");
        }
        
        WriteNewLine(0, HeaderFile, "};\n");
    }
}

s32
main(s32 ArgCount, char **Args)
{
    s32 ErrorCode = 0;
    
    HANDLE GenHeaderFile = CreateFile("meta_generated.h", 
                                      GENERIC_WRITE,
                                      FILE_SHARE_READ | FILE_SHARE_WRITE, 
                                      0, 
                                      CREATE_ALWAYS, 
                                      FILE_ATTRIBUTE_NORMAL, 
                                      0
                                      );
    
    HANDLE GenImplFile = CreateFile("meta_generated.cpp", 
                                    GENERIC_WRITE, 
                                    FILE_SHARE_READ | FILE_SHARE_WRITE, 
                                    0, 
                                    CREATE_ALWAYS, 
                                    FILE_ATTRIBUTE_NORMAL, 
                                    0
                                    );
    
    ScanProjectForMetaInformation(".\\*");
    
    if(FirstStructNode || FirstEnumNode)
    {
        char *IntrospectionBanner = 
        {
            "//////////////////////////////////\n"
                "// Generated introspection data //\n"
                "//////////////////////////////////\n"
        };
        WriteNewLine(0, GenImplFile, "%s", IntrospectionBanner);
        
        if(FirstStructNode)
        {
            char *StructMembersInfo = 
            {
                "/**********************************************************************\n"
                    "* Member info layout\n"
                    "* { IsPointer, PointerDepth, MetaType, MemberName, ArrayCounter, Offset }\n"
                    "**********************************************************************/\n"
            };
            
            WriteNewLine(0, GenImplFile, "%s", StructMembersInfo);
        }
        
        b32 InstrospectablesParsed = false;
        
        for(meta_struct *Meta = FirstStructNode;
            Meta;
            Meta = Meta->Next)
        {
            if(Meta->IsIntrospectable)
            {
                RetrieveAdditionalMetaTypes(Meta->Name);
            }
        }
        
        for(meta_struct *Meta = FirstStructNode;
            Meta;
            Meta = Meta->Next)
        {
            if(Meta->IsIntrospectable)
            {
                GenerateStructIntrospectionData(GenImplFile, Meta);
                InstrospectablesParsed = true;
            }
        }
        
        InsertMetaTypesEnum(GenHeaderFile);
        
        for(meta_enum *Meta = FirstEnumNode;
            Meta;
            Meta = Meta->Next)
        {
            if(Meta->IsIntrospectable)
            {
                GenerateEnumIntrospectionData(GenImplFile, Meta);
                InstrospectablesParsed = true;
            }
        }
        
        if(!InstrospectablesParsed)
        {
            fprintf(stdout, "Introspection data not generated: no introspectables found\n");
        }
    }
    /*else
    {
        if(!FirstStructNode)
        {
            fprintf(stderr, "ERROR: Meta information not found (structs)\n");
            ErrorCode = -1;
        }
        
        if(!FirstEnumNode)
        {
            fprintf(stderr, "ERROR: Meta information not found (enums)\n");
            ErrorCode = -1;
        }
    }*/
    
    CloseHandle(GenHeaderFile);
    CloseHandle(GenImplFile);
    
    return ErrorCode;
}