#include "text.h"

/*struct Character
    {
        unsigned int TextureID;
        glm::ivec2 Size;
        glm::ivec2 Bearing;
        unsigned int Advance;
    };*/

    //Character CharList[128];

    // FREETYPE LIBRARY INIT
    // Note: Freetype functions return non-zero values on error

    //FT_Library Freetype;
    //FT_Face FreetypeFace;
    //if (FT_Init_FreeType(&Freetype))
    //{
    //    printf("ERROR::FREETYPE: Could not init FreeType library\n");
    //}
    //else
    //{
    //    if (FT_New_Face(Freetype, "Resources/Fonts/arial.ttf", 0, &FreetypeFace))
    //    {
    //        printf("ERROR::FREETYPE: Failed to load font\n");
    //    }
    //    else
    //    {
    //        FT_Set_Pixel_Sizes(FreetypeFace, 0, 48);

    //        glPixelStorei(GL_UNPACK_ALIGNMENT, 1); // disable byte-alignment restriction

    //        for (char character = 0;
    //            character < sizeof(CharList) / sizeof(Character);
    //            character++)
    //        {
    //            if (FT_Load_Char(FreetypeFace, character, FT_LOAD_RENDER))
    //            {
    //                printf("ERROR::FREETYTPE: Failed to load Glyph\n");
    //                continue;
    //            }

    //            GLuint GlyphTexture;
    //            glGenTextures(1, &GlyphTexture);
    //            glBindTexture(GL_TEXTURE_2D, GlyphTexture);

    //            glTexImage2D(
    //                GL_TEXTURE_2D,
    //                0,
    //                GL_RED,
    //                FreetypeFace->glyph->bitmap.width,
    //                FreetypeFace->glyph->bitmap_top,
    //                0,
    //                GL_RED,
    //                GL_UNSIGNED_BYTE,
    //                FreetypeFace->glyph->bitmap.buffer);

    //            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    //            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    //            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    //            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    //            Character Result = {};
    //            Result.TextureID = GlyphTexture;
    //            Result.Size = glm::ivec2(FreetypeFace->glyph->bitmap.width, FreetypeFace->glyph->bitmap.rows);
    //            Result.Bearing = glm::ivec2(FreetypeFace->glyph->bitmap_left, FreetypeFace->glyph->bitmap_top);
    //            Result.Advance = FreetypeFace->glyph->advance.x;

    //            CharList[character] = Result;
    //        }

    //        FT_Done_Face(FreetypeFace);
    //        FT_Done_FreeType(Freetype);
    //    }
    //}

//void
//RenderText(shader* Shader, const char* Text, glm::vec2 Origin, float Scale, glm::vec3 TextColor)
//{
//    glm::mat4 GlyphProjection = glm::ortho(0.0f, (float)windowSize.width, 0.0f, (float)windowSize.height);
//
//    const int NrVertices = 6;
//    const int NrComponents = 4;
//
//    float CursorPos = Origin.x;
//
//    GLuint VAO, VBO;
//    glGenVertexArrays(1, &VAO);
//    glGenBuffers(1, &VBO);
//
//    glBindVertexArray(VAO);
//    glBindBuffer(GL_ARRAY_BUFFER, VBO);
//    glBufferData(GL_ARRAY_BUFFER, NrVertices * NrComponents * sizeof(GLfloat), nullptr, GL_DYNAMIC_DRAW);
//
//    glEnableVertexAttribArray(0);
//    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), nullptr);
//    glEnableVertexAttribArray(1);
//    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (void*)(2 * sizeof(GLfloat)));
//
//    glActiveTexture(GL_TEXTURE0);
//
//    glUseProgram(Shader->ID);
//    SetUniformMat4(Shader->ID, "Projection", GlyphProjection);
//    SetUniformInt(Shader->ID, "Text", 0);
//    SetUniformVec3(Shader->ID, "TextColor", TextColor);
//
//    for (char c = *Text; c; c = *++Text)
//    {
//        Character Char = CharList[c];
//
//        glBindTexture(GL_TEXTURE_2D, Char.TextureID);
//
//        float Width = Char.Size.x * Scale;
//        float Height = Char.Size.y * Scale;
//
//        float XPos = CursorPos + Char.Bearing.x * Scale;
//        float YPos = Origin.y - (Char.Size.y - Char.Bearing.y) * Scale;
//
//        // Texture coordinates inverted
//        GLfloat GlyphData[NrVertices * NrComponents] = {
//            XPos,           YPos + Height,      0.0f, 0.0f,
//            XPos,           YPos,               0.0f, 1.0f,
//            XPos + Width,   YPos,               1.0f, 1.0f,
//
//            XPos,           YPos + Height,      0.0f, 0.0f,
//            XPos + Width,   YPos,               1.0f, 1.0f,
//            XPos + Width,   YPos + Height,      1.0f, 0.0f
//        };
//
//        glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(GlyphData), GlyphData);
//
//        glDrawArrays(GL_TRIANGLES, 0, NrVertices);
//
//        CursorPos += (Char.Advance >> 6) * Scale; // bitshift by 6 to get value in pixels (2^6 = 64)
//    }
//
//    glBindTexture(GL_TEXTURE_2D, 0);
//    glBindBuffer(GL_ARRAY_BUFFER, 0);
//    glBindVertexArray(0);
//}