internal mesh *
GetFreeMeshSlot(mesh_storage *Storage)
{
    mesh *Result = 0;
    
    if(Storage->Used < Storage->Capacity)
    {
        for(u32 Index = 0;
            Index < Storage->Capacity;
            ++Index)
        {
            mesh *CurrentSlot = &Storage->Meshes[Index];
            if(!CurrentSlot->InUse)
            {
                Result = CurrentSlot;
                //CurrentSlot->InUse = true;
                Storage->Used++;
                break;
            }
        }
    }
    
    return(Result);
}

internal void
LinkMeshToAsset(asset *Asset, mesh *Mesh, mesh_storage *Storage)
{
    mesh *FreeMeshSlot = GetFreeMeshSlot(Storage);
    if(FreeMeshSlot)
    {
        if(Asset->Meshes.Head)
        {
            Asset->Meshes.Tail = Asset->Meshes.Tail->Next = FreeMeshSlot;
        }
        else
        {
            Asset->Meshes.Tail = Asset->Meshes.Head = FreeMeshSlot;
        }
        
        *FreeMeshSlot = *Mesh;
        FreeMeshSlot->InUse = true;
        
        Asset->Meshes.TotalMeshes++;
    }
}

internal void
ReleaseMeshesFromAsset(asset *Asset, mesh_storage *Storage)
{
    mesh *CurrentMesh = Asset->Meshes.Head;
    
    u32 TotalMeshes = Asset->Meshes.TotalMeshes;
    for(u32 Index = 0;
        Index < TotalMeshes;
        ++Index)
    {
        mesh *NextMesh = CurrentMesh->Next;
        
        *CurrentMesh = {};
        CurrentMesh = NextMesh;
    }
    
    Asset->Meshes.Head = Asset->Meshes.Tail = 0;
    
    Assert(Storage->Used >= Asset->Meshes.TotalMeshes);
    
    Storage->Used -= Asset->Meshes.TotalMeshes;
    
    Asset->Meshes.TotalMeshes = 0;
}