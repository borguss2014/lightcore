#define WIN32_LOAD_RENDERER(name) platform_renderer *name(HDC DeviceContextHandle, platform_api PlatformAPI, memory_arena *Arena)
typedef WIN32_LOAD_RENDERER(win32_load_renderer);

struct win32_renderer_function_table
{
    win32_load_renderer *LoadRenderer;
    renderer_begin_frame *BeginFrame;
    renderer_end_frame *EndFrame;
};

global_variable char *Win32RendererFunctionTableNames[] = 
{
    "Win32LoadRenderer",
    "Win32BeginFrame",
    "Win32EndFrame",
};