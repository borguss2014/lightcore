#if LIGHTCORE_DEBUG

struct debug_state
{
    b32 DebugRecordingEnabled;
    bool ProfilingWindowActive;
};

enum debug_event_type
{
    DebugEvent_Unknown,
    
    DebugEvent_BeginBlock,
    DebugEvent_EndBlock,
    DebugEvent_BeginFrameBoundary,
    DebugEvent_EndFrameBoundary
};

struct debug_statistics
{
    u64 MinCycles;
    u64 MaxCycles;
    u64 ChildrenCycles;
    u64 TotalCycles;
    
    u32 HitCount;
    u32 __Reserved;
};

struct debug_profile_node
{
    union
    {
        debug_profile_node *Next;
        debug_profile_node *NextFree;
    };
    
    debug_profile_node *NextCleanupProfileNode;
    debug_profile_node *LastChildProfileNode;
    debug_profile_node *NextLink;
    
    debug_statistics Statistics;
    
    char *Name;
    char *GUID;
    
    char *FileName;
    char *FunctionName;
    
    u64 StartCycles;
    u64 EndCycles;
    
    u32 LineNumber;
    u32 __Reserved;
};

struct debug_profile_group
{
    union
    {
        debug_profile_group *Next;
        debug_profile_group *NextFree;
    };
    
    debug_profile_node *LastCleanupProfileNode;
    
    u32 ThreadID;
    debug_profile_node *LastProfileNode;
};

struct debug_frame
{
    debug_profile_group *LastProfileGroup;
    
    u64 FrameBeginCycles;
    u64 FrameEndCycles;
    
    u64 TotalCycles;
    u32 TotalTimeMS;
    
    u32 __Reserved;
};

struct debug_open_block
{
    union
    {
        debug_open_block *Next;
        debug_open_block *NextFree;
    };
    
    debug_profile_node *ProfileNode;
};

struct debug_thread
{
    union
    {
        debug_thread *Next;
        debug_thread *NextFree;
    };
    
    u32 ID;
    
    debug_open_block *LastOpenBlock;
    debug_profile_group *ProfileGroup;
};

#define MAX_DEBUG_FRAMES 256

struct debug_memory
{
    memory_arena DebugArena;
    
    debug_thread *FirstThreadNode;
    debug_thread *FirstFreeThreadNode;
    
    debug_open_block *FirstFreeOpenBlock;
    debug_profile_node *FirstFreeProfileNode;
    debug_profile_group *FirstFreeProfileGroup;
    
    u64 LastFrameTimestamp;
    
    u32 NextFrame;
    debug_frame Frames[MAX_DEBUG_FRAMES];
};

struct debug_event
{
    u64 Cycles;
    u32 ThreadID;
    
    char *GUID;
    char *Name;
    
    u8 Type;
    u8 __Reserved;
};

#define DEBUG_EVENTS_MAX_SIZE 16384

struct debug_table
{
    u64 volatile EventArrayIndex_EventIndex;
    
    u32 EventIncrement;
    u32 EventArrayIndex;
    
    debug_event *Events;
};

#define GenerateDebugGUID__(A, B) A "|" __FUNCTION__ "|" #B
#define GenerateDebugGUID_(FName, LNumber) GenerateDebugGUID__(FName, LNumber)
#define GenerateDebugGUID() GenerateDebugGUID_(__FILE__, __LINE__)

extern debug_table *GlobalDebugTable;

#define BeginRecordDebugEvent(EventType, EventName, GUIDInit) \
u64 EventArrayIndex_EventIndex = AtomicAddU64(&GlobalDebugTable->EventArrayIndex_EventIndex, GlobalDebugTable->EventIncrement); \
u32 EventIndex = EventArrayIndex_EventIndex & 0xFFFFFFFF; \
Assert(EventIndex < DEBUG_EVENTS_MAX_SIZE); \
debug_event *Event = &GlobalDebugTable->Events[(EventArrayIndex_EventIndex >> 32) * DEBUG_EVENTS_MAX_SIZE + EventIndex]; \
Event->ThreadID = GetThreadID(); \
Event->GUID = GUIDInit;  \
Event->Name = EventName; \
Event->Type = (u8)EventType; \
Event->Cycles = __rdtsc(); \

#define EndRecordDebugEvent(EventType, EventName, GUIDInit) \
u64 EventEndCycles = __rdtsc(); \
u64 EventArrayIndex_EventIndex = AtomicAddU64(&GlobalDebugTable->EventArrayIndex_EventIndex, GlobalDebugTable->EventIncrement); \
u32 EventIndex = EventArrayIndex_EventIndex & 0xFFFFFFFF; \
Assert(EventIndex < DEBUG_EVENTS_MAX_SIZE); \
debug_event *Event = &GlobalDebugTable->Events[(EventArrayIndex_EventIndex >> 32) * DEBUG_EVENTS_MAX_SIZE + EventIndex]; \
Event->ThreadID = GetThreadID(); \
Event->GUID = GUIDInit;  \
Event->Name = EventName; \
Event->Type = (u8)EventType; \
Event->Cycles = EventEndCycles; \

#define BEGIN_TIMED_FRAME() \
{ \
u64 EventArrayIndex_EventIndex = AtomicAddU64(&GlobalDebugTable->EventArrayIndex_EventIndex, GlobalDebugTable->EventIncrement); \
u32 EventIndex = EventArrayIndex_EventIndex & 0xFFFFFFFF; \
Assert(EventIndex < DEBUG_EVENTS_MAX_SIZE); \
debug_event *Event = &GlobalDebugTable->Events[(EventArrayIndex_EventIndex >> 32) * DEBUG_EVENTS_MAX_SIZE + EventIndex]; \
Event->Type = DebugEvent_BeginFrameBoundary; \
Event->ThreadID = GetThreadID(); \
Event->Cycles = __rdtsc(); \
} \

#define END_TIMED_FRAME() \
{ \
u64 StoredCycles = __rdtsc(); \
u64 EventArrayIndex_EventIndex = AtomicAddU64(&GlobalDebugTable->EventArrayIndex_EventIndex, GlobalDebugTable->EventIncrement); \
u32 EventIndex = EventArrayIndex_EventIndex & 0xFFFFFFFF; \
Assert(EventIndex < DEBUG_EVENTS_MAX_SIZE); \
debug_event *Event = &GlobalDebugTable->Events[(EventArrayIndex_EventIndex >> 32) * DEBUG_EVENTS_MAX_SIZE + EventIndex]; \
Event->Type = DebugEvent_EndFrameBoundary; \
Event->ThreadID = GetThreadID(); \
Event->Cycles = StoredCycles; \
} \

#define BEGIN_TIMED_BLOCK_(DebugName) { BeginRecordDebugEvent(DebugEvent_BeginBlock, DebugName, GenerateDebugGUID()); }
#define BEGIN_TIMED_BLOCK(DebugName) BEGIN_TIMED_BLOCK_(DebugName)

#define END_TIMED_BLOCK_(DebugName) { EndRecordDebugEvent(DebugEvent_EndBlock,  DebugName, GenerateDebugGUID()); }
#define END_TIMED_BLOCK() END_TIMED_BLOCK_("END_BLOCK__")

#define TIMED_BLOCK__(DebugRecordIndex, ...) timed_block TimedBlock##DebugRecordIndex(__COUNTER__, __FILE__, __FUNCTION__, __LINE__, ## __VA_ARGS__)
#define TIMED_BLOCK_(DebugRecordIndex, ...) TIMED_BLOCK__(DebugRecordIndex, ## __VA_ARGS__)
#define TIMED_BLOCK(...) TIMED_BLOCK_(__LINE__, ## __VA_ARGS__)


struct timed_block
{
    u64 StartCycles;
    
    //debug_record *Record;
    u32 HitCount;
    
    timed_block(u32 DebugRecordIndex, char *FileName, char *FunctionName, u32 LineNumber, u32 HitCountInit = 1)
    {
        StartCycles = __rdtsc();
        
        //Record = DebugRecordsArray + DebugRecordIndex;
        /*Record->FileName = FileName;
        Record->FunctionName = FunctionName;
        Record->LineNumber = LineNumber;*/
        HitCount = HitCountInit;
    }
    
    ~timed_block()
    {
        u64 CyclesDelta = (__rdtsc() - StartCycles) | ((u64)HitCount << 32);
        //AtomicAddU64(&Record->HitCount_CyclesCount, CyclesDelta);
        
        // u64 HitCount_CyclesCount = AtomicExchangeU64(&Record->HitCount_CyclesCount, 0);
        
        // u32 HCount = GetAddressHigh(HitCount_CyclesCount);
        // u32 CCount = GetAddressLow(HitCount_CyclesCount);
    }
};

#else

#define BEGIN_TIMED_BLOCK(x)
#define END_TIMED_BLOCK()

#define BEGIN_TIMED_FRAME()
#define END_TIMED_FRAME()

#define TIMED_BLOCK

#endif