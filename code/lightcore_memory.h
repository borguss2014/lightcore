#pragma once
#include <cstdint>
#include <cassert>
#include "debug_print.h"

struct memory_arena
{
    platform_memory_block *CurrentBlock;
    u32 AllocationFlags;
    memory_size MinimumBlockSize;
};

struct arena_push_params
{
    u32 Flags;
    u32 Alignment;
    
    memory_size MinimumBlockSize;
};

inline arena_push_params
DefaultPushParams(void)
{
    arena_push_params Params = {};
    Params.Alignment = 4;
    Params.MinimumBlockSize = Megabytes(1);
    
    return(Params);
}

inline b8
IsPow2(u32 Value)
{
    b8 Result = false;
    
    u32 Mask = Value - 1;
    if (!(Value & Mask))
    {
        Result = true;
    }
    
    return(Result);
}

inline memory_size
GetAlignmentOffset(memory_arena *Arena, memory_size Alignment)
{
    memory_size ResultPointer = (memory_size)Arena->CurrentBlock->Base + Arena->CurrentBlock->Used;
    memory_size AlignmentMask = Alignment - 1;
    memory_size BitOpResult = ResultPointer & AlignmentMask;
    
    memory_size AlignmentOffset = 0;
    if (BitOpResult)
    {
        AlignmentOffset = Alignment - BitOpResult;
    }
    
    return(AlignmentOffset);
}

inline memory_size
AlignToPow2(memory_size Size, memory_size Pow2Alignment)
{
    memory_size AlignmentOffset = 0;
    
    memory_size AlignmentMask = Pow2Alignment - 1;
    memory_size BitOpResult = Size & AlignmentMask;
    if (BitOpResult)
    {
        AlignmentOffset = Pow2Alignment - BitOpResult;
    }
    
    memory_size AlignedSize = Size + AlignmentOffset;
    return(AlignedSize);
}

inline memory_size
GetEffectiveSize(memory_arena *Arena, memory_size SizeInit, memory_size Alignment)
{
    memory_size Size = SizeInit;
    
    memory_size AlignmentOffset = GetAlignmentOffset(Arena, Alignment);
    Size += AlignmentOffset;
    
    return(Size);
}

#define PushStruct(Arena, Type) (Type *) PushSize_(Arena, sizeof(Type))
#define PushArray(Arena, Count, Type) (Type *) PushSize_(Arena, sizeof(Type) * Count)
#define PushSlice(Arena, Size) PushSize_(Arena, Size)

internal void
InitializeArena(memory_arena *Arena, arena_push_params Params = DefaultPushParams())
{
    Assert(Params.Alignment <= 128);
    Assert(IsPow2(Params.Alignment));
    
    memory_size Size = Params.MinimumBlockSize;
    
    if(Arena->AllocationFlags & (PlatformFlags_UnderflowCheck | PlatformFlags_OverflowCheck))
    {
        Arena->MinimumBlockSize = 0;
        Size = AlignToPow2(Size, Params.Alignment);
    }
    
    platform_memory_block *Block = Platform.AllocateMemory(Size, Arena->AllocationFlags);
    Block->PrevBlock = Arena->CurrentBlock;
    Arena->CurrentBlock = Block;
}

inline void *
PushSize_(memory_arena *Arena, memory_size SizeInit, arena_push_params Params = DefaultPushParams())
{
    void *Result = 0;
    
    Assert(Params.Alignment <= 128);
    Assert(IsPow2(Params.Alignment));
    
    memory_size Size = 0;
    if(Arena->CurrentBlock)
    {
        Size = GetEffectiveSize(Arena, SizeInit, Params.Alignment);
    }
    
    if(!Arena->CurrentBlock || 
       ((Arena->CurrentBlock->Used + Size) > Arena->CurrentBlock->Size))
    {
        Size = SizeInit;
        
        if(Arena->AllocationFlags & (PlatformFlags_UnderflowCheck | PlatformFlags_OverflowCheck))
        {
            Arena->MinimumBlockSize = 0;
            Size = AlignToPow2(Size, Params.Alignment);
        }
        else if(!Arena->MinimumBlockSize)
        {
            Arena->MinimumBlockSize = Megabytes(1);
        }
        
        memory_size AllocationSize = (Size > Arena->MinimumBlockSize) ? SizeInit : Arena->MinimumBlockSize;
        
        platform_memory_block *Block = Platform.AllocateMemory(AllocationSize, Arena->AllocationFlags);
        Block->PrevBlock = Arena->CurrentBlock;
        Arena->CurrentBlock = Block;
    }
    
    memory_size AlignmentOffset = GetAlignmentOffset(Arena, Params.Alignment);
    memory_size OffsetInBlock = Arena->CurrentBlock->Used + AlignmentOffset;
    
    Result = (void *)((ump)Arena->CurrentBlock->Base + OffsetInBlock);
    Arena->CurrentBlock->Used += Size;
    
    Assert(Size >= SizeInit);
    Assert(Arena->CurrentBlock->Used <= Arena->CurrentBlock->Size);
    
    return(Result);
}

struct temporary_memory
{
    memory_arena *Arena;
    platform_memory_block *Block;
    mmsz Used;
};

#define TemporaryMemory_InitializeArena true

internal temporary_memory
BeginTemporaryMemory(memory_arena *Arena, b32 InitArena = false)
{
    if(InitArena && !Arena->CurrentBlock)
    {
        InitializeArena(Arena);
    }
    
    temporary_memory Result;
    Result.Arena = Arena;
    Result.Block = Arena->CurrentBlock;
    Result.Used = Arena->CurrentBlock->Used;
    
    return(Result);
}

internal void
FreeLastMemoryBlock(memory_arena *Arena)
{
    platform_memory_block *FreeBlock = Arena->CurrentBlock;
    Arena->CurrentBlock = FreeBlock->PrevBlock;
    Platform.DeallocateMemory(FreeBlock);
}

internal void
EndTemporaryMemory(temporary_memory TempMemory)
{
    memory_arena *Arena = TempMemory.Arena;
    
    while(Arena->CurrentBlock != TempMemory.Block)
    {
        FreeLastMemoryBlock(Arena);
    }
    
    if(Arena->CurrentBlock)
    {
        Assert(Arena->CurrentBlock->Used >= TempMemory.Used);
        Arena->CurrentBlock->Used = TempMemory.Used;
    }
}

internal void
ClearArena(memory_arena *Arena)
{
    platform_memory_block *PreviousBlock = Arena->CurrentBlock->PrevBlock;
    while(PreviousBlock)
    {
        platform_memory_block *FreeBlock = Arena->CurrentBlock;
        Arena->CurrentBlock = PreviousBlock;
        Platform.DeallocateMemory(FreeBlock);
        
        PreviousBlock = Arena->CurrentBlock->PrevBlock;
    }
    
    Assert(Arena->CurrentBlock);
    
    Arena->CurrentBlock->Used = 0;
}

enum ringbuffer_block_flags
{
    BufferBlock_Reserved = 0,
    
    BufferBlock_Loaded,
    BufferBlock_Delete
};

struct ring_buffer
{
    void *Buffer;
    memory_size Size;
    
    memory_size volatile ReadOffset;
    memory_size volatile WriteOffset;
};

struct ringbuffer_block_header
{
    memory_size Size;
    u32 Flags;
};

internal ring_buffer
CreateRingBuffer(memory_size RequestedSize)
{
    return(Platform.AllocateRingBufferMemory(RequestedSize));
}

inline memory_size
GetRingBufferUsedSize(ring_buffer *RingBuffer)
{
    memory_size CurrentlyUsed = RingBuffer->WriteOffset - RingBuffer->ReadOffset;
    return(CurrentlyUsed);
}

inline memory_size
GetRingBufferRemainingSize(ring_buffer *RingBuffer)
{
    memory_size UsedSize = GetRingBufferUsedSize(RingBuffer);
    memory_size RemainingSize = RingBuffer->Size - UsedSize - sizeof(ringbuffer_block_header);
    return(RemainingSize);
}

// TODO(Cristian): Maybe should use a hash to verify block integrity and validity
inline ringbuffer_block_header *
GetRingBufferBlockHeader(void *Base)
{
    ringbuffer_block_header *BlockHeader = (ringbuffer_block_header *)Base - 1;
    return(BlockHeader);
}

internal void
SetRingBufferBlockFlags(void *Base, u32 Flags)
{
    ringbuffer_block_header *BlockHeader = GetRingBufferBlockHeader(Base);
    BlockHeader->Flags = Flags;
}

global_variable ticket_mutex MemoryMutex;

internal void *
PushRingBufferBlock(ring_buffer *RingBuffer, memory_size Size)
{
    void *Result = 0;
    
    Assert(RingBuffer);
    
    TicketMutexLock(&MemoryMutex);
    memory_size CurrentlyUsed = RingBuffer->WriteOffset - RingBuffer->ReadOffset;
    memory_size PotentiallyUsed = CurrentlyUsed + Size + sizeof(ringbuffer_block_header);
    if(PotentiallyUsed <= RingBuffer->Size)
    {
        void *BlockHeader = (void *)((ump)RingBuffer->Buffer + RingBuffer->WriteOffset);
        
        memory_size NewBlockSize = sizeof(ringbuffer_block_header) + Size;
        RingBuffer->WriteOffset += NewBlockSize;
        
        *(ringbuffer_block_header *)BlockHeader = {};
        ((ringbuffer_block_header *)BlockHeader)->Size = Size;
        
        Result = (void *)((ump)BlockHeader + sizeof(ringbuffer_block_header));
    }
    TicketMutexUnlock(&MemoryMutex);
    
    return(Result);
}

internal void *
PopRingBufferBlock(ring_buffer *RingBuffer)
{
    void *Result = 0;
    
    Assert(RingBuffer);
    
    memory_size CurrentlyUsed = RingBuffer->WriteOffset - RingBuffer->ReadOffset;
    if(CurrentlyUsed >= sizeof(ringbuffer_block_header))
    {
        void *BlockHeader = (void *)((ump)RingBuffer->Buffer + RingBuffer->ReadOffset);
        
        Result = (void *)((ump)BlockHeader + sizeof(ringbuffer_block_header));
        RingBuffer->ReadOffset += (sizeof(ringbuffer_block_header) + ((ringbuffer_block_header *)BlockHeader)->Size);
        
        if(RingBuffer->ReadOffset >= RingBuffer->Size)
        {
            RingBuffer->ReadOffset -= RingBuffer->Size;
            RingBuffer->WriteOffset -= RingBuffer->Size;
        }
    }
    
    return(Result);
}

internal void *
PeekRingBufferBlock(ring_buffer *RingBuffer)
{
    void *Result = 0;
    
    Assert(RingBuffer);
    
    memory_size CurrentlyUsed = RingBuffer->WriteOffset - RingBuffer->ReadOffset;
    if(CurrentlyUsed >= sizeof(ringbuffer_block_header))
    {
        Result = (void *)((ump)RingBuffer->Buffer + RingBuffer->ReadOffset + sizeof(ringbuffer_block_header));
    }
    
    return(Result);
}

struct ring_buffer_iterator
{
    ring_buffer RingBuffer;
};

internal ring_buffer_iterator
InitializeIterator(ring_buffer *RingBuffer)
{
    Assert(RingBuffer);
    
    ring_buffer_iterator Iterator = {};
    Iterator.RingBuffer = *RingBuffer;
    
    return(Iterator);
}

internal void *
NextRingBufferBlock(ring_buffer_iterator *Iterator)
{
    void *Result = 0;
    
    memory_size CurrentlyUsed = GetRingBufferUsedSize(&Iterator->RingBuffer);
    if(CurrentlyUsed >= sizeof(ringbuffer_block_header))
    {
        void *BlockHeader = (void *)((ump)Iterator->RingBuffer.Buffer + Iterator->RingBuffer.ReadOffset);
        
        Result = (void *)((ump)BlockHeader + sizeof(ringbuffer_block_header));
        Iterator->RingBuffer.ReadOffset += (sizeof(ringbuffer_block_header) + ((ringbuffer_block_header *)BlockHeader)->Size);
    }
    
    return(Result);
}

internal void
RingBufferWrapTest(ring_buffer *RingBuffer)
{
    char *Test = (char *)RingBuffer->Buffer;
    Test[0] = 'a';
    
    Assert(Test[RingBuffer->Size] == 'a');
    
    Test[0] = 0;
}

internal void
CleanupTransientStorage(ring_buffer *Storage)
{
    ring_buffer_iterator Iterator = InitializeIterator(Storage);
    
    void *NextDataBlock = NextRingBufferBlock(&Iterator);
    while(NextDataBlock)
    {
        ringbuffer_block_header *BlockHeader = GetRingBufferBlockHeader(NextDataBlock);
        
        if(BlockHeader->Flags & BufferBlock_Delete)
        {
            PopRingBufferBlock(Storage);
            
            char Msg[512];
            sprintf_s(Msg, "BLOCK_CLEANUP Deleted data block \n");
            OutputDebugStringA(Msg);
        }
        else
        {
            break;
        }
        
        NextDataBlock = NextRingBufferBlock(&Iterator);
    }
}

struct pool_allocator
{
    void *Base;
    memory_size BaseAllocationSize;
    memory_size Used;
    
    u32 NextChunkIndex;
    
    u32 ChunksTotal;
    memory_size ChunkSize;
    memory_size ChunkHeaderSize;
    
    void *PageFileBackedFileMappingHANDLE;
    u32 MagicNumber;
};

struct chunk_header
{
    b32 InUse;
    u32 MagicNumber;
    memory_size ChunkOffset;
    
    chunk_header *Next;
    
    void *MappedBlock;
};

struct pool_allocator_params
{
    memory_size ChunkSize;
    u32 MagicNumber;
    u32 __Reserved;
};

internal pool_allocator_params
DefaultPoolAllocatorParams()
{
    pool_allocator_params Params = {};
    Params.ChunkSize = Megabytes(1);
    Params.MagicNumber = 0xDEADBEEF;
    return(Params);
}

internal chunk_header *
GetChunkHeader(void *Base, pool_allocator *Allocator)
{
    chunk_header *Result = 0;
    
    chunk_header *Header = (chunk_header *)((ump)Base - Allocator->ChunkHeaderSize);
    if(Header && (Header->MagicNumber == Allocator->MagicNumber))
    {
        Result = Header;
    }
    
    return(Result);
}

internal chunk_header *
GetFreeChunk(pool_allocator *Allocator)
{
    chunk_header *Result = 0;
    
    mmsz ChunkBlockSize = Allocator->ChunkHeaderSize + Allocator->ChunkSize;
    u32 StartEndIndex = Allocator->NextChunkIndex;
    
    b32 FoundFreeChunk = false;
    while(!FoundFreeChunk)
    {
        memory_size ChunkHeaderOffset = Allocator->NextChunkIndex * ChunkBlockSize;
        
        chunk_header *ChunkHeader = (chunk_header *)((ump)Allocator->Base + ChunkHeaderOffset);
        if(!ChunkHeader->InUse)
        {
            ChunkHeader->InUse = FoundFreeChunk = true;
            ChunkHeader->MagicNumber = Allocator->MagicNumber;
            ChunkHeader->ChunkOffset = ChunkHeaderOffset + Allocator->ChunkHeaderSize;
            
            Result = ChunkHeader;
            
            Allocator->Used += Allocator->ChunkSize;
        }
        
        Allocator->NextChunkIndex = ++Allocator->NextChunkIndex % Allocator->ChunksTotal;
        
        if(Allocator->NextChunkIndex == StartEndIndex)
        {
            break;
        }
    } 
    
    return(Result);
}

internal void *
PushAllocationBlock(pool_allocator *Allocator, mmsz Size)
{
    void *Result = 0;
    
    mmsz ChunkAlignedSize = AlignToPow2(Size, Allocator->ChunkSize);
    
    mmsz TotalUsableSize = Allocator->ChunkSize * Allocator->ChunksTotal;
    mmsz AvailableSize = TotalUsableSize - Allocator->Used;
    if(ChunkAlignedSize <= AvailableSize)
    {
        chunk_header *Root = 0;
        chunk_header **NextChunkInTheLink = &Root;
        
        u32 RequiredChunks = (u32)(ChunkAlignedSize / Allocator->ChunkSize);
        for(u32 Index = 0;
            Index < RequiredChunks;
            ++Index)
        {
            chunk_header *Header = GetFreeChunk(Allocator);
            if(Header)
            {
                *NextChunkInTheLink = Header;
                NextChunkInTheLink = &Header->Next;
            }
        }
        
        mmsz RequiredSize = Allocator->ChunkHeaderSize + RequiredChunks * Allocator->ChunkSize;
        void *ContiguousView = Platform.ConstructContiguousView(Root, RequiredSize, Allocator);
        if(ContiguousView)
        {
            void *ChunkDataBlock = (void *)((ump)ContiguousView + Allocator->ChunkHeaderSize);;
            Result = Root->MappedBlock = ChunkDataBlock;
        }
    }
    
    return(Result);
}

internal b32
ClearAllocationBlock(void *Block, pool_allocator *Allocator)
{
    b32 Result = true;
    
    chunk_header *BlockChunkHeader = GetChunkHeader(Block, Allocator);
    if(BlockChunkHeader)
    {
        chunk_header *CurrentChunk = BlockChunkHeader->Next;
        
        BlockChunkHeader->InUse = false;
        BlockChunkHeader->Next = 0;
        BlockChunkHeader->MappedBlock = 0;
        
        b32 BlockChunkHeaderUnmapResult = Platform.FreeMappedRegion(BlockChunkHeader);
        if(!BlockChunkHeaderUnmapResult)
        {
            Result = false;
            OutputDebugStringA("Couldn't unmap the beginning of the region \n");
        }
        
        u32 TotalChunks = 1;
        
        void *CurrentRegion = (void *)((ump)Block + Allocator->ChunkSize);
        while(CurrentChunk)
        {
            chunk_header *NextChunk = CurrentChunk->Next;
            CurrentChunk->InUse = false;
            CurrentChunk->Next = 0;
            b32 CurrentRegionUnmapResult = Platform.FreeMappedRegion(CurrentRegion);
            if(!CurrentRegionUnmapResult)
            {
                Result = false;
                OutputDebugStringA("Couldn't unmap a chunk \n");
            }
            
            CurrentChunk = NextChunk;
            CurrentRegion = (void *)((ump)CurrentRegion + Allocator->ChunkSize);
            
            TotalChunks++;
        }
        
        Allocator->Used -= Allocator->ChunkSize * TotalChunks;
    }
    
    return(Result);
}

internal b32
ClearPoolAllocator(pool_allocator *Allocator)
{
    b32 Result = true;
    
    mmsz Offset = 0;
    while(Offset < Allocator->BaseAllocationSize)
    {
        chunk_header *Header = (chunk_header *)((ump)Allocator->Base + Offset);
        if(Header && 
           Header->InUse &&
           Header->MappedBlock &&
           Header->MagicNumber == Allocator->MagicNumber)
        {
            b32 ClearStatus = ClearAllocationBlock(Header->MappedBlock, Allocator);
            if(!ClearStatus)
            {
                Result = false;
            }
            Header->MappedBlock = 0;
        }
        
        Offset += Allocator->ChunkHeaderSize + Allocator->ChunkSize;
    }
    
    *Allocator = {};
    
    return(Result);
}

internal b32
VerifyBlockIntegrity(void *Block, pool_allocator *Allocator)
{
    b32 Result = false;
    
    chunk_header *BlockChunkHeader = GetChunkHeader(Block, Allocator);
    if(BlockChunkHeader)
    {
        Result = true;
    }
    
    return(Result);
}