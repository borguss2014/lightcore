internal material *
PushMaterialToStorage(material_storage *Storage)
{
    Assert(Storage);
    
    material *Result = 0;
    
    if(Storage->NextMaterialSlot < Storage->Capacity)
    {
        Result = &Storage->Materials[Storage->NextMaterialSlot++];
    }
    
    return(Result);
}

internal void
PrepareMaterial(aiMaterial *AiMaterial, 
                material *DestMaterial, 
                string DirectoryPath, 
                textures_storage *TexturesStorage,
                hash_table *HashTable, 
                memory_arena *CacheArena)
{
    aiString MaterialName = AiMaterial->GetName();
    CopyString(DestMaterial->Name, MaterialName.length, MaterialName.data);
    
    aiTextureType TextureTypes[] = {
        aiTextureType_DIFFUSE,
        aiTextureType_SPECULAR,
        aiTextureType_NORMALS,
        /*aiTextureType_HEIGHT,
        aiTextureType_SHININESS */
    };
    
    for(u32 TextureTypeIndex = 0;
        TextureTypeIndex < ArrayCount(TextureTypes);
        ++TextureTypeIndex)
    {
        aiTextureType TextureType = TextureTypes[TextureTypeIndex];
        
        u32 TextureCount = AiMaterial->GetTextureCount(TextureType);
        if(TextureCount)
        {
            aiString AiPath;
            AiMaterial->GetTexture(TextureType, 0, &AiPath);
            
            string TexturePath = {};
            TexturePath.Data = (char *)PushSlice(CacheArena, DirectoryPath.Count + AiPath.length);
            TexturePath.Count = DirectoryPath.Count + AiPath.length;
            ConcatenateStrings(DirectoryPath.Data, DirectoryPath.Count, AiPath.data, AiPath.length, TexturePath.Data, TexturePath.Count);
            
            render_texture *Texture = GetTextureFromStorage(TexturePath, HashTable, TexturesStorage, CacheArena);
            if(Texture)
            {
                // TODO(Cristian): Check for more types to load
                switch(TextureType)
                {
                    case aiTextureType_DIFFUSE:
                    {
                        Texture->Type = RenderTextureType_DIFFUSE;
                        DestMaterial->Textures.Diffuse = Texture;
                    } break;
                    
                    case aiTextureType_NORMALS:
                    {
                        Texture->Type = RenderTextureType_NORMAL;
                        DestMaterial->Textures.Normal = Texture;
                    } break;
                    
                    case aiTextureType_SPECULAR:
                    {
                        Texture->Type = RenderTextureType_SPECULAR;
                        DestMaterial->Textures.Specular = Texture;
                    } break;
                    
                    /*case aiTextureType_HEIGHT:
                    {
                        Assert(false);
                    } break;
                    
                    case aiTextureType_SHININESS:
                    {
                        Assert(false);
                    } break;*/
                }
                
                Texture->Status = RenderStatus_LoadToStorage;
            }
        }
    }
    
    aiColor4D AmbientColor = {};
    aiColor4D DiffuseColor = {};
    aiColor4D SpecularColor = {};
    aiColor4D TransparentColor = {};
    f32 Shininess = 0.0f;
    f32 ShininessStrength = 0.0f;
    f32 Opacity = 0.0f;
    f32 TransparencyFactor = 0.0f;
    b32 TwoSided = false;
    
    if(AiMaterial->Get(AI_MATKEY_COLOR_AMBIENT, AmbientColor) == AI_SUCCESS)
    {
        DestMaterial->AmbientColor = V4(AmbientColor.r, AmbientColor.g, AmbientColor.b, AmbientColor.a);
    }
    
    if(AiMaterial->Get(AI_MATKEY_COLOR_DIFFUSE, DiffuseColor) == AI_SUCCESS)
    {
        vec4 SolidDiffuseColor = V4(DiffuseColor.r, DiffuseColor.g, DiffuseColor.b, DiffuseColor.a);
        if(DiffuseColor == aiColor4D(0.0f))
        {
            // NOTE(Cristian): Setting the solid diffuse color to 1.0 in order to ignore its effects in the shader if not available
            SolidDiffuseColor = V4(1.0f);
        }
        
        DestMaterial->DiffuseColor = SolidDiffuseColor;
    }
    
    if(AiMaterial->Get(AI_MATKEY_COLOR_SPECULAR, SpecularColor) == AI_SUCCESS)
    {
        DestMaterial->SpecularColor = V4(SpecularColor.r, SpecularColor.g, SpecularColor.b, SpecularColor.a);
    }
    
    if(AiMaterial->Get(AI_MATKEY_COLOR_TRANSPARENT, TransparentColor) == AI_SUCCESS)
    {
        DestMaterial->TransparentColor = V4(TransparentColor.r, TransparentColor.g, TransparentColor.b, TransparentColor.a);
    }
    
    if(AiMaterial->Get(AI_MATKEY_TRANSPARENCYFACTOR, TransparencyFactor) == AI_SUCCESS)
    {
        DestMaterial->TransparencyFactor = TransparencyFactor;
    }
    
    if(AiMaterial->Get(AI_MATKEY_SHININESS, Shininess) == AI_SUCCESS)
    {
        DestMaterial->Shininess = Shininess;
    }
    
    if(AiMaterial->Get(AI_MATKEY_SHININESS_STRENGTH, ShininessStrength) == AI_SUCCESS)
    {
        DestMaterial->ShininessStrength = ShininessStrength;
    }
    
    if(AiMaterial->Get(AI_MATKEY_OPACITY, Opacity) == AI_SUCCESS)
    {
        DestMaterial->Opacity = Opacity;
    }
    
    if(AiMaterial->Get(AI_MATKEY_TWOSIDED, TwoSided) == AI_SUCCESS)
    {
        DestMaterial->TwoSided = TwoSided;
    }
    
    
}