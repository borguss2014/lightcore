@echo off
call :setESC

REM ==================================================== MSVC ====================================================
set CommonCompilerFlags=-diagnostics:column -FAsc -nologo -GR- -EHa- -Od -Oi -WL -WX -W4 -wd4100 -wd4505 -wd4101 -wd4189 -wd4702 -wd4127 -wd4311 -wd4302 -wd4201 -wd4324 -wd4723 -wd4701 -wd4703 -FC -Z7 /arch:AVX2
set CommonCompilerFlags= -DLIGHTCORE_DEBUG=1 -DLIGHTCORE_EXTRA_CHECKS=1 %CommonCompilerFlags%
set CommonLinkerFlags=-incremental:no -opt:ref /CGTHREADS:8 user32.lib gdi32.lib

if not exist ..\..\build\ (mkdir ..\..\build\)
pushd ..\..\build

del *.pdb > NUL 2> NUL

REM Meta generation pre-pass
echo [ META PRE-PASS ]
call cl %CommonCompilerFlags% ..\Lightcore\code\meta_generator.cpp /link %CommonLinkerFlags%
echo.

call :OutputCompileStatus %errorlevel% "Meta generator compilation"

pushd ..\Lightcore\code
..\..\build\meta_generator.exe
call :OutputCompileStatus %errorlevel% "Meta pre-pass"
popd


REM x64 Build
echo [ PROJECT BUILD ]

echo WAITING FOR PDB > lock.tmp

call cl %CommonCompilerFlags% -MTd -I ..\Lightcore\resources\libs\ ..\Lightcore\code\win32_renderer_opengl.cpp -LD /link -PDB:win32_renderer_opengl_%random%.pdb -incremental:no -opt:ref -EXPORT:Win32LoadRenderer -EXPORT:Win32BeginFrame -EXPORT:Win32EndFrame gdi32.lib user32.lib opengl32.lib

call cl %CommonCompilerFlags% -MTd -I ..\Lightcore\resources\libs\ ..\Lightcore\code\game.cpp -LD /link -PDB:game_%random%.pdb -incremental:no -opt:ref -EXPORT:GameUpdateAndRender assimp-vc143-mt.lib gdi32.lib user32.lib opengl32.lib shell32.lib
 
del lock.tmp

call cl %CommonCompilerFlags% -MTd -I ..\Lightcore\resources\libs\ ..\Lightcore\code\win32_lightcore.cpp /link %CommonLinkerFlags% mincore.lib
echo.

popd

call :OutputCompileStatus %errorlevel% "Project compilation"
REM ========================================================================================











@REM REM x64 Clang Build
@REM echo [ PROJECT BUILD ]

@REM set CLANGCompileFlags= -g -mno-stack-arg-probe -maes -Wno-writable-strings -Wno-microsoft-cast -DGLM_FORCE_QUAT_DATA_XYZW
@REM set CLANGLinkFlags= -fuse-ld=lld -luser32 -lgdi32 -lopengl32 -Wl,-subsystem:windows

@REM set BASE_FILES= -I ..\Lightcore\resources\libs\ ..\Lightcore\code\win32_lightcore.cpp

@REM call clang++ %CLANGCompileFlags% -fuse-ld=lld -shared ..\Lightcore\code\game.cpp -o game_debug_clang.dll -Wl,-subsystem:windows,-export:GameUpdateAndRender
@REM call clang++ %CLANGCompileFlags% -gcodeview %CLANGLinkFlags% %BASE_FILES% -o win32_lightcore_debug_clang.exe

@REM call clang++ -O3 %CLANGCompileFlags% -fuse-ld=lld -shared ..\Lightcore\code\game.cpp -o game_release_clang.dll -Wl,-subsystem:windows,-export:GameUpdateAndRender
@REM call clang++ -O3 %CLANGCompileFlags% %CLANGLinkFlags% %BASE_FILES% -o win32_lightcore_release_clang.exe

@REM where /q clang || (
@REM   echo WARNING: "clang" not found.
@REM   exit /b 1
@REM )
@REM ========================================================================================


:setESC
for /F "tokens=1,2 delims=#" %%a in ('"prompt #$H#$E# & echo on & for %%b in (1) do rem"') do (
  set ESC=%%b
  exit /B 0
)

:OutputCompileStatus
if %~1 EQU 0 (
  echo %~2 %ESC%[32mfinished %ESC%[0m
  echo.
) else (
  echo %~2 %ESC%[31mfailed %ESC%[0m 
  echo.
)
exit /B 0