#include <Windows.h>
#include <iostream>

/* 
    WARNING: This generator doesn't use proper memory management. Sometimes it doesn't even delete allocated memory. 
    DO NOT DESPAIR! This is simply a generation tool whose memory's meant to be reclaimed by the OS on program exit. 
    The output of this tool is more important than memory management.
*/

#define ArrayCount(Array) sizeof(Array)/sizeof(Array[0])

char *OpenGLFunctionsToLoad[] = 
{
    "void glGetIntegerv(GLenum pname, GLint * data)",
    
    "void glGetProgramInterfaceiv(GLuint program, GLenum programInterface, GLenum pname, GLint * params)",
    
    "void glGetActiveUniform(GLuint program, GLuint index, GLsizei bufSize, GLsizei *length, GLint *size, GLenum *type, GLchar *name)",
    
    "void glDeleteRenderbuffers(GLsizei n, GLuint *renderbuffers)",
    
    "void glDeleteFramebuffers(GLsizei n, GLuint *framebuffers)",
    "GLenum glCheckNamedFramebufferStatus(GLuint framebuffer, GLenum target)",
    "void glNamedFramebufferRenderbuffer(GLuint framebuffer, GLenum attachment, GLenum renderbuffertarget, GLuint renderbuffer)",
    "void glNamedFramebufferTexture(GLuint framebuffer, GLenum attachment, GLuint texture, GLint level)",
    "void glCreateFramebuffers(GLsizei n, GLuint *framebuffers)",
    "void glNamedRenderbufferStorage(GLuint renderbuffer, GLenum internalformat, GLsizei width, GLsizei height)",
    "void glCreateRenderbuffers(GLsizei n, GLuint *renderbuffers)",
    "void glTextureParameterf(GLuint texture, GLenum pname, GLfloat param)",
    "void glMultiDrawElements(GLenum mode, const GLsizei * count, GLenum type, const void * const * indices, GLsizei drawcount)",
    "void glFrontFace(GLenum mode)",
    "GLuint glGetUniformBlockIndex(GLuint program, const GLchar *uniformBlockName)",
    "void glVertexArrayElementBuffer(GLuint vaobj, GLuint buffer)",
    "void glBindTextureUnit(GLuint unit, GLuint texture)",
    "void glGenerateTextureMipmap(GLuint texture)",
    "void glTextureParameteri(GLuint texture, GLenum pname, GLint param)",
    "void glTextureSubImage2D(GLuint texture, GLint level, GLint xoffset, GLint yoffset, GLsizei width, GLsizei height, GLenum format, GLenum type, const void *pixels)",
    "void glTextureStorage2D(GLuint texture, GLsizei levels, GLenum internalformat, GLsizei width, GLsizei height)",
    "void glCreateTextures(GLenum target, GLsizei n, GLuint *textures)",
    "void glVertexArrayVertexBuffer(GLuint vaobj, GLuint bindingindex, GLuint buffer, GLintptr offset, GLsizei stride)",
    "void glNamedBufferStorage(GLuint buffer, GLsizeiptr size, const void *data, GLbitfield flags)",
    "GLint glGetAttribLocation(GLuint program, const GLchar *name)",
    "void glEnableVertexArrayAttrib(GLuint vaobj, GLuint index)",
    "void glDisableVertexArrayAttrib(GLuint vaobj, GLuint index)",
    "void glVertexArrayAttribFormat(GLuint vaobj, GLuint attribindex, GLint size, GLenum type, GLboolean normalized, GLuint relativeoffset)",
    "void glVertexArrayAttribBinding(GLuint vaobj, GLuint attribindex, GLuint bindingindex)",
    
    "void glValidateProgram(GLuint program)",
    "void glDebugMessageCallback(DEBUGPROC callback, const void * userParam)",
    
    "void glDrawArrays(GLenum mode, GLint first, GLsizei count)",
    "void glDrawElements(GLenum mode, GLsizei count, GLenum type, const void * indices)",
    "void glEnable(GLenum cap)",
    "void glDisable(GLenum cap)",
    
    "void glGenQueries(GLsizei n, GLuint * ids)",
    "void glBeginQuery(GLenum target, GLuint id)",
    "void glEndQuery(GLenum target)",
    "void glGetQueryObjecti64v(GLuint id, GLenum pname, GLint64 * params)",
    
    "void glDepthMask(GLboolean flag)",
    "void * glMapBuffer(GLenum target, GLenum access)",
    "void * glMapNamedBuffer(GLuint buffer, GLenum access)",
    "GLboolean glUnmapBuffer(GLenum target)",
    "GLboolean glUnmapNamedBuffer(GLuint buffer)",
    
    "void glViewport(GLint x, GLint y, GLsizei width, GLsizei height)",
    
    "void glCullFace(GLenum mode)",
    
    "void glPolygonMode(GLenum face, GLenum mode)",
    "void glLineWidth(GLfloat width)",
    
    "void glDrawBuffer(GLenum buf)",
    "void glReadBuffer(GLenum mode)",
    
    "void glBindTexture(GLenum target, GLuint texture)",
    "void glCreateVertexArrays(GLsizei n, GLuint *arrays)",
    "void glGenVertexArrays(GLsizei n, GLuint *arrays)",
    "void glBindVertexArray(GLuint array)",
    "void glGenBuffers(GLsizei n, GLuint * buffers)",
    "void glDeleteBuffers(GLsizei n, const GLuint * buffers)",
    "void glDeleteVertexArrays(GLsizei n, const GLuint *arrays)",
    
    "void glEnableVertexAttribArray(GLuint index)",
    "void glVertexAttribFormat(GLuint attribindex, GLint size, GLenum type, GLboolean normalized, GLuint relativeoffset)",
    "void glVertexAttribBinding(GLuint attribindex, GLuint bindingindex)",
    
    "void glActiveTexture(GLenum texture)",
    "void glGenTextures(GLsizei n, GLuint * textures)",
    "void glDeleteTextures(GLsizei n, const GLuint * textures)",
    "void glGenerateMipmap(GLenum target)",
    "void glTexImage1D(GLenum target, GLint level, GLint internalformat, GLsizei width, GLint border, GLenum format, GLenum type, const void * data)",
    "void glTexImage2D(GLenum target, GLint level, GLint internalformat, GLsizei width, GLsizei height, GLint border, GLenum format, GLenum type, const void * data)",
    "void glTexImage3D(GLenum target, GLint level, GLint internalformat, GLsizei width, GLsizei height, GLsizei depth, GLint border, GLenum format, GLenum type, const void *pixels)",
    "void glTexParameteri(GLenum target, GLenum pname, GLint param)",
    "void glPixelStorei(GLenum pname, GLint param)",
    
    "void glTexParameterfv(GLenum target, GLenum pname, const GLfloat * params)",
    
    "void glGenRenderbuffers(GLsizei n, GLuint *renderbuffers)",
    "void glBindRenderbuffer(GLenum target, GLuint renderbuffer)",
    "void glRenderbufferStorage(GLenum target, GLenum internalformat, GLsizei width, GLsizei height)",
    
    "void glCreateBuffers(GLsizei n, GLuint *buffers)",
    "void glBindBuffer(GLenum target, GLuint buffer)",
    "void glBindBufferBase(GLenum target, GLuint index, GLuint buffer)",
    "void glBindBufferRange(GLenum target, GLuint index, GLuint buffer, GLintptr offset, GLsizeiptr size)",
    "void glBufferData(GLenum target, GLsizeiptr size, const void * data, GLenum usage)",
    "void glNamedBufferData(GLuint buffer, GLsizeiptr size, const void *data, GLenum usage)",
    "void glBufferSubData(GLenum target, GLintptr offset, GLsizeiptr size, const void * data)",
    "void glNamedBufferSubData(GLuint buffer, GLintptr offset, GLsizeiptr size, const void *data)",
    "void glBindVertexBuffer(GLuint bindingindex, GLuint buffer, GLintptr offset, GLsizei stride)",
    
    "GLuint glCreateProgram(void)",
    "void glDeleteProgram(GLuint program)",
    "void glLinkProgram(GLuint program)",
    "void glActiveShaderProgram(GLuint pipeline, GLuint program)",
    "void glUseProgram(GLuint program)",
    "void glGetProgramiv(GLuint program, GLenum pname, GLint *params)",
    "void glGetProgramInfoLog(GLuint program, GLsizei maxLength, GLsizei *length, GLchar *infoLog)",
    
    "GLuint glCreateShader(GLenum shaderType)",
    "void glDeleteShader(GLuint shader)",
    "void glAttachShader(GLuint program, GLuint shader)",
    "void glShaderSource(GLuint shader, GLsizei count, const GLchar *const*string, const GLint *length)",
    "void glCompileShader(GLuint shader)",
    "void glGetShaderiv(GLuint shader, GLenum pname, GLint *params)",
    "void glGetShaderInfoLog(GLuint shader, GLsizei maxLength, GLsizei *length, GLchar *infoLog)",
    
    "void glUniform1i(GLint location, GLint v0)",
    "void glUniform1f(GLint location, GLfloat v0)",
    "void glUniform3fv(GLint location, GLsizei count, const GLfloat *value)",
    "void glUniformMatrix3fv(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value)",
    "void glUniformMatrix4fv(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value)",
    "void glUniform3f(GLint location, GLfloat v0, GLfloat v1, GLfloat v2)",
    "void glUniform4f(GLint location, GLfloat v0, GLfloat v1, GLfloat v2, GLfloat v3)",
    "GLint glGetUniformLocation(GLuint program, const GLchar *name)",
    
    "void glGenFramebuffers(GLsizei n, GLuint *ids)",
    "void glBindFramebuffer(GLenum target, GLuint framebuffer)",
    "void glFramebufferTexture2D(GLenum target, GLenum attachment, GLenum textarget, GLuint texture, GLint level)",
    "void glFramebufferRenderbuffer(GLenum target, GLenum attachment, GLenum renderbuffertarget, GLuint renderbuffer)",
    "GLenum glCheckFramebufferStatus(GLenum target)",
    
    "void glClear(GLbitfield mask)",
    "void glClearColor(GLclampf red, GLclampf green, GLclampf blue, GLclampf alpha)",
    "void glClearTexImage(GLuint texture, GLint level, GLenum format, GLenum type, const void * data)",
    "BOOL wglSwapIntervalEXT(int interval)"
};

char *LoadFunctionPointersDefinitionHeader = 
{
    "internal b32\n"
        "OpenGLLoadFunctionPointers(opengl_fn_table *OpenGL)\n"
        "{\n"
        "    b32 Result = true;\n"
        "\n"
        "    if(OpenGL)\n"
        "    {\n"
};

char *LoadFunctionPointersDefinitionEnd = 
{
    "    }\n"
        "else\n"
        "{\n"
        "    Result = false;\n"
        "}\n"
        "\n"
        "    return(Result);\n"
        "}"
};

char *LoadOpenGLFunctionPointerDefinition =
{
    "internal void *\n"
        "LoadOpenGLFunctionPointer(char *FunctionName)\n"
        "{\n"
        "   // Try to load the function from the graphics driver\n"
        "   void *OpenGLFunction = wglGetProcAddress(FunctionName);\n"
        "   if((OpenGLFunction == 0) ||\n"
        "   (OpenGLFunction == (void*)0x1) ||\n"
        "   (OpenGLFunction == (void*)0x2) ||\n"
        "   (OpenGLFunction == (void*)0x3) ||\n"
        "   (OpenGLFunction == (void*)-1) )\n"
        "   {\n"
        "       // Try to load from the Win32 API\n"
        "       HMODULE Module = LoadLibraryA(\"opengl32.dll\");\n"
        "       OpenGLFunction = GetProcAddress(Module, FunctionName);\n"
        "   }\n"
        "   return(OpenGLFunction);\n"
        "}\n"
};

void
WriteNewLine(uint32_t IndentationLevel, FILE *File, char *FormattedInput, ...)
{
    va_list ArgsList;
	va_start(ArgsList, FormattedInput);
    
    uint32_t MessageSize = vsnprintf(0, 0, FormattedInput, ArgsList);
    MessageSize += IndentationLevel + 1;
    
    char *FormattedString = (char *)malloc(sizeof(char) * MessageSize);
    if(FormattedString)
    {
        for(uint32_t Level = 0;
            Level < IndentationLevel;
            ++Level)
        {
            FormattedString[Level] = '\t';
        }
        
        vsprintf_s(FormattedString + IndentationLevel, MessageSize - IndentationLevel, FormattedInput, ArgsList);
        fprintf(File, "%s\n", FormattedString);
    }
    
    va_end(ArgsList);
    free(FormattedString);
}

char *
ToUpperCase(char *LowerCaseString)
{
    size_t FunctionNameSize = strnlen(LowerCaseString, 140);
    char *UpperCaseFunctionName = (char *)malloc(FunctionNameSize + 1);
    for(uint32_t i = 0;
        i < FunctionNameSize;
        ++i)
    {
        if(LowerCaseString[i] >= 'a' && LowerCaseString[i] <= 'z')
        {
            UpperCaseFunctionName[i] = LowerCaseString[i] - 32;
        }
        else
        {
            UpperCaseFunctionName[i] = LowerCaseString[i];
        }
    }
    UpperCaseFunctionName[FunctionNameSize] = '\0';
    return(UpperCaseFunctionName);
}

struct function_layout
{
    char *ReturnToken;
    char *FunctionName;
    char *ParameterList;
};

void
ClearMemoryFunctionLayout(function_layout *FnLayout)
{
    if(FnLayout)
    {
        free(FnLayout->ReturnToken);
        free(FnLayout->FunctionName);
        free(FnLayout->ParameterList);
    }
}

function_layout
InterpretFunctionDefinition(FILE *File, char *FullFunctionDefinition)
{
    size_t FunctionNameSize = strnlen(FullFunctionDefinition, 200);
    
    char *Buffer = FullFunctionDefinition;
    int32_t FoundReturnToken = false;
    
    size_t EORT = 0;
    size_t EOFNT = 0;
    size_t BOPList = 0;
    for (size_t At = 0;
         At < FunctionNameSize;)
    {
        char C0 = Buffer[At];
        // NOTE(Cristian): Buffer[FileSize] is specifically 0 so we don't go off range
        char C1 = Buffer[At + 1];
        
        if((C0 == ' ') && (C1 == '*'))
        {
            EORT = At + 1;
            At += 3;
        }
        else if((C0 == ' ') && (C1 != '*'))
        {
            EORT = At - 1;
            At += 1;
        }
        else if(C1 == '(')
        {
            EOFNT = At;
            At += 2;
            BOPList = At;
            break;
        }
        else
        {
            ++At;
        }
    }
    
    size_t ReturnTokenSize = EORT + 1;
    size_t FunctionNameTokenSize = (EOFNT + 1) - (EORT + 2);
    size_t ParameterListSize = FunctionNameSize - 1 - BOPList;
    
    char *ReturnToken = (char *)malloc(ReturnTokenSize + 1);
    char *FunctionName = (char *)malloc(FunctionNameTokenSize + 1);
    char *ParameterList = (char *)malloc(ParameterListSize + 1);
    memcpy_s(ReturnToken, ReturnTokenSize, &Buffer[0], ReturnTokenSize);
    memcpy_s(FunctionName, FunctionNameTokenSize, &Buffer[EORT + 2], FunctionNameTokenSize);
    memcpy_s(ParameterList, ParameterListSize, &Buffer[BOPList], ParameterListSize);
    ReturnToken[ReturnTokenSize]  = '\0';
    FunctionName[FunctionNameTokenSize]  = '\0';
    ParameterList[ParameterListSize]  = '\0';
    
    function_layout FnLayout = {};
    FnLayout.ReturnToken = ReturnToken;
    FnLayout.FunctionName = FunctionName;
    FnLayout.ParameterList = ParameterList;
    
    return(FnLayout);
}

void
OutputFunctionType(FILE *File, char *FullFunctionDefinition)
{
    function_layout FnLayout = InterpretFunctionDefinition(File, FullFunctionDefinition);
    
    fprintf(File, 
            "typedef %s (WINAPI * type_%s)(%s);\n", 
            FnLayout.ReturnToken,
            FnLayout.FunctionName, 
            FnLayout.ParameterList);
    
    ClearMemoryFunctionLayout(&FnLayout);
}

void
OutputStructOfFunctionTypes(FILE *File, char *StructName, char *FunctionsToLoad[], uint32_t FunctionsCount)
{
    fprintf(File, "struct %s\n", StructName);
    fprintf(File, "{\n");
    
    for(uint32_t FunctionIndex = 0;
        FunctionIndex < FunctionsCount;
        ++FunctionIndex)
    {
        char *FullFunctionDefinition = FunctionsToLoad[FunctionIndex];
        function_layout FnLayout = InterpretFunctionDefinition(File, FullFunctionDefinition);
        fprintf(File, "    type_%s %s;\n", FnLayout.FunctionName, FnLayout.FunctionName);
        
        ClearMemoryFunctionLayout(&FnLayout);
    }
    
    fprintf(File, "};\n");
    fprintf(File, "\n");
}

int main()
{
    FILE *File;
    errno_t Error = fopen_s(&File, "win32_opengl_defines_generated.h", "w");
    
    WriteNewLine(0, File, "#include <gl/gl.h>");
    WriteNewLine(0, File, "#include <GL/glext.h>");
    WriteNewLine(0, File, "#include \"GL/wglext.h\"");
    WriteNewLine(0, File, "");
    
    WriteNewLine(0, File, "typedef void (*DEBUGPROC)(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar *message, const void *userParam);");
    WriteNewLine(0, File, "");
    
    uint32_t FunctionsCount = ArrayCount(OpenGLFunctionsToLoad);
    
    for(uint32_t FunctionIndex = 0;
        FunctionIndex < FunctionsCount;
        ++FunctionIndex)
    {
        OutputFunctionType(File, OpenGLFunctionsToLoad[FunctionIndex]);
    }
    fprintf(File, "\n");
    
    OutputStructOfFunctionTypes(File, "opengl_fn_table", OpenGLFunctionsToLoad, FunctionsCount);
    
    fprintf(File, "%s", LoadOpenGLFunctionPointerDefinition);
    fprintf(File, "\n");
    
    fprintf(File, "%s", LoadFunctionPointersDefinitionHeader);
    for(uint32_t FunctionIndex = 0;
        FunctionIndex < FunctionsCount;
        ++FunctionIndex)
    {
        char *FunctionName = OpenGLFunctionsToLoad[FunctionIndex];
        
        function_layout FnLayout = InterpretFunctionDefinition(File, OpenGLFunctionsToLoad[FunctionIndex]);
        
        WriteNewLine(2, File, "OpenGL->%s = (type_%s)LoadOpenGLFunctionPointer(\"%s\");", FnLayout.FunctionName, FnLayout.FunctionName, FnLayout.FunctionName);
        WriteNewLine(2, File, "if(!OpenGL->%s) { Result = false; }", FnLayout.FunctionName);
        WriteNewLine(0, File, "");
        
        ClearMemoryFunctionLayout(&FnLayout);
    }
    
    fprintf(File, "%s\n", LoadFunctionPointersDefinitionEnd);
    fclose(File);
    
    return(0);
}