//////////////////////////////////
// Generated introspection data //
//////////////////////////////////

/**********************************************************************
* Member info layout
* { IsPointer, PointerDepth, MetaType, MemberName, ArrayCounter, Offset }
**********************************************************************/

static struct_member_info MembersOf_material[] = 
{
	{ MetaFlag_IsArray, 128, MetaType_char, "Name", "", OFFSET_OF(material, Name) },
	{ 0, 0, MetaType_render_textures, "Textures", "", OFFSET_OF(material, Textures) },
	{ 0, 0, MetaType_vec4, "AmbientColor", "", OFFSET_OF(material, AmbientColor) },
	{ 0, 0, MetaType_vec4, "DiffuseColor", "", OFFSET_OF(material, DiffuseColor) },
	{ 0, 0, MetaType_vec4, "SpecularColor", "", OFFSET_OF(material, SpecularColor) },
	{ 0, 0, MetaType_vec4, "TransparentColor", "", OFFSET_OF(material, TransparentColor) },
	{ 0, 0, MetaType_f32, "Shininess", "", OFFSET_OF(material, Shininess) },
	{ 0, 0, MetaType_f32, "ShininessStrength", "", OFFSET_OF(material, ShininessStrength) },
	{ 0, 0, MetaType_f32, "Opacity", "", OFFSET_OF(material, Opacity) },
	{ 0, 0, MetaType_f32, "TransparencyFactor", "", OFFSET_OF(material, TransparencyFactor) },
	{ 0, 0, MetaType_b32, "TwoSided", "", OFFSET_OF(material, TwoSided) }
};

