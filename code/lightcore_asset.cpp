struct asset_block_header
{
    u32 BufferID;
    
    mmsz VerticesBufferSize;
    mmsz IndicesBufferSize;
    
    u32 Flags;
};

internal asset *
PushAssetToStorage(asset_storage *Storage)
{
    Assert(Storage);
    
    asset *Result = 0;
    
    if(Storage->Used < Storage->Capacity)
    {
        Result = &Storage->Assets[Storage->Used++];
    }
    
    return(Result);
}

internal void *
PushAssetDataBlock(ring_buffer *AssetStorage, u32 TotalVertices, u32 TotalIndices = 0)
{
    void *Result = 0;
    
    mmsz VerticesBufferSize = TotalVertices * sizeof(textured_vertex);
    mmsz IndicesBufferSize = TotalIndices * sizeof(u32);
    
    mmsz TotalAllocSize = sizeof(asset_block_header) + VerticesBufferSize + IndicesBufferSize;
    void *BufferBlock = PushRingBufferBlock(AssetStorage, TotalAllocSize);
    if(BufferBlock)
    {
        asset_block_header *BlockHeader = (asset_block_header *)BufferBlock;
        BlockHeader->VerticesBufferSize = VerticesBufferSize;
        BlockHeader->IndicesBufferSize = IndicesBufferSize;
        
        Result = (void *)((ump)BufferBlock + sizeof(asset_block_header));
    }
    
    return(Result);
}

inline asset_block_header *
GetAssetBlockHeader(void *AssetBlockBase)
{
    Assert(AssetBlockBase);
    
    asset_block_header *Header = (asset_block_header *)AssetBlockBase - 1;
    return(Header);
}

internal void *
GetIndicesData(void *AssetBlockBase)
{
    Assert(AssetBlockBase);
    
    void *Result = 0;
    
    asset_block_header *BlockHeader = GetAssetBlockHeader(AssetBlockBase);
    if(BlockHeader)
    {
        Result = (void *)((ump)AssetBlockBase + BlockHeader->VerticesBufferSize);
    }
    
    return(Result);
}

internal void
ManageAssetsStorage_(game_render_commands *RenderCommands, ring_buffer *AssetStorage)
{
    ring_buffer_iterator Iterator = InitializeIterator(AssetStorage);
    
    void *NextDataBlock = NextRingBufferBlock(&Iterator);
    while(NextDataBlock)
    {
        asset_block_header *AssetBlockHeader = (asset_block_header *)NextDataBlock;
        
        if(AssetBlockHeader->Flags & BufferBlock_Loaded)
        {
            void *AssetData = (void *)((ump)NextDataBlock + sizeof(asset_block_header));
            
            PushVertexData(RenderCommands, 
                           AssetBlockHeader->BufferID, 
                           AssetData,
                           AssetBlockHeader->VerticesBufferSize,
                           AssetBlockHeader->IndicesBufferSize,
                           &AssetBlockHeader->Flags);
        }
        else if(AssetBlockHeader->Flags & BufferBlock_Delete)
        {
            PopRingBufferBlock(AssetStorage);
        }
        
        NextDataBlock = NextRingBufferBlock(&Iterator);
    }
    
    Iterator = InitializeIterator(AssetStorage);
    NextDataBlock = NextRingBufferBlock(&Iterator);
    
    while(NextDataBlock)
    {
        asset_block_header *AssetBlockHeader = (asset_block_header *)NextDataBlock;
        if(AssetBlockHeader->Flags & BufferBlock_Delete)
        {
            PopRingBufferBlock(AssetStorage);
        }
        else
        {
            break;
        }
    }
}

internal relational_node *
PushRelationalNode(nodes_storage *Storage)
{
    Assert(Storage);
    
    relational_node *Result = 0;
    
    TicketMutexLock(&GameMemoryMutex);
    for(u32 Index = 0;
        Index < Storage->Capacity;
        ++Index)
    {
        relational_node *CurrentNode = &Storage->Nodes[Index];
        if(!CurrentNode->Used)
        {
            CurrentNode->Used = true;
            Result = CurrentNode;
            break;
        }
    }
    TicketMutexUnlock(&GameMemoryMutex);
    
    return(Result);
}

internal void
UpdateAssetTransformData(asset *Asset, memory_arena *Arena)
{
    if(Asset->Transform.UpdateTransform)
    {
        mat4x4 LocalMatrix = Mat4x4Identity();
        LocalMatrix = Scale(LocalMatrix, Asset->Transform.Scale);
        LocalMatrix = Translate(LocalMatrix, Asset->Transform.Position);
        // TODO(Cristian): Rotation
        Asset->RootNode->LocalMatrix = LocalMatrix;
        
        u32 NodesCapacity = 2000;
        relational_node **NodesQueue = PushArray(Arena, NodesCapacity, relational_node *);
        u32 NextQueueSlot = 0;
        u32 QueueReadSlot = 0;
        
        NodesQueue[NextQueueSlot++] = Asset->RootNode;
        while(QueueReadSlot < NextQueueSlot)
        {
            relational_node *TargetNode = NodesQueue[QueueReadSlot++];
            
            if(!TargetNode->Parent)
            {
                TargetNode->WorldMatrix = TargetNode->LocalMatrix;
            }
            else
            {
                TargetNode->WorldMatrix = TargetNode->Parent->WorldMatrix * TargetNode->LocalMatrix;
            }
            
            mesh *CurrentMeshRef = TargetNode->MeshHeadRef;
            while(CurrentMeshRef)
            {
                CurrentMeshRef->WorldMatrix = TargetNode->WorldMatrix;
                CurrentMeshRef = CurrentMeshRef->NextRef;
            }
            
            relational_node *CurrentChild = TargetNode->SublevelNextNode;
            while(CurrentChild)
            {
                NodesQueue[NextQueueSlot++] = CurrentChild;
                CurrentChild = CurrentChild->LevelNextNode;
            }
            
        }
    }
}

JOB_QUEUE_ENTRY_CALLBACK(LoadAssetData)
{
    asset_load_entry_data *EntryData = (asset_load_entry_data *)Data;
    
    asset *Asset = EntryData->Asset;
    scene_storage *Storage = EntryData->Storage;
    game_render_commands *RenderCommands = EntryData->RenderCommands;
    
    if(EntryData && Asset && Storage && RenderCommands)
    {
        b32 IsFileAvailable = Platform.CheckIfFileExists(Asset->Path);
        if(IsFileAvailable)
        {
            temporary_memory TempMem = BeginTemporaryMemory(ThreadLocalStorage, TemporaryMemory_InitializeArena);
            
            Assimp::Importer AssetImporter;
            
            u32 ImporterFlags = 
                aiProcess_Triangulate | 
                aiProcess_FlipUVs | 
                aiProcess_JoinIdenticalVertices | 
                aiProcess_CalcTangentSpace;
            
            const aiScene *Model = AssetImporter.ReadFile(Asset->Path, ImporterFlags);
            
            if(Model && Model->mRootNode && !(Model->mFlags & AI_SCENE_FLAGS_INCOMPLETE))
            {
                u32 TotalMeshes = Model->mNumMeshes;
                u32 TotalVertices = 0;
                u32 TotalIndices = 0;
                for(u32 Index = 0;
                    Index < TotalMeshes;
                    ++Index)
                {
                    aiMesh *Mesh = Model->mMeshes[Index];
                    TotalVertices += Mesh->mNumVertices;
                    TotalIndices += Mesh->mNumFaces;
                }
                
                if(ImporterFlags & aiProcess_Triangulate)
                {
                    TotalIndices = TotalIndices * 3;
                }
                
                void *AssetDataBlock = PushAssetDataBlock(&Storage->AssetStorage.AssetTransientStorage, TotalVertices, TotalIndices);
                
                // NOTE(Cristian): Busy wait until there's space available on the transient storage.
                // Not the best solution, but it's what I've got at the moment.
                while(!AssetDataBlock)
                {
                    AssetDataBlock = PushAssetDataBlock(&Storage->AssetStorage.AssetTransientStorage, TotalVertices, TotalIndices);
                }
                
                u32 CountedVertices = 0;
                u32 CountedIndices = 0;
                
                string DirectoryPath = GetParentDirectoryName(Asset->Path);
                
                u32 TotalMaterials = Model->mNumMaterials;
                u32 MaterialSlotBaseIndex = AtomicAddU32(&Storage->MaterialStorage.NextMaterialSlot, TotalMaterials);
                
                {
                    // MATERIALS SETUP
                    
                    hash_table *TexturePathsCache = PushStruct(ThreadLocalStorage, hash_table);
                    
                    u32 CurrentMaterialSlotIndex = MaterialSlotBaseIndex;
                    for(u32 MaterialIndex = 0;
                        MaterialIndex < Model->mNumMaterials;
                        ++MaterialIndex)
                    {
                        aiMaterial *AiMaterial = Model->mMaterials[MaterialIndex];
                        
                        if(CurrentMaterialSlotIndex < Storage->MaterialStorage.Capacity)
                        {
                            material *Material = &Storage->MaterialStorage.Materials[CurrentMaterialSlotIndex++];
                            PrepareMaterial(AiMaterial, 
                                            Material, 
                                            DirectoryPath, 
                                            &Storage->RenderTexturesStorage, 
                                            TexturePathsCache, 
                                            ThreadLocalStorage);
                        }
                    }
                }
                
                // Vertex + index data processing
                textured_vertex *VertexData = (textured_vertex *)AssetDataBlock;
                for(u32 Index = 0;
                    Index < TotalMeshes;
                    ++Index)
                {
                    aiMesh *Mesh = Model->mMeshes[Index];
                    
                    u32 TotalMeshVertices = Mesh->mNumVertices;
                    u32 VerticesOffsetCount = CountedVertices;
                    
                    for(u32 VerticesIndex = 0;
                        VerticesIndex < TotalMeshVertices;
                        ++VerticesIndex)
                    {
                        textured_vertex Vertex = {};
                        
                        if(Mesh->HasPositions())
                        {
                            Vertex.Position = V3(Mesh->mVertices[VerticesIndex].x, 
                                                 Mesh->mVertices[VerticesIndex].y,
                                                 Mesh->mVertices[VerticesIndex].z);
                        }
                        
                        if(Mesh->HasNormals())
                        {
                            Vertex.Normal = V3(Mesh->mNormals[VerticesIndex].x, 
                                               Mesh->mNormals[VerticesIndex].y, 
                                               Mesh->mNormals[VerticesIndex].z);
                        }
                        
                        if(Mesh->HasTextureCoords(0))
                        {
                            Vertex.UVCoords = V2(Mesh->mTextureCoords[0][VerticesIndex].x, Mesh->mTextureCoords[0][VerticesIndex].y);
                        }
                        else if(Mesh->HasVertexColors(0))
                        {
                            u32 ColorChannels = Mesh->GetNumColorChannels();
                            switch(ColorChannels)
                            {
                                case 1:
                                {
                                    Vertex.Color = V3(Mesh->mColors[0][VerticesIndex].r, 0, 0);
                                } break;
                                
                                case 2:
                                {
                                    Vertex.Color = V3(Mesh->mColors[0][VerticesIndex].r, Mesh->mColors[0][VerticesIndex].g, 0);
                                } break;
                                
                                case 3:
                                {
                                    Vertex.Color = V3(Mesh->mColors[0][VerticesIndex].r, 
                                                      Mesh->mColors[0][VerticesIndex].g, 
                                                      Mesh->mColors[0][VerticesIndex].b);
                                } break;
                            }
                        }
                        
                        if(Mesh->HasTangentsAndBitangents())
                        {
                            Vertex.Tangent = V3(Mesh->mTangents[VerticesIndex].x, 
                                                Mesh->mTangents[VerticesIndex].y, 
                                                Mesh->mTangents[VerticesIndex].z);
                            
                            Vertex.Bitangent = V3(Mesh->mBitangents[VerticesIndex].x, 
                                                  Mesh->mBitangents[VerticesIndex].y, 
                                                  Mesh->mBitangents[VerticesIndex].z);
                        }
                        
                        VertexData[CountedVertices++] = Vertex;
                    }
                    
                    u32 *IndicesData = (u32 *)GetIndicesData(AssetDataBlock);
                    
                    u32 IndicesOffsetCount = CountedIndices;
                    mmsz IndicesOffsetSize = TotalVertices * sizeof(textured_vertex) + IndicesOffsetCount * sizeof(u32);
                    if(Mesh->HasFaces())
                    {
                        for(u32 FaceIndex = 0;
                            FaceIndex < Mesh->mNumFaces;
                            ++FaceIndex)
                        {
                            aiFace *Face = &Mesh->mFaces[FaceIndex];
                            
                            for(u32 IndicesIndex = 0;
                                IndicesIndex < Face->mNumIndices;
                                ++IndicesIndex)
                            {
                                IndicesData[CountedIndices++] = (u32)Face->mIndices[IndicesIndex] + VerticesOffsetCount;
                            }
                        }
                    }
                    
                    mesh ResultMesh = {};
                    CopyString(ResultMesh.Name, Mesh->mName.length, Mesh->mName.data);
                    ResultMesh.MaterialID = Mesh->mMaterialIndex + MaterialSlotBaseIndex;
                    ResultMesh.VerticesOffset = VerticesOffsetCount;
                    ResultMesh.VerticesCount = TotalMeshVertices;
                    ResultMesh.IndicesOffset = IndicesOffsetSize;
                    ResultMesh.IndicesCount = Mesh->mNumFaces * 3;
                    
                    TicketMutexLock(&GameMemoryMutex);
                    LinkMeshToAsset(Asset, &ResultMesh, &Storage->MeshStorage);
                    TicketMutexUnlock(&GameMemoryMutex);
                }
                
                Asset->VerticesCount = TotalVertices;
                Asset->IndicesCount = TotalIndices;
                
                asset_block_header *BlockHeader = GetAssetBlockHeader(AssetDataBlock);
                
                TicketMutexLock(&GameMemoryMutex);
                PushVertexData(RenderCommands, 
                               Asset->BufferID, 
                               AssetDataBlock,
                               BlockHeader->VerticesBufferSize,
                               BlockHeader->IndicesBufferSize,
                               &BlockHeader->Flags);
                TicketMutexUnlock(&GameMemoryMutex);
                
                
                
                // Process asset transforms tree
                Asset->RootNode = PushRelationalNode(&Storage->NodesStorage);
                
                u32 NodesCapacity = 2000;
                
                aiNode **NodesQueue = PushArray(ThreadLocalStorage, NodesCapacity, aiNode *);
                u32 NextQueueSlot = 0;
                u32 QueueReadSlot = 0;
                
                relational_node **RelNodesQueue = PushArray(ThreadLocalStorage, NodesCapacity, relational_node *);
                u32 NextRelQueueSlot = 0;
                u32 RelQueueReadSlot = 0;
                
                NodesQueue[NextQueueSlot++] = Model->mRootNode;
                RelNodesQueue[NextRelQueueSlot++] = Asset->RootNode;
                
                while(QueueReadSlot < NextQueueSlot)
                {
                    aiNode *AiCurrentNode = NodesQueue[QueueReadSlot++];
                    relational_node *TargetNode = RelNodesQueue[RelQueueReadSlot++];
                    memcpy_s(TargetNode->Name, sizeof(TargetNode->Name), AiCurrentNode->mName.data, AiCurrentNode->mName.length);
                    
                    aiMatrix4x4 Transform = AiCurrentNode->mTransformation;
                    memcpy_s(TargetNode->LocalMatrix.DataL, sizeof(mat4x4), &Transform, sizeof(mat4x4));
                    
                    mesh **NextMeshRef = &TargetNode->MeshHeadRef;
                    for(u32 AiMeshIndex = 0;
                        AiMeshIndex < AiCurrentNode->mNumMeshes;
                        ++AiMeshIndex)
                    {
                        u32 Idx = AiCurrentNode->mMeshes[AiMeshIndex];
                        
                        u32 CurrentIdx = 0;
                        mesh *MeshRef = Asset->Meshes.Head;
                        while((CurrentIdx != Idx) && (CurrentIdx < Asset->Meshes.TotalMeshes))
                        {
                            MeshRef = MeshRef->Next;
                            CurrentIdx++;
                        }
                        
                        *NextMeshRef = MeshRef;
                        NextMeshRef = &MeshRef->NextRef;
                    }
                    
                    relational_node **TargetSubnode = &TargetNode->SublevelNextNode;
                    for(u32 ChildrenIndex = 0;
                        ChildrenIndex < AiCurrentNode->mNumChildren;
                        ++ChildrenIndex)
                    {
                        relational_node *TargetChild = PushRelationalNode(&Storage->NodesStorage);
                        TargetChild->Parent = TargetNode;
                        
                        aiNode *AiCurrentChild = AiCurrentNode->mChildren[ChildrenIndex];
                        
                        *TargetSubnode = TargetChild;
                        TargetSubnode = &TargetChild->LevelNextNode;
                        
                        RelNodesQueue[NextRelQueueSlot++] = TargetChild;
                        NodesQueue[NextQueueSlot++] = AiCurrentChild;
                    }
                }
                Asset->Transform.UpdateTransform = true;
                
                UpdateAssetTransformData(Asset, ThreadLocalStorage);
                
                Asset->Status = RenderStatus_Available;
                
                OutputDebugStringA("[JOB_QUEUE] LOADED ASSET DATA: ");
                OutputDebugStringA(Asset->Path);
                OutputDebugStringA(" \n");
                
                struct pointlight_hashslot
                {
                    string Name;
                    
                    pointlight_hashslot *Next;
                };
                
                if(Model->HasLights())
                {
                    pointlight_hashslot *LightsHashMap = PushArray(ThreadLocalStorage, Model->mNumLights, pointlight_hashslot);
                    
                    for(u32 Index = 0;
                        Index < Model->mNumLights;
                        ++Index)
                    {
                        aiLight *Light = Model->mLights[Index];
                        
                        u32 HashLocation = HashDjb2(Light->mName.data, Model->mNumLights);
                        pointlight_hashslot *Slot = &LightsHashMap[HashLocation];
                        if(EmptyString(Slot->Name))
                        {
                            Slot->Name.Data = Light->mName.data;
                            Slot->Name.Count = Light->mName.length;
                        }
                        else
                        {
                            pointlight_hashslot *CurrentSlot = Slot;
                            while(CurrentSlot->Next)
                            {
                                CurrentSlot = CurrentSlot->Next;
                            }
                            
                            pointlight_hashslot *NextSlot = PushStruct(ThreadLocalStorage, pointlight_hashslot);
                            NextSlot->Name.Data = Light->mName.data;
                            NextSlot->Name.Count = Light->mName.length;
                            CurrentSlot->Next = NextSlot;
                        }
                    }
                    
                    aiNode **_NodesQueue = PushArray(ThreadLocalStorage, 2000, aiNode *);
                    u32 _NextQueueSlot = 0;
                    u32 _QueueReadSlot = 0;
                    
                    _NodesQueue[_NextQueueSlot++] = Model->mRootNode;
                    
                    while(_QueueReadSlot < _NextQueueSlot)
                    {
                        aiNode *AiCurrentNode = _NodesQueue[_QueueReadSlot++];
                        aiMatrix4x4 Transform = AiCurrentNode->mTransformation;
                        
                        string Name = {};
                        Name.Data = AiCurrentNode->mName.data;
                        Name.Count = AiCurrentNode->mName.length;
                        
                        u32 HashLocation = HashDjb2(AiCurrentNode->mName.data, Model->mNumLights);
                        pointlight_hashslot *CurrentSlot = &LightsHashMap[HashLocation];
                        while(CurrentSlot)
                        {
                            if(StringsEqual(Name, CurrentSlot->Name))
                            {
                                vec3 Position = V3(Transform.a4, Transform.b4, Transform.c4);
                                
                                point_light *Light = &Storage->PointLightsStorage.PointLights[LC_ArrayCount(Storage->PointLightsStorage.PointLights)];
                                LC_GetArrayHeader(Storage->PointLightsStorage.PointLights)->Count++;
                                
                                Light->Ambient = V4(V3(1.0f), 0.05f);
                                Light->Diffuse = V4(V3(1.0f), 1.0f);
                                Light->Specular = V4(V3(1.0f), 0.5f);
                                Light->Position = V4(Position, 1.0f);
                                Light->UpdateRequired = true;
                                
                                break;
                            }
                            
                            CurrentSlot = CurrentSlot->Next;
                        }
                        
                        
                        for(u32 ChildrenIndex = 0;
                            ChildrenIndex < AiCurrentNode->mNumChildren;
                            ++ChildrenIndex)
                        {
                            aiNode *AiCurrentChild = AiCurrentNode->mChildren[ChildrenIndex];
                            
                            _NodesQueue[_NextQueueSlot++] = AiCurrentChild;
                        }
                    }
                }
                
                EndTemporaryMemory(TempMem);
            }
            else
            {
                Asset->Status = RenderStatus_FailedToLoad;
            }
        }
        else
        {
            Asset->Status = RenderStatus_FailedToLoad;
        }
    }
}

internal void
ManageAssetsStorage(game_render_commands *RenderCommands, 
                    scene_storage *Storage,
                    platform_job_queue *JobQueue)
{
    BEGIN_TIMED_BLOCK("Assets management");
    
    CleanupTransientStorage(&Storage->AssetStorage.AssetTransientStorage);
    
    BEGIN_TIMED_BLOCK("Assets storage loading");
    for(u32 AssetIndex = 0;
        AssetIndex < Storage->AssetStorage.Used;
        ++AssetIndex)
    {
        asset *Asset = &Storage->AssetStorage.Assets[AssetIndex];
        if(Asset->Status == RenderStatus_LoadToStorage)
        {
            Asset->Status = RenderStatus_InTransit;
            
            asset_load_entry_data *EntryData = Storage->AssetStorage.LoadEntries + Storage->AssetStorage.NextLoadEntry++;
            EntryData->Asset = Asset;
            EntryData->Storage = Storage;
            EntryData->RenderCommands = RenderCommands;
            //Platform.JobQueueAddEntry(JobQueue, LoadAssetData, EntryData, 0);
            
            LoadAssetData(EntryData, 0, &Storage->SceneArena);
        }
    }
    END_TIMED_BLOCK();
    
    END_TIMED_BLOCK();
}

internal asset *
QueueAssetLoad(game_render_commands *RenderCommands, char *Path, asset_storage *Storage, b32 IsDefaultAsset = false)
{
    asset *Result = PushAssetToStorage(Storage);
    Result->Transform.Scale = V3(1.0f);
    Result->Status = RenderStatus_LoadToStorage;
    CopyString(Result->Path, StringLength(Path), Path);
    
    if(!IsDefaultAsset)
    {
        Result->BufferID = PushBufferObjectID(RenderCommands);
    }
    
    return(Result);
}

internal void
LoadAsset(game_render_commands *RenderCommands, char *Path, asset *Asset, scene_storage *Storage)
{
    Asset->BufferID = PushBufferObjectID(RenderCommands);
    if(Asset->BufferID)
    {
        Assimp::Importer AssetImporter;
        
        // NOTE(Cristian): Assimp can return "faces" with more than 3 indices, which are defined as polygons.
        // By specifying the 'aiProcess_Triangulate' flag, Assimp is forced to always return 3 indices per face.
        const aiScene *Model = AssetImporter.ReadFile(Path, aiProcess_Triangulate | aiProcess_FlipUVs | aiProcess_JoinIdenticalVertices);
        
        if(!Model || Model->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !Model->mRootNode)
        {
            // LOG ERROR (AssetImporter.GetErrorString())
        }
        else
        {
            u32 TotalMeshes = Model->mNumMeshes;
            
            u32 TotalVertices = 0;
            u32 TotalIndices = 0;
            for(u32 Index = 0;
                Index < TotalMeshes;
                ++Index)
            {
                aiMesh *Mesh = Model->mMeshes[Index];
                TotalVertices += Mesh->mNumVertices;
                TotalIndices += Mesh->mNumFaces;
            }
            TotalIndices *= 3;
            
            void *AssetDataBlock = PushAssetDataBlock(&Storage->AssetStorage.AssetTransientStorage, TotalVertices, TotalIndices);
            if(AssetDataBlock)
            {
                textured_vertex *VertexData = (textured_vertex *)AssetDataBlock;
                
                u32 VerticesCount = 0;
                u32 IndicesCount = 0;
                
                string DirectoryPath = GetParentDirectoryName(Path);
                
                u32 RelativeMaterialBaseIndex = Storage->MaterialStorage.NextMaterialSlot;
                
                {
                    // MATERIALS SETUP
                    temporary_memory TempMem = BeginTemporaryMemory(&Storage->SceneArena);
                    
                    hash_table *HashTable = PushStruct(&Storage->SceneArena, hash_table);
                    
                    for(u32 MaterialIndex = 0;
                        MaterialIndex < Model->mNumMaterials;
                        ++MaterialIndex)
                    {
                        material *Material = PushMaterialToStorage(&Storage->MaterialStorage);
                        aiMaterial *AiMaterial = Model->mMaterials[MaterialIndex];
                        PrepareMaterial(AiMaterial, Material, DirectoryPath, &Storage->RenderTexturesStorage, HashTable, &Storage->SceneArena);
                    }
                    
                    EndTemporaryMemory(TempMem);
                }
                
                for(u32 Index = 0;
                    Index < TotalMeshes;
                    ++Index)
                {
                    aiMesh *Mesh = Model->mMeshes[Index];
                    
                    aiMaterial *Mat = Model->mMaterials[Mesh->mMaterialIndex];
                    aiString name;
                    Mat->Get(AI_MATKEY_NAME,name);
                    
                    
                    u32 TotalMeshVertices = Mesh->mNumVertices;
                    u32 VerticesOffsetCount = VerticesCount;
                    
                    for(u32 VerticesIndex = 0;
                        VerticesIndex < TotalMeshVertices;
                        ++VerticesIndex)
                    {
                        textured_vertex Vertex = {};
                        
                        if(Mesh->HasPositions())
                        {
                            Vertex.Position = V3(Mesh->mVertices[VerticesIndex].x, Mesh->mVertices[VerticesIndex].y, Mesh->mVertices[VerticesIndex].z);
                        }
                        
                        if(Mesh->HasNormals())
                        {
                            Vertex.Normal = V3(Mesh->mNormals[VerticesIndex].x, Mesh->mNormals[VerticesIndex].y, Mesh->mNormals[VerticesIndex].z);
                        }
                        
                        if(Mesh->HasTextureCoords(0))
                        {
                            Vertex.UVCoords = V2(Mesh->mTextureCoords[0][VerticesIndex].x, Mesh->mTextureCoords[0][VerticesIndex].y);
                        }
                        else if(Mesh->HasVertexColors(0))
                        {
                            u32 ColorChannels = Mesh->GetNumColorChannels();
                            switch(ColorChannels)
                            {
                                case 1:
                                {
                                    Vertex.Color = V3(Mesh->mColors[0][VerticesIndex].r, 0, 0);
                                } break;
                                
                                case 2:
                                {
                                    Vertex.Color = V3(Mesh->mColors[0][VerticesIndex].r, Mesh->mColors[0][VerticesIndex].g, 0);
                                } break;
                                
                                case 3:
                                {
                                    Vertex.Color = V3(Mesh->mColors[0][VerticesIndex].r, Mesh->mColors[0][VerticesIndex].g, Mesh->mColors[0][VerticesIndex].b);
                                } break;
                            }
                        }
                        
                        VertexData[VerticesCount++] = Vertex;
                    }
                    
                    u32 *IndicesData = (u32 *)GetIndicesData(AssetDataBlock);
                    u32 IndicesOffsetCount = IndicesCount;
                    mmsz IndicesOffsetSize = TotalVertices * sizeof(textured_vertex) + IndicesOffsetCount * sizeof(u32);
                    if(Mesh->HasFaces())
                    {
                        for(u32 FaceIndex = 0;
                            FaceIndex < Mesh->mNumFaces;
                            ++FaceIndex)
                        {
                            aiFace *Face = &Mesh->mFaces[FaceIndex];
                            
                            for(u32 IndicesIndex = 0;
                                IndicesIndex < Face->mNumIndices;
                                ++IndicesIndex)
                            {
                                IndicesData[IndicesCount++] = (u32)Face->mIndices[IndicesIndex] + VerticesOffsetCount;
                            }
                        }
                    }
                    
                    mesh ResultMesh = {};
                    CopyString(ResultMesh.Name, Mesh->mName.length, Mesh->mName.data);
                    ResultMesh.MaterialID = Mesh->mMaterialIndex + RelativeMaterialBaseIndex;
                    ResultMesh.VerticesOffset = VerticesOffsetCount;
                    ResultMesh.VerticesCount = TotalMeshVertices;
                    ResultMesh.IndicesOffset = IndicesOffsetSize;
                    ResultMesh.IndicesCount = Mesh->mNumFaces * 3;
                    
                    LinkMeshToAsset(Asset, &ResultMesh, &Storage->MeshStorage);
                }
                
                asset_block_header *BlockHeader = GetAssetBlockHeader(AssetDataBlock);
                BlockHeader->BufferID = Asset->BufferID;
                BlockHeader->Flags = BufferBlock_Loaded;
                
                
                Asset->RootNode = PushRelationalNode(&Storage->NodesStorage);
                
                temporary_memory TempMem = BeginTemporaryMemory(&Storage->SceneArena);
                
                u32 NodesCapacity = 2000;
                
                aiNode **NodesQueue = PushArray(&Storage->SceneArena, NodesCapacity, aiNode *);
                u32 NextQueueSlot = 0;
                u32 QueueReadSlot = 0;
                
                relational_node **RelNodesQueue = PushArray(&Storage->SceneArena, NodesCapacity, relational_node *);
                u32 NextRelQueueSlot = 0;
                u32 RelQueueReadSlot = 0;
                
                NodesQueue[NextQueueSlot++] = Model->mRootNode;
                RelNodesQueue[NextRelQueueSlot++] = Asset->RootNode;
                
                while(QueueReadSlot < NextQueueSlot)
                {
                    aiNode *AiCurrentNode = NodesQueue[QueueReadSlot++];
                    relational_node *TargetNode = RelNodesQueue[RelQueueReadSlot++];
                    memcpy_s(TargetNode->Name, sizeof(TargetNode->Name), AiCurrentNode->mName.data, AiCurrentNode->mName.length);
                    
                    aiMatrix4x4 Transform = AiCurrentNode->mTransformation;
                    memcpy_s(TargetNode->LocalMatrix.DataL, sizeof(mat4x4), &Transform, sizeof(mat4x4));
                    
                    mesh **NextMeshRef = &TargetNode->MeshHeadRef;
                    for(u32 AiMeshIndex = 0;
                        AiMeshIndex < AiCurrentNode->mNumMeshes;
                        ++AiMeshIndex)
                    {
                        u32 Idx = AiCurrentNode->mMeshes[AiMeshIndex];
                        
                        u32 CurrentIdx = 0;
                        mesh *MeshRef = Asset->Meshes.Head;
                        while((CurrentIdx != Idx) && (CurrentIdx < Asset->Meshes.TotalMeshes))
                        {
                            MeshRef = MeshRef->Next;
                            CurrentIdx++;
                        }
                        
                        *NextMeshRef = MeshRef;
                        NextMeshRef = &MeshRef->NextRef;
                    }
                    
                    relational_node **TargetSubnode = &TargetNode->SublevelNextNode;
                    for(u32 ChildrenIndex = 0;
                        ChildrenIndex < AiCurrentNode->mNumChildren;
                        ++ChildrenIndex)
                    {
                        relational_node *TargetChild = PushRelationalNode(&Storage->NodesStorage);
                        TargetChild->Parent = TargetNode;
                        
                        aiNode *AiCurrentChild = AiCurrentNode->mChildren[ChildrenIndex];
                        
                        *TargetSubnode = TargetChild;
                        TargetSubnode = &TargetChild->LevelNextNode;
                        
                        RelNodesQueue[NextRelQueueSlot++] = TargetChild;
                        NodesQueue[NextQueueSlot++] = AiCurrentChild;
                    }
                }
                Asset->Transform.UpdateTransform = true;
                
                UpdateAssetTransformData(Asset, &Storage->SceneArena);
                
                EndTemporaryMemory(TempMem);
            }
            else
            {
                // LOG ERROR HERE
            }
        }
    }
}