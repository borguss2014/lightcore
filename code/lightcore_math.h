#include <math.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_access.hpp>
#include <glm/gtx/matrix_operation.hpp>

#define DegreesToRadians(degrees) (degrees * PI_OVER_HALF_CIRCLE_DEGREES)
#define RadiansToDegrees(radians) (radians * HALF_CIRCLE_DEGREES_OVER_PI)

#define Cube(x) ((x)*(x)*(x))
#define Square(x) ((x)*(x))

inline v2
V2(r32 X)
{
    v2 Result = {};
    Result.x = X;
    Result.y = X;
    
    return(Result);
}

inline v2
V2(r32 X, r32 Y)
{
    v2 Result = {};
    Result.x = X;
    Result.y = Y;
    
    return(Result);
}

inline v2
V2(v3 V)
{
    v2 Result = {};
    Result.x = V.x;
    Result.y = V.y;
    
    return(Result);
}

inline v2
V2(v4 V)
{
    v2 Result = {};
    Result.x = V.x;
    Result.y = V.y;
    
    return(Result);
}

inline v3
V3(r32 X, v2 M)
{
    v3 Result = {};
    Result.x = X;
    Result.y = M.x;
    Result.z = M.y;
    
    return(Result);
}

inline v3
V3(v2 M, r32 Z)
{
    v3 Result = {};
    Result.x = M.x;
    Result.y = M.y;
    Result.z = Z;
    
    return(Result);
}

inline v3
V3(r32 X)
{
    v3 Result = {};
    Result.x = X;
    Result.y = X;
    Result.z = X;
    
    return(Result);
}

inline v3
V3(r32 X, r32 Y, r32 Z)
{
    v3 Result = {};
    Result.x = X;
    Result.y = Y;
    Result.z = Z;
    
    return(Result);
}

inline v3
V3(v2 V)
{
    v3 Result = {};
    Result.x = V.x;
    Result.y = V.y;
    Result.z = 0;
    
    return(Result);
}

inline v3
V3(v4 V)
{
    v3 Result = {};
    Result.x = V.x;
    Result.y = V.y;
    Result.z = V.z;
    
    return(Result);
}

inline v4
V4(r32 X)
{
    v4 Result = {};
    Result.x = X;
    Result.y = X;
    Result.z = X;
    Result.w = X;
    
    return(Result);
}

inline v4
V4(r32 X, r32 Y, v2 M)
{
    v4 Result = {};
    Result.x = X;
    Result.y = Y;
    Result.z = M.x;
    Result.w = M.y;
    
    return(Result);
}

inline v4
V4(r32 X, v2 M, r32 W)
{
    v4 Result = {};
    Result.x = X;
    Result.y = M.x;
    Result.z = M.y;
    Result.w = W;
    
    return(Result);
}

inline v4
V4(v2 M, r32 Z, r32 W)
{
    v4 Result = {};
    Result.x = M.x;
    Result.y = M.y;
    Result.z = Z;
    Result.w = W;
    
    return(Result);
}

inline v4
V4(r32 X, r32 Y, r32 Z, r32 W)
{
    v4 Result = {};
    Result.x = X;
    Result.y = Y;
    Result.z = Z;
    Result.w = W;
    
    return(Result);
}

inline v4
V4(v3 VectorXYZ, r32 W)
{
    v4 Result = {};
    Result.x = VectorXYZ.x;
    Result.y = VectorXYZ.y;
    Result.z = VectorXYZ.z;
    Result.w = W;
    
    return(Result);
}

inline v4
V4(r32 X, v3 VectorYZW)
{
    v4 Result = {};
    Result.x = X;
    Result.y = VectorYZW.x;
    Result.z = VectorYZW.y;
    Result.w = VectorYZW.z;
    
    return(Result);
}

inline v4
V4(v2 V)
{
    v4 Result = {};
    Result.x = V.x;
    Result.y = V.y;
    Result.z = 0;
    Result.w = 0;
    
    return(Result);
}

inline v4
V4(v3 V)
{
    v4 Result = {};
    Result.x = V.x;
    Result.y = V.y;
    Result.z = V.z;
    Result.w = 0;
    
    return(Result);
}

inline mat2x2
M2x2(vec2 Row0, vec2 Row1)
{
    mat2x2 Result = {
        Row0,
        Row1
    };
    
    return(Result);
}

inline mat2x2
M2x2(mat3x3 M)
{
    mat2x2 Result = {};
    Result.Row0 = V2(M.Row0);
    Result.Row1 = V2(M.Row1);
    
    return(Result);
}

inline mat2x2
M2x2(mat4x4 M)
{
    mat2x2 Result = {};
    Result.Row0 = V2(M.Row0);
    Result.Row1 = V2(M.Row1);
    
    return(Result);
}

inline mat3x3
M3x3(vec3 Row0, vec3 Row1, vec3 Row2)
{
    mat3x3 Result = {
        Row0,
        Row1,
        Row2
    };
    
    return(Result);
}

inline mat3x3
M3x3(mat2x2 M)
{
    mat3x3 Result = {};
    Result.Row0 = V3(M.Row0);
    Result.Row1 = V3(M.Row1);
    Result.Row2 = V3(0.0f);
    
    return(Result);
}

inline mat3x3
M3x3(mat4x4 M)
{
    mat3x3 Result = {};
    Result.Row0 = V3(M.Row0);
    Result.Row1 = V3(M.Row1);
    Result.Row2 = V3(M.Row2);
    
    return(Result);
}

inline mat4x4
M4x4(vec4 Row0, vec4 Row1, vec4 Row2, vec4 Row3)
{
    mat4x4 Result = {
        Row0,
        Row1,
        Row2,
        Row3
    };
    
    return(Result);
}

inline mat4x4
M4x4(mat2x2 M)
{
    mat4x4 Result = {};
    Result.Row0 = V4(M.Row0);
    Result.Row1 = V4(M.Row1);
    Result.Row2 = V4(0.0f);
    Result.Row3 = V4(0.0f);
    
    return(Result);
}

inline mat4x4
M4x4(mat3x3 M)
{
    mat4x4 Result = {};
    Result.Row0 = V4(M.Row0);
    Result.Row1 = V4(M.Row1);
    Result.Row2 = V4(M.Row2);
    Result.Row3 = V4(0.0f);
    
    return(Result);
}

inline vec2
operator+(vec2 A, f32 B)
{
    vec2 Result = A;
    Result.x += B;
    Result.y += B;
    return(Result);
}

inline vec2
operator+(f32 A, vec2 B)
{
    vec2 Result = B;
    Result.x = A + Result.x;
    Result.y = A + Result.y;
    return(Result);
}

inline vec2
operator-(vec2 A, f32 B)
{
    vec2 Result = A;
    Result.x -= B;
    Result.y -= B;
    return(Result);
}

inline vec2
operator-(f32 A, vec2 B)
{
    vec2 Result = B;
    Result.x = A - Result.x;
    Result.y = A - Result.y;
    return(Result);
}

inline vec2
operator*(vec2 A, f32 B)
{
    vec2 Result = A;
    Result.x *= B;
    Result.y *= B;
    return(Result);
}

inline vec2
operator*(f32 A, vec2 B)
{
    vec2 Result = B;
    Result.x = A * Result.x;
    Result.y = A * Result.y;
    return(Result);
}

inline vec2
operator/(vec2 A, f32 B)
{
    vec2 Result = A;
    Result.x /= B;
    Result.y /= B;
    return(Result);
}

inline vec2
operator/(f32 A, vec2 B)
{
    vec2 Result = B;
    Result.x = A / Result.x;
    Result.y = A / Result.y;
    return(Result);
}

inline void
operator+=(vec2 &A, f32 B)
{
    A = A + B;
}

inline void
operator-=(vec2 &A, f32 B)
{
    A = A - B;
}

inline void
operator*=(vec2 &A, f32 B)
{
    A = A * B;
}

inline void
operator/=(vec2 &A, f32 B)
{
    A = A / B;
}

inline vec2
operator+(vec2 A, vec2 B)
{
    vec2 Result;
    Result.x = A.x + B.x;
    Result.y = A.y + B.y;
    return(Result);
}

inline vec2
operator-(vec2 A, vec2 B)
{
    vec2 Result;
    Result.x = A.x - B.x;
    Result.y = A.y - B.y;
    return(Result);
}

inline vec2
operator*(vec2 A, vec2 B)
{
    vec2 Result;
    Result.x = A.x * B.x;
    Result.y = A.y * B.y;
    return(Result);
}

inline vec2
operator/(vec2 A, vec2 B)
{
    vec2 Result;
    Result.x = A.x / B.x;
    Result.y = A.y / B.y;
    return(Result);
}

inline void
operator+=(vec2 &A, vec2 B)
{
    A = A + B;
}

inline void
operator-=(vec2 &A, vec2 B)
{
    A = A - B;
}

inline void
operator*=(vec2 &A, vec2 B)
{
    A = A * B;
}

inline void
operator/=(vec2 &A, vec2 B)
{
    A = A / B;
}

inline vec3
operator+(vec3 A, f32 B)
{
    vec3 Result = A;
    Result.x += B;
    Result.y += B;
    Result.z += B;
    return(Result);
}

inline vec3
operator+(f32 A, vec3 B)
{
    vec3 Result = B;
    Result.x += A;
    Result.y += A;
    Result.z += A;
    return(Result);
}

inline vec3
operator-(vec3 A, f32 B)
{
    vec3 Result = A;
    Result.x -= B;
    Result.y -= B;
    Result.z -= B;
    return(Result);
}

inline vec3
operator-(f32 A, vec3 B)
{
    vec3 Result = B;
    Result.x = A - Result.x;
    Result.y = A - Result.y;
    Result.z = A - Result.z;
    return(Result);
}

inline vec3
operator*(vec3 A, f32 B)
{
    vec3 Result = A;
    Result.x *= B;
    Result.y *= B;
    Result.z *= B;
    return(Result);
}

inline vec3
operator*(f32 A, vec3 B)
{
    vec3 Result = B;
    Result.x *= A;
    Result.y *= A;
    Result.z *= A;
    return(Result);
}

inline vec3
operator/(vec3 A, f32 B)
{
    vec3 Result = A;
    Result.x /= B;
    Result.y /= B;
    Result.z /= B;
    return(Result);
}

inline vec3
operator/(f32 A, vec3 B)
{
    vec3 Result = B;
    Result.x = A / Result.x;
    Result.y = A / Result.y;
    Result.z = A / Result.z;
    return(Result);
}

inline void
operator+=(vec3 &A, f32 B)
{
    A = A + B;
}

inline void
operator-=(vec3 &A, f32 B)
{
    A = A - B;
}

inline void
operator*=(vec3 &A, f32 B)
{
    A = A * B;
}

inline void
operator/=(vec3 &A, f32 B)
{
    A = A / B;
}

inline vec3
operator+(vec3 A, vec3 B)
{
    vec3 Result;
    Result.x = A.x + B.x;
    Result.y = A.y + B.y;
    Result.z = A.z + B.z;
    return(Result);
}

inline vec3
operator-(vec3 A, vec3 B)
{
    vec3 Result;
    Result.x = A.x - B.x;
    Result.y = A.y - B.y;
    Result.z = A.z - B.z;
    return(Result);
}

inline vec3
operator*(vec3 A, vec3 B)
{
    vec3 Result;
    Result.x = A.x * B.x;
    Result.y = A.y * B.y;
    Result.z = A.z * B.z;
    return(Result);
}

inline vec3
operator/(vec3 A, vec3 B)
{
    vec3 Result;
    Result.x = A.x / B.x;
    Result.y = A.y / B.y;
    Result.z = A.z / B.z;
    return(Result);
}

inline void
operator+=(vec3 &A, vec3 B)
{
    A = A + B;
}

inline void
operator-=(vec3 &A, vec3 B)
{
    A = A - B;
}

inline void
operator*=(vec3 &A, vec3 B)
{
    A = A * B;
}

inline void
operator/=(vec3 &A, vec3 B)
{
    A = A / B;
}

inline b32
operator==(vec3 A, vec3 B)
{
    b32 Result = false;
    if((A.x == B.x) &&
       (A.y == B.y) &&
       (A.z == B.z))
    {
        Result = true;
    }
    return(Result);
}

inline b32
operator!=(vec3 A, vec3 B)
{
    b32 Result = false;
    if((A.x != B.x) ||
       (A.y != B.y) ||
       (A.z != B.z))
    {
        Result = true;
    }
    return(Result);
}

inline vec4
operator+(vec4 A, f32 B)
{
    vec4 Result = A;
    Result.x += B;
    Result.y += B;
    Result.z += B;
    Result.w += B;
    return(Result);
}

inline vec4
operator+(f32 A, vec4 B)
{
    vec4 Result = B;
    Result.x += A;
    Result.y += A;
    Result.z += A;
    Result.w += A;
    return(Result);
}

inline vec4
operator-(vec4 A, f32 B)
{
    vec4 Result = A;
    Result.x -= B;
    Result.y -= B;
    Result.z -= B;
    Result.w -= B;
    return(Result);
}

inline vec4
operator-(f32 A, vec4 B)
{
    vec4 Result = B;
    Result.x = A - Result.x;
    Result.y = A - Result.y;
    Result.z = A - Result.z;
    Result.w = A - Result.w;
    return(Result);
}

inline vec4
operator*(vec4 A, f32 B)
{
    vec4 Result = A;
    Result.x *= B;
    Result.y *= B;
    Result.z *= B;
    Result.w *= B;
    return(Result);
}

inline vec4
operator*(f32 A, vec4 B)
{
    vec4 Result = B;
    Result.x *= A;
    Result.y *= A;
    Result.z *= A;
    Result.w *= A;
    return(Result);
}

inline vec4
operator/(vec4 A, f32 B)
{
    vec4 Result = A;
    Result.x /= B;
    Result.y /= B;
    Result.z /= B;
    Result.w /= B;
    return(Result);
}

inline vec4
operator/(f32 A, vec4 B)
{
    vec4 Result = B;
    Result.x = A / Result.x;
    Result.y = A / Result.y;
    Result.z = A / Result.z;
    Result.w = A / Result.w;
    return(Result);
}

inline void
operator+=(vec4 &A, f32 B)
{
    A = A + B;
}

inline void
operator-=(vec4 &A, f32 B)
{
    A = A - B;
}

inline void
operator*=(vec4 &A, f32 B)
{
    A = A * B;
}

inline void
operator/=(vec4 &A, f32 B)
{
    A = A / B;
}

inline vec4
operator+(vec4 A, vec4 B)
{
    vec4 Result;
    Result.x = A.x + B.x;
    Result.y = A.y + B.y;
    Result.z = A.z + B.z;
    Result.w = A.w + B.w;
    return(Result);
}

inline vec4
operator-(vec4 A, vec4 B)
{
    vec4 Result;
    Result.x = A.x - B.x;
    Result.y = A.y - B.y;
    Result.z = A.z - B.z;
    Result.w = A.w - B.w;
    return(Result);
}

inline vec4
operator*(vec4 A, vec4 B)
{
    vec4 Result;
    Result.x = A.x * B.x;
    Result.y = A.y * B.y;
    Result.z = A.z * B.z;
    Result.w = A.w * B.w;
    return(Result);
}

inline vec4
operator/(vec4 A, vec4 B)
{
    vec4 Result;
    Result.x = A.x / B.x;
    Result.y = A.y / B.y;
    Result.z = A.z / B.z;
    Result.w = A.w / B.w;
    return(Result);
}

inline void
operator+=(vec4 &A, vec4 B)
{
    A = A + B;
}

inline void
operator-=(vec4 &A, vec4 B)
{
    A = A - B;
}

inline void
operator*=(vec4 &A, vec4 B)
{
    A = A * B;
}

inline void
operator/=(vec4 &A, vec4 B)
{
    A = A / B;
}

inline mat2x2
operator+(mat2x2 A, mat2x2 B)
{
    mat2x2 Result;
    Result.Row0 = A.Row0 + B.Row0;
    Result.Row1 = A.Row1 + B.Row1;
    return(Result);
}

inline mat2x2
operator-(mat2x2 A, mat2x2 B)
{
    mat2x2 Result;
    Result.Row0 = A.Row0 - B.Row0;
    Result.Row1 = A.Row1 - B.Row1;
    return(Result);
}

inline mat2x2
operator*(mat2x2 A, mat2x2 B)
{
    mat2x2 Result;
    Result.x0 = (A.x0 * B.x0) + (A.y0 * B.x1);
    Result.y0 = (A.x0 * B.y0) + (A.y0 * B.y1);
    Result.x1 = (A.x1 * B.x0) + (A.y1 * B.x1);
    Result.y1 = (A.x1 * B.y0) + (A.y1 * B.y1);
    return(Result);
}

inline vec2
operator*(mat2x2 A, vec2 B)
{
    vec2 Result;
    Result.x = (A.x0 * B.x) + (A.y0 * B.y);
    Result.y = (A.x1 * B.x) + (A.y1 * B.y);
    return(Result);
}

inline mat2x2
operator*(mat2x2 A, f32 B)
{
    mat2x2 Result;
    Result.Row0 = A.Row0 * B;
    Result.Row1 = A.Row1 * B;
    return(Result);
}

inline mat2x2
operator*(f32 A, mat2x2 B)
{
    mat2x2 Result;
    Result.Row0 = B.Row0 * A;
    Result.Row1 = B.Row1 * A;
    return(Result);
}

inline mat2x2
operator+=(mat2x2 &A, mat2x2 B)
{
    A = A + B;
}

inline mat2x2
operator-=(mat2x2 &A, mat2x2 B)
{
    A = A - B;
}

inline mat2x2
operator*=(mat2x2 &A, f32 B)
{
    A = A * B;
}

inline mat2x2
operator*=(mat2x2 &A, mat2x2 B)
{
    A = A * B;
}

inline mat3x3
operator+(mat3x3 A, mat3x3 B)
{
    mat3x3 Result;
    Result.Row0 = A.Row0 + B.Row0;
    Result.Row1 = A.Row1 + B.Row1;
    Result.Row2 = A.Row2 + B.Row2;
    return(Result);
}

inline mat3x3
operator-(mat3x3 A, mat3x3 B)
{
    mat3x3 Result;
    Result.Row0 = A.Row0 - B.Row0;
    Result.Row1 = A.Row1 - B.Row1;
    Result.Row2 = A.Row2 - B.Row2;
    return(Result);
}

inline mat3x3
operator*(mat3x3 A, mat3x3 B)
{
    mat3x3 Result;
    Result.x0 = (A.x0 * B.x0) + (A.y0 * B.x1) + (A.z0 * B.x2);
    Result.y0 = (A.x0 * B.y0) + (A.y0 * B.y1) + (A.z0 * B.y2);
    Result.z0 = (A.x0 * B.z0) + (A.y0 * B.z1) + (A.z0 * B.z2);
    
    Result.x1 = (A.x1 * B.x0) + (A.y1 * B.x1) + (A.z1 * B.x2);
    Result.y1 = (A.x1 * B.y0) + (A.y1 * B.y1) + (A.z1 * B.y2);
    Result.z1 = (A.x1 * B.z0) + (A.y1 * B.z1) + (A.z1 * B.z2);
    
    Result.x2 = (A.x2 * B.x0) + (A.y2 * B.x1) + (A.z2 * B.x2);
    Result.y2 = (A.x2 * B.y0) + (A.y2 * B.y1) + (A.z2 * B.y2);
    Result.z2 = (A.x2 * B.z0) + (A.y2 * B.z1) + (A.z2 * B.z2);
    return(Result);
}

inline vec3
operator*(mat3x3 A, vec3 B)
{
    vec3 Result;
    Result.x = (A.x0 * B.x) + (A.y0 * B.y) + (A.z0 * B.z);
    Result.y = (A.x1 * B.x) + (A.y1 * B.y) + (A.z1 * B.z);
    Result.z = (A.x2 * B.x) + (A.y2 * B.y) + (A.z2 * B.z);
    return(Result);
}

inline mat3x3
operator*(mat3x3 A, f32 B)
{
    mat3x3 Result;
    Result.Row0 = A.Row0 * B;
    Result.Row1 = A.Row1 * B;
    Result.Row2 = A.Row2 * B;
    return(Result);
}

inline mat3x3
operator*(f32 A, mat3x3 B)
{
    mat3x3 Result;
    Result.Row0 = B.Row0 * A;
    Result.Row1 = B.Row1 * A;
    Result.Row2 = B.Row2 * A;
    return(Result);
}

inline mat3x3
operator+=(mat3x3 &A, mat3x3 B)
{
    A = A + B;
}

inline mat3x3
operator-=(mat3x3 &A, mat3x3 B)
{
    A = A - B;
}

inline mat3x3
operator*=(mat3x3 &A, f32 B)
{
    A = A * B;
}

inline mat3x3
operator*=(mat3x3 &A, mat3x3 B)
{
    A = A * B;
}

inline mat4x4
operator+(mat4x4 A, mat4x4 B)
{
    mat4x4 Result;
    Result.Row0 = A.Row0 + B.Row0;
    Result.Row1 = A.Row1 + B.Row1;
    Result.Row2 = A.Row2 + B.Row2;
    Result.Row3 = A.Row3 + B.Row3;
    return(Result);
}

inline mat4x4
operator-(mat4x4 A, mat4x4 B)
{
    mat4x4 Result;
    Result.Row0 = A.Row0 - B.Row0;
    Result.Row1 = A.Row1 - B.Row1;
    Result.Row2 = A.Row2 - B.Row2;
    Result.Row3 = A.Row3 - B.Row3;
    return(Result);
}

inline mat4x4
operator*(mat4x4 A, mat4x4 B)
{
    mat4x4 Result;
    
    Result.x0 = (A.x0 * B.x0) + (A.y0 * B.x1) + (A.z0 * B.x2) + (A.w0 * B.x3);
    Result.y0 = (A.x0 * B.y0) + (A.y0 * B.y1) + (A.z0 * B.y2) + (A.w0 * B.y3);
    Result.z0 = (A.x0 * B.z0) + (A.y0 * B.z1) + (A.z0 * B.z2) + (A.w0 * B.z3);
    Result.w0 = (A.x0 * B.w0) + (A.y0 * B.w1) + (A.z0 * B.w2) + (A.w0 * B.w3);
    
    Result.x1 = (A.x1 * B.x0) + (A.y1 * B.x1) + (A.z1 * B.x2) + (A.w1 * B.x3);
    Result.y1 = (A.x1 * B.y0) + (A.y1 * B.y1) + (A.z1 * B.y2) + (A.w1 * B.y3);
    Result.z1 = (A.x1 * B.z0) + (A.y1 * B.z1) + (A.z1 * B.z2) + (A.w1 * B.z3);
    Result.w1 = (A.x1 * B.w0) + (A.y1 * B.w1) + (A.z1 * B.w2) + (A.w1 * B.w3);
    
    Result.x2 = (A.x2 * B.x0) + (A.y2 * B.x1) + (A.z2 * B.x2) + (A.w2 * B.x3);
    Result.y2 = (A.x2 * B.y0) + (A.y2 * B.y1) + (A.z2 * B.y2) + (A.w2 * B.y3);
    Result.z2 = (A.x2 * B.z0) + (A.y2 * B.z1) + (A.z2 * B.z2) + (A.w2 * B.z3);
    Result.w2 = (A.x2 * B.w0) + (A.y2 * B.w1) + (A.z2 * B.w2) + (A.w2 * B.w3);
    
    Result.x3 = (A.x3 * B.x0) + (A.y3 * B.x1) + (A.z3 * B.x2) + (A.w3 * B.x3);
    Result.y3 = (A.x3 * B.y0) + (A.y3 * B.y1) + (A.z3 * B.y2) + (A.w3 * B.y3);
    Result.z3 = (A.x3 * B.z0) + (A.y3 * B.z1) + (A.z3 * B.z2) + (A.w3 * B.z3);
    Result.w3 = (A.x3 * B.w0) + (A.y3 * B.w1) + (A.z3 * B.w2) + (A.w3 * B.w3);
    
    return(Result);
}

inline vec4
operator*(mat4x4 A, vec4 B)
{
    vec4 Result;
    Result.x = (A.x0 * B.x) + (A.y0 * B.y) + (A.z0 * B.z) + (A.w0 * B.w);
    Result.y = (A.x1 * B.x) + (A.y1 * B.y) + (A.z1 * B.z) + (A.w1 * B.w);
    Result.z = (A.x2 * B.x) + (A.y2 * B.y) + (A.z2 * B.z) + (A.w2 * B.w);
    Result.w = (A.x3 * B.x) + (A.y3 * B.y) + (A.z3 * B.z) + (A.w3 * B.w);
    return(Result);
}

inline mat4x4
operator*(mat4x4 A, f32 B)
{
    mat4x4 Result;
    Result.Row0 = A.Row0 * B;
    Result.Row1 = A.Row1 * B;
    Result.Row2 = A.Row2 * B;
    Result.Row3 = A.Row3 * B;
    return(Result);
}

inline mat4x4
operator*(f32 A, mat4x4 B)
{
    mat4x4 Result;
    Result.Row0 = B.Row0 * A;
    Result.Row1 = B.Row1 * A;
    Result.Row2 = B.Row2 * A;
    Result.Row3 = B.Row3 * A;
    return(Result);
}

inline mat4x4
operator+=(mat4x4 &A, mat4x4 B)
{
    A = A + B;
}

inline mat4x4
operator-=(mat4x4 &A, mat4x4 B)
{
    A = A - B;
}

inline mat4x4
operator*=(mat4x4 &A, f32 B)
{
    A = A * B;
}

inline mat4x4
operator*=(mat4x4 &A, mat4x4 B)
{
    A = A * B;
}

inline quaternion
operator*(quaternion &Quaternion, vec3 &Vector)
{
    f32 w = - (Quaternion.x * Vector.x) - (Quaternion.y * Vector.y) - (Quaternion.z * Vector.z);
    f32 x =   (Quaternion.w * Vector.x) + (Quaternion.y * Vector.z) - (Quaternion.z * Vector.y);
    f32 y =   (Quaternion.w * Vector.y) + (Quaternion.z * Vector.x) - (Quaternion.x * Vector.z);
    f32 z =   (Quaternion.w * Vector.z) + (Quaternion.x * Vector.y) - (Quaternion.y * Vector.x);
    
    quaternion Result = {
        x, y, z, w
    };
    return(Result);
}


inline quaternion
operator*(quaternion LHS, quaternion RHS)
{
    f32 w = (LHS.w * RHS.w) - (LHS.x * RHS.x) - (LHS.y * RHS.y) - (LHS.z * RHS.z);
    f32 x = (LHS.x * RHS.w) + (LHS.w * RHS.x) + (LHS.y * RHS.z) - (LHS.z * RHS.y);
    f32 y = (LHS.y * RHS.w) + (LHS.w * RHS.y) + (LHS.z * RHS.x) - (LHS.x * RHS.z);
    f32 z = (LHS.z * RHS.w) + (LHS.w * RHS.z) + (LHS.x * RHS.y) - (LHS.y * RHS.x);
    
    quaternion Result = {
        x, y, z, w
    };
    return(Result);
}

internal mat2x2
Mat2x2Identity(void)
{
    mat2x2 Result = {
        V2(1.0f, 0.0f),
        V2(0.0f, 1.0f)
    };
    
    return(Result);
}

internal mat2x2
Mat2x2Identity(f32 DiagonalValue)
{
    mat2x2 Result = {
        V2(DiagonalValue, 0.0f),
        V2(0.0f, DiagonalValue)
    };
    
    return(Result);
}

internal mat3x3
Mat3x3Identity(void)
{
    mat3x3 Result = {
        V3(1.0f, 0.0f, 0.0f),
        V3(0.0f, 1.0f, 0.0f),
        V3(0.0f, 0.0f, 1.0f)
    };
    
    return(Result);
}

internal mat3x3
Mat3x3Identity(f32 DiagonalValue)
{
    mat3x3 Result = {
        V3(DiagonalValue, 0.0f, 0.0f),
        V3(0.0f, DiagonalValue, 0.0f),
        V3(0.0f, 0.0f, DiagonalValue)
    };
    
    return(Result);
}

internal mat4x4
Mat4x4Identity(void)
{
    mat4x4 Result = {
        V4(1.0f, 0.0f, 0.0f, 0.0f),
        V4(0.0f, 1.0f, 0.0f, 0.0f),
        V4(0.0f, 0.0f, 1.0f, 0.0f),
        V4(0.0f, 0.0f, 0.0f, 1.0f)
    };
    
    return(Result);
};

internal mat4x4
Mat4x4Identity(f32 DiagonalValue)
{
    mat4x4 Result = {
        V4(DiagonalValue, 0.0f, 0.0f, 0.0f),
        V4(0.0f, DiagonalValue, 0.0f, 0.0f),
        V4(0.0f, 0.0f, DiagonalValue, 0.0f),
        V4(0.0f, 0.0f, 0.0f, DiagonalValue)
    };
    
    return(Result);
};

internal mat2x2
Transpose(mat2x2 M)
{
    mat2x2 Result = {};
    
    Result.Data[0][0] = M.Data[0][0];
    Result.Data[1][0] = M.Data[0][1];
    
    Result.Data[0][1] = M.Data[1][0];
    Result.Data[1][1] = M.Data[1][1];
    
    return(Result);
}

internal mat3x3
Transpose(mat3x3 M)
{
    mat3x3 Result = {};
    
    Result.Data[0][0] = M.Data[0][0];
    Result.Data[1][0] = M.Data[0][1];
    Result.Data[2][0] = M.Data[0][2];
    
    Result.Data[0][1] = M.Data[1][0];
    Result.Data[1][1] = M.Data[1][1];
    Result.Data[2][1] = M.Data[1][2];
    
    Result.Data[0][2] = M.Data[2][0];
    Result.Data[1][2] = M.Data[2][1];
    Result.Data[2][2] = M.Data[2][2];
    
    return(Result);
}

internal mat4x4
Transpose(mat4x4 M)
{
    mat4x4 Result = {};
    
    Result.Data[0][0] = M.Data[0][0];
    Result.Data[1][0] = M.Data[0][1];
    Result.Data[2][0] = M.Data[0][2];
    Result.Data[3][0] = M.Data[0][3];
    
    Result.Data[0][1] = M.Data[1][0];
    Result.Data[1][1] = M.Data[1][1];
    Result.Data[2][1] = M.Data[1][2];
    Result.Data[3][1] = M.Data[1][3];
    
    Result.Data[0][2] = M.Data[2][0];
    Result.Data[1][2] = M.Data[2][1];
    Result.Data[2][2] = M.Data[2][2];
    Result.Data[3][2] = M.Data[2][3];
    
    Result.Data[0][3] = M.Data[3][0];
    Result.Data[1][3] = M.Data[3][1];
    Result.Data[2][3] = M.Data[3][2];
    Result.Data[3][3] = M.Data[3][3];
    
    return(Result);
}

internal f32
Determinant(mat2x2 M)
{
    f32 Result = M.Data[0][0] * M.Data[1][1] - M.Data[0][1] * M.Data[1][0];
    return(Result);
}

internal f32
Determinant(mat3x3 M)
{
    mat2x2 SubMatrix0 = {};
    SubMatrix0.Row0 = V2(M.Data[1][1], M.Data[1][2]);
    SubMatrix0.Row1 = V2(M.Data[2][1], M.Data[2][2]);
    
    mat2x2 SubMatrix1 = {};
    SubMatrix1.Row0 = V2(M.Data[1][0], M.Data[1][2]);
    SubMatrix1.Row1 = V2(M.Data[2][0], M.Data[2][2]);
    
    mat2x2 SubMatrix2 = {};
    SubMatrix2.Row0 = V2(M.Data[1][0], M.Data[1][1]);
    SubMatrix2.Row1 = V2(M.Data[2][0], M.Data[2][1]);
    
    f32 Det0 = Determinant(SubMatrix0);
    f32 Det1 = Determinant(SubMatrix1);
    f32 Det2 = Determinant(SubMatrix2);
    
    f32 A = M.Data[0][0] * Det0;
    f32 B = M.Data[0][1] * Det1;
    f32 C = M.Data[0][2] * Det2;
    
    f32 Result = A - B + C;
    return(Result);
}

internal f32
Determinant(mat4x4 M)
{
    mat3x3 SubMatrix0 = {};
    SubMatrix0.Row0 = V3(M.Data[1][1], M.Data[1][2], M.Data[1][3]);
    SubMatrix0.Row1 = V3(M.Data[2][1], M.Data[2][2], M.Data[2][3]);
    SubMatrix0.Row2 = V3(M.Data[3][1], M.Data[3][2], M.Data[3][3]);
    
    mat3x3 SubMatrix1 = {};
    SubMatrix1.Row0 = V3(M.Data[1][0], M.Data[1][2], M.Data[1][3]);
    SubMatrix1.Row1 = V3(M.Data[2][0], M.Data[2][2], M.Data[2][3]);
    SubMatrix1.Row2 = V3(M.Data[3][0], M.Data[3][2], M.Data[3][3]);
    
    mat3x3 SubMatrix2 = {};
    SubMatrix2.Row0 = V3(M.Data[1][0], M.Data[1][1], M.Data[1][3]);
    SubMatrix2.Row1 = V3(M.Data[2][0], M.Data[2][1], M.Data[2][3]);
    SubMatrix2.Row2 = V3(M.Data[3][0], M.Data[3][1], M.Data[3][3]);
    
    mat3x3 SubMatrix3 = {};
    SubMatrix3.Row0 = V3(M.Data[1][0], M.Data[1][1], M.Data[1][2]);
    SubMatrix3.Row1 = V3(M.Data[2][0], M.Data[2][1], M.Data[2][2]);
    SubMatrix3.Row2 = V3(M.Data[3][0], M.Data[3][1], M.Data[3][2]);
    
    f32 Det0 = Determinant(SubMatrix0);
    f32 Det1 = Determinant(SubMatrix1);
    f32 Det2 = Determinant(SubMatrix2);
    f32 Det3 = Determinant(SubMatrix3);
    
    f32 A = M.Data[0][0] * Det0;
    f32 B = M.Data[0][1] * Det1;
    f32 C = M.Data[0][2] * Det2;
    f32 D = M.Data[0][3] * Det3;
    
    f32 Result = A - B  + C - D;
    return(Result);
}

internal mat2x2
Adjugate(mat2x2 M)
{
    mat2x2 Result = {};
    Result.Row0 = V2(M.Data[1][1], -M.Data[0][1]);
    Result.Row1 = V2(-M.Data[1][0], M.Data[0][0]);
    
    return(Result);
}

internal mat3x3
Adjugate(mat3x3 M)
{
    mat2x2 A00 = {};
    A00.Row0 = V2(M.Data[1][1], M.Data[1][2]);
    A00.Row1 = V2(M.Data[2][1], M.Data[2][2]);
    
    mat2x2 A01 = {};
    A01.Row0 = V2(M.Data[1][0], M.Data[1][2]);
    A01.Row1 = V2(M.Data[2][0], M.Data[2][2]);
    
    mat2x2 A02 = {};
    A02.Row0 = V2(M.Data[1][0], M.Data[1][1]);
    A02.Row1 = V2(M.Data[2][0], M.Data[2][1]);
    
    mat2x2 A10 = {};
    A10.Row0 = V2(M.Data[0][1], M.Data[0][2]);
    A10.Row1 = V2(M.Data[2][1], M.Data[2][2]);
    
    mat2x2 A11 = {};
    A11.Row0 = V2(M.Data[0][0], M.Data[0][2]);
    A11.Row1 = V2(M.Data[2][0], M.Data[2][2]);
    
    mat2x2 A12 = {};
    A12.Row0 = V2(M.Data[0][0], M.Data[0][1]);
    A12.Row1 = V2(M.Data[2][0], M.Data[2][1]);
    
    mat2x2 A20 = {};
    A20.Row0 = V2(M.Data[0][1], M.Data[0][2]);
    A20.Row1 = V2(M.Data[1][1], M.Data[1][2]);
    
    mat2x2 A21 = {};
    A21.Row0 = V2(M.Data[0][0], M.Data[0][2]);
    A21.Row1 = V2(M.Data[1][0], M.Data[1][2]);
    
    mat2x2 A22 = {};
    A22.Row0 = V2(M.Data[0][0], M.Data[0][1]);
    A22.Row1 = V2(M.Data[1][0], M.Data[1][1]);
    
    mat3x3 Result = {};
    Result.Row0 = V3(Determinant(A00), -Determinant(A01), Determinant(A02));
    Result.Row1 = V3(-Determinant(A10), Determinant(A11), -Determinant(A12));
    Result.Row2 = V3(Determinant(A20), -Determinant(A21), Determinant(A22));
    Result = Transpose(Result);
    
    return(Result);
}

internal mat4x4
Adjugate(mat4x4 M)
{
    mat3x3 A00 = {};
    A00.Row0 = V3(M.Data[1][1], M.Data[1][2], M.Data[1][3]);
    A00.Row1 = V3(M.Data[2][1], M.Data[2][2], M.Data[2][3]);
    A00.Row2 = V3(M.Data[3][1], M.Data[3][2], M.Data[3][3]);
    
    mat3x3 A01 = {};
    A01.Row0 = V3(M.Data[1][0], M.Data[1][2], M.Data[1][3]);
    A01.Row1 = V3(M.Data[2][0], M.Data[2][2], M.Data[2][3]);
    A01.Row2 = V3(M.Data[3][0], M.Data[3][2], M.Data[3][3]);
    
    mat3x3 A02 = {};
    A02.Row0 = V3(M.Data[1][0], M.Data[1][1], M.Data[1][3]);
    A02.Row1 = V3(M.Data[2][0], M.Data[2][1], M.Data[2][3]);
    A02.Row2 = V3(M.Data[3][0], M.Data[3][1], M.Data[3][3]);
    
    mat3x3 A03 = {};
    A03.Row0 = V3(M.Data[1][0], M.Data[1][1], M.Data[1][2]);
    A03.Row1 = V3(M.Data[2][0], M.Data[2][1], M.Data[2][2]);
    A03.Row2 = V3(M.Data[3][0], M.Data[3][1], M.Data[3][2]);
    
    
    mat3x3 A10 = {};
    A10.Row0 = V3(M.Data[0][1], M.Data[0][2], M.Data[0][3]);
    A10.Row1 = V3(M.Data[2][1], M.Data[2][2], M.Data[2][3]);
    A10.Row2 = V3(M.Data[3][1], M.Data[3][2], M.Data[3][3]);
    
    mat3x3 A11 = {};
    A11.Row0 = V3(M.Data[0][0], M.Data[0][2], M.Data[0][3]);
    A11.Row1 = V3(M.Data[2][0], M.Data[2][2], M.Data[2][3]);
    A11.Row2 = V3(M.Data[3][0], M.Data[3][2], M.Data[3][3]);
    
    mat3x3 A12 = {};
    A12.Row0 = V3(M.Data[0][0], M.Data[0][1], M.Data[0][3]);
    A12.Row1 = V3(M.Data[2][0], M.Data[2][1], M.Data[2][3]);
    A12.Row2 = V3(M.Data[3][0], M.Data[3][1], M.Data[3][3]);
    
    mat3x3 A13 = {};
    A13.Row0 = V3(M.Data[0][0], M.Data[0][1], M.Data[0][2]);
    A13.Row1 = V3(M.Data[2][0], M.Data[2][1], M.Data[2][2]);
    A13.Row2 = V3(M.Data[3][0], M.Data[3][1], M.Data[3][2]);
    
    
    mat3x3 A20 = {};
    A20.Row0 = V3(M.Data[0][1], M.Data[0][2], M.Data[0][3]);
    A20.Row1 = V3(M.Data[1][1], M.Data[1][2], M.Data[1][3]);
    A20.Row2 = V3(M.Data[3][1], M.Data[3][2], M.Data[3][3]);
    
    mat3x3 A21 = {};
    A21.Row0 = V3(M.Data[0][0], M.Data[0][2], M.Data[0][3]);
    A21.Row1 = V3(M.Data[1][0], M.Data[1][2], M.Data[1][3]);
    A21.Row2 = V3(M.Data[3][0], M.Data[3][2], M.Data[3][3]);
    
    mat3x3 A22 = {};
    A22.Row0 = V3(M.Data[0][0], M.Data[0][1], M.Data[0][3]);
    A22.Row1 = V3(M.Data[1][0], M.Data[1][1], M.Data[1][3]);
    A22.Row2 = V3(M.Data[3][0], M.Data[3][1], M.Data[3][3]);
    
    mat3x3 A23 = {};
    A23.Row0 = V3(M.Data[0][0], M.Data[0][1], M.Data[0][2]);
    A23.Row1 = V3(M.Data[1][0], M.Data[1][1], M.Data[1][2]);
    A23.Row2 = V3(M.Data[3][0], M.Data[3][1], M.Data[3][2]);
    
    
    mat3x3 A30 = {};
    A30.Row0 = V3(M.Data[0][1], M.Data[0][2], M.Data[0][3]);
    A30.Row1 = V3(M.Data[1][1], M.Data[1][2], M.Data[1][3]);
    A30.Row2 = V3(M.Data[2][1], M.Data[2][2], M.Data[2][3]);
    
    mat3x3 A31 = {};
    A31.Row0 = V3(M.Data[0][0], M.Data[0][2], M.Data[0][3]);
    A31.Row1 = V3(M.Data[1][0], M.Data[1][2], M.Data[1][3]);
    A31.Row2 = V3(M.Data[2][0], M.Data[2][2], M.Data[2][3]);
    
    mat3x3 A32 = {};
    A32.Row0 = V3(M.Data[0][0], M.Data[0][1], M.Data[0][3]);
    A32.Row1 = V3(M.Data[1][0], M.Data[1][1], M.Data[1][3]);
    A32.Row2 = V3(M.Data[2][0], M.Data[2][1], M.Data[2][3]);
    
    mat3x3 A33 = {};
    A33.Row0 = V3(M.Data[0][0], M.Data[0][1], M.Data[0][2]);
    A33.Row1 = V3(M.Data[1][0], M.Data[1][1], M.Data[1][2]);
    A33.Row2 = V3(M.Data[2][0], M.Data[2][1], M.Data[2][2]);
    
    mat4x4 Result = {};
    Result.Row0 = V4(Determinant(A00), -Determinant(A01), Determinant(A02), -Determinant(A03));
    Result.Row1 = V4(-Determinant(A10), Determinant(A11), -Determinant(A12), Determinant(A13));
    Result.Row2 = V4(Determinant(A20), -Determinant(A21), Determinant(A22), -Determinant(A23));
    Result.Row3 = V4(-Determinant(A30), Determinant(A31), -Determinant(A32), Determinant(A33));
    Result = Transpose(Result);
    
    return(Result);
}

internal mat2x2
Inverse(mat2x2 M)
{
    f32 Det = Determinant(M);
    mat2x2 Result = (1 / Det) * Adjugate(M);
    return(Result);
}

internal mat3x3
Inverse(mat3x3 M)
{
    f32 Det = Determinant(M);
    mat3x3 Result = (1 / Det) * Adjugate(M);
    return(Result);
}

internal mat4x4
Inverse(mat4x4 M)
{
    f32 Det = Determinant(M);
    mat4x4 Result = (1 / Det) * Adjugate(M);
    return(Result);
}

inline f32
SquareRoot(f32 Value)
{
    __m128 InputValue = _mm_set_ss(Value);
    __m128 OutputSquaredValue = _mm_sqrt_ss(InputValue);
    f32 Result = _mm_cvtss_f32(OutputSquaredValue);
    return(Result);
}

internal b32
IsVectorEqual(v2 &A, v2 &B)
{
    b32 Result = ((A.x == B.x) && (A.y == B.y));
    return(Result);
}

internal b32
IsVectorEqual(v3 &A, v3 &B)
{
    b32 Result = ((A.x == B.x) && (A.y == B.y) && (A.z == B.z));
    return(Result);
}

internal b32
IsVectorEqual(v4 &A, v4 &B)
{
    b32 Result = ((A.x == B.x) && (A.y == B.y) && (A.z == B.z) && (A.w == B.w));
    return(Result);
}

internal f32
VectorLength(vec2 V)
{
    f32 Result = SquareRoot(Square(V.x) + Square(V.y));
    return(Result);
}

internal f32
VectorLength(vec3 V)
{
    f32 Result = SquareRoot(Square(V.x) + Square(V.y) + Square(V.z));
    return(Result);
}

internal f32
VectorLength(vec4 V)
{
    f32 Result = SquareRoot(Square(V.x) + Square(V.y) + Square(V.z) + Square(V.w));
    return(Result);
}

internal f32
Dot(vec2 A, vec2 B)
{
    f32 Result = (A.x * B.x) + (A.y * B.y);
    return(Result);
}

internal f32
Dot(vec3 A, vec3 B)
{
    f32 Result = (A.x * B.x) + (A.y * B.y) + (A.z * B.z);
    return(Result);
}

internal f32
Dot(vec4 A, vec4 B)
{
    f32 Result = (A.x * B.x) + (A.y * B.y) + (A.z * B.z) + (A.w * B.w);
    return(Result);
}

internal vec3
Cross(vec3 A, vec3 B)
{
    vec3 Result;
    Result.x = (A.y * B.z) - (A.z * B.y);
    Result.y = (A.z * B.x) - (A.x * B.z);
    Result.z = (A.x * B.y) - (A.y * B.x);
    return(Result);
}

internal vec2
Normalize(vec2 Vector)
{
    f32 Length = VectorLength(Vector);
    
    Vector.x /= Length;
    Vector.y /= Length;
    
    return(Vector);
}

internal vec3
Normalize(vec3 Vector)
{
    f32 Length = VectorLength(Vector);
    
    Vector.x /= Length;
    Vector.y /= Length;
    Vector.z /= Length;
    
    return(Vector);
}

internal vec4
Normalize(vec4 Vector)
{
    f32 Length = VectorLength(Vector);
    
    Vector.x /= Length;
    Vector.y /= Length;
    Vector.z /= Length;
    Vector.w /= Length;
    
    return(Vector);
}

internal quaternion
ConjugateQuaternion(quaternion Q)
{
    quaternion Result = {
        -Q.x, -Q.y, -Q.z, Q.w
    };
    return(Result);
}

internal void
NormalizeQuaternion(quaternion *Quaternion)
{
    f32 Length = SquareRoot(Quaternion->x * Quaternion->x + 
                            Quaternion->y * Quaternion->y + 
                            Quaternion->z * Quaternion->z + 
                            Quaternion->w * Quaternion->w);
    Quaternion->x /= Length;
    Quaternion->y /= Length;
    Quaternion->z /= Length;
    Quaternion->w /= Length;
}

internal b32
IsMatrixEqual(mat2x2 &A, mat2x2 &B)
{
    b32 Result = ((IsVectorEqual(A.Row0, B.Row0)) &&
                  (IsVectorEqual(A.Row1, B.Row1)));
    return(Result);
}

internal b32
IsMatrixEqual(mat3x3 &A, mat3x3 &B)
{
    b32 Result = ((IsVectorEqual(A.Row0, B.Row0)) &&
                  (IsVectorEqual(A.Row1, B.Row1)) &&
                  (IsVectorEqual(A.Row2, B.Row2)));
    return(Result);
}

internal b32
IsMatrixEqual(mat4x4 &A, mat4x4 &B)
{
    b32 Result = ((IsVectorEqual(A.Row0, B.Row0)) &&
                  (IsVectorEqual(A.Row1, B.Row1)) &&
                  (IsVectorEqual(A.Row2, B.Row2)) &&
                  (IsVectorEqual(A.Row3, B.Row3)));
    return(Result);
}

internal mat4x4
Translate(mat4x4 Matrix, vec3 TranslationVector)
{
    Matrix.Data[0][3] += TranslationVector.x;
    Matrix.Data[1][3] += TranslationVector.y;
    Matrix.Data[2][3] += TranslationVector.z;
    
    return(Matrix);
}

internal mat4x4
Translate(mat4x4 Matrix, f32 TranslationX, f32 TranslationY, f32 TranslationZ)
{
    Matrix.Data[0][3] += TranslationX;
    Matrix.Data[1][3] += TranslationY;
    Matrix.Data[2][3] += TranslationZ;
    
    return(Matrix);
}

internal mat4x4
Scale(mat4x4 Matrix, vec3 ScaleVector)
{
    Matrix.Data[0][0] *= ScaleVector.x;
    Matrix.Data[1][1] *= ScaleVector.y;
    Matrix.Data[2][2] *= ScaleVector.z;
    return(Matrix);
}

internal mat4x4
Scale(mat4x4 Matrix, f32 ScaleX, f32 ScaleY, f32 ScaleZ)
{
    Matrix.Data[0][0] *= ScaleX;
    Matrix.Data[1][1] *= ScaleY;
    Matrix.Data[2][2] *= ScaleZ;
    return(Matrix);
}

internal vec3
Rotate(vec3 Vector, f32 AngleInRadians, vec3 RotationVector)
{
    f32 HalfAngleInRadians = AngleInRadians / 2.0f;
    
    f32 SinHalfAngle = sinf(HalfAngleInRadians);
    f32 CosHalfAngle = cosf(HalfAngleInRadians);
    
    quaternion Input = {};
    Input.x = RotationVector.x * SinHalfAngle;
    Input.y = RotationVector.y * SinHalfAngle;
    Input.z = RotationVector.z * SinHalfAngle;
    Input.w = CosHalfAngle;
    
    quaternion InputConjugate = ConjugateQuaternion(Input);
    
    quaternion Temp = {};
    Temp = Input * Vector * InputConjugate;
    
    vec3 Result = {
        Temp.x, Temp.y, Temp.z
    };
    return(Result);
}

internal quaternion
Rotate(quaternion Quaternion, f32 AngleInRadians, vec3 RotationVector)
{
    f32 HalfAngleInRadians = AngleInRadians / 2.0f;
    
    f32 SinHalfAngle = sinf(HalfAngleInRadians);
    f32 CosHalfAngle = cosf(HalfAngleInRadians);
    
    quaternion Input = {};
    Input.x = RotationVector.x * SinHalfAngle;
    Input.y = RotationVector.y * SinHalfAngle;
    Input.z = RotationVector.z * SinHalfAngle;
    Input.w = CosHalfAngle;
    
    quaternion InputConjugate = ConjugateQuaternion(Input);
    
    vec3 TempVec = {
        Quaternion.x, Quaternion.y, Quaternion.z
    };
    
    quaternion Result = Input * TempVec * InputConjugate;
    return(Result);
}

internal mat4x4
GetRotationMatrix(quaternion Quaternion)
{
    f32 xx2 = 2.0f * powf(Quaternion.x, 2);
    f32 yy2 = 2.0f * powf(Quaternion.y, 2);
    f32 zz2 = 2.0f * powf(Quaternion.z, 2);
    
    f32 xy2 = 2.0f * Quaternion.x * Quaternion.y;
    f32 xz2 = 2.0f * Quaternion.x * Quaternion.z;
    f32 yz2 = 2.0f * Quaternion.y * Quaternion.z;
    
    f32 xw2 = 2.0f * Quaternion.x * Quaternion.w;
    f32 yw2 = 2.0f * Quaternion.y * Quaternion.w;
    f32 zw2 = 2.0f * Quaternion.z * Quaternion.w;
    
    f32 Rot00 = 1.0f - yy2 - zz2;
    f32 Rot01 = xy2 - zw2;
    f32 Rot02 = xz2 + yw2;
    f32 Rot03 = 0;
    
    f32 Rot10 = xy2 + zw2;
    f32 Rot11 = 1.0f - xx2 - zz2;
    f32 Rot12 = yz2 - xw2;
    f32 Rot13 = 0;
    
    f32 Rot20 = xz2 - yw2;
    f32 Rot21 = yz2 + xw2;
    f32 Rot22 = 1.0f - xx2 - yy2;
    f32 Rot23 = 0;
    
    f32 Rot30 = 0;
    f32 Rot31 = 0;
    f32 Rot32 = 0;
    f32 Rot33 = 1.0f;
    
    mat4x4 Result = {
        Rot00, Rot01, Rot02, Rot03,
        Rot10, Rot11, Rot12, Rot13,
        Rot20, Rot21, Rot22, Rot23,
        Rot30, Rot31, Rot32, Rot33,
    };
    
    return(Result);
}

//internal mat4x4
//PerspectiveProjection(f32 VerticalFOV, f32 AspectRatio, f32 Near, f32 Far)
//{
//f32 TanHalfVFOV = tanf(DegreesToRadians(VerticalFOV / 2.0f));
//f32 d = 1 / TanHalfVFOV;
//
//f32 zRange = Near - Far;
//
//f32 A = (-Far - Near) / zRange;
//f32 B = (2.0f * Far * Near) / zRange;
//
//mat4x4 Result = 
//{
//d / AspectRatio, 0.0f, 0.0f, 0.0f,
//0.0f, d,    0.0f, 0.0f,
//0.0f, 0.0f, A,    B,
//0.0f, 0.0f, 1.0f, 0.0f
//};
//
//return(Result);
//}

internal mat4x4
PerspectiveProjection(f32 VerticalFOV, f32 AspectRatio, f32 Near, f32 Far)
{
    f32 TanHalfVFOV = tanf(DegreesToRadians(VerticalFOV) / 2.0f);
    f32 d = 1 / TanHalfVFOV;
    
    f32 zRange = Far - Near;
    
    f32 A = (- Far - Near) / zRange;
    f32 B = - (2.0f * Far * Near) / zRange;
    
    mat4x4 Result = 
    {
        1 / (TanHalfVFOV * AspectRatio), 0.0f, 0.0f, 0.0f,
        0.0f, d,    0.0f, 0.0f,
        0.0f, 0.0f, A,    B,
        0.0f, 0.0f, -1.0f, 0.0f
    };
    
    return(Result);
}

internal void
TestVec2(glm::vec2 GLMVector, vec2 Vector)
{
    b32 IsNanX = false;
    b32 IsNanY = false;
    
    b32 IsInfX = false;
    b32 IsInfY = false;
    
    if(isnan(GLMVector.x))
    {
        IsNanX = true;
        Assert(isnan(Vector.x));
    }
    
    if(isnan(GLMVector.y))
    {
        IsNanY = true;
        Assert(isnan(Vector.y));
    }
    
    
    if(isinf(GLMVector.x))
    {
        IsInfX = true;
        Assert(isinf(Vector.x));
    }
    
    if(isinf(GLMVector.y))
    {
        IsInfY = true;
        Assert(isinf(Vector.y));
    }
    
    
    if(!IsNanX && !IsInfX)
    {
        Assert((GLMVector.x == Vector.x));
    }
    
    if(!IsNanY && !IsInfY)
    {
        Assert((GLMVector.y == Vector.y));
    }
}

internal void
TestVec3(glm::vec3 GLMVector, vec3 Vector)
{
    b32 IsNanX = false;
    b32 IsNanY = false;
    b32 IsNanZ = false;
    
    b32 IsInfX = false;
    b32 IsInfY = false;
    b32 IsInfZ = false;
    
    if(isnan(GLMVector.x))
    {
        IsNanX = true;
        Assert(isnan(Vector.x));
    }
    
    if(isnan(GLMVector.y))
    {
        IsNanY = true;
        Assert(isnan(Vector.y));
    }
    
    if(isnan(GLMVector.z))
    {
        IsNanZ = true;
        Assert(isnan(Vector.z));
    }
    
    
    if(isinf(GLMVector.x))
    {
        IsInfX = true;
        Assert(isinf(Vector.x));
    }
    
    if(isinf(GLMVector.y))
    {
        IsInfY = true;
        Assert(isinf(Vector.y));
    }
    
    if(isinf(GLMVector.z))
    {
        IsInfZ = true;
        Assert(isinf(Vector.z));
    }
    
    
    if(!IsNanX && !IsInfX)
    {
        Assert((GLMVector.x == Vector.x));
    }
    
    if(!IsNanY && !IsInfY)
    {
        Assert((GLMVector.y == Vector.y));
    }
    
    if(!IsNanZ && !IsInfZ)
    {
        Assert((GLMVector.z == Vector.z));
    }
}

internal void
TestVec4(glm::vec4 GLMVector, vec4 Vector)
{
    b32 IsNanX = false;
    b32 IsNanY = false;
    b32 IsNanZ = false;
    b32 IsNanW = false;
    
    b32 IsInfX = false;
    b32 IsInfY = false;
    b32 IsInfZ = false;
    b32 IsInfW = false;
    
    if(isnan(GLMVector.x))
    {
        IsNanX = true;
        Assert(isnan(Vector.x));
    }
    
    if(isnan(GLMVector.y))
    {
        IsNanY = true;
        Assert(isnan(Vector.y));
    }
    
    if(isnan(GLMVector.z))
    {
        IsNanZ = true;
        Assert(isnan(Vector.z));
    }
    
    if(isnan(GLMVector.w))
    {
        IsNanW = true;
        Assert(isnan(Vector.w));
    }
    
    
    if(isinf(GLMVector.x))
    {
        IsInfX = true;
        Assert(isinf(Vector.x));
    }
    
    if(isinf(GLMVector.y))
    {
        IsInfY = true;
        Assert(isinf(Vector.y));
    }
    
    if(isinf(GLMVector.z))
    {
        IsInfZ = true;
        Assert(isinf(Vector.z));
    }
    
    if(isinf(GLMVector.w))
    {
        IsInfW = true;
        Assert(isinf(Vector.w));
    }
    
    
    if(!IsNanX && !IsInfX)
    {
        Assert((GLMVector.x == Vector.x));
    }
    
    if(!IsNanY && !IsInfY)
    {
        Assert((GLMVector.y == Vector.y));
    }
    
    if(!IsNanZ && !IsInfZ)
    {
        Assert((GLMVector.z == Vector.z));
    }
    
    if(!IsNanW && !IsInfW)
    {
        Assert((GLMVector.w == Vector.w));
    }
}

internal void
Vec2TestAgainstGLM(void)
{
    f32 TestScalar = 5.0f;
    
    glm::vec2 A = glm::vec2(11.0f, 12.0f);
    glm::vec2 B = glm::vec2(7.0f, 8.0f);
    
    glm::vec2 Result1 = A + B;
    glm::vec2 Result2 = A - B;
    glm::vec2 Result3 = A * B;
    glm::vec2 Result4 = A / B;
    
    glm::vec2 Result5 = A + TestScalar;
    glm::vec2 Result6 = A - TestScalar;
    glm::vec2 Result7 = A * TestScalar;
    glm::vec2 Result8 = A / TestScalar;
    
    glm::vec2 Result9 = TestScalar + A;
    glm::vec2 Result10 = TestScalar - A;
    glm::vec2 Result11 = TestScalar * A;
    glm::vec2 Result12 = TestScalar / A;
    
    vec2 C = V2(11.0f, 12.0f);
    vec2 D = V2(7.0f, 8.0f);
    
    vec2 Result1x = C + D;
    vec2 Result2x = C - D;
    vec2 Result3x = C * D;
    vec2 Result4x = C / D;
    
    vec2 Result5x = C + TestScalar;
    vec2 Result6x = C - TestScalar;
    vec2 Result7x = C * TestScalar;
    vec2 Result8x = C / TestScalar;
    
    vec2 Result9x = TestScalar + C;
    vec2 Result10x = TestScalar - C;
    vec2 Result11x = TestScalar * C;
    vec2 Result12x = TestScalar / C;
    
    TestVec2(Result1, Result1x);
    TestVec2(Result2, Result2x);
    TestVec2(Result3, Result3x);
    TestVec2(Result4, Result4x);
    
    TestVec2(Result5, Result5x);
    TestVec2(Result6, Result6x);
    TestVec2(Result7, Result7x);
    TestVec2(Result8, Result8x);
    
    TestVec2(Result9, Result9x);
    TestVec2(Result10, Result10x);
    TestVec2(Result11, Result11x);
    TestVec2(Result12, Result12x);
    
    Result1 += TestScalar;
    Result2 -= TestScalar;
    Result3 *= TestScalar;
    Result4 /= TestScalar;
    
    Result1x += TestScalar;
    Result2x -= TestScalar;
    Result3x *= TestScalar;
    Result4x /= TestScalar;
    
    TestVec2(Result1, Result1x);
    TestVec2(Result2, Result2x);
    TestVec2(Result3, Result3x);
    TestVec2(Result4, Result4x);
    
    Result1 += Result5;
    Result2 -= Result6;
    Result3 *= Result7;
    Result4 /= Result8;
    
    Result1x += Result5x;
    Result2x -= Result6x;
    Result3x *= Result7x;
    Result4x /= Result8x;
    
    TestVec2(Result1, Result1x);
    TestVec2(Result2, Result2x);
    TestVec2(Result3, Result3x);
    TestVec2(Result4, Result4x);
}

internal void
Vec3TestAgainstGLM(void)
{
    f32 TestScalar = 5.0f;
    
    glm::vec3 A = glm::vec3(11.0f, 12.0f, 13.0f);
    glm::vec3 B = glm::vec3(7.0f, 8.0f, 9.0f);
    
    glm::vec3 Result1 = A + B;
    glm::vec3 Result2 = A - B;
    glm::vec3 Result3 = A * B;
    glm::vec3 Result4 = A / B;
    
    glm::vec3 Result5 = A + TestScalar;
    glm::vec3 Result6 = A - TestScalar;
    glm::vec3 Result7 = A * TestScalar;
    glm::vec3 Result8 = A / TestScalar;
    
    glm::vec3 Result9 = TestScalar + A;
    glm::vec3 Result10 = TestScalar - A;
    glm::vec3 Result11 = TestScalar * A;
    glm::vec3 Result12 = TestScalar / A;
    
    vec3 C = V3(11.0f, 12.0f, 13.0f);
    vec3 D = V3(7.0f, 8.0f, 9.0f);
    
    vec3 Result1x = C + D;
    vec3 Result2x = C - D;
    vec3 Result3x = C * D;
    vec3 Result4x = C / D;
    
    vec3 Result5x = C + TestScalar;
    vec3 Result6x = C - TestScalar;
    vec3 Result7x = C * TestScalar;
    vec3 Result8x = C / TestScalar;
    
    vec3 Result9x = TestScalar + C;
    vec3 Result10x = TestScalar - C;
    vec3 Result11x = TestScalar * C;
    vec3 Result12x = TestScalar / C;
    
    TestVec3(Result1, Result1x);
    TestVec3(Result2, Result2x);
    TestVec3(Result3, Result3x);
    TestVec3(Result4, Result4x);
    
    TestVec3(Result5, Result5x);
    TestVec3(Result6, Result6x);
    TestVec3(Result7, Result7x);
    TestVec3(Result8, Result8x);
    
    TestVec3(Result9, Result9x);
    TestVec3(Result10, Result10x);
    TestVec3(Result11, Result11x);
    TestVec3(Result12, Result12x);
    
    Result1 += TestScalar;
    Result2 -= TestScalar;
    Result3 *= TestScalar;
    Result4 /= TestScalar;
    
    Result1x += TestScalar;
    Result2x -= TestScalar;
    Result3x *= TestScalar;
    Result4x /= TestScalar;
    
    TestVec3(Result1, Result1x);
    TestVec3(Result2, Result2x);
    TestVec3(Result3, Result3x);
    TestVec3(Result4, Result4x);
    
    Result1 += Result5;
    Result2 -= Result6;
    Result3 *= Result7;
    Result4 /= Result8;
    
    Result1x += Result5x;
    Result2x -= Result6x;
    Result3x *= Result7x;
    Result4x /= Result8x;
    
    TestVec3(Result1, Result1x);
    TestVec3(Result2, Result2x);
    TestVec3(Result3, Result3x);
    TestVec3(Result4, Result4x);
}

internal void
Vec4TestAgainstGLM(void)
{
    f32 TestScalar = 5.0f;
    
    glm::vec4 A = glm::vec4(11.0f, 12.0f, 13.0f, 14.0f);
    glm::vec4 B = glm::vec4(7.0f, 8.0f, 9.0f, 10.0f);
    
    glm::vec4 Result1 = A + B;
    glm::vec4 Result2 = A - B;
    glm::vec4 Result3 = A * B;
    glm::vec4 Result4 = A / B;
    
    glm::vec4 Result5 = A + TestScalar;
    glm::vec4 Result6 = A - TestScalar;
    glm::vec4 Result7 = A * TestScalar;
    glm::vec4 Result8 = A / TestScalar;
    
    glm::vec4 Result9 = TestScalar + A;
    glm::vec4 Result10 = TestScalar - A;
    glm::vec4 Result11 = TestScalar * A;
    glm::vec4 Result12 = TestScalar / A;
    
    vec4 C = V4(11.0f, 12.0f, 13.0f, 14.0f);
    vec4 D = V4(7.0f, 8.0f, 9.0f, 10.0f);
    
    vec4 Result1x = C + D;
    vec4 Result2x = C - D;
    vec4 Result3x = C * D;
    vec4 Result4x = C / D;
    
    vec4 Result5x = C + TestScalar;
    vec4 Result6x = C - TestScalar;
    vec4 Result7x = C * TestScalar;
    vec4 Result8x = C / TestScalar;
    
    vec4 Result9x = TestScalar + C;
    vec4 Result10x = TestScalar - C;
    vec4 Result11x = TestScalar * C;
    vec4 Result12x = TestScalar / C;
    
    TestVec4(Result1, Result1x);
    TestVec4(Result2, Result2x);
    TestVec4(Result3, Result3x);
    TestVec4(Result4, Result4x);
    
    TestVec4(Result5, Result5x);
    TestVec4(Result6, Result6x);
    TestVec4(Result7, Result7x);
    TestVec4(Result8, Result8x);
    
    TestVec4(Result9, Result9x);
    TestVec4(Result10, Result10x);
    TestVec4(Result11, Result11x);
    TestVec4(Result12, Result12x);
    
    Result1 += TestScalar;
    Result2 -= TestScalar;
    Result3 *= TestScalar;
    Result4 /= TestScalar;
    
    Result1x += TestScalar;
    Result2x -= TestScalar;
    Result3x *= TestScalar;
    Result4x /= TestScalar;
    
    TestVec4(Result1, Result1x);
    TestVec4(Result2, Result2x);
    TestVec4(Result3, Result3x);
    TestVec4(Result4, Result4x);
    
    Result1 += Result5;
    Result2 -= Result6;
    Result3 *= Result7;
    Result4 /= Result8;
    
    Result1x += Result5x;
    Result2x -= Result6x;
    Result3x *= Result7x;
    Result4x /= Result8x;
    
    TestVec4(Result1, Result1x);
    TestVec4(Result2, Result2x);
    TestVec4(Result3, Result3x);
    TestVec4(Result4, Result4x);
}

/*#define TestMat2x2(glmMatrix2d, matrix2d) TestVec2(matrix2d.Row0, glm::row(glmMatrix2d, 0)); TestVec2(matrix2d.Row1, glm::row(glmMatrix2d, 1));*/

internal void
TestMat2x2(glm::mat2 GLMMatrix, mat2x2 Matrix)
{
    glm::vec2 Row0 = glm::column(GLMMatrix, 0);
    glm::vec2 Row1 = glm::column(GLMMatrix, 1);
    
    TestVec2(Row0, Matrix.Row0);
    TestVec2(Row1, Matrix.Row1);
}

internal void
TestMat3x3(glm::mat3 GLMMatrix, mat3x3 Matrix)
{
    glm::vec3 Row0 = glm::column(GLMMatrix, 0);
    glm::vec3 Row1 = glm::column(GLMMatrix, 1);
    glm::vec3 Row2 = glm::column(GLMMatrix, 2);
    
    TestVec3(Row0, Matrix.Row0);
    TestVec3(Row1, Matrix.Row1);
    TestVec3(Row2, Matrix.Row2);
}

internal void
TestMat4x4(glm::mat4 GLMMatrix, mat4x4 Matrix)
{
    glm::vec4 Row0 = glm::column(GLMMatrix, 0);
    glm::vec4 Row1 = glm::column(GLMMatrix, 1);
    glm::vec4 Row2 = glm::column(GLMMatrix, 2);
    glm::vec4 Row3 = glm::column(GLMMatrix, 3);
    
    TestVec4(Row0, Matrix.Row0);
    TestVec4(Row1, Matrix.Row1);
    TestVec4(Row2, Matrix.Row2);
    TestVec4(Row3, Matrix.Row3);
}

internal void
Mat2x2TestAgainstGLM(void)
{
    f32 TestScalar = 5.0f;
    
    glm::mat2 A = glm::mat2(1.0f);
    glm::mat2 B = glm::mat2(3.0f);
    
    glm::mat2 Result1 = A + B;
    glm::mat2 Result2 = A - B;
    glm::mat2 Result3 = A * B;
    glm::mat2 Result44 = A / B;
    
    glm::mat2 Result4 = A + TestScalar;
    glm::mat2 Result5 = A - TestScalar;
    glm::mat2 Result6 = A * TestScalar;
    glm::mat2 Result7 = A / TestScalar;
    
    glm::mat2 Result8 = TestScalar + A;
    glm::mat2 Result9 = TestScalar - A;
    glm::mat2 Result10 = TestScalar * A;
    glm::mat2 Result11 = TestScalar / A;
    
    mat2x2 C = Mat2x2Identity(1.0f);
    mat2x2 D = Mat2x2Identity(3.0f);
    
    mat2x2 Result1x = C + D;
    mat2x2 Result2x = C - D;
    mat2x2 Result3x = C * D;
    
    mat2x2 Result6x = C * TestScalar;
    
    TestMat2x2(Result1, Result1x);
    TestMat2x2(Result2, Result2x);
    TestMat2x2(Result3, Result3x);
    
    
    TestMat2x2(Result6, Result6x);
    
    //===========================================================
    
    glm::mat2 GLMMatrixInput = glm::mat2(glm::vec2(0.0f, 0.0f), 
                                         glm::vec2(0.0f, 1.0f));
    
    mat2x2 MatrixInput = {};
    MatrixInput.Row0 = V2(0.0f, 0.0f);
    MatrixInput.Row1 = V2(0.0f, 1.0f);
    
    
    glm::mat2 MatrixTranspose = glm::transpose(GLMMatrixInput);
    mat2x2 MTranspose = Transpose(MatrixInput);
    TestMat2x2(MatrixTranspose, MTranspose);
    
    f32 Det = glm::determinant(GLMMatrixInput);
    f32 MDet = Determinant(MatrixInput);
    Assert(Det == MDet);
    
    glm::mat2 MatrixAdjugate = glm::adjugate(GLMMatrixInput);
    mat2x2 MMatrixAdjugate = Adjugate(MatrixInput);
    TestMat2x2(MatrixAdjugate, MMatrixAdjugate);
    
    glm::mat2 MatrixInverse = glm::inverse(GLMMatrixInput);
    mat2x2 MInverse = Inverse(MatrixInput);
    TestMat2x2(MatrixInverse, MInverse);
    
    glm::mat2 TransposeInverse = glm::transpose(glm::inverse(GLMMatrixInput));
    mat2x2 MTransposeInverse = Transpose(Inverse(MatrixInput));
    TestMat2x2(TransposeInverse, MTransposeInverse);
}

internal void
Mat3x3TestAgainstGLM(void)
{
    f32 TestScalar = 5.0f;
    
    glm::mat3 A = glm::mat3(1.0f);
    glm::mat3 B = glm::mat3(3.0f);
    
    glm::mat3 Result1 = A + B;
    glm::mat3 Result2 = A - B;
    glm::mat3 Result3 = A * B;
    glm::mat3 Result44 = A / B;
    
    glm::mat3 Result4 = A + TestScalar;
    glm::mat3 Result5 = A - TestScalar;
    glm::mat3 Result6 = A * TestScalar;
    glm::mat3 Result7 = A / TestScalar;
    
    glm::mat3 Result8 = TestScalar + A;
    glm::mat3 Result9 = TestScalar - A;
    glm::mat3 Result10 = TestScalar * A;
    glm::mat3 Result11 = TestScalar / A;
    
    mat3x3 C = Mat3x3Identity(1.0f);
    mat3x3 D = Mat3x3Identity(3.0f);
    
    mat3x3 Result1x = C + D;
    mat3x3 Result2x = C - D;
    mat3x3 Result3x = C * D;
    
    mat3x3 Result6x = C * TestScalar;
    
    TestMat3x3(Result1, Result1x);
    TestMat3x3(Result2, Result2x);
    TestMat3x3(Result3, Result3x);
    
    
    TestMat3x3(Result6, Result6x);
    
    //===========================================================
    
    glm::mat3 GLMMatrixInput = glm::mat3(glm::vec3(0.0f, 0.0f, -1.0f), 
                                         glm::vec3(0.0f, 1.0f, 0.0f),
                                         glm::vec3(9.0f, 0.0f, 0.0f));
    
    mat3x3 MatrixInput = {};
    MatrixInput.Row0 = V3(0.0f, 0.0f, -1.0f);
    MatrixInput.Row1 = V3(0.0f, 1.0f, 0.0f);
    MatrixInput.Row2 = V3(9.0f, 0.0f, 0.0f);
    
    
    glm::mat3 MatrixTranspose = glm::transpose(GLMMatrixInput);
    mat3x3 MTranspose = Transpose(MatrixInput);
    TestMat3x3(MatrixTranspose, MTranspose);
    
    f32 Det = glm::determinant(GLMMatrixInput);
    f32 MDet = Determinant(MatrixInput);
    Assert(Det == MDet);
    
    glm::mat3 MatrixAdjugate = glm::adjugate(GLMMatrixInput);
    mat3x3 MMatrixAdjugate = Adjugate(MatrixInput);
    TestMat3x3(MatrixAdjugate, MMatrixAdjugate);
    
    glm::mat3 MatrixInverse = glm::inverse(GLMMatrixInput);
    mat3x3 MInverse = Inverse(MatrixInput);
    TestMat3x3(MatrixInverse, MInverse);
    
    glm::mat3 TransposeInverse = glm::transpose(glm::inverse(GLMMatrixInput));
    mat3x3 MTransposeInverse = Transpose(Inverse(MatrixInput));
    TestMat3x3(TransposeInverse, MTransposeInverse);
}

internal void
Mat4x4TestAgainstGLM(void)
{
    f32 TestScalar = 5.0f;
    
    glm::mat4 A = glm::mat4(1.0f);
    glm::mat4 B = glm::mat4(3.0f);
    
    glm::mat4 Result1 = A + B;
    glm::mat4 Result2 = A - B;
    glm::mat4 Result3 = A * B;
    glm::mat4 Result44 = A / B;
    
    glm::mat4 Result4 = A + TestScalar;
    glm::mat4 Result5 = A - TestScalar;
    glm::mat4 Result6 = A * TestScalar;
    glm::mat4 Result7 = A / TestScalar;
    
    glm::mat4 Result8 = TestScalar + A;
    glm::mat4 Result9 = TestScalar - A;
    glm::mat4 Result10 = TestScalar * A;
    glm::mat4 Result11 = TestScalar / A;
    
    mat4x4 C = Mat4x4Identity(1.0f);
    mat4x4 D = Mat4x4Identity(3.0f);
    
    mat4x4 Result1x = C + D;
    mat4x4 Result2x = C - D;
    mat4x4 Result3x = C * D;
    
    mat4x4 Result6x = C * TestScalar;
    
    TestMat4x4(Result1, Result1x);
    TestMat4x4(Result2, Result2x);
    TestMat4x4(Result3, Result3x);
    
    
    TestMat4x4(Result6, Result6x);
    
    //===========================================================
    glm::mat4 GLMMatrixInput = glm::mat4(glm::vec4(0.0f, 0.0f, -1.0f, 2.0f), 
                                         glm::vec4(0.0f, 1.0f, 0.0f, 0.0f),
                                         glm::vec4(9.0f, 0.0f, 0.0f, 0.0f),
                                         glm::vec4(0.0f, 0.0f, 0.0f, 1.0f));
    
    mat4x4 MatrixInput = {};
    MatrixInput.Row0 = V4(0.0f, 0.0f, -1.0f, 2.0f);
    MatrixInput.Row1 = V4(0.0f, 1.0f, 0.0f, 0.0f);
    MatrixInput.Row2 = V4(9.0f, 0.0f, 0.0f, 0.0f);
    MatrixInput.Row3 = V4(0.0f, 0.0f, 0.0f, 1.0f);
    
    
    glm::mat4 MatrixTranspose = glm::transpose(GLMMatrixInput);
    mat4x4 MTranspose = Transpose(MatrixInput);
    TestMat4x4(MatrixTranspose, MTranspose);
    
    f32 Det = glm::determinant(GLMMatrixInput);
    f32 MDet = Determinant(MatrixInput);
    Assert(Det == MDet);
    
    glm::mat4 MatrixAdjugate = glm::adjugate(GLMMatrixInput);
    mat4x4 MMatrixAdjugate = Adjugate(MatrixInput);
    TestMat4x4(MatrixAdjugate, MMatrixAdjugate);
    
    glm::mat4 MatrixInverse = glm::inverse(GLMMatrixInput);
    mat4x4 MInverse = Inverse(MatrixInput);
    TestMat4x4(MatrixInverse, MInverse);
    
    glm::mat4 TransposeInverse = glm::transpose(glm::inverse(GLMMatrixInput));
    mat4x4 MTransposeInverse = Transpose(Inverse(MatrixInput));
    TestMat4x4(TransposeInverse, MTransposeInverse);
}

internal void
MiscMathTests()
{
    
    glm::mat3 Transpose = glm::transpose(glm::mat3(1.0));
    {
        glm::vec3 VertexA = glm::vec3(4.5f, 8.8f, 12.1f);
        glm::vec3 Position = glm::vec3(2.0f, 1.5f, 8.9f);
        
        glm::mat4 Model = glm::mat4(1.0f);
        Model = glm::translate(Model, Position);
        Model = glm::scale(Model, glm::vec3(1.0f));
        
        glm::vec4 ResultA = Model * glm::vec4(VertexA, 1.0f);
        
        
        vec3 VertexB = V3(4.5f, 8.8f, 12.1f);
        vec3 Pos = V3(2.0f, 1.5f, 8.9f);
        
        mat4x4 Mod = Mat4x4Identity();
        Mod = Translate(Mod, Pos);
        Mod = Scale(Mod, V3(1.0f));
        
        vec4 ResultB = Mod * V4(VertexB, 1.0f);
        
        u32 T = 4 * 6;
    }
    
    glm::vec3 Vertex = glm::vec3(3.0f, 3.0f, 3.0f);
    glm::vec3 Position = glm::vec3(2.0f, 1.5f, 8.9f);
    
    glm::mat4 TranslationMatrix = glm::mat4(1.0f);
    TranslationMatrix = glm::translate(TranslationMatrix, Position);
    glm::vec3 TempTranslate = TranslationMatrix * glm::vec4(Vertex, 1.0f);
    
    
    glm::mat4 RotationMatrixX = glm::mat4(1.0f);
    RotationMatrixX = glm::rotate(RotationMatrixX, glm::radians(10.0f), glm::vec3(1.0f, 0.0f, 0.0f));
    glm::vec3 TempRotateX = RotationMatrixX * glm::vec4(Vertex, 1.0f);
    
    glm::mat4 RotationMatrixY = glm::mat4(1.0f);
    RotationMatrixY = glm::rotate(RotationMatrixY, glm::radians(20.0f), glm::vec3(0.0f, 1.0f, 0.0f));
    glm::vec3 TempRotateY = RotationMatrixY * glm::vec4(Vertex, 1.0f);
    
    glm::mat4 RotationMatrixZ = glm::mat4(1.0f);
    RotationMatrixZ = glm::rotate(RotationMatrixZ, glm::radians(30.0f), glm::vec3(0.0f, 0.0f, 1.0f));
    glm::vec3 TempRotateZ = RotationMatrixZ * glm::vec4(Vertex, 1.0f);
    
    
    
    
    
    glm::mat4 ScaleMatrix = glm::mat4(1.0f);
    ScaleMatrix= glm::scale(ScaleMatrix, glm::vec3(6.0f, 5.0f, 4.0f));
    glm::vec3 TempScale = ScaleMatrix * glm::vec4(Vertex, 1.0f);
    
    
    
    
    vec3 Vert = V3(3.0f, 3.0f, 3.0f);
    vec3 Pos = V3(2.0f, 1.5f, 8.9f);
    
    mat4x4 TranslationMtx = Mat4x4Identity();
    TranslationMtx = Translate(TranslationMtx, Pos);
    
    vec4 Tmp = TranslationMtx * V4(Vert, 1.0f);
    vec3 TempTranslation = V3(Tmp.x, Tmp.y, Tmp.z);
    
    
    mat4x4 ScaleMtx = Mat4x4Identity();
    ScaleMtx = Scale(ScaleMtx, V3(6.0f, 5.0f, 4.0f));
    
    Tmp = ScaleMtx * V4(Vert, 1.0f);
    vec3 TempSc = V3(Tmp.x, Tmp.y, Tmp.z);
    
    Vert = Rotate(Vert, DegreesToRadians(10.0f), V3(1.0f, 0.0f, 0.0f));
    Vert = Rotate(Vert, DegreesToRadians(20.0f), V3(0.0f, 1.0f, 0.0f));
    Vert = Rotate(Vert, DegreesToRadians(30.0f), V3(0.0f, 0.0f, 1.0f));
    
    
    r64 Angle = DegreesToRadians(10.0f);
    r64 HalfAngleInRadians = Angle / 2.0f;
    
    r64 SinHalfAngle = sin(HalfAngleInRadians);
    r64 CosHalfAngle = cos(HalfAngleInRadians);
    
    vec3 RotationVector = V3(1.0f, 0.0f, 0.0f);
    quaternion Input = {};
    Input.x = (f32)(RotationVector.x * SinHalfAngle);
    Input.y = (f32)(RotationVector.y * SinHalfAngle);
    Input.z = (f32)(RotationVector.z * SinHalfAngle);
    Input.w = (f32)(CosHalfAngle);
    
    quaternion InputConjugate = ConjugateQuaternion(Input);
    
    vec3 Vert2 = V3(3.0f, 3.0f, 3.0f);
    quaternion Temp = {};
    Temp = Input * Vert2 * InputConjugate;
    
    vec3 Result = {
        Temp.x, Temp.y, Temp.z
    };
    
    
    
    quaternion VertQuat = {};
    VertQuat.x = 3.0f;
    VertQuat.y = 3.0f;
    VertQuat.z = 3.0f;
    
    VertQuat = Rotate(VertQuat, DegreesToRadians(10.0f), V3(1.0f, 0.0f, 0.0f));
    VertQuat = Rotate(VertQuat, DegreesToRadians(20.0f), V3(0.0f, 1.0f, 0.0f));
    VertQuat = Rotate(VertQuat, DegreesToRadians(30.0f), V3(0.0f, 0.0f, 1.0f));
    
    
    f32 GLMRadians = glm::radians(45.0f);
    f32 CustomRadians = DegreesToRadians(45.0f);
    f32 AspectRatio = (float)1920 / (float)1080;
    
    glm::mat4 projection = glm::perspective(glm::radians(45.0f), AspectRatio, 0.1f, 100.0f);
    mat4x4 Proj = PerspectiveProjection(45.0f, AspectRatio, 0.1f, 100.0f);
    
    
    glm::vec3 GLMCameraPos = glm::vec3(0.0f, 0.0f, 3.0f);
    
    glm::vec3 GLMUpWorld = glm::vec3(0.0f, 1.0f, 0.0f);
    glm::vec3 GLMCameraFront = glm::vec3(0.0f, 0.0f, -1.0f);
    
    glm::vec3 GLMCameraTarget = GLMCameraPos + GLMCameraFront;
    
    glm::vec3 GLMCameraDirection = glm::normalize(GLMCameraPos - GLMCameraTarget);
    glm::vec3 GLMCameraRight = glm::normalize(glm::cross(GLMUpWorld, GLMCameraDirection));
    glm::vec3 GLMCameraUp = glm::cross(GLMCameraDirection, GLMCameraRight);
    
    glm::mat4 view = glm::lookAt(GLMCameraPos, GLMCameraPos + GLMCameraFront, GLMUpWorld);
    
    
    
    vec3 CameraPos = V3(0.0f, 0.0f, 3.0f);
    
    vec3 UpWorld = V3(0.0f, 1.0f, 0.0f);
    vec3 CameraFront = V3(0.0f, 0.0f, -1.0f);
    
    vec3 CameraTarget = CameraPos + CameraFront;
    
    vec3 CameraDirection = Normalize(CameraPos - CameraTarget);
    vec3 CameraRight = Normalize(Cross(UpWorld, CameraDirection));
    vec3 CameraUp = Cross(CameraDirection, CameraRight);
    
    mat4x4 LookAt1 = {
        CameraRight.x,     CameraRight.y,     CameraRight.z,     0,
        CameraUp.x,        CameraUp.y,        CameraUp.z,        0,
        CameraDirection.x, CameraDirection.y, CameraDirection.z, 0,
        0, 0, 0, 1
    };
    
    mat4x4 LookAt2 = {
        1, 0, 0, -CameraPos.x,
        0, 1, 0, -CameraPos.y,
        0, 0, 1, -CameraPos.z,
        0, 0, 0, 1
    };
    
    mat4x4 LookAt = LookAt1 * LookAt2;
}

internal void
MathTest(void)
{
    Vec2TestAgainstGLM();
    Vec3TestAgainstGLM();
    Vec4TestAgainstGLM();
    Mat2x2TestAgainstGLM();
    Mat3x3TestAgainstGLM();
    Mat4x4TestAgainstGLM();
    MiscMathTests();
}

internal void
NormalizePlane(plane *Plane)
{
    r32 NormalMagnitude = 1.0f / SquareRoot(Plane->A * Plane->A + Plane->B * Plane->B + Plane->C * Plane->C);
    
    Plane->A *= NormalMagnitude;
    Plane->B *= NormalMagnitude;
    Plane->C *= NormalMagnitude;
    Plane->D *= NormalMagnitude;
}

//internal void
//Swap(u32 *a, u32 *b)
//{
//*a = *a + *b;
//*b = *a - *b;
//*a = *a - *b;
//}
//
//internal void
//SIMD256Matrix4x4Mul(glm::mat4 *A, glm::mat4 *B, glm::mat4 *Dest)
//{
//r32 *AData = (r32*)glm::value_ptr(*B);
//r32 *BData = (r32*)glm::value_ptr(*A);
//r32 *Destination = (r32*)glm::value_ptr(*Dest);
//
//__m256 BFirstHalf = _mm256_load_ps(&BData[0]);
//__m256 BSecondHalf = _mm256_load_ps(&BData[8]);
//
//for(u32 Index = 0;
//Index < 16;
//Index += 4)
//{
//__m128 A0 = _mm_broadcast_ss(&AData[Index]);
//__m128 A1 = _mm_broadcast_ss(&AData[Index + 1]);
//__m128 A2 = _mm_broadcast_ss(&AData[Index + 2]);
//__m128 A3 = _mm_broadcast_ss(&AData[Index + 3]);
//
//__m256 A00001111 = _mm256_insertf128_ps(_mm256_castps128_ps256(A0), A1, 1);
//__m256 A22223333 = _mm256_insertf128_ps(_mm256_castps128_ps256(A2), A3, 1);
//
//__m256 IntermRes = _mm256_mul_ps(A00001111, BFirstHalf);
//IntermRes = _mm256_fmadd_ps(A22223333, BSecondHalf, IntermRes);
//
//__m128 TempRes1 = _mm256_extractf128_ps(IntermRes, 0);
//__m128 TempRes2 = _mm256_extractf128_ps(IntermRes, 1);
//__m128 Result = _mm_add_ps(TempRes1, TempRes2);
//
//_mm_store_ps(&Destination[Index], Result);
//}
//}
//
//internal void
//SIMD256Matrix4x4VectorMul(glm::mat4 *A, glm::vec4 *B, glm::vec4 *Dest)
//{
//r32 *AData = (r32*)glm::value_ptr(*B);
//r32 *BData = (r32*)glm::value_ptr(*A);
//r32 *Destination = (r32*)glm::value_ptr(*Dest);
//
//__m256 BFirstHalf = _mm256_load_ps(&BData[0]);
//__m256 BSecondHalf = _mm256_load_ps(&BData[8]);
//
//__m128 A0 = _mm_broadcast_ss(&AData[0]);
//__m128 A1 = _mm_broadcast_ss(&AData[1]);
//__m128 A2 = _mm_broadcast_ss(&AData[2]);
//__m128 A3 = _mm_broadcast_ss(&AData[3]);
//
//__m256 A00001111 = _mm256_insertf128_ps(_mm256_castps128_ps256(A0), A1, 1);
//__m256 A22223333 = _mm256_insertf128_ps(_mm256_castps128_ps256(A2), A3, 1);
//
//__m256 IntermRes = _mm256_mul_ps(A00001111, BFirstHalf);
//IntermRes = _mm256_fmadd_ps(A22223333, BSecondHalf, IntermRes);
//
//__m128 TempRes1 = _mm256_extractf128_ps(IntermRes, 0);
//__m128 TempRes2 = _mm256_extractf128_ps(IntermRes, 1);
//__m128 Result = _mm_add_ps(TempRes1, TempRes2);
//
//_mm_store_ps(&Destination[0], Result);
//}
//
//internal void
//SIMD128Matrix2x2Mul(glm::mat2 *A, glm::mat2 *B, glm::mat2 *Dest)
//{
//r32 *AData = (r32*)glm::value_ptr(*B);
//r32 *BData = (r32*)glm::value_ptr(*A);
//r32 *Destination = (r32*)glm::value_ptr(*Dest);
//
//__m128 Res;
//
//__m128 A0010 = _mm_set_ps(AData[0], AData[0], AData[2], AData[2]);
//__m128 BRow0 = _mm_set_ps(BData[0], BData[1], BData[0], BData[1]);
//Res = _mm_mul_ps(A0010, BRow0);
//
//__m128 A0111 = _mm_set_ps(AData[1], AData[1], AData[3], AData[3]);
//__m128 BRow1 = _mm_set_ps(BData[2], BData[3], BData[2], BData[3]);
//Res = _mm_fmadd_ps(A0111, BRow1, Res);
//
//_mm_store_ps(&Destination[0], Res);
//}