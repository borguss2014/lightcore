#include <imgui/includes/imgui.cpp>
#include <imgui/includes/imgui_draw.cpp>
#include <imgui/includes/imgui_widgets.cpp>
#include <imgui/includes/imgui_tables.cpp>
#include <imgui/includes/imgui_demo.cpp>

#include <imgui/includes/imgui_impl_win32.cpp>
#include <imgui/includes/imgui_impl_opengl3.cpp>

enum overlay_position
{
    OverlayPosition_Center = -2,
    OverlayPosition_TopLeft = 0,
    OverlayPosition_TopRight = 1,
    OverlayPosition_BottomLeft = 2,
    OverlayPosition_BottomRight = 3
};

internal void
CreateSimpleOverlay(bool *p_open, char *Message, u32 FPS, f32 DeltaTime, u32 Position = 0)
{
    int location = Position;
    ImGuiIO& io = ImGui::GetIO();
    ImGuiWindowFlags window_flags = ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoFocusOnAppearing | ImGuiWindowFlags_NoNav;
    if (location >= 0)
    {
        const float PAD = 10.0f;
        const ImGuiViewport* viewport = ImGui::GetMainViewport();
        ImVec2 work_pos = viewport->WorkPos; // Use work area to avoid menu-bar/task-bar, if any!
        ImVec2 work_size = viewport->WorkSize;
        ImVec2 window_pos, window_pos_pivot;
        window_pos.x = (location & 1) ? (work_pos.x + work_size.x - PAD) : (work_pos.x + PAD);
        window_pos.y = (location & 2) ? (work_pos.y + work_size.y - PAD) : (work_pos.y + PAD);
        window_pos_pivot.x = (location & 1) ? 1.0f : 0.0f;
        window_pos_pivot.y = (location & 2) ? 1.0f : 0.0f;
        ImGui::SetNextWindowPos(window_pos, ImGuiCond_Always, window_pos_pivot);
        window_flags |= ImGuiWindowFlags_NoMove;
    }
    else if (location == -2)
    {
        // Center window
        ImGui::SetNextWindowPos(ImGui::GetMainViewport()->GetCenter(), ImGuiCond_Always, ImVec2(0.5f, 0.5f));
        window_flags |= ImGuiWindowFlags_NoMove;
    }
    ImGui::SetNextWindowBgAlpha(0.35f); // Transparent background
    if (ImGui::Begin("Overlay", p_open, window_flags))
    {
        ImGui::Text(Message, FPS, DeltaTime);
        //ImGui::Text("Simple overlay" "(right-click to change position)");
    }
    ImGui::End();
}

internal void
CreateMainMenuBar(debug_state *DebugState)
{
    if (ImGui::BeginMainMenuBar())
    {
        if (ImGui::BeginMenu("Options"))
        {
            if (ImGui::MenuItem("Open profiler", "CTRL+P")) 
            { 
                DebugState->ProfilingWindowActive = true;
                DebugState->DebugRecordingEnabled = true;
            }
            
            if (ImGui::MenuItem("Redo", "CTRL+Y", false, false)) {}  // Disabled item
            ImGui::Separator();
            if (ImGui::MenuItem("Cut", "CTRL+X")) {}
            if (ImGui::MenuItem("Copy", "CTRL+C")) {}
            if (ImGui::MenuItem("Paste", "CTRL+V")) {}
            ImGui::EndMenu();
        }
        ImGui::EndMainMenuBar();
    }
}

internal int
GetPlotHoveredID(ImGuiPlotType plot_type,
                 int values_count,
                 const char* label,
                 const ImVec2& size_arg)
{
    ImGuiContext& g = *ImGui::GetCurrentContext();
    ImGuiWindow* window = ImGui::GetCurrentWindow();
    if (window->SkipItems) {return -1;}
    
    const ImGuiStyle& style = g.Style;
    const ImGuiID id = window->GetID(label);
    
    const ImVec2 label_size = ImGui::CalcTextSize(label, NULL, true);
    const ImVec2 frame_size = ImGui::CalcItemSize(size_arg, ImGui::CalcItemWidth(), label_size.y + style.FramePadding.y * 2.0f);
    
    const ImRect frame_bb(window->DC.CursorPos, window->DC.CursorPos + frame_size);
    const ImRect inner_bb(frame_bb.Min + style.FramePadding, frame_bb.Max - style.FramePadding);
    const ImRect total_bb(frame_bb.Min, frame_bb.Max + ImVec2(label_size.x > 0.0f ? style.ItemInnerSpacing.x + label_size.x : 0.0f, 0));
    
    ImGui::ItemSize(total_bb, style.FramePadding.y);
    if (!ImGui::ItemAdd(total_bb, 0, &frame_bb)) {return -1;}
    
    const bool hovered = ImGui::ItemHoverable(frame_bb, id);
    
    const int values_count_min = (plot_type == ImGuiPlotType_Lines) ? 2 : 1;
    int idx_hovered = -1;
    if (values_count >= values_count_min)
    {
        int item_count = values_count + ((plot_type == ImGuiPlotType_Lines) ? -1 : 0);
        
        if (hovered && inner_bb.Contains(g.IO.MousePos))
        {
            const float t = ImClamp((g.IO.MousePos.x - inner_bb.Min.x) / (inner_bb.Max.x - inner_bb.Min.x), 0.0f, 0.9999f);
            const int v_idx = (int)(t * item_count);
            IM_ASSERT(v_idx >= 0 && v_idx < values_count);
            
            idx_hovered = v_idx;
        }
    }
    
    return(idx_hovered);
}

internal int
PlotEx(int SelectedFrameIndex,
       unsigned int LastAvailableFrame, 
       ImGuiPlotType plot_type, 
       const char* label, 
       float (*values_getter)(void* data, int idx), void* data, 
       int values_count, int values_offset, 
       const char* overlay_text, 
       float scale_min, float scale_max, 
       const ImVec2& size_arg)
{
    ImGuiContext& g = *ImGui::GetCurrentContext();
    ImGuiWindow* window = ImGui::GetCurrentWindow();
    if (window->SkipItems)
        return -1;
    
    const ImGuiStyle& style = g.Style;
    const ImGuiID id = window->GetID(label);
    
    const ImVec2 label_size = ImGui::CalcTextSize(label, NULL, true);
    const ImVec2 frame_size = ImGui::CalcItemSize(size_arg, ImGui::CalcItemWidth(), label_size.y + style.FramePadding.y * 2.0f);
    
    const ImRect frame_bb(window->DC.CursorPos, window->DC.CursorPos + frame_size);
    const ImRect inner_bb(frame_bb.Min + style.FramePadding, frame_bb.Max - style.FramePadding);
    const ImRect total_bb(frame_bb.Min, frame_bb.Max + ImVec2(label_size.x > 0.0f ? style.ItemInnerSpacing.x + label_size.x : 0.0f, 0));
    ImGui::ItemSize(total_bb, style.FramePadding.y);
    if (!ImGui::ItemAdd(total_bb, 0, &frame_bb))
        return -1;
    const bool hovered = ImGui::ItemHoverable(frame_bb, id);
    
    // Determine scale from values if not specified
    if (scale_min == FLT_MAX || scale_max == FLT_MAX)
    {
        float v_min = FLT_MAX;
        float v_max = -FLT_MAX;
        for (int i = 0; i < values_count; i++)
        {
            const float v = values_getter(data, i);
            if (v != v) // Ignore NaN values
                continue;
            v_min = ImMin(v_min, v);
            v_max = ImMax(v_max, v);
        }
        if (scale_min == FLT_MAX)
            scale_min = v_min;
        if (scale_max == FLT_MAX)
            scale_max = v_max;
    }
    
    ImGui::RenderFrame(frame_bb.Min, frame_bb.Max, ImGui::GetColorU32(ImGuiCol_FrameBg), true, style.FrameRounding);
    
    const int values_count_min = (plot_type == ImGuiPlotType_Lines) ? 2 : 1;
    int idx_hovered = -1;
    if (values_count >= values_count_min)
    {
        int res_w = ImMin((int)frame_size.x, values_count) + ((plot_type == ImGuiPlotType_Lines) ? -1 : 0);
        int item_count = values_count + ((plot_type == ImGuiPlotType_Lines) ? -1 : 0);
        
        // Tooltip on hover
        if (hovered && inner_bb.Contains(g.IO.MousePos))
        {
            const float t = ImClamp((g.IO.MousePos.x - inner_bb.Min.x) / (inner_bb.Max.x - inner_bb.Min.x), 0.0f, 0.9999f);
            const int v_idx = (int)(t * item_count);
            IM_ASSERT(v_idx >= 0 && v_idx < values_count);
            
            const float v0 = values_getter(data, (v_idx + values_offset) % values_count);
            const float v1 = values_getter(data, (v_idx + 1 + values_offset) % values_count);
            if (plot_type == ImGuiPlotType_Lines)
                ImGui::SetTooltip("%d: %8.4g\n%d: %8.4g", v_idx, v0, v_idx + 1, v1);
            else if (plot_type == ImGuiPlotType_Histogram)
                ImGui::SetTooltip("%d: %8.4g", v_idx, v0);
            idx_hovered = v_idx;
        }
        
        const float t_step = 1.0f / (float)res_w;
        const float inv_scale = (scale_min == scale_max) ? 0.0f : (1.0f / (scale_max - scale_min));
        
        float v0 = values_getter(data, (0 + values_offset) % values_count);
        float t0 = 0.0f;
        ImVec2 tp0 = ImVec2( t0, 1.0f - ImSaturate((v0 - scale_min) * inv_scale) );                       // Point in the normalized space of our target rectangle
        float histogram_zero_line_t = (scale_min * scale_max < 0.0f) ? (1 + scale_min * inv_scale) : (scale_min < 0.0f ? 0.0f : 1.0f);   // Where does the zero line stands
        
        const ImU32 col_base = ImGui::GetColorU32((plot_type == ImGuiPlotType_Lines) ? ImGuiCol_PlotLines : ImGuiCol_PlotHistogram);
        const ImU32 col_hovered = ImGui::GetColorU32((plot_type == ImGuiPlotType_Lines) ? ImGuiCol_PlotLinesHovered : ImGuiCol_PlotHistogramHovered);
        const ImU32 col_frame_recorded = ImGui::GetColorU32(ImVec4(0, 1, 0, 1));
        const ImU32 col_selected = ImGui::GetColorU32(ImVec4(0, 0, 1, 1));
        
        for (int n = 0; n < res_w; n++)
        {
            const float t1 = t0 + t_step;
            const int v1_idx = (int)(t0 * item_count + 0.5f);
            IM_ASSERT(v1_idx >= 0 && v1_idx < values_count);
            const float v1 = values_getter(data, (v1_idx + values_offset + 1) % values_count);
            const ImVec2 tp1 = ImVec2( t1, 1.0f - ImSaturate((v1 - scale_min) * inv_scale) );
            
            // NB: Draw calls are merged together by the DrawList system. Still, we should render our batch are lower level to save a bit of CPU.
            ImVec2 pos0 = ImLerp(inner_bb.Min, inner_bb.Max, tp0);
            ImVec2 pos1 = ImLerp(inner_bb.Min, inner_bb.Max, (plot_type == ImGuiPlotType_Lines) ? tp1 : ImVec2(tp1.x, histogram_zero_line_t));
            if (plot_type == ImGuiPlotType_Lines)
            {
                window->DrawList->AddLine(pos0, pos1, idx_hovered == v1_idx ? col_hovered : col_base);
            }
            else if (plot_type == ImGuiPlotType_Histogram)
            {
                if (pos1.x >= pos0.x + 2.0f)
                {
                    pos1.x -= 1.0f;
                }
                
                if((unsigned int)v1_idx == LastAvailableFrame)
                {
                    window->DrawList->AddRectFilled(pos0, pos1, idx_hovered == v1_idx ? col_hovered : col_frame_recorded);
                }
                else if(v1_idx == SelectedFrameIndex)
                {
                    window->DrawList->AddRectFilled(pos0, pos1, idx_hovered == v1_idx ? col_hovered : col_selected);
                }
                else
                {
                    window->DrawList->AddRectFilled(pos0, pos1, idx_hovered == v1_idx ? col_hovered : col_base);
                }
            }
            
            t0 = t1;
            tp0 = tp1;
        }
    }
    
    // Text overlay
    if (overlay_text)
        ImGui::RenderTextClipped(ImVec2(frame_bb.Min.x, frame_bb.Min.y + style.FramePadding.y), frame_bb.Max, overlay_text, NULL, NULL, ImVec2(0.5f, 0.0f));
    
    if (label_size.x > 0.0f)
        ImGui::RenderText(ImVec2(frame_bb.Max.x + style.ItemInnerSpacing.x, inner_bb.Min.y), label);
    
    // Return hovered index or -1 if none are hovered.
    // This is currently not exposed in the public API because we need a larger redesign of the whole thing, but in the short-term we are making it available in PlotEx().
    return idx_hovered;
}

internal int
PlotHistogram(int SelectedFrameIndex, unsigned int LastAvailableFrame, const char* label, float (*values_getter)(void* data, int idx), void* data, int values_count, int values_offset, const char* overlay_text, float scale_min, float scale_max, ImVec2 graph_size)
{
    int IdxHovered = PlotEx(SelectedFrameIndex, LastAvailableFrame, ImGuiPlotType_Histogram, label, values_getter, data, values_count, values_offset, overlay_text, 
                            scale_min, scale_max, graph_size);
    return(IdxHovered);
}

internal int
PlotHistogram(int SelectedFrameIndex, unsigned int LastAvailableFrame, const char* label, const float* values, int values_count, int values_offset = 0, const char* overlay_text = NULL, float scale_min = FLT_MAX, float scale_max = FLT_MAX, ImVec2 graph_size = ImVec2(0, 0), int stride = sizeof(float))
{
    ImGuiPlotArrayGetterData data(values, stride);
    int IdxHovered = PlotEx(SelectedFrameIndex, LastAvailableFrame, ImGuiPlotType_Histogram, label, &Plot_ArrayGetter, (void*)&data, values_count, values_offset, overlay_text, scale_min, scale_max, graph_size);
    return(IdxHovered);
}

internal b32
DrawDebugRow(debug_profile_node *ProfileNode, debug_frame *Frame, b32 HasChildren = false)
{
    b32 TreeNodeOpen = false;
    
    if(HasChildren)
    {
        TreeNodeOpen = ImGui::TreeNodeEx((char *)(smp)ProfileNode->GUID, ImGuiTreeNodeFlags_SpanFullWidth, ProfileNode->Name);
    }
    else
    {
        TreeNodeOpen = ImGui::TreeNodeEx(ProfileNode->Name, ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_Bullet | ImGuiTreeNodeFlags_NoTreePushOnOpen | ImGuiTreeNodeFlags_SpanFullWidth);
    }
    
    debug_statistics *Statistics = &ProfileNode->Statistics;
    
    u64 SelfCycles = Statistics->TotalCycles - Statistics->ChildrenCycles;
    f32 CyclesPercentage = ((f32)SelfCycles * 100) / (f32)Frame->TotalCycles;
    ImGui::TableNextColumn();
    ImGui::Text("%.2f", CyclesPercentage);
    
    ImGui::TableNextColumn();
    ImGui::Text("%lu", SelfCycles);
    
    ImGui::TableNextColumn();
    ImGui::Text("%lu", SelfCycles / Statistics->HitCount);
    
    f32 TaskTotalCyclesPercentage = ((f32)Statistics->TotalCycles * 100) / (f32)Frame->TotalCycles;
    ImGui::TableNextColumn();
    ImGui::Text("%.2f", TaskTotalCyclesPercentage);
    
    ImGui::TableNextColumn();
    ImGui::Text("%lu", Statistics->TotalCycles);
    
    ImGui::TableNextColumn();
    ImGui::Text("%lu", Statistics->TotalCycles / Statistics->HitCount);
    
    ImGui::TableNextColumn();
    ImGui::Text("%d", Statistics->HitCount);
    
    ImGui::TableNextColumn();
    ImGui::Text("%lu", Statistics->MinCycles);
    
    ImGui::TableNextColumn();
    ImGui::Text("%lu", Statistics->MaxCycles);
    
    return(TreeNodeOpen);
}

internal void
DrawDebugTableHeader(u32 ThreadID)
{
    f32 TEXT_BASE_WIDTH = ImGui::GetFontSize();
    
    char Buffer[256];
    sprintf_s(Buffer, "Tasks [Thread: %d]", ThreadID);
    
    ImGui::TableSetupColumn(Buffer, ImGuiTableColumnFlags_NoHide);
    
    ImGui::TableSetupColumn("Self Cycles %", ImGuiTableColumnFlags_WidthFixed, TEXT_BASE_WIDTH * 6.0f);
    ImGui::TableSetupColumn("Self Cycles", ImGuiTableColumnFlags_WidthFixed, TEXT_BASE_WIDTH * 6.0f);
    ImGui::TableSetupColumn("Avg. Self", ImGuiTableColumnFlags_WidthFixed, TEXT_BASE_WIDTH * 6.0f);
    
    ImGui::TableSetupColumn("Total %", ImGuiTableColumnFlags_WidthFixed, TEXT_BASE_WIDTH * 6.0f);
    ImGui::TableSetupColumn("Total Cycles", ImGuiTableColumnFlags_WidthFixed, TEXT_BASE_WIDTH * 6.0f);
    ImGui::TableSetupColumn("Avg. Total", ImGuiTableColumnFlags_WidthFixed, TEXT_BASE_WIDTH * 6.0f);
    
    ImGui::TableSetupColumn("Count", ImGuiTableColumnFlags_WidthFixed, TEXT_BASE_WIDTH * 6.0f);
    ImGui::TableSetupColumn("Min Cycles", ImGuiTableColumnFlags_WidthFixed, TEXT_BASE_WIDTH * 5.0f);
    ImGui::TableSetupColumn("Max Cycles", ImGuiTableColumnFlags_WidthFixed, TEXT_BASE_WIDTH * 5.0f);
    
    ImGui::TableSetupColumn("Fn - File (line)", ImGuiTableColumnFlags_WidthFixed, TEXT_BASE_WIDTH * 18.0f);
    
    ImGui::TableHeadersRow();
}

internal void
DrawDebugFrameHierarchical(debug_frame *Frame, debug_memory *DebugMemory)
{
    debug_profile_node *LastStoredParentNode = 0;
    
    static ImGuiTableFlags flags = ImGuiTableFlags_BordersV | ImGuiTableFlags_BordersOuterH | ImGuiTableFlags_Resizable | ImGuiTableFlags_RowBg | ImGuiTableFlags_NoBordersInBody | ImGuiTableFlags_Sortable;
    
    f32 TEXT_BASE_WIDTH = ImGui::GetFontSize();
    u32 TableColumns = 11;
    
    debug_profile_group *CurrentProfileGroup = Frame->LastProfileGroup;
    while(CurrentProfileGroup)
    {
        if (ImGui::BeginTable("ProfilingTable", TableColumns, flags))
        {
            DrawDebugTableHeader(CurrentProfileGroup->ThreadID);
            
            debug_profile_node *CurrentProfileNode = CurrentProfileGroup->LastProfileNode;
            while(CurrentProfileNode)
            {
                ImGui::TableNextRow();
                ImGui::TableNextColumn();
                
                debug_statistics *Statistics = &CurrentProfileNode->Statistics;
                
                /*string GUID = GetString(CurrentProfileNode->GUID);
                u32 HashedIndex = HashString(GUID, 16);*/
                
                if(CurrentProfileNode->LastChildProfileNode)
                {
                    string_iterator StringIterator = InitStringIterator(CurrentProfileNode->GUID);
                    
                    string FileName = NextSubstring(&StringIterator, '|');
                    string FunctionName = NextSubstring(&StringIterator, '|');
                    string LineNumber = NextSubstring(&StringIterator);
                    
                    b32 TreeNodeOpen = DrawDebugRow(CurrentProfileNode, Frame, true);
                    if(TreeNodeOpen)
                    {
                        CurrentProfileNode->NextLink = LastStoredParentNode;
                        LastStoredParentNode = CurrentProfileNode;
                        
                        CurrentProfileNode = CurrentProfileNode->LastChildProfileNode;
                    }
                    else
                    {
                        if(CurrentProfileNode->Next)
                        {
                            CurrentProfileNode = CurrentProfileNode->Next;
                        }
                        else
                        {
                            if(LastStoredParentNode)
                            {
                                while(LastStoredParentNode)
                                {
                                    CurrentProfileNode = LastStoredParentNode->Next;
                                    
                                    debug_profile_node *NextStoredNode = LastStoredParentNode->NextLink;
                                    LastStoredParentNode->NextLink = 0;
                                    LastStoredParentNode = NextStoredNode;
                                    
                                    ImGui::TreePop();
                                    
                                    if(CurrentProfileNode)
                                    {
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                CurrentProfileNode = 0;
                            }
                        }
                    }
                }
                else if(CurrentProfileNode->Next)
                {
                    DrawDebugRow(CurrentProfileNode, Frame);
                    
                    CurrentProfileNode = CurrentProfileNode->Next;
                }
                else
                {
                    DrawDebugRow(CurrentProfileNode, Frame);
                    
                    if(!LastStoredParentNode)
                    {
                        break;
                    }
                    else
                    {
                        while(LastStoredParentNode)
                        {
                            CurrentProfileNode = LastStoredParentNode->Next;
                            
                            debug_profile_node *NextStoredNode = LastStoredParentNode->NextLink;
                            LastStoredParentNode->NextLink = 0;
                            LastStoredParentNode = NextStoredNode;
                            
                            ImGui::TreePop();
                            
                            if(CurrentProfileNode)
                            {
                                break;
                            }
                        }
                    }
                }
            }
            
            ImGui::EndTable();
        }
        
        CurrentProfileGroup = CurrentProfileGroup->Next;
    }
}

internal void
DrawDebugFrameFlattened(debug_frame *Frame, debug_memory *DebugMemory)
{
    debug_profile_node *LastStoredChildNode = 0;
    
    static ImGuiTableFlags flags = ImGuiTableFlags_BordersV | ImGuiTableFlags_BordersOuterH | ImGuiTableFlags_Resizable | ImGuiTableFlags_RowBg | ImGuiTableFlags_NoBordersInBody | ImGuiTableFlags_Sortable;
    
    f32 TEXT_BASE_WIDTH = ImGui::GetFontSize();
    u32 TableColumns = 11;
    if (ImGui::BeginTable("ProfilingTable", TableColumns, flags))
    {
        DrawDebugTableHeader(0);
        
        debug_profile_group *CurrentProfileGroup = Frame->LastProfileGroup;
        while(CurrentProfileGroup)
        {
            debug_profile_node *CurrentProfileNode = CurrentProfileGroup->LastProfileNode;
            while(CurrentProfileNode)
            {
                ImGui::TableNextRow();
                ImGui::TableNextColumn();
                
                if(CurrentProfileNode->LastChildProfileNode)
                {
                    CurrentProfileNode->LastChildProfileNode->NextLink = LastStoredChildNode;
                    LastStoredChildNode = CurrentProfileNode->LastChildProfileNode;
                }
                
                DrawDebugRow(CurrentProfileNode, Frame);
                
                CurrentProfileNode = CurrentProfileNode->Next;
                if(!CurrentProfileNode)
                {
                    CurrentProfileNode = LastStoredChildNode;
                    
                    if(LastStoredChildNode)
                    {
                        LastStoredChildNode = LastStoredChildNode->NextLink;
                        CurrentProfileNode->NextLink = 0;
                    }
                }
            }
            
            CurrentProfileGroup = CurrentProfileGroup->Next;
        }
        
        ImGui::EndTable();
    }
}