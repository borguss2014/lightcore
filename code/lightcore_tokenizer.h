enum token_type
{
    Token_Unknown,
    
    Token_OpenParen,
    Token_CloseParen,
    Token_OpenBrace,
    Token_CloseBrace,
    Token_OpenBracket,
    Token_CloseBracket,
    Token_Colon,
    Token_Semicolon,
    Token_Asterisk,
    Token_Comma,
    Token_Dot,
    Token_Dash,
    Token_Hash,
    Token_OpenAngleBracket,
    Token_CloseAngleBracket,
    Token_Equal,
    Token_ForwardSlash,
    Token_BackwardSlash,
    
    Token_String,
    Token_Identifier,
    Token_SingleLine_Comment,
    Token_MultiLine_Comment,
    Token_Whitespace,
    Token_EndOfLine,
    Token_Number,
    
    Token_EndOfStream
};

struct token
{
    string FileName;
    u32 LineNumber;
    u32 ColumnNumber;
    
    token_type Type;
    string Text;
};

struct tokenizer
{
    string FileName;
    u32 LineNumber;
    u32 ColumnNumber;
    
    string Input;
    
    char At[2];
    b32 Error;
};