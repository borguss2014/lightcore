#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif
#include <Windows.h>
#include "lightcore_platform.h"
#include "lightcore_shared_utils.h"
#include "lightcore_memory.h"

#include "lightcore_debug.h"

#include "lightcore_renderer.h"

#include "lightcore_renderer_opengl.h"
#include "lightcore_renderer_opengl.cpp"

#include "win32_renderer_table.h"

platform_api Platform;

global_variable PFNWGLCHOOSEPIXELFORMATARBPROC wglChoosePixelFormatARB = 0;
global_variable PFNWGLCREATECONTEXTATTRIBSARBPROC wglCreateContextAttribsARB = 0;
global_variable PFNWGLSWAPINTERVALEXTPROC wglSwapIntervalEXT = 0;

internal void 
FatalError(char* message)
{
    MessageBoxA(NULL, message, "Error", MB_ICONEXCLAMATION);
    ExitProcess(0);
}

#ifdef LIGHTCORE_DEBUG
internal void
Win32OpenGLDebugCallback(GLenum source, GLenum type, GLuint id, GLenum severity,
                         GLsizei length, const GLchar* message, const void* user)
{
    open_gl *OpenGL = (open_gl *)user;
    if(OpenGL->PrintDebugMessages)
    {
        OutputDebugStringA(message);
        OutputDebugStringA("\n");
    }
    
    if (severity == GL_DEBUG_SEVERITY_HIGH || severity == GL_DEBUG_SEVERITY_MEDIUM)
    {
        if (IsDebuggerPresent())
        {
            Assert(!"OpenGL error - check the callstack in debugger");
        }
        FatalError("OpenGL API usage error! Use debugger to examine call stack!");
    }
}
#endif

// Retrieves a context for the latest version of OpenGL supported natively by Windows (version 1.1)
internal HGLRC
Win32OpenGLCreateLegacyContext(HDC DeviceContextHandle)
{
    HGLRC RenderContext = 0;
    
    PIXELFORMATDESCRIPTOR DesiredPixelFormat = {};
    DesiredPixelFormat.nSize = sizeof(DesiredPixelFormat);
    DesiredPixelFormat.nVersion = 1;
    DesiredPixelFormat.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
    DesiredPixelFormat.iPixelType = PFD_TYPE_RGBA;
    DesiredPixelFormat.cColorBits = 32;
    DesiredPixelFormat.cAlphaBits = 8;
    DesiredPixelFormat.cDepthBits = 24;
    
    u32 SuggestedPixelFormatID = ChoosePixelFormat(DeviceContextHandle, &DesiredPixelFormat);
    if(SuggestedPixelFormatID)
    {
        PIXELFORMATDESCRIPTOR SuggestedPixelFormat = {};
        DescribePixelFormat(DeviceContextHandle, SuggestedPixelFormatID, sizeof(SuggestedPixelFormat), &SuggestedPixelFormat);
        if(SetPixelFormat(DeviceContextHandle, SuggestedPixelFormatID, &SuggestedPixelFormat))
        {
            RenderContext = wglCreateContext(DeviceContextHandle);
            if(!RenderContext)
            {
                // LOGGING HERE
                OutputDebugStringA("SetupOpenGLContext wglCreateContext() failed \n");
                RenderContext = 0;
            }
        }
        else
        {
            // LOGGING HERE
            OutputDebugStringA("SetupOpenGLContext SetPixelFormat() failed \n");
        }
    }
    else
    {
        // LOGGING HERE
        OutputDebugStringA("SetupOpenGLContext ChoosePixelFormat() failed \n");
    }
    
    return(RenderContext);
}

internal HGLRC
Win32OpenglCreateContext(HDC DeviceContextHandle)
{
    HGLRC Result = {};
    
    WNDCLASS WindowClass = {};
    WindowClass.lpszClassName = "LightcoreGLLoader";
    WindowClass.lpfnWndProc = DefWindowProcA;
    
    if(RegisterClassA(&WindowClass))
    {
        HWND TempWindowHandle =  CreateWindowExA(0,
                                                 WindowClass.lpszClassName, "Lightcore", WS_OVERLAPPEDWINDOW,
                                                 CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT,
                                                 0, 0, 0, 0);
        
        HDC TempDeviceContextHandle = GetDC(TempWindowHandle);
        HGLRC OpenGLLegacyRenderContext = Win32OpenGLCreateLegacyContext(TempDeviceContextHandle);
        if(OpenGLLegacyRenderContext)
        {
            if (!wglMakeCurrent(TempDeviceContextHandle, OpenGLLegacyRenderContext))
            {
                // LOGGING HERE
                OutputDebugStringA("wglMakeCurrent(OpenGLOldRenderContext) failed \n");
            }
            else
            {
                PFNWGLGETEXTENSIONSSTRINGARBPROC wglGetExtensionsStringARB =
                (PFNWGLGETEXTENSIONSSTRINGARBPROC)wglGetProcAddress("wglGetExtensionsStringARB");
                if(!wglGetExtensionsStringARB)
                {
                    FatalError("wglGetExtensionsStringARB failed! Cannot acquire OpenGL extensions list");
                }
                else
                {
                    const char *Extensions = wglGetExtensionsStringARB(TempDeviceContextHandle);
                    const char *Start = Extensions;
                    
                    for(;;)
                    {
                        while(*Extensions != 0 && *Extensions != ' ')
                        {
                            Extensions++;
                        }
                        
                        mmsz Length = Extensions - Start;
                        
                        string Extension = {};
                        Extension.Data = (char *)Start;
                        Extension.Count = Length;
                        
                        if(StringsEqual(Extension, "WGL_ARB_pixel_format"))
                        {
                            // https://www.khronos.org/registry/OpenGL/extensions/ARB/WGL_ARB_pixel_format.txt
                            wglChoosePixelFormatARB = (PFNWGLCHOOSEPIXELFORMATARBPROC)wglGetProcAddress("wglChoosePixelFormatARB");
                        }
                        else if(StringsEqual(Extension, "WGL_ARB_create_context"))
                        {
                            // https://www.khronos.org/registry/OpenGL/extensions/ARB/WGL_ARB_create_context.txt
                            wglCreateContextAttribsARB = (PFNWGLCREATECONTEXTATTRIBSARBPROC)wglGetProcAddress("wglCreateContextAttribsARB");
                        }
                        else if(StringsEqual(Extension, "WGL_EXT_swap_control"))
                        {
                            // https://www.khronos.org/registry/OpenGL/extensions/EXT/WGL_EXT_swap_control.txt
                            wglSwapIntervalEXT = (PFNWGLSWAPINTERVALEXTPROC)wglGetProcAddress("wglSwapIntervalEXT");
                        }
                        
                        if(*Extensions == 0)
                        {
                            break;
                        }
                        
                        // wglGetExtensionsStringARB extensions have only one space between them
                        Extensions++;
                        
                        Start = Extensions;
                    }
                    
                    if(!wglChoosePixelFormatARB || !wglCreateContextAttribsARB || !wglSwapIntervalEXT)
                    {
                        FatalError("OpenGL extensions required for a modern context are not supported!");
                    }
                    else
                    {
                        wglMakeCurrent(0, 0);
                        
                        s32 DesiredPixelAttribs[] = {
                            WGL_DRAW_TO_WINDOW_ARB, GL_TRUE,
                            WGL_SUPPORT_OPENGL_ARB, GL_TRUE,
                            WGL_DOUBLE_BUFFER_ARB, GL_TRUE,
                            WGL_PIXEL_TYPE_ARB, WGL_TYPE_RGBA_ARB,
                            WGL_ACCELERATION_ARB, WGL_FULL_ACCELERATION_ARB,
                            WGL_COLOR_BITS_ARB, 32,
                            WGL_ALPHA_BITS_ARB, 8,
                            WGL_DEPTH_BITS_ARB, 24,
                            WGL_STENCIL_BITS_ARB, 8,
                            WGL_SAMPLE_BUFFERS_ARB, GL_TRUE,
                            WGL_SAMPLES_ARB, 4,
                            0
                        };
                        
                        s32 SuggestedPixelFormatID;
                        u32 NumFormats;
                        BOOL PixelFormatChosen = wglChoosePixelFormatARB(DeviceContextHandle, DesiredPixelAttribs, 0, 1, &SuggestedPixelFormatID, &NumFormats);
                        if(PixelFormatChosen)
                        {
                            PIXELFORMATDESCRIPTOR SuggestedPixelFormat = {};
                            DescribePixelFormat(DeviceContextHandle, SuggestedPixelFormatID, sizeof(SuggestedPixelFormat), &SuggestedPixelFormat);
                            
                            if(SetPixelFormat(DeviceContextHandle, SuggestedPixelFormatID, &SuggestedPixelFormat))
                            {
                                u32 OpenGLMajorMinVersion = 4;
                                u32 OpenGLMinorMinVersion = 6;
                                s32 ContextAttribs[] = 
                                {
                                    WGL_CONTEXT_MAJOR_VERSION_ARB, (s32)OpenGLMajorMinVersion,
                                    WGL_CONTEXT_MINOR_VERSION_ARB, (s32)OpenGLMinorMinVersion,
                                    WGL_CONTEXT_PROFILE_MASK_ARB, WGL_CONTEXT_CORE_PROFILE_BIT_ARB,
#ifdef LIGHTCORE_DEBUG
                                    WGL_CONTEXT_FLAGS_ARB, WGL_CONTEXT_DEBUG_BIT_ARB,
#endif
                                    0
                                };
                                
                                Result = wglCreateContextAttribsARB(DeviceContextHandle, 0, ContextAttribs);
                                if (!Result)
                                {
                                    OutputDebugStringA("wglCreateContextAttribsARB() failed \n");
                                }
                            }
                            else
                            {
                                OutputDebugStringA("SetPixelFormat() failed\n");
                            }
                        }
                        else if (!PixelFormatChosen || NumFormats == 0) 
                        {
                            OutputDebugStringA("wglChoosePixelFormatARB() failed \n");
                        }
                    }
                }
            }
        }
        else
        {
            FatalError("Failed to retrieve legacy OpenGL context");
        }
        
        wglDeleteContext(OpenGLLegacyRenderContext);
        ReleaseDC(TempWindowHandle, TempDeviceContextHandle);
        DestroyWindow(TempWindowHandle);
    }
    else
    {
        FatalError("RegisterClassA() Cannot register class 'LightcoreGLLoader'");
    }
    
    return(Result);
}

internal void *
Win32RendererAlloc(mmsz Size)
{
    void *Result =  VirtualAlloc(0, Size, MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
    return(Result);
}

internal void
PlatformSetVSync(b32 EnableVSync)
{
    wglSwapIntervalEXT(EnableVSync ? 1 : 0);
    
    EnableVSync ? OutputDebugStringA("VSync enabled \n") : OutputDebugStringA("VSync disabled \n");
}

internal shader_code
GetShaderCode(char *FileSource, mmsz Size)
{
    shader_code Result = {};
    
    char *At = FileSource;
    char *Start = At;
    char *End = &FileSource[Size];
    
    for(;;)
    {
        if(At == End)
        {
            break;
        }
        
        SkipToChar('@', &At, End);
        Start = At;
        
        while(*At != ' ' &&
              *At != '\n' &&
              *At != '\r')
        {
            At++;
        }
        
        string Identifier = {};
        Identifier.Data = Start;
        Identifier.Count = At - Start;
        
        if(StringsEqual(Identifier, "@VERTEX_SHADER"))
        {
            SkipWhitespace(&At);
            Start = At;
            
            SkipToChar('@', &At, End);
            
            Result.VertexCode.Data = Start;
            Result.VertexCode.Count = At - Start;
        }
        else if(StringsEqual(Identifier, "@FRAGMENT_SHADER"))
        {
            SkipWhitespace(&At);
            Start = At;
            
            SkipToChar('@', &At, End);
            
            Result.FragmentCode.Data = Start;
            Result.FragmentCode.Count = At - Start;
        }
        else
        {
            At++;
        }
    }
    return(Result);
}

internal opengl_program *
PushProgram(open_gl *OpenGL, memory_arena *Arena, char *ShaderPath)
{
    Assert(OpenGL->NextProgramID < ArrayCount(OpenGL->Programs));
    opengl_program *Program = &OpenGL->Programs[OpenGL->NextProgramID++];
    
    platform_file_info FileInfo = Platform.GetFileInfo(ShaderPath);
    
    temporary_memory TempMem = BeginTemporaryMemory(Arena);
    
    char *FileData = (char *)PushSlice(Arena, FileInfo.Size);
    if(FileData)
    {
        platform_file_handle FileHandle = Platform.OpenFile(ShaderPath, OpenFile_Read);
        if(FileHandle.NoErrors)
        {
            Platform.ReadFile(&FileHandle, FileData, FileInfo.Size, 0);
            if(FileHandle.NoErrors)
            {
                shader_code ShaderCode = GetShaderCode(FileData, FileInfo.Size);
                OpenGLCreateProgram(OpenGL, Program, ShaderCode);
            }
            
            Platform.CloseFile(&FileHandle);
        }
    }
    
    EndTemporaryMemory(TempMem);
    
    Program->ShaderInfo.FileInfo = FileInfo;
    CopyString(Program->ShaderInfo.Path, StringLength(ShaderPath), ShaderPath);
    
    return(Program);
}

internal void
UpdatePrograms(open_gl *OpenGL, memory_arena *Arena)
{
    for(u32 ProgramID = 0;
        ProgramID < OpenGL->NextProgramID;
        ++ProgramID)
    {
        opengl_program *Program = &OpenGL->Programs[ProgramID];
        
        platform_file_info CurrentFileInfo = Platform.GetFileInfo(Program->ShaderInfo.Path);
        if(CurrentFileInfo.LastWriteTime != Program->ShaderInfo.FileInfo.LastWriteTime)
        {
            platform_file_handle FileHandle = Platform.OpenFile(Program->ShaderInfo.Path, OpenFile_Read);
            if(FileHandle.NoErrors)
            {
                temporary_memory TempMem = BeginTemporaryMemory(Arena);
                
                char *FileData = (char *)PushSlice(Arena, CurrentFileInfo.Size);
                if(FileData)
                {
                    u64 Offset = 0;
                    Platform.ReadFile(&FileHandle, FileData, CurrentFileInfo.Size, Offset);
                    if(FileHandle.NoErrors)
                    {
                        if(Program->Handle)
                        {
                            OPENGL_API.glDeleteProgram(Program->Handle);
                        }
                        
                        shader_code ShaderCode = GetShaderCode(FileData, CurrentFileInfo.Size);
                        OpenGLCreateProgram(OpenGL, Program, ShaderCode);
                        Program->ShaderInfo.FileInfo = CurrentFileInfo;
                        
                        OutputDebugStringA("Shader reload!\n");
                    }
                    
                    Platform.CloseFile(&FileHandle);
                }
                
                EndTemporaryMemory(TempMem);
            }
        }
    }
}

internal open_gl *
Win32InitOpenGL(HDC DeviceContextHandle, memory_arena *Arena)
{
    open_gl *OpenGL = PushStruct(Arena, open_gl);
    
    HGLRC OpenGLRenderContext = Win32OpenglCreateContext(DeviceContextHandle);
    if(!wglMakeCurrent(DeviceContextHandle, OpenGLRenderContext))
    {
        FatalError("wglMakeCurrent() Failed to set the render context! \n");
    }
    else
    {
        if(OpenGLLoadFunctionPointers(&OpenGL->OpenGLAPI))
        {
#ifdef LIGHTCORE_DEBUG
            // enable debug callback
            OpenGL->PrintDebugMessages = true;
            OpenGL->OpenGLAPI.glDebugMessageCallback(&Win32OpenGLDebugCallback, OpenGL);
            OpenGL->OpenGLAPI.glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
#endif
            OpenGL->OpenGLAPI.glEnable(GL_DEPTH_TEST);
            /*OpenGL->OpenGLAPI.glEnable(GL_CULL_FACE);
            OpenGL->OpenGLAPI.glCullFace(GL_BACK);*/
            
            /*OpenGL->OpenGLAPI.glEnable(GL_CULL_FACE);
            OpenGL->OpenGLAPI.glCullFace(GL_BACK);
            OpenGL->OpenGLAPI.glFrontFace(GL_CCW);*/
            
            OpenGL->ForwardPassLightingProgram = PushProgram(OpenGL, Arena, "../code/shaders/ForwardPass.glsl");
            OpenGL->PostprocessPassProgram = PushProgram(OpenGL, Arena, "../code/shaders/Postprocess.glsl");
            
            OpenGL->NextBufferID = 1;
            OpenGL->NextTextureID = 1;
            
            if(!OpenGL->CameraTransformsHandle)
            {
                OPENGL_API.glCreateBuffers(1, &OpenGL->CameraTransformsHandle);
                OPENGL_API.glNamedBufferStorage(OpenGL->CameraTransformsHandle, sizeof(mat4x4) * 3, 0, GL_DYNAMIC_STORAGE_BIT);
                
                u32 BindingPoint = 0;
                OPENGL_API.glBindBufferBase(GL_UNIFORM_BUFFER, BindingPoint, OpenGL->CameraTransformsHandle);
            }
            
            if(!OpenGL->PostprocessHandle)
            {
                textured_vertex QuadData[] = 
                {
                    { -1.0f, 1.0f, 0.0f,   0.0f, 0.0f, 0.0f,   0.0f, 1.0f,  0.0f, 0.0f, 0.0f },
                    { -1.0f,-1.0f, 0.0f,   0.0f, 0.0f, 0.0f,   0.0f, 0.0f,  0.0f, 0.0f, 0.0f },
                    {  1.0f,-1.0f, 0.0f,   0.0f, 0.0f, 0.0f,   1.0f, 0.0f,  0.0f, 0.0f, 0.0f },
                    
                    { -1.0f, 1.0f, 0.0f,   0.0f, 0.0f, 0.0f,   0.0f, 1.0f,  0.0f, 0.0f, 0.0f },
                    {  1.0f,-1.0f, 0.0f,   0.0f, 0.0f, 0.0f,   1.0f, 0.0f,  0.0f, 0.0f, 0.0f },
                    {  1.0f, 1.0f, 0.0f,   0.0f, 0.0f, 0.0f,   1.0f, 1.0f,  0.0f, 0.0f, 0.0f },
                };
                
                OPENGL_API.glCreateBuffers(1, &OpenGL->PostprocessHandle);
                OPENGL_API.glNamedBufferStorage(OpenGL->PostprocessHandle, sizeof(QuadData), 0, GL_DYNAMIC_STORAGE_BIT);
                
                OPENGL_API.glNamedBufferSubData(OpenGL->PostprocessHandle, 0, sizeof(QuadData), QuadData);
            }
            
            OpenGL->RenderOutputWidth = 3840;
            OpenGL->RenderOutputHeight = 2160;
            OpenGL->RenderOutputScale = 100;
            
            PlatformSetVSync(OpenGL->Settings.EnableVSync);
        }
        else
        {
            // LOG HERE
            OutputDebugStringA("OpenGL function pointers not loaded!\n");
        }
    }
    return(OpenGL);
}

extern "C"
WIN32_LOAD_RENDERER(Win32LoadRenderer)
{
    Platform = PlatformAPI;
    
    platform_renderer *Renderer = (platform_renderer *)Win32InitOpenGL(DeviceContextHandle, Arena);
    Renderer->RendererArena = Arena;
    return(Renderer);
}

extern "C"
RENDERER_BEGIN_FRAME(Win32BeginFrame)
{
    open_gl *OpenGL = (open_gl *)Renderer;
    OpenGL->PrintDebugMessages = true;
    
    UpdatePrograms(OpenGL, Renderer->RendererArena);
    
    game_render_commands *Frame = OpenGLBeginFrame(OpenGL);
    return(Frame);
}

extern "C"
RENDERER_END_FRAME(Win32EndFrame)
{
    open_gl *OpenGL = (open_gl *)Renderer;
    
    OpenGLEndFrame(OpenGL, Frame);
    //SwapBuffers(wglGetCurrentDC());
    
    OpenGL->PrintDebugMessages = false;
}