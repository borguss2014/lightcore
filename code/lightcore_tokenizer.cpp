internal void
Refill(tokenizer *Tokenizer)
{
    if(Tokenizer->Input.Count == 0)
    {
        Tokenizer->At[0] = 0;
        Tokenizer->At[1] = 0;
    }
    else if(Tokenizer->Input.Count == 1)
    {
        Tokenizer->At[0] = Tokenizer->Input.Data[0];
        Tokenizer->At[1] = 0;
    }
    else
    {
        Tokenizer->At[0] = Tokenizer->Input.Data[0];
        Tokenizer->At[1] = Tokenizer->Input.Data[1];
    }
}

internal void
AdvanceChars(tokenizer *Tokenizer, u32 Steps)
{
    Tokenizer->ColumnNumber += Steps;
    
    if(Tokenizer->Input.Count >= Steps)
    {
        Tokenizer->Input.Data += Steps;
        Tokenizer->Input.Count -= Steps;
    }
    else
    {
        Tokenizer->Input.Data += Tokenizer->Input.Count;
        Tokenizer->Input.Count = 0;
    }
    
    Refill(Tokenizer);
}

internal void
SkipLine(tokenizer *Tokenizer)
{
    while(!IsEndOfLine(Tokenizer->At[0]))
    {
        AdvanceChars(Tokenizer, 1);
    }
    
    ++Tokenizer->LineNumber;
}

internal void
Error(tokenizer *Tokenizer, token OnToken, char *Format, ...)
{
    va_list ArgsList;
    va_start(ArgsList, Format);
    
    u32 BufferSize = vsnprintf(0, 0, Format, ArgsList);
    ++BufferSize;
    
    char *Buffer = (char *)malloc(BufferSize);
    if(Buffer)
    {
        vsprintf_s(Buffer, BufferSize, Format, ArgsList);
        fprintf(stderr, "ERROR: %s (line: %d, column: %d) -> ' %.*s ' in %.*s\n\n",
                Buffer,
                OnToken.LineNumber, OnToken.ColumnNumber, 
                (u32)OnToken.Text.Count, OnToken.Text.Data,
                (u32)OnToken.FileName.Count, OnToken.FileName.Data);
    }
    
    va_end(ArgsList);
    free(Buffer);
    
    Tokenizer->Error = true;
}

internal void
Warning(tokenizer *Tokenizer, token OnToken, char *Format, ...)
{
    va_list ArgsList;
    va_start(ArgsList, Format);
    
    u32 BufferSize = vsnprintf(0, 0, Format, ArgsList);
    ++BufferSize;
    
    char *Buffer = (char *)malloc(BufferSize);
    if(Buffer)
    {
        vsprintf_s(Buffer, BufferSize, Format, ArgsList);
        fprintf(stderr, "WARNING: %s (line: %d, column: %d) -> ' %.*s ' in %.*s\n\n",
                Buffer,
                OnToken.LineNumber, OnToken.ColumnNumber, 
                (u32)OnToken.Text.Count, OnToken.Text.Data,
                (u32)OnToken.FileName.Count, OnToken.FileName.Data);
    }
    
    va_end(ArgsList);
    free(Buffer);
}

internal token
GetTokenRaw(tokenizer *Tokenizer)
{
    char CurrentChar = Tokenizer->At[0];
    
    token Result = {};
    Result.FileName = Tokenizer->FileName;
    Result.LineNumber = Tokenizer->LineNumber;
    Result.ColumnNumber = Tokenizer->ColumnNumber;
    Result.Text = Tokenizer->Input;
    Result.Text.Count = 1;
    
    AdvanceChars(Tokenizer, 1);
    
    switch(CurrentChar)
    {
        case '\0': {Result.Type = Token_EndOfStream;} break;
        
        case '(': {Result.Type = Token_OpenParen;} break;
        case ')': {Result.Type = Token_CloseParen;} break;
        case '{': {Result.Type = Token_OpenBrace;} break;
        case '}': {Result.Type = Token_CloseBrace;} break;
        case '[': {Result.Type = Token_OpenBracket;} break;
        case ']': {Result.Type = Token_CloseBracket;} break;
        case ':': {Result.Type = Token_Colon;} break;
        case ';': {Result.Type = Token_Semicolon;} break;
        case '*': {Result.Type = Token_Asterisk;} break;
        case ',': {Result.Type = Token_Comma;} break;
        case '.': {Result.Type = Token_Dot;} break;
        case '-': {Result.Type = Token_Dash;} break;
        case '#': {Result.Type = Token_Hash;} break;
        case '<': {Result.Type = Token_OpenAngleBracket;} break;
        case '>': {Result.Type = Token_CloseAngleBracket;} break;
        case '=': {Result.Type = Token_Equal;} break;
        case '\\': {Result.Type = Token_BackwardSlash;} break;
        
        case '"':
        {
            Result.Type = Token_String;
            Result.Text = Tokenizer->Input;
            
            while(Tokenizer->At[0] && 
                  Tokenizer->At[0] != '"')
            {
                if(Tokenizer->At[0] == '\\' &&
                   Tokenizer->At[1])
                {
                    AdvanceChars(Tokenizer, 1);
                }
                AdvanceChars(Tokenizer, 1);
            }
            
            Result.Text.Count = Tokenizer->Input.Data - Result.Text.Data;
            
            if(Tokenizer->At[0] == '"')
            {
                AdvanceChars(Tokenizer, 1);
            }
        } break;
        
        default:
        {   
            if(IsWhitespace(CurrentChar))
            {
                Result.Type = Token_Whitespace;
                
                while(IsWhitespace(Tokenizer->At[0]))
                {
                    AdvanceChars(Tokenizer, 1);
                }
            }
            else if(IsEndOfLine(CurrentChar))
            {
                Result.Type = Token_EndOfLine;
                
                if(((CurrentChar == '\n') && (Tokenizer->At[0] == '\r')) ||
                   ((CurrentChar == '\r') && (Tokenizer->At[0] == '\n')))
                {
                    AdvanceChars(Tokenizer, 1);
                }
                
                Tokenizer->ColumnNumber = 1;
                ++Tokenizer->LineNumber;
            }
            // C Style comments
            else if(CurrentChar == '/' && 
                    Tokenizer->At[0] == '/')
            {
                Result.Type = Token_SingleLine_Comment;
                
                AdvanceChars(Tokenizer, 2);
                while(Tokenizer->At[0] && !IsEndOfLine(Tokenizer->At[0]))
                {
                    AdvanceChars(Tokenizer, 1);
                }
            }
            // C++ Style comments
            else if(CurrentChar == '/' &&
                    Tokenizer->At[0] == '*')
            {
                Result.Type = Token_MultiLine_Comment;
                
                AdvanceChars(Tokenizer, 2);
                while(Tokenizer->At[0] && 
                      !(Tokenizer->At[0] == '*' &&
                        Tokenizer->At[1] == '/'))
                {
                    if(IsEndOfLine(Tokenizer->At[0]))
                    {
                        ++Tokenizer->LineNumber;
                    }
                    
                    AdvanceChars(Tokenizer, 1);
                }
                
                if(Tokenizer->At[0] == '*')
                {
                    AdvanceChars(Tokenizer, 2);
                }
            }
            else if(IsAlpha(CurrentChar) || CurrentChar == '_')
            {
                Result.Type = Token_Identifier;
                
                while(IsAlpha(Tokenizer->At[0]) ||
                      IsNumber(Tokenizer->At[0]) ||
                      Tokenizer->At[0] == '_')
                {
                    AdvanceChars(Tokenizer, 1);
                }
                
                Result.Text.Count = Tokenizer->Input.Data - Result.Text.Data;
            }
#if 1
            else if(IsNumber(CurrentChar))
            {
                // TODO(Cristian): Multiple cases: integer, 0.10, 1.54f, 0x54
                Result.Type = Token_Number;
                
                while(IsNumber(Tokenizer->At[0]))
                {
                    AdvanceChars(Tokenizer, 1);
                }
                
                Result.Text.Count = Tokenizer->Input.Data - Result.Text.Data;
            }
#endif
            else
            {
                Result.Type = Token_Unknown;
            }
        } break;
    }
    
    return(Result);
}

internal token
GetToken(tokenizer *Tokenizer)
{
    token Token = {};
    for(;;)
    {
        Token = GetTokenRaw(Tokenizer);
        if(Token.Type == Token_EndOfLine || 
           Token.Type == Token_Whitespace || 
           Token.Type == Token_SingleLine_Comment || 
           Token.Type == Token_MultiLine_Comment)
        {
            // IGNORE
        }
        else
        {
            break;
        }
    }
    
    return(Token);
}

internal string
GetTokenTypeName(token_type Type)
{
    string Result = {};
    switch(Type)
    {
        case Token_OpenParen: { Result = BundleZ("open parentheses"); } break;
        case Token_CloseParen: { Result = BundleZ("close parentheses"); } break;
        case Token_OpenBrace: { Result = BundleZ("open brace"); } break;
        case Token_CloseBrace: { Result = BundleZ("close brace"); } break;
        case Token_OpenBracket: { Result = BundleZ("open bracket"); } break;
        case Token_CloseBracket: { Result = BundleZ("close bracket"); } break;
        case Token_Colon: { Result = BundleZ("colon"); } break;
        case Token_Semicolon: { Result = BundleZ("semicolon"); } break;
        case Token_Asterisk: { Result = BundleZ("asterisk"); } break;
        case Token_Comma: { Result = BundleZ("comma"); } break;
        case Token_Dot: { Result = BundleZ("dot"); } break;
        case Token_Dash: { Result = BundleZ("dash"); } break;
        case Token_Hash: { Result = BundleZ("hash/pound"); } break;
        case Token_OpenAngleBracket: { Result = BundleZ("open angle bracket"); } break;
        case Token_CloseAngleBracket: { Result = BundleZ("close angle bracket"); } break;
        case Token_String: { Result = BundleZ("string"); } break;
        case Token_Identifier: { Result = BundleZ("identifier"); } break;
        case Token_EndOfStream: { Result = BundleZ("end of stream"); } break;
        case Token_SingleLine_Comment: { Result = BundleZ("single line comment"); } break;
        case Token_MultiLine_Comment: { Result = BundleZ("multi line comment"); } break;
        case Token_Number: { Result = BundleZ("number"); } break;
        
        case Token_Whitespace: { Result = BundleZ("whitespace"); } break;
        case Token_EndOfLine: { Result = BundleZ("end of line"); } break;
        
        default:
        {
            Result = BundleZ("unknown");
        }
    }
    
    return(Result);
}

internal b32
TokenEquals(token Token, char *Match)
{
    b32 Result = StringsEqual(Token.Text.Count, Token.Text.Data, Match);
    return(Result);
}

internal token
PeekToken(tokenizer *Tokenizer)
{
    tokenizer Temp = *Tokenizer;
    token Result = GetToken(&Temp);
    return(Result);
}

internal b32
OptionalToken(tokenizer *Tokenizer, token_type MatchType)
{
    b32 Result = false;
    
    token Token = PeekToken(Tokenizer);
    if(Token.Type == MatchType)
    {
        Result = true;
        GetToken(Tokenizer);
    }
    
    return(Result);
}

internal token
RequireToken(tokenizer *Tokenizer, token_type MatchType)
{
    token Token = GetToken(Tokenizer);
    if(Token.Type != MatchType)
    {
        string TokenTypeName = GetTokenTypeName(Token.Type);
        string MatchTypeName = GetTokenTypeName(MatchType);
        
        Error(Tokenizer, Token, "Required type is '%.*s' instead of '%.*s'",
              MatchTypeName.Count, MatchTypeName.Data,
              TokenTypeName.Count, TokenTypeName.Data);
    }
    
    return(Token);
}

internal tokenizer
Tokenize(string Input, string FileName)
{
    tokenizer Tokenizer = {};
    Tokenizer.Input = Input;
    Tokenizer.FileName = FileName;
    Tokenizer.LineNumber = 1;
    Tokenizer.ColumnNumber = 1;
    Refill(&Tokenizer);
    return(Tokenizer);
}