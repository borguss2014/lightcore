#include "win32_opengl_defines_generated.h"

#include "lightcore_math.h"

struct shader_code
{
    string VertexCode;
    string FragmentCode;
};

struct shader_file_info
{
    char Path[1024];
    platform_file_info FileInfo;
};

struct uniform_data
{
    GLint UniformLocation;
    string UniformName;
};

struct opengl_program
{
    GLuint Handle;
    
    GLuint VertexArrayID;
    GLuint CameraTransformsID;
    
    GLint PositionAttribID;
    GLint NormalAttribID;
    GLint UVAttribID;
    GLint ColorAttribID;
    GLint TangentAttribID;
    GLint BitangentAttribID;
    
    shader_file_info ShaderInfo;
    
    uniform_data *Uniforms;
    u32 UniformsCount;
};

struct opengl_rendertarget_info
{
    GLuint RenderTargetHandle;
    GLuint DepthBufferHandle;
    GLuint OutputTextureHandle;
};

#define OPENGL_API OpenGL->OpenGLAPI

struct open_gl
{
    platform_renderer Header;
    
    opengl_program Programs[1024];
    u32 NextProgramID;
    
    GLuint BufferObjects[1024];
    u32 NextBufferID;
    
    GLuint TextureHandles[1024];
    u32 NextTextureID;
    
    u8 PushBufferMemory[262144];
    game_render_commands RenderCommands;
    opengl_fn_table OpenGLAPI;
    
    GLuint CameraTransformsHandle;
    
    v4 BufferClearColor;
    
    u32 WindowDrawRegionWidth;
    u32 WindowDrawRegionHeight;
    
    u32 RenderOutputWidth;
    u32 RenderOutputHeight;
    u32 RenderOutputScale;
    
    render_setup Settings;
    
    opengl_program *ForwardPassLightingProgram;
    opengl_rendertarget_info ForwardPassRenderTarget;
    
    opengl_program *PostprocessPassProgram;
    GLuint PostprocessHandle;
    
    GLuint RenderPassInterAttachment;
    
    opengl_program *CurrentProgram;
    
#ifdef LIGHTCORE_DEBUG
    b32 PrintDebugMessages;
#endif
};

internal void PlatformSetVSync(b32 EnableVSync);