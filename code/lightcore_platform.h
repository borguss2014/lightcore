#include "lightcore_types.h"

#if LIGHTCORE_DEBUG

#ifndef AssertMsg
#define AssertMsg(Cond, Msg, ...) do { \
if (!(Cond)) { \
DebugTrap(); \
} \
} while (0)
#endif

#ifndef Assert
#define Assert(Cond) AssertMsg(Cond, 0)
#endif

#if defined(_MSC_VER)
#if _MSC_VER < 1300
#define DebugTrap() __asm int 3 /* Trap to debugger! */
#else
#define DebugTrap() __debugbreak()
#endif
#else
#define DebugTrap() __builtin_trap()
#endif

#else
#define DebugTrap()
#define Assert(Cond)
#define AssertMsg(Cond, Msg, ...)

#endif



// Compile time assert
// Ex: Useful for checking if certain data structures sizes have been changed
#if 0
#define CTAssert3(Expr, Line) struct CTAssert____##Line { u32 Foo[(Expr) ? 1 : -1]; };
#define CTAssert2(Expr, Line) CTAssert3(Expr, Line)
#define CTAssert(Expr) CTAssert2(Expr, __LINE__)
#else
#define CTAssert(Expr) static_assert(Expr, #Expr);
#endif

#if LIGHTCORE_DEBUG
#define InvalidCodePath Assert(!"InvalidCodePath")
#else
#define InvalidCodePath
#endif

#define MemberSize(type, member) sizeof(((type *)0)->member)
#define MemberNumberOfComponents(struct_type, struct_member, component_type) MemberSize(struct_type, struct_member) / sizeof(component_type)

struct lc_array_header
{
    u32 Count;
    u32 Capacity;
};

#define Cast(__Type_) (__Type_)

#define LC_Array(__Type_) __Type_ *

#define LC_GetArrayHeader(__Arr_)    (Cast(lc_array_header *)(__Arr_) - 1)
#define LC_ArrayCount(__Arr_)     (LC_GetArrayHeader(__Arr_)->Count)
#define LC_ArrayCapacity(__Arr_)  (LC_GetArrayHeader(__Arr_)->Capacity)

#define LC_ArrayInitDefault(__Arr_, __Capacity_) do \
{ \
void **LC_Array_ = Cast(void **)&(__Arr_); \
lc_array_header *LC_ArrayHeader = Cast(lc_array_header *)malloc(sizeof(lc_array_header) + sizeof(*(__Arr_)) * (__Capacity_)); \
LC_ArrayHeader->Count = 0; \
LC_ArrayHeader->Capacity = __Capacity_; \
*LC_Array_ = Cast(void *)(LC_ArrayHeader + 1); \
} while (0)

#define LC_ArrayInit(__Arr_, __Arena_, __Capacity_) do \
{ \
void **LC_Array_ = Cast(void **)&(__Arr_); \
lc_array_header *LC_ArrayHeader = Cast(lc_array_header *) PushSize_(__Arena_, sizeof(lc_array_header) + sizeof(*(__Arr_)) * (__Capacity_)); \
LC_ArrayHeader->Count = 0; \
LC_ArrayHeader->Capacity = __Capacity_; \
*LC_Array_ = Cast(void *)(LC_ArrayHeader + 1); \
} while (0)

#define LC_ArrayAppend(__Arr_, __Item_) do \
{ \
Assert(LC_ArrayCapacity(__Arr_) > LC_ArrayCount(__Arr_)); \
(__Arr_)[LC_ArrayCount(__Arr_)++] = (__Item_); \
} while (0)

#define LC_ArrayAppendv(__Arr_, __Items_, __ItemCount_) do \
{ \
lc_array_header *LC_ArrayHeader = LC_GetArrayHeader(__Arr_); \
Assert(sizeof((__Items_)[0]) == sizeof((__Arr_)[0])); \
Assert(LC_ArrayHeader->Capacity >= LC_ArrayHeader->Count + (__ItemCount_)); \
memcpy(&(__Arr_)[LC_ArrayHeader->Count], (__Items_), sizeof((__Arr_)[0])*(__ItemCount_));\
LC_ArrayHeader->Count += (__ItemCount_); \
} while (0)


#define LC_ArrayPop(__Arr_) do \
{ \
Assert(LC_GetArrayHeader(__Arr_)->Count > 0); \
LC_GetArrayHeader(__Arr_)->Count--; \
} while (0)

#define LC_ArrayClear(__Arr_) do { LC_GetArrayHeader(__Arr_)->Count = 0; } while (0)
#define LC_ArrayDelete(__Index_, __Arr_) do \
{ \
Assert(LC_GetArrayHeader(__Arr_)->Count > 0); \
Assert((__Index_ >= 0) && (__Index_ < LC_ArrayCount(__Arr_))); \
if(__Index_ != LC_ArrayCount(__Arr_) - 1) \
{ \
(__Arr_)[__Index_] = (__Arr_)[LC_ArrayCount(__Arr_) - 1]; \
} \
LC_GetArrayHeader(__Arr_)->Count--; \
} while (0)

#define ArrayCount(Array) (sizeof(Array) / sizeof((Array)[0]))

#define introspect(vars)


struct platform_job_queue;
struct memory_arena;
#define JOB_QUEUE_ENTRY_CALLBACK(name) void name(void *Data, u32 ThreadIndex, memory_arena *ThreadLocalStorage)
typedef JOB_QUEUE_ENTRY_CALLBACK(platform_job_queue_callback);

typedef void platform_job_queue_add_entry(platform_job_queue *Queue, platform_job_queue_callback *Callback, void *Data, u32 ThreadIndex);
typedef void platform_job_queue_complete_all_work(platform_job_queue *Queue, u32 ThreadIndex, memory_arena *ThreadLocalStorage);

enum platform_memory_block_flags
{
    PlatformFlags_UnderflowCheck = 0x01,
    PlatformFlags_OverflowCheck  = 0x02
};

struct platform_memory_block
{
	u64 Flags;
    ump Base;
    mmsz Size;
    mmsz Used;
    platform_memory_block *PrevBlock;
};

struct platform_file_handle
{
    void *PlatformHandle;
    b32 NoErrors;
};

enum platform_file_open_flags
{
    OpenFile_Read  = 0x1,
    OpenFile_Write = 0x2
};

struct platform_file_info
{
    mmsz Size;
    u64 CreationTime;
    u64 LastAccessTime;
    u64 LastWriteTime;
};

#define PLATFORM_ALLOCATE_MEMORY(name) platform_memory_block *name(memory_size Size, u32 AllocationFlags)
typedef PLATFORM_ALLOCATE_MEMORY(platform_allocate_memory);

#define PLATFORM_DEALLOCATE_MEMORY(name) void name(platform_memory_block *PlatformBlock)
typedef PLATFORM_DEALLOCATE_MEMORY(platform_deallocate_memory);

struct ring_buffer;
#define PLATFORM_ALLOCATE_RINGBUFFER_MEMORY(name) ring_buffer name(memory_size RequestedSize)
typedef PLATFORM_ALLOCATE_RINGBUFFER_MEMORY(platform_allocate_ringbuffer_memory);

#define PLATFORM_DEALLOCATE_RINGBUFFER_MEMORY(name) void name(ring_buffer *RingBuffer)
typedef PLATFORM_DEALLOCATE_RINGBUFFER_MEMORY(platform_deallocate_ringbuffer_memory);

struct pool_allocator;
struct pool_allocator_params;
#define PLATFORM_CREATE_POOL_ALLOCATOR(name) pool_allocator name(memory_size RequestedSize, pool_allocator_params Params)
typedef PLATFORM_CREATE_POOL_ALLOCATOR(platform_create_pool_allocator);

#define PLATFORM_FREE_POOL_ALLOCATOR(name) b32 name(pool_allocator *Allocator)
typedef PLATFORM_FREE_POOL_ALLOCATOR(platform_free_pool_allocator);

struct chunk_header;
#define PLATFORM_CONSTRUCT_CONTIGUOUS_VIEW(name) void *name(chunk_header *Header, memory_size Size, pool_allocator *Allocator)
typedef PLATFORM_CONSTRUCT_CONTIGUOUS_VIEW(platform_construct_contiguous_view);

#define PLATFORM_FREE_MAPPED_REGION(name) b32 name(void *MappedRegion)
typedef PLATFORM_FREE_MAPPED_REGION(platform_free_mapped_region);

#define PLATFORM_CHECK_FILE_EXISTS(name) b32 name(char *Path)
typedef PLATFORM_CHECK_FILE_EXISTS(platform_check_file_exists);

#define PLATFORM_OPEN_FILE(name) platform_file_handle name(char *Path, u32 Flags)
typedef PLATFORM_OPEN_FILE(platform_open_file);

#define PLATFORM_GET_FILE_INFO(name) platform_file_info name(char *Path)
typedef PLATFORM_GET_FILE_INFO(platform_get_file_info);

#define PLATFORM_CHECK_FILE_IN_USE(name) b32 name(char *Path)
typedef PLATFORM_CHECK_FILE_IN_USE(platform_check_file_in_use);

#define PLATFORM_GET_FILE_SIZE(name) mmsz name(platform_file_handle *Handle)
typedef PLATFORM_GET_FILE_SIZE(platform_get_file_size);

#define PLATFORM_READ_FILE(name) void name(platform_file_handle *Handle, void *Dest, u64 Size, u64 Offset)
typedef PLATFORM_READ_FILE(platform_read_file);

#define PLATFORM_FILE_ERROR(name) void name(platform_file_handle *Handle, char *Message)
typedef PLATFORM_FILE_ERROR(platform_file_error);

#define PLATFORM_CLOSE_FILE(name) void name(platform_file_handle *Handle)
typedef PLATFORM_CLOSE_FILE(platform_close_file);

#define PLATFORM_GET_FILE_PATH(name) void name(platform_file_handle *Handle, char *Dst, memory_size DstSz)
typedef PLATFORM_GET_FILE_PATH(platform_get_file_path);

struct platform_api
{
    platform_job_queue_add_entry         *JobQueueAddEntry;
    platform_job_queue_complete_all_work *JobQueueCompleteAllWork;
    
    platform_allocate_memory             *AllocateMemory;
	platform_deallocate_memory 	      *DeallocateMemory;
    
    platform_allocate_ringbuffer_memory  *AllocateRingBufferMemory;
    platform_deallocate_ringbuffer_memory *DeallocateRingBufferMemory;
    
    platform_create_pool_allocator       *CreatePoolAllocator;
    platform_free_pool_allocator         *FreePoolAllocator;
    platform_construct_contiguous_view   *ConstructContiguousView;
    platform_free_mapped_region          *FreeMappedRegion;
    
    platform_open_file                   *OpenFile;
    platform_get_file_info               *GetFileInfo;
    platform_get_file_size               *GetFileSize;
    platform_read_file                   *ReadFile;
    platform_file_error                  *FileError;
    platform_close_file                  *CloseFile;
    platform_get_file_path               *GetFilePath;
    platform_check_file_in_use           *CheckFileInUse;
    platform_check_file_exists           *CheckIfFileExists;
};

extern platform_api Platform;

struct game_input
{
    b32 KeyboardKeys[512];
    s32 MouseDeltaX;
    s32 MouseDeltaY;
};

struct game_memory
{
    b32 IsInitialized;
    platform_api PlatformAPI;
    
    game_input Input;
    
    struct game_state *GameState;
    
    platform_job_queue *HighPriorityQueue;
    platform_job_queue *LowPriorityQueue;
    
    struct debug_table *DebugTable;
    struct debug_memory *DebugMemory;
};

struct game_render_commands;

#define GAME_UPDATE_AND_RENDER(name) void name(game_memory *GameMemory, game_render_commands *RenderCommands, r64 DeltaTime)
typedef GAME_UPDATE_AND_RENDER(game_update_and_render);