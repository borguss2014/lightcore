/*
	NOTE(Cristian): These engine specific virtual keycodes use the following table as source,
	as described by Microsoft in their documentation table:

	https://docs.microsoft.com/en-us/windows/win32/inputdev/virtual-key-codes
*/

#ifndef HID_USAGE_PAGE_GENERIC
#define HID_USAGE_PAGE_GENERIC         ((USHORT) 0x01)
#endif
#ifndef HID_USAGE_GENERIC_MOUSE
#define HID_USAGE_GENERIC_MOUSE        ((USHORT) 0x02)
#endif
#ifndef HID_USAGE_GENERIC_KEYBOARD
#define HID_USAGE_GENERIC_KEYBOARD     ((USHORT) 0x06)
#endif

/*
	ACTIONS
*/
#define LC_PRESS WM_KEYDOWN
#define LC_RELEASE WM_KEYUP
#define LC_MOUSEMOVE WM_MOUSEMOVE

/*
	CONTROL KEYS GROUP
*/

// ESC key
#define LC_ESCAPE VK_ESCAPE

// PRINT key
#define LC_PRINT VK_PRINT

// PRINT SCREEN key
#define LC_SNAPSHOT VK_SNAPSHOT

// SCROLL LOCK key
#define LC_SCROLL VK_SCROLL

// PAUSE key
#define LC_PAUSE VK_PAUSE

// CTRL key
#define LC_CONTROL VK_CONTROL

// Left CONTROL key
#define LC_LCONTROL VK_LCONTROL

// Right CONTROL key
#define LC_RCONTROL VK_RCONTROL

// Left Windows key (Natural keyboard)
#define LC_LWIN VK_LWIN

// Right Windows key (Natural keyboard)
#define LC_RWIN VK_RWIN

// ALT key
#define LC_ALT VK_MENU

// Left MENU key
#define LC_LMENU VK_LMENU

// Right MENU key
#define LC_RMENU VK_RMENU

// ================== CONTROL KEYS END ==================



/*
	FUNCTION KEYS GROUP
*/

// F1 key
#define LC_F1 VK_F1	

// F2 key
#define LC_F2 VK_F2

// F3 key
#define LC_F3 VK_F3

// F4 key
#define LC_F4 VK_F4

// F5 key
#define LC_F5 VK_F5

// F6 key
#define LC_F6 VK_F6

// F7 key
#define LC_F7 VK_F7

// F8 key
#define LC_F8 VK_F8

// F9 key
#define LC_F9 VK_F9

// F10 key
#define LC_F10 VK_F10

// F11 key
#define LC_F11 VK_F11

// F12 key
#define LC_F12 VK_F12

// F13 key
#define LC_F13 VK_F13

// F14 key
#define LC_F14 VK_F14
	
// F15 key
#define LC_F15 VK_F15

// F16 key
#define LC_F16 VK_F16

// F17 key
#define LC_F17 VK_F17

// F18 key
#define LC_F18 VK_F18

// F19 key
#define LC_F19 VK_F19

// F20 key
#define LC_F20 VK_F20

// F21 key
#define LC_F21 VK_F21

// F22 key
#define LC_F22 VK_F22

// F23 key
#define LC_F23 VK_F23

// F24 key
#define LC_F24 VK_F24

// ================== FUNCTION KEYS END ==================



/*
	NAVIGATION KEYS GROUP
*/

// INS key
#define LC_INSERT VK_INSERT

// DEL key
#define LC_DELETE VK_DELETE

// HOME key
#define LC_HOME VK_HOME

// PAGE UP key
#define LC_PRIOR VK_PRIOR

// PAGE DOWN key
#define LC_NEXT VK_NEXT

// END key
#define LC_END VK_END

// LEFT ARROW key
#define LC_LEFT VK_LEFT

// UP ARROW key
#define LC_UP VK_UP

// RIGHT ARROW key
#define LC_RIGHT VK_RIGHT

// DOWN ARROW key
#define LC_DOWN VK_DOWN
// ================== NAVIGATION KEYS END ==================



/*
	NUMERIC KEYPAD GROUP
*/

// Numeric keypad 0 key
#define LC_NUMPAD0 VK_NUMPAD0

// Numeric keypad 1 key
#define LC_NUMPAD1 VK_NUMPAD1

// Numeric keypad 2 key
#define LC_NUMPAD2 VK_NUMPAD2

// Numeric keypad 3 key
#define LC_NUMPAD3 VK_NUMPAD3

// Numeric keypad 4 key
#define LC_NUMPAD4 VK_NUMPAD4

// Numeric keypad 5 key
#define LC_NUMPAD5 VK_NUMPAD5

// Numeric keypad 6 key
#define LC_NUMPAD6 VK_NUMPAD6

// Numeric keypad 7 key
#define LC_NUMPAD7 VK_NUMPAD7

// Numeric keypad 8 key
#define LC_NUMPAD8 VK_NUMPAD8

// Numeric keypad 9 key
#define LC_NUMPAD9 VK_NUMPAD9

// NUM LOCK key
#define LC_NUMLOCK VK_NUMLOCK


// ================== NUMERIC KEYPAD END ==================



/*
	ALPHANUMERIC KEYS GROUP
*/

// 0 key
#define LC_NUMBER0 0x30

// 1 key
#define LC_NUMBER1 0x31

// 2 key
#define LC_NUMBER2 0x32

// 3 key
#define LC_NUMBER3 0x33

// 4 key
#define LC_NUMBER4 0x34

// 5 key
#define LC_NUMBER5 0x35

// 6 key
#define LC_NUMBER6 0x36

// 7 key
#define LC_NUMBER7 0x37

// 8 key
#define LC_NUMBER8 0x38

// 9 key
#define LC_NUMBER9 0x39

// A key
#define LC_KEY_A 0x41

// B key
#define LC_KEY_B 0x42

// C key
#define LC_KEY_C 0x43

// D key
#define LC_KEY_D 0x44

// E key
#define LC_KEY_E 0x45

// F key
#define LC_KEY_F 0x46

// G key
#define LC_KEY_G 0x47

// H key
#define LC_KEY_H 0x48

// I key
#define LC_KEY_I 0x49

// J key
#define LC_KEY_J 0x4A

// K key
#define LC_KEY_K 0x4B

// L key
#define LC_KEY_L 0x4C

// M key
#define LC_KEY_M 0x4D

// N key
#define LC_KEY_N 0x4E

// O key
#define LC_KEY_O 0x4F

// P key
#define LC_KEY_P 0x50

// Q key
#define LC_KEY_Q 0x51

// R key
#define LC_KEY_R 0x52

// S key
#define LC_KEY_S 0x53

// T key
#define LC_KEY_T 0x54

// U key
#define LC_KEY_U 0x55

// V key
#define LC_KEY_V 0x56

// W key
#define LC_KEY_W 0x57

// X key
#define LC_KEY_X 0x58

// Y key
#define LC_KEY_Y 0x59

// Z key
#define LC_KEY_Z 0x5A

// Multiply key
#define LC_MULTIPLY VK_MULTIPLY

// Add key
#define LC_ADD VK_ADD

// Separator key
#define LC_SEPARATOR VK_SEPARATOR

// Subtract key
#define LC_SUBSTRACT VK_SUBTRACT

// Decimal key
#define LC_DECIMAL VK_DECIMAL

// Divide key
#define LC_DIVIDE VK_DIVIDE

// BACKSPACE key 
#define LC_BACK VK_BACK

// CLEAR key
#define LC_CLEAR VK_CLEAR

// TAB key
#define LC_TAB VK_TAB

// SHIFT key
#define LC_SHIFT VK_SHIFT

// Left SHIFT key
#define LC_LSHIFT VK_LSHIFT

// Right SHIFT key
#define LC_RSHIFT VK_RSHIFT

// CAPS LOCK key
#define LC_CAPITAL VK_CAPITAL

// SPACEBAR
#define LC_SPACE VK_SPACE

// SELECT key
#define LC_SELECT VK_SELECT

// EXECUTE key
#define LC_EXECUTE VK_EXECUTE

// ENTER key
#define LC_RETURN VK_RETURN

// ================== ALPHANUMERIC KEYS END ==================



/*
	MOUSE KEYS
*/
// Left mouse button
#define LC_LBUTTON VK_LBUTTON

// Right mouse button
#define LC_RBUTTON VK_RBUTTON

// X1 mouse button
#define LC_XBUTTON1 VK_XBUTTON1

// X2 mouse button
#define LC_XBUTTON2 VK_XBUTTON2

// Middle mouse button (three-button mouse)
#define LC_MBUTTON VK_MBUTTON
// ================== MOUSE KEYS END ==================


/*
	MEDIA KEYS
*/

// Volume Mute key
#define LC_VOLUME_MUTE VK_VOLUME_MUTE

// Volume Down key
#define LC_VOLUME_DOWN VK_VOLUME_DOWN

// Volume Up key
#define LC_VOLUME_UP VK_VOLUME_UP

// Next Track key
#define LC_MEDIA_NEXT_TRACK VK_MEDIA_NEXT_TRACK

// Previous Track key
#define LC_MEDIA_PREV_TRACK VK_MEDIA_PREV_TRACK
// ================== MEDIA KEYS END ==================



/*
	OEM KEYS

*/

/*
	Used for miscellaneous characters; it can vary by keyboard.
	For the US standard keyboard, the ';:' key
*/
#define LC_OEM_1 VK_OEM_1

// For any country/region, the '+' key
#define LC_OEM_PLUS VK_OEM_PLUS

// For any country/region, the ',' key
#define LC_OEM_COMMA VK_OEM_COMMA

// For any country/region, the '-' key
#define LC_OEM_MINUS VK_OEM_MINUS

// For any country/region, the '.' key
#define LC_OEM_PERIOD VK_OEM_PERIOD

/*
	Used for miscellaneous characters; it can vary by keyboard.
	For the US standard keyboard, the '/?' key
*/
#define LC_OEM_2 VK_OEM_2

/*
	Used for miscellaneous characters; it can vary by keyboard.
	For the US standard keyboard, the '`~' key
*/
#define LC_OEM_3 VK_OEM_3

/*
	Used for miscellaneous characters; it can vary by keyboard.
	For the US standard keyboard, the '[{' key
*/
#define LC_OEM_4 VK_OEM_4

/*
	Used for miscellaneous characters; it can vary by keyboard.
	For the US standard keyboard, the '\|' key
*/
#define LC_OEM_5 VK_OEM_5

/*
	Used for miscellaneous characters; it can vary by keyboard.
	For the US standard keyboard, the ']}' key
*/
#define LC_OEM_6 VK_OEM_6

/*
	Used for miscellaneous characters; it can vary by keyboard.
	For the US standard keyboard, the 'single-quote/double-quote' key
*/
#define LC_OEM_7 VK_OEM_7

// Used for miscellaneous characters; it can vary by keyboard.
#define LC_OEM_8 VK_OEM_8

// Clear key
#define LC_OEM_CLEAR VK_OEM_CLEAR
// ================== OEM KEYS END ==================



/*
	Used to pass Unicode characters as if they were keystrokes.
	The VK_PACKET key is the low word of a 32-bit Virtual Key value used for non-keyboard input methods. 
	For more information, see Remark in KEYBDINPUT, SendInput, WM_KEYDOWN, and WM_KEYUP
*/
#define LC_PACKET VK_PACKET

// Control-break processing
#define LC_CANCEL VK_CANCEL	

// Applications key (Natural keyboard)
#define LC_APPS VK_APPS

// Computer Sleep key
#define LC_SLEEP VK_SLEEP