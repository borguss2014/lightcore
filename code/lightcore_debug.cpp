#define QUEUE_LINK_HEAD(Obj, QueueHead) \
if(Obj) { Obj->Next = QueueHead; QueueHead = Obj; } \

#define FREELIST_ALLOCATE(Result, FreelistObj, FallbackArena, Type) \
if(FreelistObj) { Result = FreelistObj; FreelistObj = FreelistObj->NextFree; *Result = {};} \
else { Result = PushStruct(FallbackArena, Type);} \

#define FREELIST_DEALLOCATE(DeallocationObj, FreelistBegin) \
if(DeallocationObj) { DeallocationObj->NextFree = FreelistBegin; FreelistBegin = DeallocationObj; } \

internal debug_thread *
GetDebugThread(debug_memory *DebugMemory, u32 ThreadID)
{
    debug_thread *Result = 0;
    
    debug_thread *PreviousThreadNode = 0;
    debug_thread *CurrentThreadNode = DebugMemory->FirstThreadNode;
    while(CurrentThreadNode)
    {
        if(CurrentThreadNode->ID == ThreadID)
        {
            Result = CurrentThreadNode;
            break;
        }
        
        PreviousThreadNode = CurrentThreadNode;
        CurrentThreadNode = CurrentThreadNode->Next;
    }
    
    if(!Result)
    {
        FREELIST_ALLOCATE(Result, DebugMemory->FirstFreeThreadNode, &DebugMemory->DebugArena, debug_thread);
        Result->ID = ThreadID;
        Result->LastOpenBlock = 0;
        
        if(PreviousThreadNode)
        {
            PreviousThreadNode->Next = Result;
        }
        else
        {
            DebugMemory->FirstThreadNode = Result;
        }
    }
    
    return(Result);
}

internal void
CleanupFrame(debug_frame *Frame, debug_memory *DebugMemory)
{
    debug_profile_group *ProfileGroup = Frame->LastProfileGroup;
    while(ProfileGroup)
    {
        debug_profile_node *CurrentCleanupProfileNode = ProfileGroup->LastCleanupProfileNode;
        while(CurrentCleanupProfileNode)
        {
            debug_profile_node *NextCleanupProfileNode = CurrentCleanupProfileNode->NextCleanupProfileNode;
            
            FREELIST_DEALLOCATE(CurrentCleanupProfileNode, DebugMemory->FirstFreeProfileNode);
            ProfileGroup->LastCleanupProfileNode = NextCleanupProfileNode;
            CurrentCleanupProfileNode = ProfileGroup->LastCleanupProfileNode;
        }
        
        debug_profile_group *NextProfileGroup = ProfileGroup->Next;
        
        FREELIST_DEALLOCATE(ProfileGroup, DebugMemory->FirstFreeProfileGroup);
        Frame->LastProfileGroup = NextProfileGroup;
        ProfileGroup = Frame->LastProfileGroup;
    }
    
    Frame->LastProfileGroup = 0;
}

inline void
AdvanceFrameIndex(debug_memory *DebugMemory)
{
    DebugMemory->NextFrame = (++DebugMemory->NextFrame) % MAX_DEBUG_FRAMES;
}

internal debug_frame *
GetNextFrame(debug_memory *DebugMemory)
{
    debug_frame *CurrentFrame = &DebugMemory->Frames[DebugMemory->NextFrame];
    CleanupFrame(CurrentFrame, DebugMemory);
    
    AdvanceFrameIndex(DebugMemory);
    
    return(CurrentFrame);
}

internal void
AccumulateDebugStatistics(debug_statistics *Statistics, u64 Cycles)
{
    Statistics->HitCount++;
    
    if(!Statistics->MinCycles && !Statistics->MaxCycles)
    {
        Statistics->MinCycles = Cycles;
        Statistics->MaxCycles = Cycles;
    }
    else
    {
        if(Cycles < Statistics->MinCycles)
        {
            Statistics->MinCycles = Cycles;
        }
        
        if(Cycles > Statistics->MaxCycles)
        {
            Statistics->MaxCycles = Cycles;
        }
        
    }
    
    Statistics->TotalCycles += Cycles;
}

internal debug_profile_node *
RetrieveProfileNodeByGUID(debug_profile_node **FirstProfileNode, 
                          char *EventGUID, 
                          debug_memory *DebugMemory, 
                          debug_profile_group *ProfileGroup)
{
    debug_profile_node *Result = 0;
    
    debug_profile_node *CurrentProfileNode = *FirstProfileNode;
    while(CurrentProfileNode)
    {
        if(StringsEqual(CurrentProfileNode->GUID, EventGUID))
        {
            Result = CurrentProfileNode;
            break;
        }
        
        CurrentProfileNode = CurrentProfileNode->Next;
    }
    
    if(!Result)
    {
        FREELIST_ALLOCATE(Result, DebugMemory->FirstFreeProfileNode, &DebugMemory->DebugArena, debug_profile_node);
        
        QUEUE_LINK_HEAD(Result, *FirstProfileNode);
        
        /*Result->Name = Event->Name;
        Result->GUID = Event->GUID;*/
        
        Result->NextCleanupProfileNode = ProfileGroup->LastCleanupProfileNode;
        ProfileGroup->LastCleanupProfileNode = Result;
    }
    
    return(Result);
}

internal debug_profile_node *
CopyProfileNode(debug_profile_node *Source, debug_memory *DebugMemory, debug_profile_group *ProfileGroup)
{
    Assert(Source);
    
    debug_profile_node *ProfileNodeDest = 0;
    FREELIST_ALLOCATE(ProfileNodeDest, DebugMemory->FirstFreeProfileNode, &DebugMemory->DebugArena, debug_profile_node);
    
    ProfileNodeDest->Name = Source->Name;
    ProfileNodeDest->GUID = Source->GUID;
    ProfileNodeDest->FileName = Source->FileName;
    ProfileNodeDest->FunctionName = Source->FunctionName;
    ProfileNodeDest->LineNumber = Source->LineNumber;
    ProfileNodeDest->StartCycles = Source->StartCycles;
    ProfileNodeDest->EndCycles = Source->EndCycles;
    ProfileNodeDest->Statistics = Source->Statistics;
    
    ProfileNodeDest->NextCleanupProfileNode = ProfileGroup->LastCleanupProfileNode;
    ProfileGroup->LastCleanupProfileNode = ProfileNodeDest;
    
    return(ProfileNodeDest);
}

internal debug_profile_group *
CopyEntireProfileGroup(debug_profile_group *Source, debug_memory *DebugMemory)
{
    Assert(Source);
    
    debug_profile_group *Result = 0;
    FREELIST_ALLOCATE(Result, DebugMemory->FirstFreeProfileGroup, &DebugMemory->DebugArena, debug_profile_group);
    Result->ThreadID = Source->ThreadID;
    
    debug_profile_node *LastStoredParentNodeSrc = 0;
    debug_profile_node *LastStoredParentNodeDest = 0;
    
    debug_profile_node *CurrentProfileNodeSrc = Source->LastProfileNode;
    debug_profile_node *CurrentProfileNodeDest = CopyProfileNode(CurrentProfileNodeSrc, DebugMemory, Result);
    
    Result->LastProfileNode = CurrentProfileNodeDest;
    
    while(CurrentProfileNodeSrc)
    {
        if(CurrentProfileNodeSrc->LastChildProfileNode)
        {
            debug_profile_node *ChildProfileNodeDest = CopyProfileNode(CurrentProfileNodeSrc->LastChildProfileNode, DebugMemory, Result);
            CurrentProfileNodeDest->LastChildProfileNode = ChildProfileNodeDest;
            
            CurrentProfileNodeSrc->NextLink = LastStoredParentNodeSrc;
            LastStoredParentNodeSrc = CurrentProfileNodeSrc;
            
            CurrentProfileNodeDest->NextLink = LastStoredParentNodeDest;
            LastStoredParentNodeDest = CurrentProfileNodeDest;
        }
        
        if(CurrentProfileNodeSrc->Next)
        {
            debug_profile_node *NextProfileNodeDest = CopyProfileNode(CurrentProfileNodeSrc->Next, DebugMemory, Result);
            CurrentProfileNodeDest->Next = NextProfileNodeDest;
            
            CurrentProfileNodeSrc = CurrentProfileNodeSrc->Next;
            CurrentProfileNodeDest = CurrentProfileNodeDest->Next;
        }
        else
        {
            CurrentProfileNodeSrc = LastStoredParentNodeSrc;
            if(LastStoredParentNodeSrc)
            {
                debug_profile_node *NextStoredParentNodeSrc = LastStoredParentNodeSrc->NextLink;
                LastStoredParentNodeSrc->NextLink = 0;
                LastStoredParentNodeSrc = NextStoredParentNodeSrc;
            }
            
            CurrentProfileNodeDest = LastStoredParentNodeDest;
            if(LastStoredParentNodeDest)
            {
                debug_profile_node *NextStoredParentNodeDest = LastStoredParentNodeDest->NextLink;
                LastStoredParentNodeDest->NextLink = 0;
                LastStoredParentNodeDest = NextStoredParentNodeDest;
            }
        }
    }
    
    return(Result);
}

internal debug_open_block *
CopyOpenBlock(debug_open_block *Source, debug_memory *DebugMemory, debug_profile_group *ProfileGroup)
{
    Assert(Source);
    
    debug_open_block *Result = 0;
    FREELIST_ALLOCATE(Result, DebugMemory->FirstFreeOpenBlock, &DebugMemory->DebugArena, debug_open_block);
    
    debug_profile_node *ProfileNodeCopy = CopyProfileNode(Source->ProfileNode, DebugMemory, ProfileGroup);
    Result->ProfileNode = ProfileNodeCopy;
    
    return(Result);
}

internal void
FreeEntireProfileGroup(debug_profile_group *ProfileGroup, debug_memory *DebugMemory)
{
    debug_profile_node *CurrentCleanupProfileNode = ProfileGroup->LastCleanupProfileNode;
    while(CurrentCleanupProfileNode)
    {
        debug_profile_node *NextCleanupProfileNode = CurrentCleanupProfileNode->NextCleanupProfileNode;
        
        FREELIST_DEALLOCATE(CurrentCleanupProfileNode, DebugMemory->FirstFreeProfileNode);
        ProfileGroup->LastCleanupProfileNode = NextCleanupProfileNode;
        CurrentCleanupProfileNode = ProfileGroup->LastCleanupProfileNode;
    }
    
    ProfileGroup->LastProfileNode = 0;
}

internal void
ProcessDebugEvents(debug_memory *DebugMemory, debug_event *Events, u32 EventsCount)
{
    debug_frame *CurrentFrame = GetNextFrame(DebugMemory);
    
    u64 FrameStartTimestamp = 0;
    u64 FrameEndTimestamp = 0;
    
    for(u32 EventIndex = 0;
        EventIndex < EventsCount;
        ++EventIndex)
    {
        debug_event *Event = Events + EventIndex;
        debug_thread *DebugThread = GetDebugThread(DebugMemory, Event->ThreadID);
        
        switch(Event->Type)
        {
            case DebugEvent_BeginBlock:
            {
                debug_profile_group *ProfileGroup = DebugThread->ProfileGroup;
                if(!ProfileGroup)
                {
                    FREELIST_ALLOCATE(ProfileGroup, DebugMemory->FirstFreeProfileGroup, &DebugMemory->DebugArena, debug_profile_group);
                    ProfileGroup->ThreadID = DebugThread->ID;
                    
                    DebugThread->ProfileGroup = ProfileGroup;
                }
                
                debug_open_block *OpenBlock = 0;
                FREELIST_ALLOCATE(OpenBlock, DebugMemory->FirstFreeOpenBlock, &DebugMemory->DebugArena, debug_open_block);
                
                debug_profile_node **TargetProfileNode = 0;
                
                debug_open_block *ParentOpenBlock = DebugThread->LastOpenBlock;
                if(!ParentOpenBlock)
                {
                    TargetProfileNode = &ProfileGroup->LastProfileNode;
                }
                else
                {
                    TargetProfileNode = &ParentOpenBlock->ProfileNode->LastChildProfileNode;
                }
                
                //debug_profile_node *MatchingProfileNode = RetrieveProfileNodeByGUID(TargetProfileNode, Event->GUID, DebugMemory, ProfileGroup);
                
                
                debug_profile_node *ProfileNode = 0;
                FREELIST_ALLOCATE(ProfileNode, DebugMemory->FirstFreeProfileNode, &DebugMemory->DebugArena, debug_profile_node);
                
                QUEUE_LINK_HEAD(ProfileNode, *TargetProfileNode);
                
                ProfileNode->Name = Event->Name;
                ProfileNode->GUID = Event->GUID;
                ProfileNode->StartCycles = Event->Cycles;
                
                ProfileNode->NextCleanupProfileNode = ProfileGroup->LastCleanupProfileNode;
                ProfileGroup->LastCleanupProfileNode = ProfileNode;
                
                
                OpenBlock->ProfileNode = ProfileNode;
                QUEUE_LINK_HEAD(OpenBlock, DebugThread->LastOpenBlock);
            } break;
            
            case DebugEvent_EndBlock:
            {
                debug_open_block *MatchingOpenBlock = DebugThread->LastOpenBlock;
                if(MatchingOpenBlock)
                {
                    debug_profile_node *ProfileNode = MatchingOpenBlock->ProfileNode;
                    
                    ProfileNode->EndCycles = Event->Cycles;
                    
                    u64 TotalCycles = Event->Cycles - ProfileNode->StartCycles;
                    ProfileNode->Statistics.MinCycles = TotalCycles;
                    ProfileNode->Statistics.MaxCycles = TotalCycles;
                    ProfileNode->Statistics.TotalCycles = TotalCycles;
                    ProfileNode->Statistics.HitCount = 1;
                    
                    debug_open_block *ParentOpenBlock = MatchingOpenBlock->Next;
                    if(ParentOpenBlock)
                    {
                        debug_profile_node *ParentProfileNode = ParentOpenBlock->ProfileNode;
                        if(ParentProfileNode)
                        {
                            ParentProfileNode->Statistics.ChildrenCycles += TotalCycles;
                        }
                    }
                    
                    FREELIST_DEALLOCATE(MatchingOpenBlock, DebugMemory->FirstFreeOpenBlock);
                    DebugThread->LastOpenBlock = ParentOpenBlock;
                }
            } break;
            
            case DebugEvent_BeginFrameBoundary:
            {
                DebugMemory->LastFrameTimestamp = Event->Cycles;
                
                FrameStartTimestamp = Event->Cycles;
            } break;
            
            case DebugEvent_EndFrameBoundary:
            {
                u64 TotalFrameCycles = Event->Cycles - DebugMemory->LastFrameTimestamp;
                CurrentFrame->TotalCycles = TotalFrameCycles;
                
                FrameEndTimestamp = Event->Cycles;
            } break;
        }
    }
    
    CurrentFrame->FrameBeginCycles = FrameStartTimestamp;
    CurrentFrame->FrameEndCycles = FrameEndTimestamp;
    
    debug_thread *CurrentThread = DebugMemory->FirstThreadNode;
    while(CurrentThread)
    {
        u32 ThreadID = CurrentThread->ID;
        debug_thread *NextThread = CurrentThread->Next;
        
        if(!CurrentThread->LastOpenBlock)
        {
            QUEUE_LINK_HEAD(CurrentThread->ProfileGroup, CurrentFrame->LastProfileGroup);
            
            CurrentThread->ID = ThreadID;
            CurrentThread->ProfileGroup = 0;
        }
        else
        {
            debug_open_block *OpenBlock = CurrentThread->LastOpenBlock;
            while(OpenBlock)
            {
                debug_profile_node *ProfileNode = OpenBlock->ProfileNode;
                
                if(ProfileNode->StartCycles < FrameStartTimestamp)
                {
                    ProfileNode->StartCycles = FrameStartTimestamp;
                }
                ProfileNode->EndCycles = FrameEndTimestamp;
                
                u64 TotalCycles = ProfileNode->EndCycles - ProfileNode->StartCycles;
                ProfileNode->Statistics.MinCycles = TotalCycles;
                ProfileNode->Statistics.MaxCycles = TotalCycles;
                ProfileNode->Statistics.TotalCycles = TotalCycles;
                ProfileNode->Statistics.HitCount = 1;
                
                OpenBlock = OpenBlock->Next;
            }
            
            
            debug_profile_group *ProfileGroupCopy = CopyEntireProfileGroup(CurrentThread->ProfileGroup, DebugMemory);
            QUEUE_LINK_HEAD(ProfileGroupCopy, CurrentFrame->LastProfileGroup);
            
            
            debug_open_block *CurrentOpenBlock = CurrentThread->LastOpenBlock;
            debug_open_block *CurrentOpenBlockCopy = CopyOpenBlock(CurrentOpenBlock, DebugMemory, ProfileGroupCopy);
            
            debug_open_block *NewOpenBlocksList = CurrentOpenBlockCopy;
            CurrentThread->LastOpenBlock = NewOpenBlocksList;
            
            debug_profile_node *NewProfileGroupRootNode = 0;
            
            while(CurrentOpenBlock)
            {
                if(CurrentOpenBlock->Next)
                {
                    debug_open_block *OpenBlockCopy = CopyOpenBlock(CurrentOpenBlock->Next, DebugMemory, ProfileGroupCopy);
                    CurrentOpenBlockCopy->Next = OpenBlockCopy;
                    
                    QUEUE_LINK_HEAD(CurrentOpenBlockCopy->ProfileNode, OpenBlockCopy->ProfileNode->LastChildProfileNode);
                    
                    CurrentOpenBlockCopy = CurrentOpenBlockCopy->Next;
                }
                else
                {
                    NewProfileGroupRootNode = CurrentOpenBlockCopy->ProfileNode;
                }
                
                debug_open_block *NextOpenBlock = CurrentOpenBlock->Next;
                FREELIST_DEALLOCATE(CurrentOpenBlock, DebugMemory->FirstFreeOpenBlock);
                CurrentOpenBlock = NextOpenBlock;
            }
            
            FreeEntireProfileGroup(CurrentThread->ProfileGroup, DebugMemory);
            CurrentThread->ProfileGroup->LastProfileNode = NewProfileGroupRootNode;
        }
        
        CurrentThread = NextThread;
    }
}