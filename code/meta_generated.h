enum meta_type
{
	MetaType_material,
	MetaType_char,
	MetaType_render_textures,
	MetaType_vec4,
	MetaType_f32,
	MetaType_b32
};

