struct win32_dynamic_code
{
    HMODULE DLL;
    
    char *SourceDLLPath;
    char *LockFilePath;
    char *TransientDLLName;
    
    u32 TempDLLNumber;
    
    FILETIME LastDLLWriteTime;
    
    u32 FunctionsCount;
    char **FunctionNames;
    void **Functions;
    
    b32 IsValid;
};

struct win32_game_function_table
{
    game_update_and_render *GameUpdateAndRender;
};

global_variable char *Win32GameFunctionTableNames[] =
{
    "GameUpdateAndRender"
};

struct win32_window_info
{
    HWND WindowHandle;
    WINDOWPLACEMENT WindowPreviousPlacement;
    
    char *WindowTitle;
    char *WindowClassName;
    
    u32 Width;
    u32 Height;
    
    HDC DeviceContext;
    HGLRC RenderContext;
};

struct win32_memory_block
{
    platform_memory_block Block;
    win32_memory_block *Prev;
    win32_memory_block *Next;
};

struct win32_state
{
    ticket_mutex MemoryMutex;
    win32_memory_block MemorySentinel;
    
    char EXEPath[MAX_PATH];
    char *OnePastLastEXEPathSlash;
};

#define MAIN_THREAD_INDEX 0

struct platform_job_queue_entry
{
    platform_job_queue_callback *Callback;
    void *Data;
};

struct platform_job_queue
{
    //HANDLE SemaphoreHandle;
    void *SemaphoreHandle;
    
    u32 volatile AvailableEntriesCount;
    
    // TODO(Cristian): Implement a circular FIFO queue
    u32 volatile NextJobEntryToRead;
    u32 volatile NextJobEntryToWrite;
    
    platform_job_queue_entry JobEntries[256];
};

struct win32_thread_info
{
    platform_job_queue *Queue;
    DWORD LogicalThreadIndex;
};