#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif
#include <Windows.h> //TODO(Cristian): this shouldn't be here, but it's needed for keycodes for now

#include "game.h"

#include "meta.h"

#include "lightcore_renderer.cpp"

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>


platform_api Platform;

global_variable render_texture DefaultDiffuseTexture = {};
global_variable ticket_mutex GameMemoryMutex = {};

// TODO(Cristian): Should take a look at organizing these a little better
#include "lightcore_texture.cpp"
#include "lightcore_material.cpp"
#include "lightcore_mesh.cpp"
#include "lightcore_asset.cpp"

#include "meta.cpp"

internal void
ViewFromCamera(game_render_commands *Commands, camera *Camera)
{
    u32 ScaledRenderOutputWidth = (Commands->RenderOutputWidth * Commands->RenderOutputScale) / 100;
    u32 ScaledRenderOutputHeight = (Commands->RenderOutputHeight * Commands->RenderOutputScale) / 100;
    
    f32 AspectRatio = (f32)ScaledRenderOutputWidth / (f32)ScaledRenderOutputHeight;
    
    Camera->AspectRatio = AspectRatio;
    
    camera_matrices Transforms = {};
    Transforms.CameraViewMatrix = GetViewMatrix(Camera);
    Transforms.CameraPerspectiveMatrix = GetPerspectiveProjectionMatrix(Camera);
    Transforms.CameraViewPerspectiveMatrix = Transforms.CameraPerspectiveMatrix * Transforms.CameraViewMatrix;
    
    PushCameraUpdate(Commands, Transforms, Camera->Position);
}

internal void
ProcessInput(game_render_commands *Commands, game_input *GameInput, camera *SceneCamera, r64 DeltaTime)
{
    BEGIN_TIMED_BLOCK("Input processing");
    
    if(GameInput->MouseDeltaX || GameInput->MouseDeltaY)
    {
        ProcessCameraDirection(SceneCamera, GameInput->MouseDeltaX, GameInput->MouseDeltaY);
    }
    
    if(GameInput->KeyboardKeys[LC_KEY_W])
    {
        ProcessCameraPosition(SceneCamera, CameraDirection_Forward, DeltaTime);
    }
    
    if(GameInput->KeyboardKeys[LC_KEY_A])
    {
        ProcessCameraPosition(SceneCamera, CameraDirection_Left, DeltaTime);
    }
    
    if(GameInput->KeyboardKeys[LC_KEY_S])
    {
        ProcessCameraPosition(SceneCamera, CameraDirection_Backward, DeltaTime);
    }
    
    if(GameInput->KeyboardKeys[LC_KEY_D])
    {
        ProcessCameraPosition(SceneCamera, CameraDirection_Right, DeltaTime);
    }
    
    if(GameInput->KeyboardKeys[LC_SPACE])
    {
        ProcessCameraPosition(SceneCamera, CameraDirection_Up, DeltaTime);
    }
    
    if(GameInput->KeyboardKeys[LC_CONTROL])
    {
        ProcessCameraPosition(SceneCamera, CameraDirection_Down, DeltaTime);
    }
    
    
    if(GameInput->KeyboardKeys[LC_F5])
    {
        Commands->CurrentSettings.EnableVSync = true;
    }
    
    if(GameInput->KeyboardKeys[LC_F6])
    {
        Commands->CurrentSettings.EnableVSync = false;
    }
    
    if(GameInput->KeyboardKeys[LC_F7])
    {
        Commands->CurrentWindowSettings.Fullscreen = true;
    }
    
    if(GameInput->KeyboardKeys[LC_F8])
    {
        Commands->CurrentWindowSettings.Fullscreen = false;
    }
    
    if(GameInput->KeyboardKeys[LC_SHIFT])
    {
        SceneCamera->Speed = 12.0f;
    }
    else
    {
        SceneCamera->Speed = 2.5f;
    }
    
    if(GameInput->KeyboardKeys[LC_OEM_PLUS])
    {
        Commands->RenderOutputScale += 5;
        
        if(Commands->RenderOutputScale > 100)
        {
            Commands->RenderOutputScale = 100;
        }
    }
    
    if(GameInput->KeyboardKeys[LC_OEM_MINUS])
    {
        Commands->RenderOutputScale -= 5;
        
        if(Commands->RenderOutputScale < 5)
        {
            Commands->RenderOutputScale = 5;
        }
    }
    
    /*if(GameInput->KeyboardKeys[LC_KEY_L])
    {
        asset *Asset = PushAssetToStorage(&SceneStorage.AssetStorage);
        Asset->Transform.Scale = V3(0.01f);
        LoadAsset(Commands, "assets/classic sponza/sponza.obj", Asset, &SceneStorage);
    }*/
    
    END_TIMED_BLOCK();
}

#define MAX_MESHES_STORAGE 1000000
#define MAX_MATERIALS_STORAGE 16384
#define MAX_ASSETS_STORAGE 100000
#define MAX_NODES_STORAGE 100000
#define MAX_TEXTURES_STORAGE 100000
#define MAX_LIGHTS_STORAGE 64

internal void
InitializePointLightsStorage(game_render_commands *RenderCommands, pointlight_storage *PointLightsStorage, memory_arena *Arena)
{
    LC_ArrayInit(PointLightsStorage->PointLights, Arena, MAX_LIGHTS_STORAGE);
    PointLightsStorage->BufferID = PushBufferObjectID(RenderCommands);
    
    u32 BufferLocation = 2;
    b32 LargeBuffer = true;
    PushBufferData(RenderCommands,
                   PointLightsStorage->BufferID,
                   BufferLocation,
                   0,
                   0,
                   4 * sizeof(u32) + MAX_LIGHTS_STORAGE * sizeof(point_light),
                   LargeBuffer);
}

internal void
InitializeSceneStorage(scene_storage *Storage, memory_arena *Arena, game_render_commands *RenderCommands)
{
    Storage->AssetStorage.AssetTransientStorage = CreateRingBuffer(Megabytes(512));
    
    pool_allocator_params Params = DefaultPoolAllocatorParams();
    Params.ChunkSize = Megabytes(4);
    Storage->RenderTexturesStorage.MemoryPool = Platform.CreatePoolAllocator(Megabytes(512), Params);
    
    Storage->MeshStorage.Meshes = PushArray(Arena, MAX_MESHES_STORAGE, mesh);
    Storage->MeshStorage.Capacity = MAX_MESHES_STORAGE;
    
    Storage->MaterialStorage.Materials = PushArray(Arena, MAX_MATERIALS_STORAGE, material);
    Storage->MaterialStorage.Capacity = MAX_MATERIALS_STORAGE;
    Storage->MaterialStorage.NextMaterialSlot = 1;
    
    Storage->AssetStorage.Assets = PushArray(Arena, MAX_ASSETS_STORAGE, asset);
    Storage->AssetStorage.Capacity = MAX_ASSETS_STORAGE;
    
    Storage->NodesStorage.Nodes = PushArray(Arena, MAX_NODES_STORAGE, relational_node);
    Storage->NodesStorage.Capacity = MAX_NODES_STORAGE;
    
    Storage->RenderTexturesStorage.Textures = PushArray(Arena, MAX_TEXTURES_STORAGE, render_texture);
    Storage->RenderTexturesStorage.Capacity = MAX_TEXTURES_STORAGE;
    
    InitializePointLightsStorage(RenderCommands, &Storage->PointLightsStorage, Arena);
}

inline void
InitPointLight(point_light *Light)
{
    Assert(Light);
    
    Light->Ambient = V4(V3(1.0f), 0.05f);
    Light->Diffuse = V4(V3(1.0f), 1.0f);
    Light->Specular = V4(V3(1.0f), 0.5f);
    Light->UpdateRequired = true;
}

internal point_light *
PushPointLight(pointlight_storage *PointLightsStorage)
{
    u32 NextIndexToWrite = LC_ArrayCount(PointLightsStorage->PointLights);
    
    point_light *PointLight = &PointLightsStorage->PointLights[NextIndexToWrite];
    LC_GetArrayHeader(PointLightsStorage->PointLights)->Count++;
    
    return(PointLight);
}

internal void
UpdatePointLights(game_render_commands *RenderCommands, pointlight_storage *PointLightsStorage)
{
    point_light *PointLights = PointLightsStorage->PointLights;
    for(u32 Index = 0;
        Index < LC_ArrayCount(PointLights);
        ++Index)
    {
        point_light *Light = &PointLights[Index];
        if(Light->UpdateRequired)
        {
            mmsz PointLightsOffset = 4 * sizeof(u32) + Index * sizeof(point_light);
            PushBufferData(RenderCommands, 
                           PointLightsStorage->BufferID,
                           PointLightsStorage->BufferLocation, 
                           PointLightsOffset,
                           Light,
                           sizeof(point_light));
            
            Light->UpdateRequired = false;
        }
    }
    
    mmsz CounterOffset = 0;
    u32 *ArrayCount = &LC_ArrayCount(PointLightsStorage->PointLights);
    PushBufferData(RenderCommands, 
                   PointLightsStorage->BufferID, 
                   PointLightsStorage->BufferLocation, 
                   CounterOffset, 
                   ArrayCount, 
                   sizeof(u32));
}

#ifdef LIGHTCORE_DEBUG
debug_table *GlobalDebugTable;
#endif

scene_storage *SceneStorage;
camera *SceneCamera;

internal void
UpdateAssetTransforms(scene_storage *Storage, memory_arena *Arena)
{
    BEGIN_TIMED_BLOCK("Assets update");
    for(u32 AssetIndex = 0;
        AssetIndex < Storage->AssetStorage.Used;
        ++AssetIndex)
    {
        asset *Asset = &Storage->AssetStorage.Assets[AssetIndex];
        if(Asset->Transform.UpdateTransform)
        {
            temporary_memory TempMem = BeginTemporaryMemory(Arena);
            UpdateAssetTransformData(Asset, Arena);
            EndTemporaryMemory(TempMem);
            
            Asset->Transform.UpdateTransform = false;
        }
    }
    END_TIMED_BLOCK();
}

inline void
SetAssetScale(asset *Asset, vec3 Scale)
{
    Asset->Transform.Scale = Scale;
    Asset->Transform.UpdateTransform = true;
}

extern "C"
GAME_UPDATE_AND_RENDER(GameUpdateAndRender)
{
    //OutputDebugStringA("Calling from DLL\n");
    
#ifdef LIGHTCORE_DEBUG
    GlobalDebugTable = GameMemory->DebugTable;
#endif
    
    BEGIN_TIMED_BLOCK("GameUpdateAndRender");
    
    game_input GameInput = GameMemory->Input;
    
    Platform = GameMemory->PlatformAPI;
    
    game_state *GameState = GameMemory->GameState;
    if(!GameState)
    {
        MathTest();
        
        memory_arena Arena = {};
        GameMemory->GameState = PushStruct(&Arena, game_state);
        GameMemory->GameState->Arena = Arena;
        GameState = GameMemory->GameState;
        
        scene_storage *_SceneStorage = &GameMemory->GameState->SceneStorage;
        memory_arena *_Arena = &GameMemory->GameState->SceneStorage.SceneArena;
        InitializeSceneStorage(_SceneStorage, _Arena, RenderCommands);
        
        GameState->SceneCamera = GetStandardPerspectiveCamera();
        
        LoadTextureFile(RenderCommands, 
                        "assets\\checkerboard_missing_texture.jpg", 
                        RenderTextureType_DEFAULT_DIFFUSE, 
                        &_SceneStorage->RenderTexturesStorage, 
                        GameMemory->LowPriorityQueue);
        
        /*asset *ClassicSponza = QueueAssetLoad(RenderCommands, 
                                              "assets\\ClassicSponza\\sponza.obj", 
                                              &_SceneStorage->AssetStorage);
        ClassicSponza->Transform.Scale = V3(0.01f);*/
        
        /*asset *Test = QueueAssetLoad(RenderCommands, 
                                     "assets\\sponza\\NewSponza_Main_Yup_002.fbx", 
                                     &_SceneStorage->AssetStorage);
        Test->Transform.Scale = V3(0.01f);*/
        
        /*asset *Test2 = QueueAssetLoad(RenderCommands,
                                      "assets\\adamHead\\adamHead.gltf",
                                      &_SceneStorage->AssetStorage);
        Test2->Transform.Scale = V3(0.3f);
        Test2->Transform.Position = V3(0.0f, 0.8f, 0.0f);*/
        
        asset *Test = QueueAssetLoad(RenderCommands, 
                                     "assets\\sponza\\NewSponza_Main_glTF_002.gltf", 
                                     &_SceneStorage->AssetStorage);
        
        asset *Test1 = QueueAssetLoad(RenderCommands, 
                                      "assets\\sponza_curtains\\NewSponza_Curtains_glTF.gltf", 
                                      &_SceneStorage->AssetStorage);
        
        /*asset *Nanosuit = QueueAssetLoad(RenderCommands, 
                                         "assets/nanosuit/nanosuit.obj", 
                                         &_SceneStorage->AssetStorage);
        Nanosuit->Transform.Scale = V3(0.1f);*/
        
        /*point_light *PointLight = PushPointLight(&_SceneStorage->PointLightsStorage);
        InitPointLight(PointLight);
        PointLight->Position = V4(0.0f, 1.0f, -1.5f, 1.0f);*/
        
        /*point_light *PointLight2 = PushPointLight(RenderCommands, &_SceneStorage->PointLightsStorage);
        InitPointLight(PointLight2);
        PointLight2->Position = V4(0.0f, 1.5f, 0.5f, 1.0f);*/
        
        //INTROSPECTION TEST
        /*for(u32 Index = 0;
            Index < ArrayCount(GetIntrospectionData(asset));
            ++Index)
        {
            struct_member_info MemberInfo = GetIntrospectionData(asset)[Index];
            switch(MemberInfo.Type)
            {
                case MetaType_u32:
                {
                    u32 Offset = MemberInfo.Offset;
                    u32 *Value = (u32 *)((ump)Nanosuit + Offset);
                    
                    char Buffer[256];
                    sprintf_s(Buffer, "Type: u32 | Identifier: %s | Value: %d \n", MemberInfo.Name, *Value);
                    
                    OutputDebugStringA(Buffer);
                } break;
            }
        }*/
        
        
        /*asset *Test3 = QueueAssetLoad(RenderCommands, 
                                      "assets\\woman\\Girl_1.obj", 
                                      &_SceneStorage->AssetStorage);*/
        
        /*asset *Test3 = QueueAssetLoad(RenderCommands, 
                                      "assets\\backpack\\backpack.obj", 
                                      &_SceneStorage->AssetStorage);*/
        
        RenderCommands->CurrentSettings.EnableVSync = true;
        RenderCommands->CurrentSettings.EnableGammaCorrection = true;
    }
    
    memory_arena *GameArena = &GameState->Arena;
    SceneStorage = &GameState->SceneStorage;
    SceneCamera = &GameState->SceneCamera;
    
    ManageAssetsStorage(RenderCommands, SceneStorage, GameMemory->LowPriorityQueue);
    ManageRenderTexturesStorage(RenderCommands, &SceneStorage->RenderTexturesStorage, GameMemory->LowPriorityQueue, DeltaTime);
    
    ProcessInput(RenderCommands, &GameInput, SceneCamera, DeltaTime);
    
    // HOT RELOAD TEST
    //RenderCommands->CurrentSettings.EnableVSync = false;
    
    /*asset *Test = &SceneStorage->AssetStorage.Assets[1];
    SetAssetScale(Test, V3(0.3f));*/
    
    point_light *Light = &SceneStorage->PointLightsStorage.PointLights[0];
    //Light->Position = V4(0.0f, 1.0f, -1.5f, 1.0f);
    //Light->UpdateRequired = true;
    //Light->Diffuse = V4(1.0f);
    
    
    f32 LightSpeed = 10.0f;
    if(GameInput.KeyboardKeys[LC_KEY_R])
    {
        Light->Position.x -= LightSpeed * (f32)DeltaTime;
    }
    
    if(GameInput.KeyboardKeys[LC_KEY_T])
    {
        Light->Position.x += LightSpeed * (f32)DeltaTime;
    }
    
    if(GameInput.KeyboardKeys[LC_KEY_F])
    {
        Light->Position.z -= LightSpeed * (f32)DeltaTime;
    }
    
    if(GameInput.KeyboardKeys[LC_KEY_G])
    {
        Light->Position.z += LightSpeed * (f32)DeltaTime;
    }
    
    if(GameInput.KeyboardKeys[LC_KEY_V])
    {
        Light->Position.y -= LightSpeed * (f32)DeltaTime;
    }
    
    if(GameInput.KeyboardKeys[LC_KEY_B])
    {
        Light->Position.y += LightSpeed * (f32)DeltaTime;
    }
    
    UpdateAssetTransforms(SceneStorage, GameArena);
    UpdatePointLights(RenderCommands, &SceneStorage->PointLightsStorage);
    
    render_pass ForwardRenderPass = BeginRenderPass(RenderCommands, RenderPassType_ForwardPass, RenderPass_OffscreenRender);
    ViewFromCamera(RenderCommands, SceneCamera);
    for(u32 AssetIndex = 0;
        AssetIndex < SceneStorage->AssetStorage.Used;
        ++AssetIndex)
    {
        asset *Asset = &SceneStorage->AssetStorage.Assets[AssetIndex];
        if(Asset->Status == RenderStatus_Available)
        {
            PushAssetRender(RenderCommands, Asset, &SceneStorage->MaterialStorage);
        }
    }
    EndRenderPass(&ForwardRenderPass);
    
    render_pass PostprocessRenderPass = BeginRenderPass(RenderCommands, RenderPassType_Postprocess);
    EndRenderPass(&PostprocessRenderPass);
    
    END_TIMED_BLOCK();
}