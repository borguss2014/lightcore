internal void
SkipWhitespace(char **At)
{
    while(**At == ' ' ||
          **At == '\n' ||
          **At == '\r')
    {
        (*At)++;
    }
}

internal void
SkipToChar(char Character, char **At, char *EndOfFile)
{
    while(**At != Character && (EndOfFile - *At) > 0)
    {
        (*At)++;
    }
}

internal u32
StringLength(char *String)
{
    u32 Length = 0;
    
    char *At = String;
    if(String)
    {
        for(;;)
        {
            if(*At == '\0')
            {
                break;
            }
            
            ++Length;
            ++At;
        }
    }
    
    return(Length);
}

internal void
CopyString(char *Dest, mmsz DestSize, char *Src)
{
    Assert(Dest);
    Assert(Src);
    
    memcpy_s(Dest, DestSize, Src, DestSize);
}

internal void
CopyString(char *Dest, mmsz DestSize, string *Src)
{
    Assert(Dest);
    Assert(Src->Count <= DestSize && DestSize);
    
    memcpy_s(Dest, DestSize, Src->Data, Src->Count);
}

internal void
CopyString(string *A, char *B, memory_size Length)
{
    u32 BLength = StringLength(B);
    Assert(BLength);
    
    if(!A->Data)
    {
        A->Data = (char *)malloc(Length);
        A->Count = Length;
    }
    memcpy(A->Data, B, A->Count);
}

internal void
CopyString(string *A, string *B, memory_size Length)
{
    Assert(B->Count);
    
    if(!A->Data)
    {
        A->Data = (char *)malloc(Length);
        A->Count = Length;
    }
    memcpy(A->Data, B->Data, A->Count);
}

internal void
CopyString(string *A, char *B)
{
    u32 BLength = StringLength(B);
    
    if(!A->Data)
    {
        A->Data = (char *)malloc(BLength);
        A->Count = BLength;
    }
    memcpy(A->Data, B, A->Count);
}

internal void
CopyString(string *A, string *B)
{
    if(!A->Data)
    {
        A->Data = (char *)malloc(B->Count);
        A->Count = B->Count;
    }
    memcpy(A->Data, B->Data, A->Count);
}

internal void
ConcatenateStrings(char *SourceA, memory_size SourceASize,
                   char *SourceB, memory_size SourceBSize,
                   char *Dest, memory_size DestSize,
                   b32 NullTerminate = false)
{
    if((SourceASize + SourceBSize) <= DestSize)
    {
        memcpy_s(Dest, DestSize, SourceA, SourceASize);
        memcpy_s(Dest + SourceASize, DestSize, SourceB, SourceBSize);
    }
    
    if(NullTerminate)
    {
        char *OnePastDest = Dest + DestSize - 1;
        *OnePastDest = 0;
    }
}

internal char *
ConcatenateStrings(char *StringA, char *StringB)
{
    char *Result = 0;
    u32 StringALength = StringLength(StringA);
    u32 StringBLength = StringLength(StringB);
    u32 TotalStringLength = StringALength + StringBLength + 1;
    
    if(StringA && StringB)
    {
        Result = (char *)malloc(TotalStringLength);
        ConcatenateStrings(StringA, StringALength, StringB, StringBLength, Result, TotalStringLength);
    }
    
    return(Result);
}

internal b32
StringsEqual(memory_size ALength, char *A, char *B)
{
    b32 Result = false;
    
    if(B)
    {
        char *At = B;
        for(u32 CharIndex = 0;
            CharIndex < ALength;
            ++CharIndex, ++At)
        {
            if(A[CharIndex] != *At ||
               *At == '\0')
            {
                return(false);
            }
        }
        
        Result = (*At == '\0');
    }
    else
    {
        Result = (ALength == 0);
    }
    
    return(Result);
}

internal b32
StringsEqual(char *A, char *B)
{
    b32 Result = StringsEqual(StringLength(A), A, B);
    return(Result);
}

internal b32
StringsEqual(memory_size ALength, char *A, memory_size BLength, char *B)
{
    b32 Result = (ALength == BLength);
    
    if(Result)
    {
        for(u32 Index = 0;
            Index < ALength;
            ++Index)
        {
            if(A[Index] != B[Index])
            {
                Result = false;
                break;
            }
        }
    }
    
    return(Result);
}

internal b32
StringsEqual(string A, string B)
{
    b32 Result = StringsEqual(A.Count, A.Data, B.Count, B.Data);
    return(Result);
}

internal b32
StringsEqual(string A, char *B)
{
    b32 Result = StringsEqual(A.Count, A.Data, B);
    return(Result);
}

#define BundleZ(_string) BundleString(sizeof(_string), _string)

internal string
BundleString(memory_size Count, char *String)
{
    string Result = {};
    Result.Data = String;
    Result.Count = Count - 1;
    
    return(Result);
}

internal string
GetParentDirectoryName(char *Path)
{
    string Result = {};
    
    Assert(Path);
    
    u32 Length = StringLength(Path);
    
    u32 Index = Length - 1;
    char CurrentChar = Path[Index];
    while(Index > 0)
    {
        if(CurrentChar == '\\' ||
           CurrentChar == '/')
        {
            break;
        }
        
        Index--;
        CurrentChar = Path[Index];
    }
    
    Result.Data = Path;
    Result.Count = Index + 1;
    
    return(Result);
}

internal void
GetParentDirectoryName(char *Path, string *Dest)
{
    Assert(Path);
    
    string Result = {};
    
    u32 Length = StringLength(Path);
    
    u32 Index = Length - 1;
    char CurrentChar = Path[Index];
    while(Index > 0)
    {
        if(CurrentChar == '\\' ||
           CurrentChar == '/')
        {
            break;
        }
        
        Index--;
        CurrentChar = Path[Index];
    }
    
    CopyString(&Result, Path, Index);
}

internal b32
MemoryIsEqual(mmsz Count, void *AInit, void *BInit)
{
    u8 *A = (u8 *)AInit;
    u8 *B = (u8 *)BInit;
    
    while(Count--)
    {
        if(*A++ != *B++)
        {
            return false;
        }
    }
    
    return true;
}

internal b32
IsEndOfLine(char C)
{
    b32 Result = (C == '\n' || C == '\r');
    return(Result);
}

internal b32
IsWhitespace(char C)
{
    b32 Result = (C == '\t' || C == '\v' ||
                  C == '\f' || C == ' ');
    
    return(Result);
}

internal b32
IsAlpha(char C)
{
    b32 Result = (((C >= 'a') && (C <= 'z')) ||
                  ((C >= 'A') && (C <= 'Z')));
    
    return(Result);
}

// TODO(Cristian): Incomplete
internal b32
IsNumber(char C)
{
    b32 Result = ((C >= '0') && (C <= '9'));
    return(Result);
}

internal f32
GetMaximum(f32 A, f32 B)
{
    f32 Result = A > B ? A : B;
    return(Result);
}

internal f32
GetMinimum(f32 A, f32 B)
{
    f32 Result = A < B ? A : B;
    return(Result);
}

internal u32
GetAddressLow(u64 Address)
{
    return((u32)(Address & 0xFFFFFFFF));
}

internal u32
GetAddressHigh(u64 Address)
{
    return((u32)(Address >> 32));
}

// NOTE(Cristian): u32 basically already sets an upper limit to around 4 billion
// NOTE(Cristian): This is a very slow and naive implementation of string hashing!!!!
internal u32
HashString(string String, u32 UpperIndexLimit)
{
    u32 Size = UpperIndexLimit;
    u32 Pow2Alignment = 2;
    u32 AlignmentOffset = 0;
    
    u32 AlignmentMask = Pow2Alignment - 1;
    u32 BitOpResult = Size & AlignmentMask;
    if (BitOpResult)
    {
        AlignmentOffset = Pow2Alignment - BitOpResult;
    }
    u32 AlignedSize = Size + AlignmentOffset;
    
    u32 Accumulator = 0;
    for(u32 Index = 0;
        Index < String.Count;
        ++Index)
    {
        Accumulator += ((u32)String.Data[Index] * (Index + 1));
    }
    
    u32 HashIndex = Accumulator & (AlignedSize - 1);
    if(HashIndex >= UpperIndexLimit)
    {
        HashIndex = UpperIndexLimit - 1;
    }
    return(HashIndex);
}

internal u32
HashDjb2(char *String, u32 UpperIndexLimit)
{
    u32 Size = UpperIndexLimit;
    u32 Pow2Alignment = 2;
    u32 AlignmentOffset = 0;
    
    u32 AlignmentMask = Pow2Alignment - 1;
    u32 BitOpResult = Size & AlignmentMask;
    if (BitOpResult)
    {
        AlignmentOffset = Pow2Alignment - BitOpResult;
    }
    u32 AlignedSize = Size + AlignmentOffset;
    
    
    u32 Hash = 5381;
    s32 Character = *String++;
    while(Character)
    {
        Hash = ((Hash << 5) + Hash) + Character; /* hash * 33 + c */
        Character = *String++;
    }
    
    u32 HashIndex = Hash & (AlignedSize - 1);
    if(HashIndex >= UpperIndexLimit)
    {
        HashIndex = UpperIndexLimit - 1;
    }
    return(HashIndex);
}

internal string_iterator
InitStringIterator(char *String)
{
    Assert(String);
    
    string_iterator Result = {};
    Result.Position = String;
    
    return(Result);
}

internal string_iterator
InitStringIterator(string *String)
{
    Assert(String);
    
    string_iterator Result = {};
    Result.Position = String->Data;
    
    return(Result);
}

inline void
AdvanceIterator(string_iterator *Iterator)
{
    Assert(Iterator);
    
    if(Iterator->Position)
    {
        Iterator->Position += 1;
    }
}

internal string
NextSubstring(string_iterator *Iterator, char TargetChar = 0)
{
    Assert(Iterator);
    
    string Result = {};
    
    char *InitialPos = Iterator->Position;
    
    while(Iterator->Position)
    {
        char C = (*Iterator->Position);
        if(C == TargetChar)
        {
            break;
        }
        
        AdvanceIterator(Iterator);
    }
    AdvanceIterator(Iterator);
    
    Result.Data = InitialPos;
    Result.Count = (Iterator->Position - InitialPos) - 1;
    return(Result);
}

inline u32
RandomGenerator(u32 Seed)
{
    Seed = (Seed << 13) ^ Seed;
    return((Seed * (Seed * 15731 + 789221) + 1376312589) & 0x7fffffff);
}

internal string
GetString(char *InputString)
{
    string Result = {};
    Result.Data = InputString;
    Result.Count = StringLength(InputString);
    return(Result);
}

inline b32
EmptyString(string S)
{
    b32 Result = !S.Data && (S.Count == 0);
    return(Result);
}