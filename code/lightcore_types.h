#include "win32_lightcore_keycodes.h"

#if !defined(COMPILER_MSVC)
#define COMPILER_MSVC 0
#endif

#if !COMPILER_MSVC
#if _MSC_VER
#undef COMPILER_MSVC
#define COMPILER_MSVC 1
#endif
#endif

#if COMPILER_MSVC
#include <intrin.h>
#endif

#include <stdint.h>

typedef uint8_t  b8;
typedef uint16_t b16;
typedef uint32_t b32;
typedef uint64_t b64;

typedef uint8_t  u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef int8_t   s8;
typedef uint16_t s16;
typedef int32_t  s32;
typedef int64_t  s64;

typedef float  real32;
typedef double real64;

typedef real32 f32;
typedef real32 r32;

typedef real64 f64;
typedef real64 r64;

typedef size_t memory_size;
typedef memory_size mmsz;
typedef uintptr_t ump;
typedef intptr_t smp;

#define PI 3.14159265358979323846
#define TWO_PI 6.28318530718
#define HALF_PI 1.57079632679

#define PI_OVER_HALF_CIRCLE_DEGREES 0.01745329251994329576923690768489f
#define HALF_CIRCLE_DEGREES_OVER_PI 57.295779513082320876798154814105f

#define Kilobytes(Value) (Value * 1024LL)
#define Megabytes(Value) (Kilobytes(Value) * 1024LL)
#define Gigabytes(Value) (Megabytes(Value) * 1024LL)
#define Terabytes(Value) (Gigabytes(Value) * 1024LL)

#define internal static
#define local_persist static
#define global_variable static

#define enum32(type) u32

union vec2
{
    struct { f32 x, y; };
    struct { f32 r, g; };
    struct { f32 s, t; };
    f32 Data[2];
};

union vec3
{
    struct { f32 x, y, z; };
    struct { f32 r, g, b; };
    struct { f32 s, t, p; };
    f32 Data[3];
};

union vec4
{
    struct { f32 x, y, z, w; };
    struct { f32 r, g, b, a; };
    struct { f32 s, t, p, q; };
    f32 Data[4];
};

union mat2x2
{
    struct
    {
        vec2 Row0;
        vec2 Row1;
    };
    struct
    {
        f32 x0, y0;
        f32 x1, y1;
    };
    f32 Data[2][2];
    f32 DataL[4];
};

union mat3x3
{
    struct
    {
        vec3 Row0;
        vec3 Row1;
        vec3 Row2;
    };
    struct
    {
        f32 x0, y0, z0;
        f32 x1, y1, z1;
        f32 x2, y2, z2;
    };
    f32 Data[3][3];
    f32 DataL[9];
};

union mat4x4
{
    struct
    {
        vec4 Row0;
        vec4 Row1;
        vec4 Row2;
        vec4 Row3;
    };
    struct 
    {
        f32 x0, y0, z0, w0;
        f32 x1, y1, z1, w1;
        f32 x2, y2, z2, w2;
        f32 x3, y3, z3, w3;
    };
    f32 Data[4][4];
    f32 DataL[16];
};

typedef vec2 v2;
typedef vec3 v3;
typedef vec4 v4;

union quaternion
{
    struct { f32 x, y, z, w; };
    f32 Data[4];
};

union plane
{
    struct
    {
        r32 A, B, C, D;
    };
    r32 E[4];
};

struct buffer
{
    char *Data;
    memory_size Count;
};

typedef buffer string;

struct string_iterator
{
    char *Position;
};

struct ticket_mutex
{
    u64 volatile CurrentTicket;
    u64 volatile ServedTicket;
};

#if COMPILER_MSVC
#define READ_BARRIER _ReadBarrier()
#define WRITE_BARRIER _WriteBarrier()

inline u64
AtomicExchangeU64(u64 volatile *Value, u64 New)
{
    u64 Result = _InterlockedExchange64((__int64 volatile *)Value, New);
    return(Result);
}

inline u32
AtomicExchangeU32(u32 volatile *Value, u32 New)
{
    u32 Result = _InterlockedExchange((long volatile *)Value, New);
    return(Result);
}

inline u64
AtomicAddU64(u64 volatile *Value, u64 Addend)
{
    u64 Result = _InterlockedExchangeAdd64((__int64 volatile *)Value, Addend);
    return(Result);
}

inline u32
AtomicAddU32(u32 volatile *Value, u32 Addend)
{
    u32 Result = _InterlockedExchangeAdd((long volatile *)Value, Addend);
    return(Result);
}

inline u64
AtomicCompareExchangeU64(u64 volatile *Destination, u64 Exchange, u64 Comparand)
{
    u64 Result = _InterlockedCompareExchange64((LONG64 volatile *)Destination, Exchange, Comparand);
    return(Result);
}

inline u32
AtomicCompareExchangeU32(u32 volatile *Destination, u32 Exchange, u32 Comparand)
{
    u32 Result = _InterlockedCompareExchange((LONG volatile *)Destination, Exchange, Comparand);
    return(Result);
}

inline u64
AtomicIncrement(u64 volatile *Addend)
{
    u64 Result = InterlockedIncrement64((LONG64 volatile *) Addend);
    return(Result);
}

inline u32
AtomicIncrement(u32 volatile *Addend)
{
    u32 Result = InterlockedIncrement((LONG volatile *) Addend);
    return(Result);
}

inline u64
AtomicDecrement(u64 volatile *Addend)
{
    u64 Result = InterlockedDecrement64((LONG64 volatile *) Addend);
    return(Result);
}

inline u32
AtomicDecrement(u32 volatile *Addend)
{
    u32 Result = InterlockedDecrement((LONG volatile *) Addend);
    return(Result);
}

inline u32
GetThreadID()
{
    ump ThreadLocalStorage = (ump)__readgsqword(0x30);
    u32 ThreadID = *(u32 *)(ThreadLocalStorage + 0x48);
    return(ThreadID);
}

#endif

inline void
TicketMutexLock(ticket_mutex *Mutex)
{
    u64 TicketNumber = AtomicAddU64(&Mutex->CurrentTicket, 1);
    while (TicketNumber != Mutex->ServedTicket); // TODO(Cristian): _mm_pause here?
}

inline void
TicketMutexUnlock(ticket_mutex *Mutex)
{
    AtomicAddU64(&Mutex->ServedTicket, 1);
}