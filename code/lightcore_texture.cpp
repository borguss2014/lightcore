struct texture_data_info
{
    s32 Width;
    s32 Height;
    s32 ColorChannels;
    mmsz Size;
};

struct texture_data
{
    u8 *Data;
    texture_data_info Info;
};

internal render_texture *
PushTextureToStorage(textures_storage *Storage)
{
    Assert(Storage);
    
    render_texture *Result = 0;
    
    if(Storage->Used < Storage->Capacity)
    {
        Result = &Storage->Textures[Storage->Used++];
    }
    
    return(Result);
}

internal void *
PushTextureDataBlock(ring_buffer *TexturesTransientStorage, mmsz Size)
{
    void *Result = 0;
    
    void *BufferBlock = PushRingBufferBlock(TexturesTransientStorage, Size);
    if(BufferBlock)
    {
        ringbuffer_block_header *Header = (ringbuffer_block_header *)BufferBlock - 1;
        Header->Flags = BufferBlock_Reserved;
        
        Result = BufferBlock;
    }
    
    return(Result);
}

internal void
GetTextureFileInfo(char *Path, texture_data *TextureData)
{
    stbi_info(Path, &TextureData->Info.Width, &TextureData->Info.Height, &TextureData->Info.ColorChannels);
    TextureData->Info.Size = TextureData->Info.Width * TextureData->Info.Height * TextureData->Info.ColorChannels;
}

internal void
GetTextureFileData(char *Path, texture_data *TextureData)
{
    s32 Width = 0;
    s32 Height = 0;
    s32 ColorChannels = 0;
    TextureData->Data = stbi_load(Path, &Width, &Height, &ColorChannels, 0);
}

internal void
DeleteTextureFileData(texture_data *TextureData)
{
    stbi_image_free(TextureData->Data);
}

JOB_QUEUE_ENTRY_CALLBACK(LoadTextureData)
{
    BEGIN_TIMED_BLOCK("Texture file load");
    
    texture_load_entry_data *EntryData = (texture_load_entry_data *)Data;
    if(EntryData)
    {
        char Msg[256];
        sprintf_s(Msg, "Thread %d started job: %s \n", ThreadIndex, EntryData->TextureOp->Texture->Path);
        OutputDebugStringA(Msg);
        
        texture_data TextureData = {};
        GetTextureFileData(EntryData->Path, &TextureData);
        if(TextureData.Data)
        {
            mmsz TextureSize = EntryData->TextureOp->Width * EntryData->TextureOp->Height * EntryData->TextureOp->ColorChannels;
            memcpy_s(EntryData->TextureData.Data, EntryData->TextureData.Count, TextureData.Data, TextureSize);
            //SetRingBufferBlockFlags(EntryData->Dest, BufferBlock_Loaded);
            
            DeleteTextureFileData(&TextureData);
            
            sprintf_s(Msg, "Thread %d loaded texture: %s \n", ThreadIndex, EntryData->TextureOp->Texture->Path);
            OutputDebugStringA(Msg); 
            
            EntryData->TextureOp->Status = TextureOpStatus_UploadAvailable;
        }
        else
        {
            OutputDebugStringA("READING THE TEXTURE DATA HAS FAILED!_+_+_++_+_+_+_+_+_+_++_+\n");
            EntryData->TextureOp->Status = TextureOpStatus_UploadFailed;
            //SetRingBufferBlockFlags(EntryData->Dest, TextureBlock_Delete);
        }
    }
    
    END_TIMED_BLOCK();
}

internal buffer
PushTextureDataBlock(pool_allocator *Allocator, mmsz RequestedSize)
{
    buffer Result = {};
    Result.Data = (char *)PushAllocationBlock(Allocator, RequestedSize);
    Result.Count = RequestedSize;
    return(Result);
}

internal void
ClearTextureDataBlock(render_texture *Texture, pool_allocator *MemoryPool)
{
    BEGIN_TIMED_BLOCK("ClearTextureDataBlock");
    
    b32 ValidBlock = VerifyBlockIntegrity(Texture->RawData.Data, MemoryPool);
    if(ValidBlock)
    {
        ClearAllocationBlock(Texture->RawData.Data, MemoryPool);
        Texture->RawData = {};
    }
    else
    {
        OutputDebugStringA("[INTEGRITY_CHECK_FAIL] Couldn't clear data block for texture ");
        OutputDebugStringA(Texture->Path);
        OutputDebugStringA("\n");
    }
    
    END_TIMED_BLOCK();
}

internal void
AttemptTextureLoad(texture_op_queue *TextureOpQueue, 
                   render_texture *Texture, 
                   textures_storage *Storage,
                   platform_job_queue *JobQueue,
                   texture_parameters Parameters = DefaultTextureParameters())
{
    if(Texture->Path)
    {
        texture_data TextureData = {};
        GetTextureFileInfo(Texture->Path, &TextureData);
        
        mmsz TextureDataSize = TextureData.Info.Size;
        if(TextureDataSize)
        {
            Texture->RawData = PushTextureDataBlock(&Storage->MemoryPool, TextureDataSize);
            if(Texture->RawData.Data)
            {
                Texture->Status = RenderStatus_InTransit;
                
                texture_op *TextureOp = PushTextureOperation(TextureOpQueue);
                if(TextureOp)
                {
                    TextureOp->Status = TextureOpStatus_PendingUpload;
                    
                    TextureOp->Texture = Texture;
                    TextureOp->TextureData = Texture->RawData;
                    
                    TextureOp->Width = (u32)TextureData.Info.Width;
                    TextureOp->Height = (u32)TextureData.Info.Height;
                    TextureOp->ColorChannels = (u32)TextureData.Info.ColorChannels;
                    
                    TextureOp->MipmapLevels = 8;
                    TextureOp->TextureParameters = Parameters;
                    
                    if(Storage->NextLoadEntry <= ArrayCount(Storage->LoadEntries))
                    {
                        char Msg[256];
                        sprintf_s(Msg, "[MAIN_THREAD] Queued texture: %s \n", Texture->Path);
                        OutputDebugStringA(Msg);
                        
                        texture_load_entry_data *EntryData = Storage->LoadEntries + Storage->NextLoadEntry++;
                        EntryData->Path = Texture->Path;
                        EntryData->TextureOp = TextureOp;
                        EntryData->TextureData = Texture->RawData;
                        Platform.JobQueueAddEntry(JobQueue, LoadTextureData, EntryData, 0);
                    }
                    else
                    {
                        // TODO(Cristian): Need a ring buffer for entries
                        TextureOp->Status = TextureOpStatus_Ignored;
                        
                        ClearTextureDataBlock(Texture, &Storage->MemoryPool);
                        Texture->Status = RenderStatus_LoadToStorage;
                    }
                }
                else
                {
                    //SetRingBufferBlockFlags(DataBlock, BufferBlock_Delete);
                    ClearTextureDataBlock(Texture, &Storage->MemoryPool);
                    Texture->Status = RenderStatus_LoadToStorage;
                }
            }
        }
        else
        {
            Texture->Status = RenderStatus_FailedToLoad;
        }
    }
}

internal render_texture *
LoadTextureFile(game_render_commands *Commands,
                char *Path,
                render_texture_type Type,
                textures_storage *Storage, 
                platform_job_queue *JobQueue)
{
    render_texture *Texture = PushTextureToStorage(Storage);
    Texture->Type = Type;
    CopyString(Texture->Path, ArrayCount(Texture->Path), Path);
    Texture->FileInfo = Platform.GetFileInfo(Texture->Path);
    
    AttemptTextureLoad(&Commands->TextureOpQueue, Texture, Storage, JobQueue);
    
    return(Texture);
}

internal void
ManageRenderTexturesStorage(game_render_commands *Commands, 
                            textures_storage *Storage,
                            platform_job_queue *JobQueue,
                            r64 DeltaTime)
{
    BEGIN_TIMED_BLOCK("Textures management");
    
    b32 DoReloadCheck = false;
    Storage->ElapsedTime += (DeltaTime * 1000.0);
    if(Storage->ElapsedTime >= 1000)
    {
        DoReloadCheck = true;
        Storage->ElapsedTime = 0;
    }
    
    BEGIN_TIMED_BLOCK("Texture data block clear");
    u32 TexturesCleaned = 0;
    for(u32 TextureIndex = 0;
        TextureIndex < Storage->Used;
        ++TextureIndex)
    {
        render_texture *Texture = &Storage->Textures[TextureIndex];
        
        if(Texture->Status == RenderStatus_FailedToLoad || 
           Texture->Status == RenderStatus_Available)
        {
            if(Texture->RawData.Data && Texture->RawData.Count)
            {
                ClearTextureDataBlock(Texture, &Storage->MemoryPool);
                TexturesCleaned++;
            }
        }
    }
    END_TIMED_BLOCK();
    
    if(TexturesCleaned)
    {
        char Buffer[256];
        sprintf_s(Buffer, "===== TexturesCleaned: %u this frame! ====== \n", TexturesCleaned);
        OutputDebugStringA(Buffer);
    }
    
    mmsz MaxSize = Storage->MemoryPool.ChunksTotal * Storage->MemoryPool.ChunkSize;
    for(u32 TextureIndex = 0;
        TextureIndex < Storage->Used;
        ++TextureIndex)
    {
        if(Storage->MemoryPool.Used == MaxSize)
        {
            break;
        }
        
        render_texture *Texture = &Storage->Textures[TextureIndex];
        if(Texture->Status == RenderStatus_LoadToStorage)
        {
            AttemptTextureLoad(&Commands->TextureOpQueue, Texture, Storage, JobQueue);
        }
    }
    
    if(DoReloadCheck)
    {
        BEGIN_TIMED_BLOCK("Textures reload check");
        
        for(u32 TextureIndex = 0;
            TextureIndex < Storage->Used;
            ++TextureIndex)
        {
            render_texture *Texture = &Storage->Textures[TextureIndex];
            
            platform_file_info CurrentFileInfo = Platform.GetFileInfo(Texture->Path);
            
            if(Texture->FileInfo.LastWriteTime != CurrentFileInfo.LastWriteTime)
            {
                OutputDebugStringA("File write time reported as changing: ");
                OutputDebugStringA(Texture->Path);
                OutputDebugStringA("\n");
                
                AttemptTextureLoad(&Commands->TextureOpQueue, Texture, Storage, JobQueue);
                
                OutputDebugStringA("Reloaded texture file ");
                OutputDebugStringA(Texture->Path);
                OutputDebugStringA("\n");
            }
            
            Texture->FileInfo = CurrentFileInfo;
        }
        
        END_TIMED_BLOCK();
    }
    
    END_TIMED_BLOCK();
}

internal render_texture *
GetTextureFromStorage(string Path, hash_table *HashTable, textures_storage *TexturesStorage, memory_arena *CacheArena)
{
    render_texture *Result = 0;
    
    u32 HashIndex = HashString(Path, ArrayCount(HashTable->HashSlots));
    hash_slot *HashSlot = &HashTable->HashSlots[HashIndex];
    
    if(HashSlot->CachedTexture && StringsEqual(Path, HashSlot->CachedTexture->Path))
    {
        Result = HashSlot->CachedTexture;
        
        char Msg[512];
        sprintf_s(Msg, "    Supplying cached texture: %s ... [PATH_COLLISION]\n", HashSlot->CachedTexture->Path);
        OutputDebugStringA(Msg);
    }
    else
    {
        hash_slot *CurrentHashSlotLink = HashSlot->NextHashSlot;
        hash_slot **LastHashSlotLink = &HashSlot->NextHashSlot;
        while(CurrentHashSlotLink)
        {
            if(CurrentHashSlotLink->CachedTexture && StringsEqual(Path, CurrentHashSlotLink->CachedTexture->Path))
            {
                Result = CurrentHashSlotLink->CachedTexture;
                
                char Msg[512];
                sprintf_s(Msg, "    Supplying cached texture: %s ... [PATH_COLLISION_EXTERNAL_LINKAGE]\n", CurrentHashSlotLink->CachedTexture->Path);
                OutputDebugStringA(Msg);
                
                break;
            }
            
            if(CurrentHashSlotLink->NextHashSlot)
            {
                LastHashSlotLink = &CurrentHashSlotLink->NextHashSlot;
            }
            
            CurrentHashSlotLink = CurrentHashSlotLink->NextHashSlot;
        }
        
        if(!CurrentHashSlotLink)
        {
            render_texture *Texture = PushTextureToStorage(TexturesStorage);
            CopyString(Texture->Path, ArrayCount(Texture->Path), &Path);
            Texture->FileInfo = Platform.GetFileInfo(Texture->Path);
            
            if(HashSlot->CachedTexture)
            {
                hash_slot *NewHashSlot = PushStruct(CacheArena, hash_slot);
                NewHashSlot->CachedTexture = Texture;
                
                *LastHashSlotLink = NewHashSlot;
            }
            else
            {
                HashSlot->CachedTexture = Texture;
            }
            
            Result = Texture;
        }
    }
    
    return(Result);
}