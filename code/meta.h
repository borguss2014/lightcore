#ifndef introspect
#define introspect(Type)
#endif

#ifndef counted_array
#define counted_array(CountMember)
#endif

#ifndef OFFSET_OF
#define OFFSET_OF(Type, Member) ((size_t)&(((Type *)0)->Member))
#endif

#define _JOIN(X, Y) X##Y
#define JOIN(X, Y) _JOIN(X, Y)

#define GetIntrospectionData(type) MembersOf_##type

enum meta_type; // Necessary in case 'meta_type' is never generated due to no introspection

#include "meta_generated.h"

enum
{
    MetaFlag_IsPointer = 0,
    MetaFlag_IsLCArray,
    MetaFlag_IsArray
};

struct struct_member_info
{
    u32 Flags;
    u32 Counter;
    meta_type Type;
    char *Name;
    char *ArrayCount;
    u32 Offset;
};

struct enum_member_info
{
    u32 Type;
    char *Name;
};