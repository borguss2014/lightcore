internal void
PushSetup(render_pass *RenderPass, render_setup *Settings)
{
    RenderPass->Commands->CurrentSettings = *Settings;
}

internal render_entry_header *
PushRenderBuffer(game_render_commands *RenderCommands, mmsz Size)
{
    render_entry_header *Result = 0;
    u8 *PushBufferEnd = RenderCommands->PushBufferBase + RenderCommands->PushBufferSize;
    if((RenderCommands->PushBufferAt + Size) <= PushBufferEnd)
    {
        Result = (render_entry_header *)RenderCommands->PushBufferAt;
        RenderCommands->PushBufferAt += Size;
    }
    else
    {
        InvalidCodePath;
    }
    
    return(Result);
}

#define PushRenderEntry(Commands, Type) (Type *)PushRenderEntry_(Commands, sizeof(Type), RenderEntryType_##Type)
internal void *
PushRenderEntry_(game_render_commands *RenderCommands, mmsz Size, render_entry_type Type)
{
    Size += sizeof(render_entry_header);
    void *Result = 0;
    render_entry_header *Header = PushRenderBuffer(RenderCommands, Size);
    if(Header)
    {
        Header->Type = Type;
        Result = (u8 *)Header + sizeof(render_entry_header);
    }
    return(Result);
}

internal void
PushForwardPassBegin(render_pass *RenderPass)
{
    render_entry_forwardpass_begin *Entry = PushRenderEntry(RenderPass->Commands, render_entry_forwardpass_begin);
    if(Entry)
    {
        if(!(RenderPass->Flags & RenderPass_DefaultFramebuffer))
        {
            Entry->OffscreenRender = true;
        }
        else
        {
            Entry->OffscreenRender = false;
        }
    }
}

internal void
PushPostprocessPassBegin(render_pass *RenderPass)
{
    render_entry_postprocesspass_begin *Entry = PushRenderEntry(RenderPass->Commands, render_entry_postprocesspass_begin);
    if(Entry)
    {
        
    }
}

internal void
PushClearColor(render_pass *RenderPass, v4 ClearColor = V4(0))
{
    render_entry_clear_color *ClearEntry = PushRenderEntry(RenderPass->Commands, render_entry_clear_color);
    if(ClearEntry)
    {
        ClearEntry->Color = ClearColor;
    }
}

internal void
PushClearDepth(render_pass *RenderPass)
{
    PushRenderEntry_(RenderPass->Commands, 0, RenderEntryType_render_entry_clear_depth);
}

internal void
PushFullClear(render_pass *RenderPass, v4 ClearColor = V4(0))
{
    render_entry_clear_color *ClearEntry = PushRenderEntry(RenderPass->Commands, render_entry_clear_color);
    if(ClearEntry)
    {
        ClearEntry->Color = ClearColor;
        ClearEntry->FullClear = true;
    }
}

internal void
PushVertexData(game_render_commands *Commands, 
               u32 BufferID, 
               void *Data,
               mmsz VerticesSize,
               mmsz IndicesSize,
               u32 *Flags = 0)
{
    render_entry_upload_vertex_buffer_data *Entry = PushRenderEntry(Commands, render_entry_upload_vertex_buffer_data);
    if(Entry)
    {
        Entry->BufferID = BufferID;
        Entry->Data = Data;
        Entry->VerticesSize = VerticesSize;
        Entry->IndicesSize = IndicesSize;
        Entry->Flags = Flags;
    }
}

internal void
PushBufferData(game_render_commands *Commands, 
               u32 BufferID,
               u32 BufferLocation, 
               smp Offset,
               void *Data,
               mmsz Size, 
               b32 LargeBuffer = false)
{
    render_entry_upload_buffer_data *Entry = PushRenderEntry(Commands, render_entry_upload_buffer_data);
    if(Entry)
    {
        Entry->LargeBuffer = LargeBuffer;
        Entry->BufferID = BufferID;
        Entry->BufferLocation = BufferLocation;
        Entry->Offset = Offset;
        Entry->Data = Data;
        Entry->Size = Size;
    }
}

internal u32
PushBufferObjectID(game_render_commands *Commands)
{ 
    u32 Result = 0;
    if(Commands->NextBufferID < Commands->BufferObjectsCount)
    {
        Result = Commands->NextBufferID++;
    }
    return(Result);
}

internal void
PushAssetRender(game_render_commands *RenderCommands, asset *Asset, material_storage *MaterialStorage)
{
    BEGIN_TIMED_BLOCK("PushAssetRender");
    
    mesh *CurrentMesh = Asset->Meshes.Head;
    if(CurrentMesh)
    {
        render_entry_setup_asset *SetupEntry = PushRenderEntry(RenderCommands, render_entry_setup_asset);
        if(SetupEntry)
        {
            SetupEntry->BufferID = Asset->BufferID;
            SetupEntry->IndexedBuffer = (Asset->IndicesCount > 0) ? true : false;
            
            for(u32 Index = 0;
                Index < Asset->Meshes.TotalMeshes;
                ++Index)
            {
                material *Material = &MaterialStorage->Materials[CurrentMesh->MaterialID];
                
                // TODO(Cristian): Remove this in the future! This is only used because there's no alpha blending
                if(Material->Opacity < 1.0f)
                {
                    CurrentMesh = CurrentMesh->Next;
                    continue;
                }
                
                render_entry_mesh_draw *Entry = PushRenderEntry(RenderCommands, render_entry_mesh_draw);
                if(Entry)
                {
                    Entry->VerticesOffset = CurrentMesh->VerticesOffset;
                    Entry->VerticesCount = CurrentMesh->VerticesCount;
                    
                    Entry->IndicesOffset = CurrentMesh->IndicesOffset;
                    Entry->IndicesCount = CurrentMesh->IndicesCount;
                    
                    Entry->Material = Material;
                    Entry->ModelMatrix = CurrentMesh->WorldMatrix;
                }
                
                CurrentMesh = CurrentMesh->Next;
            }
        }
    }
    
    END_TIMED_BLOCK();
}

internal void
PushAssetRenderIndexed(render_pass *RenderPass, asset *Asset, scene_storage *Storage)
{
    render_entry_asset_draw_indexed *Entry = PushRenderEntry(RenderPass->Commands, render_entry_asset_draw_indexed);
    if(Entry)
    {
        u32 NextIndex = 0;
        
        mesh *NextMesh = Asset->Meshes.Head;
        for(u32 MeshIndex = 0;
            MeshIndex< Asset->Meshes.TotalMeshes;
            ++MeshIndex)
        {
            material *Material = &Storage->MaterialStorage.Materials[NextMesh->MaterialID];
            Entry->DiffuseTextureID = Material->Textures.Diffuse->ID;
            
            Entry->Counts[NextIndex] = NextMesh->IndicesCount;
            Entry->Offsets[NextIndex] = NextMesh->IndicesOffset;
            NextIndex++;
            
            NextMesh = NextMesh->Next;
        }
        
        Entry->Size = NextIndex;
        Entry->BufferID = Asset->BufferID;
        Entry->RenderPassType = RenderPass->Type;
    }
}

internal void
PushCameraUpdate(game_render_commands *Commands, camera_matrices CameraTransforms, vec3 CameraPosition)
{
    render_entry_camera_update *Entry = PushRenderEntry(Commands, render_entry_camera_update);
    if(Entry)
    {
        Entry->CameraTransforms = CameraTransforms;
        Entry->CameraPosition = CameraPosition;
    }
}

// NOTE(Cristian): Treats the texture ops queue as a ring buffer
inline void
GetNextOpQueueSlot(texture_op_queue *Queue)
{
    u32 TotalOps = ArrayCount(Queue->Operations);
    Queue->NextFreeOpSlot = ++Queue->NextFreeOpSlot % TotalOps;
}

internal texture_op *
PushTextureOperation(texture_op_queue *Queue)
{
    Assert(Queue);
    
    texture_op *Result = 0;
    
    u32 BeginLinkID = Queue->NextFreeOpSlot;
    
    do
    {
        texture_op *TextureOp = &Queue->Operations[Queue->NextFreeOpSlot];
        if(TextureOp->Status == TextureOpStatus_Uninitialized && !TextureOp->Texture)
        {
            if(!Queue->Head)
            {
                Queue->Head = TextureOp;
                Queue->Tail = TextureOp;
            }
            else
            {
                Queue->Tail->Next = TextureOp;
                Queue->Tail = TextureOp;
            }
            
            Result = TextureOp;
            break;
        }
        GetNextOpQueueSlot(Queue);
    }
    while(Queue->NextFreeOpSlot != BeginLinkID);
    
    if(!Result)
    {
        OutputDebugStringA("ERROR: Cannot push texture op. Backing array full! \n");
    }
    
    return(Result);
}

internal render_pass
BeginRenderPass(game_render_commands *Commands, 
                render_pass_type RenderPassType,
                enum32(render_pass_flags) Flags = RenderPass_Default,
                v4 ClearColor = V4(0))
{
    BEGIN_TIMED_BLOCK("BeginRenderPass");
    
    render_pass RenderPass = {};
    RenderPass.Commands = Commands;
    RenderPass.Type = RenderPassType;
    RenderPass.Flags = Flags;
    
    switch(RenderPassType)
    {
        case RenderPassType_ForwardPass:
        {
            PushForwardPassBegin(&RenderPass);
        } break;
        
        case RenderPassType_Postprocess:
        {
            PushPostprocessPassBegin(&RenderPass);
        } break;
    }
    
    if((Flags & RenderPass_ClearColor) && (Flags & RenderPass_ClearDepth))
    {
        PushFullClear(&RenderPass, ClearColor);
    }
    else if(Flags & RenderPass_ClearColor)
    {
        PushClearColor(&RenderPass, ClearColor);
    }
    else if(Flags & RenderPass_ClearDepth)
    {
        PushClearDepth(&RenderPass);
    }
    
    END_TIMED_BLOCK();
    
    return(RenderPass);
}

internal void
EndRenderPass(render_pass *RenderPass)
{
    BEGIN_TIMED_BLOCK("EndRenderPass");
    
    switch(RenderPass->Type)
    {
        case RenderPassType_ForwardPass:
        {
            PushRenderEntry_(RenderPass->Commands, 0, RenderEntryType_render_entry_forwardpass_end);
        } break;
        
        case RenderPassType_Postprocess:
        {
            PushRenderEntry_(RenderPass->Commands, 0, RenderEntryType_render_entry_postprocesspass_end);
        } break;
    }
    
    END_TIMED_BLOCK();
}

internal texture_parameters
DefaultTextureParameters(void)
{
    texture_parameters Parameters = {};
    Parameters.AnisotropicLevel = 16.0f;
    Parameters.WrapS = TextureWrap_REPEAT;
    Parameters.WrapT = TextureWrap_REPEAT;
    Parameters.MinFilter = TextureFilter_LINEAR_MIPMAP_LINEAR;
    Parameters.MagFilter = TextureFilter_LINEAR;
    
    return(Parameters);
}

internal void
GenerateMipmaps(texture_op_queue *TextureOpQueue, render_texture *Texture)
{
    texture_op *TextureOp = PushTextureOperation(TextureOpQueue);
    if(TextureOp)
    {
        TextureOp->Status = TextureOpStatus_GenerateMipmaps;
        TextureOp->Texture = Texture;
    }
}

internal void
SetTextureParameters(texture_op_queue *TextureOpQueue, render_texture *Texture, texture_parameters Parameters)
{
    texture_op *TextureOp = PushTextureOperation(TextureOpQueue);
    if(TextureOp)
    {
        TextureOp->Status = TextureOpStatus_SetTextureParameters;
        TextureOp->Texture = Texture;
        TextureOp->TextureParameters = Parameters;
    }
}