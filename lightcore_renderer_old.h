enum shadowmap_quality
{
    SHADOWMAP_QUALITY_NONE = 0,
    
    SHADOWMAP_QUALITY_LOW,
    SHADOWMAP_QUALITY_MEDIUM,
    SHADOWMAP_QUALITY_HIGH,
    SHADOWMAP_QUALITY_ULTRA,
    SHADOWMAP_QUALITY_COUNT
};

struct shader
{
    GLuint ID;
    char *FilePath;
    time_t LastModifiedTime;
    
    string VertexSource;
    string FragmentSource;
    string GeometrySource;
};

enum light_type
{
    LIGHT_TYPE_DIRECTIONAL,
    LIGHT_TYPE_POINTLIGHT
};

// NOTE(Cristian): Applicable only for pointlight shadowmaps
enum shadowmap_face
{
    SHADOWMAP_FACE_POSITIVE_X = 0,
    SHADOWMAP_FACE_NEGATIVE_X,
    SHADOWMAP_FACE_POSITIVE_Y,
    SHADOWMAP_FACE_NEGATIVE_Y,
    SHADOWMAP_FACE_POSITIVE_Z,
    SHADOWMAP_FACE_NEGATIVE_Z
};

struct alignas(16) ubo_atlas_info
{
    u32 Size;
    u32 TileSize;
};

struct shadowmap_atlas
{
    u32 Size;
    u32 TileSize;
    
    u32 NextFreeIndex;
    alignas(64) LC_Array(u32) Freelist;
};

struct shadowmap_info
{
    shadowmap_atlas Atlas;
    GLuint FBO;
    GLuint DepthMap;
};

struct alignas(16) point_light
{
    glm::vec4 Position;
    
    glm::vec4 Ambient;
    glm::vec4 Diffuse;
    glm::vec4 Specular;
    
    glm::mat4 LightSpaceMatrix[6];
    u32 ShadowmapTileIndices[6];
    
    shadowmap_quality ShadowmapQuality;
    b32 EnableShadows;
    
    b32 UpdateRequired;
};

// TODO(Cristian): Move to SSBO storage
struct directional_light
{
    glm::vec4 Ambient;
    glm::vec4 Diffuse;
    glm::vec4 Specular;
    
    glm::mat4 LightSpaceMatrix;
    
    glm::vec3 Direction;
    
    shadowmap_quality ShadowmapQuality;
    b32 EnableShadows;
    
    u32 ShadowmapTileIndex;
    b32 UpdateRequired;
};

struct transformation_matrices
{
    glm::mat4 ViewMatrix;
    glm::mat4 PerspectiveMatrix;
    glm::mat4 PerspectiveViewMatrix;
};

enum gl_state_type
{
    DEPTH_TEST,
    STENCIL_TEST,
    ALPHA_BLEND,
    FACE_CULLING,
    INVALID_STATE
};

struct gl_state_cache
{
    b32 DepthTest;
    b32 StencilTest;
    b32 AlphaBlend;
    b32 FaceCulling;
    GLenum CulledFaceType;
    GLuint BoundFramebufferID;
    GLuint BoundVertexArrayID;
    GLuint UsedProgramID;
};

struct scene_node_transform
{
    // Local model information
    glm::vec3 Position;
    glm::vec3 Rotation;
    glm::vec3 Scale;
    
    // Global model information
    glm::mat4 WorldModelMatrix;
};

enum scene_asset_type
{
    ASSET_GEOMETRY,
    ASSET_POINT_LIGHT,
    ASSET_DIRECTIONAL_LIGHT
};

struct scene_node
{
    scene_node_transform Transform;
    
    string Name;
    scene_asset_type AssetType;
    u32 AssetIndex;
    u32 LightInformationIndex;
    
    b32 UpdateRequired;
    b32 CastsShadows;
    
    LC_Array(scene_node) Children;
};

struct extents_info
{
    glm::vec3 Min;
    glm::vec3 Max;
};

struct aabb_info
{
    u32 DebugDataVAO;
    u32 DebugDataVBO;
};

struct sphere_bound_smoothness
{
    u32 StackCount;
    u32 SectorCount;
};

struct sphere_bound_info
{
    sphere_bound_smoothness Smoothness;
    
    u32 VertexComponents;
    u32 NormalComponents;
    u32 TextureComponents;
    
    u32 TotalVertices;
    u32 TotalIndices;
    
    u32 DebugDataVAO;
    u32 DebugDataVBO;
    u32 DebugDataEBO;
};

struct asset
{
    u32 VAO;
    u32 VBO;
    
    extents_info Extents;
    aabb_info AABBBound;
    sphere_bound_info SphereBound;
    
    u32 VertexComponents;
    u32 NormalComponents;
    u32 TextureComponents;
    
    u32 TotalVertices;
    u32 TotalIndices;
    render_texture Texture;
};